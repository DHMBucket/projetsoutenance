<?php
require_once('./communs/connect.php');
require_once('./models/backend/BackDriver.php');
require_once('./controllers/backend/AuteurController.php');
require_once('./controllers/backend/LivreRevueController.php');
require_once('./controllers/backend/EditeurController.php');
require_once('./controllers/backend/ClientMembreController.php');
require_once('./controllers/backend/ClientInviteController.php');
require_once('./controllers/backend/MonnaieController.php');
require_once('./controllers/backend/AbonnementController.php');
require_once('./controllers/backend/CommandeController.php');
require_once('./controllers/backend/PanierController.php');
require_once('./controllers/backend/MotCleController.php');
require_once('./controllers/backend/SpecifRevueController.php');
require_once('./controllers/backend/ThemeController.php');
require_once('./controllers/backend/ArticleController.php');
require_once('./controllers/backend/UserController.php');            // loginnnnnnnnnn
require_once('./controllers/backend/AuthController.php');
require_once('./controllers/frontend/PublicController.php');

class Router
{
  private $ctrMbr;
  private $ctrAut;
  private $ctrLvR;
  private $ctrEdt;
  private $ctrCmd;
  private $ctrPan;
  private $ctrMon;
  private $ctrMcl;
  private $ctrAbn;
  private $ctrCti;
  private $ctrRev;
  private $ctrThm;
  private $ctrArt;
  private $ctrPln;
  private $driver;
  private $ctrUsr;
  private $ctrPub;

    

  public function __construct($base) {

    $this->ctrPub= new PublicController($base);
    $this->driver= new BackDriver($base);

    $this->ctrAut= new AuteurController($base);
    $this->ctrLvR= new LivreRevueController($base);
    $this->ctrEdt= new EditeurController($base);
    $this->ctrMbr= new ClientMembreController($base);
    $this->ctrCti= new ClientInviteController($base);
    $this->ctrMon= new MonnaieController($base);
    $this->ctrAbn= new AbonnementController($base);
    $this->ctrCmd= new CommandeController($base);
    $this->ctrPan= new PanierController($base);
    $this->ctrMcl= new MotcleController($base);
    $this->ctrRev= new SpecifRevueController($base);
    $this->ctrThm= new ThemeController($base);
    $this->ctrArt= new ArticleController($base);
    
    // $this->ctrPln= new PlanController($base);


    $this->ctrUsr= new UserController($base);   // loginnnnnnnnnn
  }

  public function reqUrl() {

    try {

      if ($_SERVER['REQUEST_METHOD']==='POST' || (isset($_GET['action'])) ) {

        if (isset($_GET['register'])) {
             $this->ctrUsr->verifierAccesAction($_GET['action'], $_GET['register']);
        } else {
              if (isset($_GET['passercommande'])) {
                    $this->ctrUsr->verifierAccesAction($_GET['action'],"verifier");
              } else {
                    if (isset($_GET['search'])) {
                      $this->ctrUsr->verifierAccesAction($_GET['action'], $_GET['search']);
                    } else { 
                          $this->ctrUsr->verifierAccesAction($_GET['action'],"");
                    }
              }
        }


        if ($_GET['action']=="synthese" && isset($_GET['synthese']) && ($_GET['synthese'] == 'margebrute')) {
          if (!(isset($_POST['calculMB']))) {
            $dataCmd= $this->driver->listeCommande(0, "", "", -1, 0, 0, "-1", 0, 0, "", "2100-01-01", "2100-01-01", "2100-01-01","", 0);
            $dataMbr= $this->driver->listeClientMembre(0,"","","",0,0,"","2100",0);
            $dataMon= $this->driver->listeMonnaie(0,"");
            $_SESSION['dataCmd']= $dataCmd; $_SESSION['dataMbr']= $dataMbr; $_SESSION['dataMon']= $dataMon;
            $donnMB= $this->driver->calculMB($dataCmd, $dataMbr, $dataMon, "1970-01-01", "2100-01-01");
            $deb= "1970-01-01"; $fn= "2100-01-01";
          } else {
            $dataCmd= $_SESSION['dataCmd']; $dataMbr= $_SESSION['dataMbr']; $dataMon= $_SESSION['dataMon'];
            $donnMB= $this->driver->calculMB($dataCmd, $dataMbr, $dataMon, $_POST['datedebut'], $_POST['datefin']);
            $deb= $_POST['datedebut']; $fn= $_POST['datefin'];
          }
          $this->ctrUsr->tableauVoirMargeBrute($donnMB, $deb, $fn);
        }


        if ($_GET['action']=="voirCommande") {
          
          if (isset($_SESSION['Auth'])) { 
            $type_client1= (int)$_SESSION['Auth']['role'];
            $id_client1= (int)$_SESSION['Auth']['numero'];
          } else {
            $type_client1= 0;
            $id_client1= (int)$_SESSION['AuthClt']['numero'];
          }
          $dataA= $this->driver->listeCommande(0, "", "", $type_client1, $id_client1, 0, "-1", 0, 0, "", "2100-01-01", "2100-01-01", "2100-01-01","", 0);
          $_SESSION['dataA']=$dataA;
          $this->ctrCmd->tableauVoirCommande($_SESSION['dataA']);
        }


        if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="supprimer" ) {


          if (isset($_GET['supprimer']) && $_GET['supprimer']=="user") {

            if(AuthController::isLogged()){         
              if (isset($_GET['id'])) {
                if ( $_SESSION['Auth']['role'] == 1 ) {

                  $idmb= (int)$_GET['id'];
                    
                  $typmb= 1;
                  $donnCmd1= $this->driver->listeCommande(0, "", "", $typmb, $idmb, 0, "-1", 0, 0, "", "2100-01-01", "2100-01-01", "2100-01-01","", 0);   
                  $donnAbn1= $this->driver->listeAbonnement(0, "", $idmb, $typmb, 0, 0);
                  $donnPan1= $this->driver->listePanier(0, $idmb, $typmb, 0, "-1", 0, 0, "-1", 0, 0, "-1", 0, "",0);               
                  $typmb= 2;
                  $donnCmd2= $this->driver->listeCommande(0, "", "", $typmb, $idmb, 0, "-1", 0, 0, "", "2100-01-01", "2100-01-01", "2100-01-01","", 0);   
                  $donnAbn2= $this->driver->listeAbonnement(0, "", $idmb, $typmb, 0, 0);
                  $donnPan2= $this->driver->listePanier(0, $idmb, $typmb, 0, "-1", 0, 0, "-1", 0, 0, "-1", 0, "",0);               

                  if (count($donnCmd1) == 0 && count($donnAbn1) == 0 && count($donnCmd2) == 0 && count($donnAbn2) == 0) {
                    if (count($donnPan1) > 0) {
                      $this->ctrPan->supprimerPanier($donnPan1[0]->getId_panier());
                    }
                    if (count($donnPan2) > 0) {
                      $this->ctrPan->supprimerPanier($donnPan2[0]->getId_panier());
                    }
                    $this->ctrUsr->supprimerUser($_GET['id']);
                    require_once('./views/backend/succesSupprUser.php');
                  } else { 
                    
                    if (count($donnPan1) > 0) {
                      $this->ctrPan->supprimerPanier($donnPan1[0]->getId_panier());
                    }
                    if (count($donnPan2) > 0) {
                      $this->ctrPan->supprimerPanier($donnPan2[0]->getId_panier());
                    }
                    $this->driver->changerOperUser($_GET['id'], 0);
                    require_once('./views/backend/nonsuccesSupprUser.php');  
                  }
                }
              }
            }
          }


          if (isset($_GET['supprimer']) && $_GET['supprimer']=="motcle") {
               
            if (isset($_GET['id'])) {
              
              $this->ctrMcl->supprimerMotcle($_GET['id']);

              $donnMcl= $this->driver->listeMotsCles(0,"",0,0,0);               
              $_SESSION["dataA"]= $donnMcl;
              $this->ctrMcl->tableauRecapMotcle($_SESSION['dataA']);


            } 
          }


          if (isset($_GET['supprimer']) && $_GET['supprimer']=="livre") {
               
            if (isset($_GET['id'])) {

              $donnArt= $this->driver->listeArticle(0, "", "", "", "", $_GET['id'], -1, "", "", 0);
              $donnPan1= $this->driver->listePanierLvr(0, 0, -1, $_GET['id'], 1, 1); 
              $donnPan2= $this->driver->listePanierLvr(0, 0, -1, $_GET['id'], 1, 2); 
              $donnPan3= $this->driver->listePanierLvr(0, 0, -1, $_GET['id'], 1, 3); 

              if (count($donnPan3) > 0) {
                foreach ($donnPan3 as $dataPan) {
                  $dataPan->setNbre3(0); $nbre=0;
                  $nbPan= $dataPan->getNbre1()+$dataPan->getNbre2()+$dataPan->getNbre3();
                  $this->driver->modifierPanier($dataPan ,$dataPan->getId_panier(), $nbre, $nbPan, 3);
                }
              }
              if (count($donnPan2) > 0) {
                foreach ($donnPan2 as $dataPan) {
                  $dataPan->setNbre2(0); $nbre=0;
                  $nbPan= $dataPan->getNbre1()+$dataPan->getNbre2()+$dataPan->getNbre3();
                  $this->driver->modifierPanier($dataPan ,$dataPan->getId_panier(), $nbre, $nbPan, 2);
                }
              }
              if (count($donnPan1) > 0) {
                foreach ($donnPan1 as $dataPan) {
                  $dataPan->setNbre1(0); $nbre=0;
                  $nbPan= $dataPan->getNbre1()+$dataPan->getNbre2()+$dataPan->getNbre3();
                  $this->driver->modifierPanier($dataPan ,$dataPan->getId_panier(), $nbre, $nbPan, 1);
                }
              }
              $donnLvR= $this->driver->listeLivreRevue(0,"","","",-1,0,"toutes");               
              $_SESSION["dataA"]= $donnLvR;

              if (count($donnArt) == 0 && count($donnPan1) == 0 && count($donnPan2) == 0 && count($donnPan3) == 0) {
                $this->ctrLvR->supprimerLivreRevue($_GET['id']);
                require_once('./views/backend/succesSupprLivre.php');
              } else {
                  if (count($donnArt) == 0) {
                    $this->ctrLvR->supprimerLivreRevue($_GET['id']);
                    require_once('./views/backend/succesSupprLivre.php'); 
                  } else {
                    require_once('./views/backend/nonsuccesSupprLivre.php'); 
                  }
              }
            }
          }

               

          if (isset($_GET['supprimer']) && $_GET['supprimer']=="auteur") {
          
            if (isset($_GET['id'])) {

              $donnAut= $this->driver->listeAuteur((int)$_GET['id'], "", "", "", 0);  
              $_SESSION["dataA"]= $donnAut;

              $nom= $donnAut[0]->getNom(); $prenom= $donnAut[0]->getPrenom(); $instit= $donnAut[0]->getInstitution();

              $donnArt1= $this->driver->listeArticle(0, "", "", $nom, "", 0, -1, $prenom, "", 0);               
              $donnArt2= $this->driver->listeArticle(0, "", "", $instit, "", 0, -1, $prenom, "", 0);
              $bool1=0;$bool2=0;
              if (count($donnArt1) == 0) { $bool1=1;} else {
                $bool1=1;
                foreach($donnArt1 as $art) {
                  if (($art->getAuteur1_nom() == $nom && $art->getAuteur1_prenom() == $prenom && $art->getAuteur1_institution() == $instit) ||
                      ($art->getAuteur2_nom() == $nom && $art->getAuteur2_prenom() == $prenom && $art->getAuteur2_institution() == $instit)) {
                    $bool1=0;
                  }
                }
              }
              if (count($donnArt2) == 0) { $bool2=1;} else {
                $bool2=1;
                foreach($donnArt2 as $art) {
                
                  if (($art->getAuteur1_nom() == $nom && $art->getAuteur1_prenom() == $prenom && $art->getAuteur1_institution() == $instit) ||
                      ($art->getAuteur2_nom() == $nom && $art->getAuteur2_prenom() == $prenom && $art->getAuteur2_institution() == $instit)) {
                    $bool2=0;
                  }
                }
              }

              $donnLvR1= $this->driver->listeLivreRevue(0,"","",$nom,-1,0,"toutes");   
              $donnLvR2= $this->driver->listeLivreRevue(0,"","",$instit,-1,0,"toutes");   
              $bool3=0;$bool4=0;
              if (count($donnLvR1) == 0) { $bool3=1;} else {
                $bool3=1;
                foreach($donnLvR1 as $art) {
                  if (($art->getAuteur_princ1_nom() == $nom && $art->getAuteur_princ1_prenom() == $prenom && $art->getAuteur_princ1_institution() == $instit) ||
                      ($art->getAuteur_princ2_nom() == $nom && $art->getAuteur_princ2_prenom() == $prenom && $art->getAuteur_princ2_institution() == $instit) ||
                      ($art->getRedac_chef_nom() == $nom && $art->getRedac_chef_prenom() == $prenom && $art->getRedac_chef_institution() == $instit)) {
                    $bool3=0;
                  }
                }
              }
              if (count($donnLvR2) == 0) { $bool4=1;} else {
                $bool4=1;
                foreach($donnLvR2 as $art) {
                  if (($art->getAuteur_princ1_nom() == $nom && $art->getAuteur_princ1_prenom() == $prenom && $art->getAuteur_princ1_institution() == $instit) ||
                      ($art->getAuteur_princ2_nom() == $nom && $art->getAuteur_princ2_prenom() == $prenom && $art->getAuteur_princ2_institution() == $instit) ||
                      ($art->getRedac_chef_nom() == $nom && $art->getRedac_chef_prenom() == $prenom && $art->getRedac_chef_institution() == $instit)) {
                    $bool4=0;
                  }
                }
              }

              if ($bool1 == 1 && $bool2 == 1 && $bool3 == 1 && $bool4 == 1) {
                $this->ctrAut->supprimerAuteur($_GET['id']);
                require_once('./views/backend/succesSupprAuteur.php');
              } else { 
                require_once('./views/backend/nonsuccesSupprAuteur.php');  
              }
            }
          }


           
          if (isset($_GET['supprimer']) && $_GET['supprimer']=="editeur") {

            if (isset($_GET['id'])) {

              $donnEdt= $this->driver->listeEditeur((int)$_GET['id'], "", "", "", 0);               
              $_SESSION["dataA"]= $donnEdt;

              $nom= $donnEdt[0]->getNom_editeur();


              $donnLvR1= $this->driver->listeLivreRevue(0,"","",$nom,-1,0,"toutes");   
                
              $bool3=0;
              if (count($donnLvR1) == 0) { $bool3=1;} else {
                $bool3=1;
                foreach($donnLvR1 as $art) {
                  if (($art->getEditeur1() == $nom) || ($art->getEditeur2() == $nom)) {
                    $bool3=0;
                  }
                }
              }

              if ($bool3 == 1) {
                $this->ctrEdt->supprimerEditeur($_GET['id']);
                require_once('./views/backend/succesSupprEditeur.php');
              } else { 
                require_once('./views/backend/nonsuccesSupprEditeur.php');  
              }
            }
          }

  

          if (isset($_GET['supprimer']) && $_GET['supprimer']=="abonnement") {

                if (isset($_GET['id'])) {

                  $donnAbn= $this->driver->listeAbonnement((int)$_GET['id'], "", 0, 0, 0, 0);               
                  $_SESSION["dataA"]= $donnAbn;

                  $date= $donnAbn[0]->getDate_fin();

                  if (strtotime($date) < strtotime(date("Y-m-d"))) {
                    $this->ctrAbn->supprimerAbonnement($_GET['id']);
                    require_once('./views/backend/succesSupprAbonnement.php');
                  } else { 
                    require_once('./views/backend/nonsuccesSupprAbonnement.php');  
                  }
                }
              }



              if (isset($_GET['supprimer']) && $_GET['supprimer']=="commande") {

                  if (isset($_GET['id'])) {
  
                    $donnCmd= $this->driver->listeCommande((int)$_GET['id'], "", "", -1, 0, 0, "-1", 0, 0, "", "2100-01-01", "2100-01-01", "2100-01-01","", 0);               
                    $_SESSION["dataA"]= $donnCmd;
    
                    $date= $donnCmd[0]->getDate_livrais_real();
                    
                  
  
                    if (strtotime($date) < strtotime(date("Y-m-d"))) {
                      $this->ctrCmd->supprimerCommande($_GET['id']);
                      require_once('./views/backend/succesSupprCommande.php');
                    } else { 
                      require_once('./views/backend/nonsuccesSupprCommande.php');  
                    }
                  }
                }
  


              if (isset($_GET['supprimer']) && $_GET['supprimer']=="panier") {
               
                if (isset($_GET['id'])) {
  
                  $donnPan= $this->driver->listePanier((int)$_GET['id'], 0, -1, 0, "-1", 0, 0, "-1", 0, 0, "-1", 0, "",0);               
                  $_SESSION["dataA"]= $donnPan;
                
                  $this->ctrPan->supprimerPanier($_GET['id']);
                  require_once('./views/backend/succesSupprPanier.php');
                  
                }
              }
  
  

              if (isset($_GET['supprimer']) && $_GET['supprimer']=="cltinv") {

                  if (isset($_GET['id'])) {
      
                    $donnCti= $this->driver->listeClientInvite((int)$_GET['id'],0,"","","",0);               
                    $_SESSION["dataA"]= $donnCti;
      
                    $idci= $donnCti[0]->getNum_compteur();
                    
                    $typci= 0;
      
                    $donnCmd= $this->driver->listeCommande(0, "", "", $typci, $idci, 0, "-1", 0, 0, "", "2100-01-01", "2100-01-01", "2100-01-01","", 0);   
                  
                    $donnPan= $this->driver->listePanier(0, $idci, $typci, 0, "-1", 0, 0, "-1", 0, 0, "-1", 0, "",0);               
                    if (count($donnPan) > 0) {
                      $this->ctrPan->supprimerPanier($donnPan[0]->getId_panier());
                    }
                    
                    if (count($donnCmd) == 0) {
                      $this->ctrCti->supprimerClientInvite($_GET['id']);
                      require_once('./views/backend/succesSupprClientInvite.php');
                    } else { 
                      require_once('./views/backend/nonsuccesSupprClientInvite.php');  
                    }
                  }
                }

                

              if (isset($_GET['supprimer']) && $_GET['supprimer']=="cltmbr") {

                  if (isset($_GET['id'])) {
      
                    $donnMbr= $this->driver->listeClientMembre((int)$_GET['id'],"","","",0,0,"","2100",0);               
                    $_SESSION["dataA"]= $donnMbr;
      
                    $idmb= $donnMbr[0]->getId_clt_membre();
                    
                    $typmb= $donnMbr[0]->getType_membre();
      
                    $donnCmd= $this->driver->listeCommande(0, "", "", $typmb, $idmb, 0, "-1", 0, 0, "", "2100-01-01", "2100-01-01", "2100-01-01","", 0);   
                    $donnAbn= $this->driver->listeAbonnement(0, "", $idmb, $typmb, 0, 0);
                    
                    $donnPan= $this->driver->listePanier(0, $idmb, $typmb, 0, "-1", 0, 0, "-1", 0, 0, "-1", 0, "",0);               
                    if (count($donnPan) > 0) {
                      $this->ctrPan->supprimerPanier($donnPan[0]->getId_panier());
                    }
                    
                    if (count($donnCmd) == 0 && count($donnAbn) == 0) {
                      $this->ctrMbr->supprimerClientMembre($_GET['id']);
                      require_once('./views/backend/succesSupprClientMembre.php');
                    } else { 
                      require_once('./views/backend/nonsuccesSupprClientMembre.php');  
                    }
                  }
                }
      

                if (isset($_GET['supprimer']) && $_GET['supprimer']=="theme") {


                  if (isset($_GET['id'])) {
      
                    $donnThm= $this->driver->listeTheme((int)$_GET['id'],"",0);               
                    $_SESSION["dataA"]= $donnThm;
      
                    $nthm= $donnThm[0]->getnom_theme();

                    $donnArt= $this->driver->listeArticle(0, "", $nthm, "", "", 0, -1, "", "", 0);
                    
                    if (count($donnArt) == 0) {
                      $this->ctrThm->supprimerTheme($_GET['id']);
                      require_once('./views/backend/succesSupprTheme.php');
                    } else { 
                      require_once('./views/backend/nonsuccesSupprTheme.php');  
                    }
                  }
                }

  


        
              if (isset($_GET['supprimer']) && $_GET['supprimer']=="spcrev") {
               
                if (isset($_GET['id'])) {

                    $idrev= (int)$_GET['id'];
                    
                    $donnRev= $this->driver->listeSpecifRevue($idrev,"",0,0);               
                    $_SESSION["dataA"]= $donnRev;

                    $nomrev= $donnRev[0]->getNom_specif_revue();

                    $donnAbn= $this->driver->listeAbonnement(0, $nomrev, 0, -1, 0, 0);

                    $donnLvR= $this->driver->listeLivreRevue(0,"",$nomrev,"",-1,0,"toutes"); 
                    if (count($donnLvR) > 0) {
                      $donnLvR1= [];
                      foreach ($donnLvR as $dataLvR) {
                        if ($dataLvR->getNom_specif_revue() <> "") {
                          array_push($donnLvR1, $dataLvR);
                        }  
                      }
                    } else {
                      $donnLvR1= $donnLvR;
                    }

                    $donnCmd=[];
                    if (count($donnLvR1) > 0) {
                      foreach ($donnLvR1 as $dataLvR) {
                        $idlvr= $dataLvR->getId_livre();
                        $donnCmd1= $this->driver->listeCommande(0, "", "", -1, 0, $idlvr, "-1", 0, 0, "", "2100-01-01", "2100-01-01", "2100-01-01","", 0);    
                        if (count($donnCmd1) > 0) {array_push($donnCmd, $donnCmd1[0]);}
                      }
                    }

                    $donnPan3=[];
                    if (count($donnLvR1) > 0) {
                      foreach ($donnLvR1 as $dataLvR) {
                        $idlvr= $dataLvR->getId_livre();
                        $donnPan= $this->driver->listePanier(0, 0, -1, 0, "-1", 0, 0, "-1", 0, $idlvr, "-1", 0, "",0);   
                        if (count($donnPan) > 0) {
                          foreach ($donnPan as $dataPan) {
                            array_push($donnPan3, $dataPan);
                          }
                        }
                      }

                      if (count($donnPan3) > 0) {
                        foreach ($donnPan3 as $dataPan) {
                          $dataPan->setNbre3(0); $nbre=0;
                          $nbPan= $dataPan->getNbre1()+$dataPan->getNbre2()+$dataPan->getNbre3();
                          $this->driver->modifierPanier($dataPan ,$dataPan->getId_panier(), $nbre, $nbPan, 3);
                        }
                      }
                    }

                    $donnPan2=[];
                    if (count($donnLvR1) > 0) {
                      foreach ($donnLvR1 as $dataLvR) {
                        $idlvr= $dataLvR->getId_livre();
                        $donnPan= $this->driver->listePanier(0, 0, -1, 0, "-1", 0, $idlvr, "-1", 0, 0, "-1", 0, "",0);   
                        if (count($donnPan) > 0) {
                          foreach ($donnPan as $dataPan) {
                            array_push($donnPan2, $dataPan);
                          }
                        }
                      }

                      if (count($donnPan2) > 0) {
                        foreach ($donnPan2 as $dataPan) {
                          $dataPan->setNbre2(0); $nbre=0;
                          $nbPan= $dataPan->getNbre1()+$dataPan->getNbre2()+$dataPan->getNbre3();
                          $this->driver->modifierPanier($dataPan ,$dataPan->getId_panier(), $nbre, $nbPan, 2);
                        }
                      }
                    }


                    $donnPan1=[];
                    if (count($donnLvR1) > 0) {
                      foreach ($donnLvR1 as $dataLvR) {
                        $idlvr= $dataLvR->getId_livre();
                        $donnPan= $this->driver->listePanier(0, 0, -1, $idlvr, "-1", 0, 0, "-1", 0, 0, "-1", 0, "",0);   
                        if (count($donnPan) > 0) {
                          foreach ($donnPan as $dataPan) {
                            array_push($donnPan1, $dataPan);
                          }
                        }
                      }

                      if (count($donnPan1) > 0) {
                        foreach ($donnPan1 as $dataPan) {
                          $dataPan->setNbre1(0); $nbre=0;
                          $nbPan= $dataPan->getNbre1()+$dataPan->getNbre2()+$dataPan->getNbre3();
                          $this->driver->modifierPanier($dataPan ,$dataPan->getId_panier(), $nbre, $nbPan, 1);
                        }
                      }
                    }
                   
                    if (count($donnAbn) == 0 && count($donnCmd) == 0 && count($donnLvR1) == 0) {
                      $this->ctrRev->supprimerRevue($idrev);
                      require_once('./views/backend/succesSupprRevue.php');
                    } else { 
                      require_once('./views/backend/nonsuccesSupprRevue.php');  
                    }
                } 
              }
  


              if (isset($_GET['supprimer']) && $_GET['supprimer']=="article") {
               
                if (isset($_GET['id'])) {

                  $idart=  (int)$_GET['id'];
                    
                  $donnPla= $this->driver->listePlan(0, $idart, 0);
                  $donnCmd= $this->driver->listeCommande(0, "", "", -1, 0, 0, "-1", $idart, 0, "", "2100-01-01", "2100-01-01", "2100-01-01","", 0);    
                  $donnMcl= $this->driver->listeMotsCles(0, "", 0, $idart, 0); 
  
                  $donnPan1= $this->driver->listePanier(0, 0, -1, 0, "-1", $idart, 0, "-1", 0, 0, "-1", 0, "",0);               
                  $donnPan2= $this->driver->listePanier(0, 0, -1, 0, "-1", 0, 0, "-1", $idart, 0, "-1", 0, "",0);               
                  $donnPan3= $this->driver->listePanier(0, 0, -1, 0, "-1", 0, 0, "-1", 0, 0, "-1", $idart, "",0);               
    
                  if (count($donnPan3) > 0) {
                    foreach ($donnPan3 as $dataPan) {
                      $dataPan->setNbre3(0); $nbre=0;
                      $nbPan= $dataPan->getNbre1()+$dataPan->getNbre2()+$dataPan->getNbre3();
                      $this->driver->modifierPanier($dataPan ,$dataPan->getId_panier(), $nbre, $nbPan, 3);
                    }
                  }
                  if (count($donnPan2) > 0) {
                    foreach ($donnPan2 as $dataPan) {
                      $dataPan->setNbre2(0); $nbre=0;
                      $nbPan= $dataPan->getNbre1()+$dataPan->getNbre2()+$dataPan->getNbre3();
                      $this->driver->modifierPanier($dataPan ,$dataPan->getId_panier(), $nbre, $nbPan, 2);
                    }
                  }
                  if (count($donnPan1) > 0) {
                    foreach ($donnPan1 as $dataPan) {
                      $dataPan->setNbre1(0); $nbre=0;
                      $nbPan= $dataPan->getNbre1()+$dataPan->getNbre2()+$dataPan->getNbre3();
                      $this->driver->modifierPanier($dataPan ,$dataPan->getId_panier(), $nbre, $nbPan, 1);
                    }
                  }
    
                  if (count($donnPla) == 0 && count($donnCmd) == 0 && count($donnMcl) == 0) {
                    $this->ctrArt->supprimerArticle($idart);
                    require_once('./views/backend/succesSupprArticle.php');
                  } else { 
                    require_once('./views/backend/nonsuccesSupprArticle.php');  
                  }
                } 
              }
        } 


        /********************************** Fin suppression enregistrements ***************/

        
        if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="detail" ) {

          if (isset($_GET['detail']) && $_GET['detail']=="livre") {
        
              if (isset($_GET['id'])) {

                $donnLvR= $this->driver->listeLivreRevue((int)$_GET['id'],"","","",-1,0,"toutes");               
                $_SESSION["dataA"]= $donnLvR;
             
                $this->ctrLvR->detailLivreRevue($donnLvR);
                  
                } 
              }


              if (isset($_GET['detail']) && $_GET['detail']=="auteur") {
               
                if (isset($_GET['id'])) {
                    
                  $donnAut= $this->driver->listeAuteur((int)$_GET['id'], "", "", "", 0);               
                  $_SESSION["dataA"]= $donnAut;

                  $this->ctrAut->detailAuteur($donnAut);

                } 
              }
  

              if (isset($_GET['detail']) && $_GET['detail']=="editeur") {
               
                if (isset($_GET['id'])) {
                    
                  $donnEdt= $this->driver->listeEditeur((int)$_GET['id'], "", "", "", 0);               
                  $_SESSION["dataA"]= $donnEdt;

                  $this->ctrEdt->detailEditeur($donnEdt);

                } 
              }
  

              if (isset($_GET['detail']) && $_GET['detail']=="abonnement") {
               
                if (isset($_GET['id'])) {
                    
                  $donnAbn= $this->driver->listeAbonnement((int)$_GET['id'], "", 0, 0, 0, 0);               
                  $_SESSION["dataA"]= $donnAbn;

                  $this->ctrAbn->detailAbonnement($donnAbn);

                } 
              }
  

              if (isset($_GET['detail']) && $_GET['detail']=="commande") {
               
                if (isset($_GET['id'])) {
                  $donnMonn= $this->driver->listeMonnaie(0,"");  
                  $donnCmd= $this->driver->listeCommande((int)$_GET['id'], "", "", -1, 0, 0, "-1", 0, 0, "", "2100-01-01", "2100-01-01", "2100-01-01","", 0);               
                  $_SESSION["dataA"]= $donnCmd;

                  $this->ctrCmd->detailCommande($donnCmd, $donnMonn);

                } 
              }
  

              if (isset($_GET['detail']) && $_GET['detail']=="panier") {
               
                if (isset($_GET['id'])) {
                    
                  $donnPan= $this->driver->listePanier((int)$_GET['id'], 0, -1, 0, "-1", 0, 0, "-1", 0, 0, "-1", 0, "",0);               
                  $_SESSION["dataA"]= $donnPan;

                  $this->ctrPan->detailPanier($donnPan);

                } 
              }


              if (isset($_GET['detail']) && $_GET['detail']=="cltinv") {
               
                if (isset($_GET['id'])) {
                    
                  $donnCti= $this->driver->listeClientInvite((int)$_GET['id'],0,"","","",0);               
                  $_SESSION["dataA"]= $donnCti;

                  $this->ctrCti->detailClientInvite($donnCti);

                } 
              }  


              if (isset($_GET['detail']) && $_GET['detail']=="cltmbr") {
               
                if (isset($_GET['id'])) {
            
                  $donnMbr= $this->driver->listeClientMembre((int)$_GET['id'],"","","",0,0,"","2100",0);               
                  $_SESSION["dataA"]= $donnMbr;

                  $this->ctrMbr->detailClientMembre($donnMbr);

                } 
              }


              if (isset($_GET['detail']) && $_GET['detail']=="article") {
               
                if (isset($_GET['id'])) {
                    
                 
                  $donnArt= $this->driver->listeArticle((int)$_GET['id'], "", "", "", "", 0, -1, "", "", 0);               
                  $_SESSION["dataA"]= $donnArt;

                  $this->ctrArt->detailArticle($donnArt);

                } 
              }

        } 


/**************************** Fin Edition ***************************************/


         if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="modifierUsr" ) {
    
          if (isset($_POST['modifusr'])) {

            $this->driver->modifierUser();
            require_once('./views/backend/succesModifUser.php');              
          }
        }


         if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="modifierArt" ) {
    
          if (isset($_POST['modifart'])) {

            $this->driver->modifierArticle();
            require_once('./views/backend/succesModifArticle.php');              
          }
        }


         if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="modifierThm" ) {
    
          if (isset($_POST['modifthm'])) {

            $this->driver->modifierTheme();
            require_once('./views/backend/succesModifTheme.php');              
          }
        }

        if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="modifierRev" ) {
    
          if (isset($_POST['modifrev'])) {

            $this->driver->modifierRevue();
            require_once('./views/backend/succesModifSpecifRevue.php');              
          }
        }

        if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="modifierMbr" ) {
    
          if (isset($_POST['modifmbr'])) {

            $this->driver->modifierMembre();
            require_once('./views/backend/succesModifClientMembre.php');              
          }
        }

        if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="modifierMon" ) {
    
          if (isset($_POST['modifmon'])) {
            $this->driver->modifierMonnaie();
            require_once('./views/backend/succesModifMonnaie.php');              
          }
        }

        
         if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="modifierCmd" ) {
    
          if (isset($_POST['modifcmd'])) {

            $this->driver->modifierCommande();
            require_once('./views/backend/succesModifCommande.php');              
          }
        }


         if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="modifierEdt" ) {
    
          if (isset($_POST['modifedt'])) {

            $modifEdt= new Editeur();

            $this->driver->ajouterEditeur($modifEdt, 2);
            require_once('./views/backend/succesModifEditeur.php');              
          }
        }


        if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="modifierAut" ) {
    
          if (isset($_POST['modifaut'])) {

            $modifAut= new Auteur();
            
            $this->driver->ajouterAuteur($modifAut, 2);
            require_once('./views/backend/succesModifAuteur.php');              
          }
        }



         if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="modifierLvR" ) {
    
          if (isset($_POST['modiflvrv'])) {

            $modifLvR= new LivreRevue();
            
            $this->driver->ajouterLivre($modifLvR, 2);
            require_once('./views/backend/succesModifLivre.php');              
          }
        }


         if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="modifier" ) {

          if (isset($_GET['modifier']) && $_GET['modifier']=="livre") {

              if (isset($_GET['id'])) {

                $donnRev= $this->driver->listeSpecifRevue(0, "", 0, 0);
                $donnAut1= $this->driver->listeAuteur(0, "", "", "", 0);
                $donnAut2= $this->driver->listeAuteur(0, "", "", "", 1);
                $donnEdt1= $this->driver->listeEditeur(0, "", "", "", 0);
                $donnEdt2= $this->driver->listeEditeur(0, "", "", "", 1);
                $donnLvR= $this->driver->listeLivreRevue((int)$_GET['id'],"","","",-1,0,"toutes");
                
                $_SESSION["dataA"]= $donnLvR;
                

                $this->ctrLvR->modifierLivreRevue($donnLvR, $donnRev, $donnAut1, $donnAut2, $donnEdt1, $donnEdt2);
        
                } 
              }


              if (isset($_GET['modifier']) && $_GET['modifier']=="auteur") {
               
                if (isset($_GET['id'])) {
                    
                  $donnAut= $this->driver->listeAuteur((int)$_GET['id'], "", "", "", 0);               
                  $_SESSION["dataA"]= $donnAut;

                  $this->ctrAut->modifierAuteur($donnAut);

                } 
              }
  

              if (isset($_GET['modifier']) && $_GET['modifier']=="editeur") {
               
                if (isset($_GET['id'])) {
                    
                  $donnEdt= $this->driver->listeEditeur((int)$_GET['id'], "", "", "", 0);               
                  $_SESSION["dataA"]= $donnEdt;

                  $this->ctrEdt->modifierEditeur($donnEdt);

                } 
              }
  

              if (isset($_GET['modifier']) && $_GET['modifier']=="abonnement") {
               
                if (isset($_GET['id'])) {
                    
                  $donnAbn= $this->driver->listeAbonnement((int)$_GET['id'], "", 0, 0, 0, 0);               
                  $_SESSION["dataA"]= $donnAbn;

                  $this->ctrAbn->modifierAbonnement($donnAbn);

                } 
              }
  

              if (isset($_GET['modifier']) && $_GET['modifier']=="commande") {
               
                if (isset($_GET['id'])) {
                  $donnMonn= $this->driver->listeMonnaie(0,"");  
                  $donnCmd= $this->driver->listeCommande((int)$_GET['id'], "", "", -1, 0, 0, "-1", 0, 0, "", "2100-01-01", "2100-01-01", "2100-01-01","", 0);               
                  $_SESSION["dataA"]= $donnCmd;

                  $this->ctrCmd->modifierCommande($donnCmd, $donnMonn);

                } 
              }
  

              if (isset($_GET['modifier']) && $_GET['modifier']=="panier") {
               
                if (isset($_GET['id'])) {
                    
                  $donnPan= $this->driver->listePanier((int)$_GET['id'], 0, -1, 0, "-1", 0, 0, "-1", 0, 0, "-1", 0, "",0);               
                  $_SESSION["dataA"]= $donnPan;

                  $this->ctrPan->modifierPanier($donnPan);

                } 
              }
  

              if (isset($_GET['modifier']) && $_GET['modifier']=="monnaie") {
               
                if (isset($_GET['id'])) {
                    
                  $donnMon= $this->driver->listeMonnaie((int)$_GET['id'], "");               
                  $_SESSION["dataA"]= $donnMon;

                  $this->ctrMon->modifierMonnaie($donnMon);

                } 
              }
  

              if (isset($_GET['modifier']) && $_GET['modifier']=="cltmbr") {
               
                if (isset($_GET['id'])) {
                    
                  $donnMbr= $this->driver->listeClientMembre((int)$_GET['id'],"","","",0,0,"","2100",0);               
                  $_SESSION["dataA"]= $donnMbr;

                  $this->ctrMbr->modifierClientMembre($donnMbr);

                } 
              }
  

              if (isset($_GET['modifier']) && $_GET['modifier']=="spcrev") {
               
                if (isset($_GET['id'])) {
                    
                  $donnRev= $this->driver->listeSpecifRevue((int)$_GET['id'],"",0,0);               
                  $_SESSION["dataA"]= $donnRev;

                  $this->ctrRev->modifierSpecifRevue($donnRev);

                } 
              }
  

              if (isset($_GET['modifier']) && $_GET['modifier']=="theme") {
               
                if (isset($_GET['id'])) {
                    
                  $donnThm= $this->driver->listeTheme((int)$_GET['id'],"",0);               
                  $_SESSION["dataA"]= $donnThm;

                  $this->ctrThm->modifierTheme($donnThm);

                } 
              }
  

              if (isset($_GET['modifier']) && $_GET['modifier']=="article") {
               
                if (isset($_GET['id'])) {
                    
                  $donnAut1= $this->driver->listeAuteur(0, "", "", "", 0);
                  $donnAut2= $this->driver->listeAuteur(0, "", "", "", 1);
                  $donnThm= $this->driver->listeTheme(0,"",0);  
  
                  $donnArt= $this->driver->listeArticle((int)$_GET['id'], "", "", "", "", 0, -1, "", "", 0);               
                  $_SESSION["dataA"]= $donnArt;

                  $this->ctrArt->modifierArticle($donnArt, $donnAut1, $donnAut2, $donnThm);

                } 
              }
  

              if (isset($_GET['modifier']) && $_GET['modifier']=="user") {

                if(AuthController::isLogged()){
               
                  if (isset($_GET['id'])) {
    
                    $donnUsr= $this->driver->listeUser((int)$_GET['id'],"","",0,"1970-01-01",0);               
                    $_SESSION["dataA"]= $donnUsr;

                    if ( $_SESSION['Auth']['role'] == 1 ) {
                      if ($donnUsr[0]->getRole() == 1) {
                        $donnUsrR1= $this->driver->listeUser(0,"","",1,"1970-01-01",0);
                        if (count($donnUsrR1) > 1) {
                          $this->ctrUsr->modifierUser($donnUsr,1);
                        } else {
                          $this->ctrUsr->modifierUser($donnUsr,0);
                        }
                      } else {
                        $this->ctrUsr->modifierUser($donnUsr, 1);
                      }
                      
                    } else {
                      $dataA= $this->driver->listeUser(0,"","",0,"1970-01-01",0);
                      $_SESSION['dataA']=$dataA;
                      $this->ctrUsr->tableauRecapUser($_SESSION['dataA']);
                    }

                  } 
                }
              }

        } 
/************************************************ */

         if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="abonnerMbr" ) {

          echo "<br>";

          if (isset($_GET['abonnerMbr'])) {
            
            $clientMbr= $this->driver->listeClientMembre($_POST['id_clt_membre'],"","","",0,0,"","2100",0);
          } else {
            $clientUsr= $this->driver->listeUser($_POST['id_util'], "", "", 0, "2100-01-01", 0);
            $client= new ClientMembre(); $clientMbr= []; $clientMbr[0]= $client;
            $clientMbr[0]->setId_clt_membre($clientUsr[0]->getId_util());
            $clientMbr[0]->setNom_membre($clientUsr[0]->getNom());
            $clientMbr[0]->setPrenom_membre($clientUsr[0]->getPrenom());
            $clientMbr[0]->setEmail($clientUsr[0]->getEmail());
            $clientMbr[0]->setType_membre($clientUsr[0]->getRole());

            $clientMbr[0]->setInstitution($_POST['institution']);
            $clientMbr[0]->setAdr_livrais($_POST['inputAddress']."//".$_POST['inputAddress2']); 
            $clientMbr[0]->setCode_postal($_POST['inputZip']);
            $clientMbr[0]->setVille($_POST['inputCity']);
            $clientMbr[0]->setPays($_POST['inputState']);
            $clientMbr[0]->setIndicatif($_POST['indicatif']);
            $clientMbr[0]->setTelephone($_POST['telClt']);

          }

            $_SESSION['mbr1']= $clientMbr[0];
            $_SESSION['pxtotal']= $_POST["tarif1"];

            $mon= explode("-", $_POST["monnaie1"]);
            $_SESSION['monnaie']= $mon[1];
            
            require_once('./views/backend/paiementmembreabonnement.php');

          
        }

        if ($_GET['action']=='abonnement') {

            $donnMonn= $this->driver->listeMonnaie(0,"");
            $donnRevue= $this->driver->listeSpecifRevue(0, $_GET['nomrevue'], 0, 0);

            if (gettype($_SESSION['dataA']) != 'object') {$_SESSION['dataA']=$_SESSION['dataA'][$_GET['t']];}
            if ((int)$_SESSION["Auth"]['role']<3) {
              $dataM= $this->driver->listeUser($_SESSION["Auth"]['numero'], "", "", 0, "2100-01-01", 0);
              $this->ctrUsr->registerAbonnemtUsr($donnMonn, $donnRevue, $dataM);
            } else {
              $dataM= $this->driver->listeClientMembre($_SESSION["Auth"]['numero'],"","","",0,0,"","2100",0);
              $this->ctrUsr->registerAbonnemtMbr($donnMonn, $donnRevue, $dataM);
            }
        }


        if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="paiementacceptAbonnemt" ) {
            
          if ($_SERVER['REQUEST_METHOD']==='POST') {
            $this->ctrPub->paiement($_SESSION['monnaie'], 'Abonnement');

            if (isset($_GET['paiementacceptAbonnemtMbr'])) {
              $this->driver->enregistrerAbonnement("1", $_REQUEST['stripeToken'], "Domicile", "");
            }

            $dataA= $_SESSION["dataA"];
            
          } else {
            $this->ctrPub->checkout();
          }
                              
          require_once('./views/backend/succesAbonnement.php');

        }


        if ($_GET['action']=='passercommande') {
              
          if (isset($_GET['passercommande'])) {
            $donnMonn= $this->driver->listeMonnaie(0,"");
            
              if ((int)$_SESSION["Auth"]['role']<3) {
                $dataM= $this->driver->listeUser($_SESSION["Auth"]['numero'], "", "", 0, "2100-01-01", 0);
               
                $this->ctrUsr->registerCommandUsr($donnMonn, $dataM);
              } else {
                $dataM= $this->driver->listeClientMembre($_SESSION["Auth"]['numero'],"","","",0,0,"","2100",0);
                $this->ctrUsr->registerCommandMbr($donnMonn, $dataM);
              }
          }    
          if (isset($_GET['enregistrcoordinv'])) {
            $donnMonn= $this->driver->listeMonnaie(0,"");
            $this->ctrUsr->registerInv($donnMonn);

          }     
        }

        if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="commanderMbr" ) {

          echo "<br>";

          if (isset($_GET['commanderMbr'])) {

            $clientMbr= $this->driver->listeClientMembre($_POST['id_clt_membre'],"","","",0,0,"","2100",0);

          } else {

            $clientUsr= $this->driver->listeUser($_POST['id_util'], "", "", 0, "2100-01-01", 0);
            $client= new ClientMembre(); $clientMbr= []; $clientMbr[0]= $client;
            $clientMbr[0]->setId_clt_membre($clientUsr[0]->getId_util());
            $clientMbr[0]->setNom_membre($clientUsr[0]->getNom());
            $clientMbr[0]->setPrenom_membre($clientUsr[0]->getPrenom());
            $clientMbr[0]->setEmail($clientUsr[0]->getEmail());
            $clientMbr[0]->setType_membre($clientUsr[0]->getRole());

            $clientMbr[0]->setInstitution($_POST['institution']);
            $clientMbr[0]->setAdr_livrais($_POST['inputAddress']."//".$_POST['inputAddress2']); 
            $clientMbr[0]->setCode_postal($_POST['inputZip']);
            $clientMbr[0]->setVille($_POST['inputCity']);
            $clientMbr[0]->setPays($_POST['inputState']);
            $clientMbr[0]->setIndicatif($_POST['indicatif']);
            $clientMbr[0]->setTelephone($_POST['telClt']);

          }

            $_SESSION['mbr1']= $clientMbr[0];
            $_SESSION['pxtotal']= $_POST["tarif1"];
            $mon= explode("-", $_POST["monnaie1"]);
            $_SESSION['monnaie']= $mon[1];
            
            require_once('./views/backend/paiementmembrecommande.php');

          
        }


         if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="ajouterInv" ) {

          echo "<br>";

          if (isset($_GET['ajoutInvite'])) {

            $ajoutInv= new ClientInvite();

            $ajoutInv= $this->ctrCti->coordClientInvite();

            $_SESSION['mbr1']= $ajoutInv;
            $_SESSION['pxtotal']= $_POST["tarif1"];

            $mon= explode("-", $_POST["monnaie1"]);
            $_SESSION['monnaie']= $mon[1];
            
            require_once('./views/backend/paiementinvitecommande.php');

          }
        }

        if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="paiementacceptCommand" ) {
            
          if ($_SERVER['REQUEST_METHOD']==='POST') {
            $this->ctrPub->paiement($_SESSION['monnaie'], 'Commande');
            
            if (isset($_GET['paiementacceptCommandInv'])) {
              $this->driver->ajouterClientInvite();
              $this->driver->enregistrerCommande("1", $_REQUEST['stripeToken'], "Domicile", "");
            }
            if (isset($_GET['paiementacceptCommandMbr'])) {
              $this->driver->enregistrerCommande("1", $_REQUEST['stripeToken'], "Domicile", "");
            }

            
            $dataA= $_SESSION["dataA"][0];
            
            $dataA->setNbre3(0); $nbPan= $dataA->getNbre1()+$dataA->getNbre2()+$dataA->getNbre3();
            $this->driver->modifierPanier($dataA ,$dataA->getId_panier(), 0, $nbPan, 3);
            $dataA->setNbre2(0); $nbPan= $dataA->getNbre1()+$dataA->getNbre2()+$dataA->getNbre3();
            $this->driver->modifierPanier($dataA ,$dataA->getId_panier(), 0, $nbPan, 2);
            $dataA->setNbre1(0); $nbPan= $dataA->getNbre1()+$dataA->getNbre2()+$dataA->getNbre3();
            $this->driver->modifierPanier($dataA ,$dataA->getId_panier(), 0, $nbPan, 1);

            if (isset($_SESSION["Auth"]["pseudo"])) {
              $dataA= $this->driver->listePanier(0, $_SESSION["Auth"]["numero"], $_SESSION["Auth"]["role"], 0, "-1", 0, 0, "-1", 0, 0, "-1", 0, "", 0);
            } else {
              $dataA= $this->driver->listePanier(0, $_SESSION["AuthClt"]["numero"], 0, 0, "-1", 0, 0, "-1", 0, 0, "-1", 0, "", 0);
            }
            $_SESSION['dataA']= $dataA;
            if (count($dataA) == 0) {$_SESSION['panier']=0;}
            
          } else {
            $this->ctrPub->checkout();
          }
                              
          require_once('./views/backend/succesCommande.php');

        }


          if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="voirpanier" ) {

            if (isset($_SESSION["Auth"]["pseudo"])) {
              $dataA= $this->driver->listePanier(0, $_SESSION["Auth"]["numero"], $_SESSION["Auth"]["role"], 0, "-1", 0, 0, "-1", 0, 0, "-1", 0, "", 0);
            } else {
              $dataA= $this->driver->listePanier(0, $_SESSION["AuthClt"]["numero"], 0, 0, "-1", 0, 0, "-1", 0, 0, "-1", 0, "", 0);
            }

            $dataL1=[];$dataL2=[];$dataL3=[];
            $id_livre= $dataA[0]->getId_livre1();$reference= $dataA[0]->getRef_livre1();
            $id_artcl= $dataA[0]->getId_artcl1();

            if ($id_artcl < 2) {
              if ($id_livre > 1) {
                $dataL1= $this->driver->listeLivreRevue($id_livre, $reference,"","",-1,0,"toutes");
                if (count($dataL1) == 1) {
                  if ($dataA[0]->getNbre1() >= ($dataL1[0]->getStock_iris()+$dataL1[0]->getStock_editeur())) 
                    {$dataA[0]->setNbre1($dataL1[0]->getStock_iris()+$dataL1[0]->getStock_editeur());}
                }
              }
            }

            $id_livre= $dataA[0]->getId_livre2();$reference= $dataA[0]->getRef_livre2();
            $id_artcl= $dataA[0]->getId_artcl2();

            if ($id_artcl < 2) {
              if ($id_livre > 1) {
                $dataL2= $this->driver->listeLivreRevue($id_livre, $reference,"","",-1,0,"toutes");
                if (count($dataL2) == 1) {
                  if ($dataA[0]->getNbre2() >= ($dataL2[0]->getStock_iris()+$dataL2[0]->getStock_editeur())) 
                    {$dataA[0]->setNbre2($dataL2[0]->getStock_iris()+$dataL2[0]->getStock_editeur());}
                }
              }
            }
            $id_livre= $dataA[0]->getId_livre3();$reference= $dataA[0]->getRef_livre3();
            $id_artcl= $dataA[0]->getId_artcl3();

            if ($id_artcl < 2) {
              if ($id_livre > 1) {
                $dataL3= $this->driver->listeLivreRevue($id_livre, $reference,"","",-1,0,"toutes");
                if (count($dataL3) == 1) {
                  if ($dataA[0]->getNbre3() >= ($dataL3[0]->getStock_iris()+$dataL3[0]->getStock_editeur())) 
                    {$dataA[0]->setNbre3($dataL3[0]->getStock_iris()+$dataL3[0]->getStock_editeur());}
                }
              }
            }
            
            $_SESSION['panier']= $dataA[0]->getNbre1()+$dataA[0]->getNbre2()+$dataA[0]->getNbre3();
            $_SESSION['dataA']= $dataA;

            require_once('./views/backend/afficheTableauPanierClient.php');    // exec 9
           
          }     


          if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="changernbrepanier" ) {

           
            $dataL1=[];$dataL2=[];$dataL3=[];

            if (isset($_GET['supprimerPan'])) {

              $dataA= $_SESSION["dataA"][0];

              if ($_GET['pos']=='0') {$dataA->setNbre1(0); $nbre=0;} 

              if ($_GET['pos']=='1') {$dataA->setNbre2(0); $nbre=0;} 
    
              if ($_GET['pos']=='2') {$dataA->setNbre3(0); $nbre=0;}

              $nbPan= $dataA->getNbre1()+$dataA->getNbre2()+$dataA->getNbre3();
              $this->driver->modifierPanier($dataA ,$dataA->getId_panier(), $nbre, $nbPan, $_GET['pos']+1);

              if (isset($_SESSION["Auth"]["pseudo"])) {
                $dataA= $this->driver->listePanier(0, $_SESSION["Auth"]["numero"], $_SESSION["Auth"]["role"], 0, "-1", 0, 0, "-1", 0, 0, "-1", 0, "", 0);
              } else {
                $dataA= $this->driver->listePanier(0, $_SESSION["AuthClt"]["numero"], 0, 0, "-1", 0, 0, "-1", 0, 0, "-1", 0, "", 0);
              }
              $_SESSION['dataA']= $dataA;
              if (count($dataA) > 0) {header('location: ./index.php?action=voirpanier&voirpanier');}
              else {
                $_SESSION['panier']=0;
                $dataA= $this->driver->listeLivreRevue(0,"","","",-1,0,"toutes");
                $_SESSION["dataA"]=$dataA;$_COOKIE['total']=count($dataA);setcookie('total', count($dataA));
                $this->ctrPub->accueilLivreRevue($dataA); 
              }  
            }


            if (isset($_GET['diminuernbrepanier'])) {

              $dataA= $_SESSION["dataA"][0];

              if ($_GET['pos']=='0') 
                { $dataA->setNbre1((int)$dataA->getNbre1()-1); $nbre=$dataA->getNbre1();} 

              if ($_GET['pos']=='1') 
                {$dataA->setNbre2((int)$dataA->getNbre2()-1); $nbre=$dataA->getNbre2();} 
    
              if ($_GET['pos']=='2') 
                {$dataA->setNbre3((int)$dataA->getNbre3()-1); $nbre=$dataA->getNbre3();} 

              $nbPan= $dataA->getNbre1()+$dataA->getNbre2()+$dataA->getNbre3();
              $this->driver->modifierPanier($dataA ,$dataA->getId_panier(), $nbre, $nbPan, $_GET['pos']+1);

              if (isset($_SESSION["Auth"]["pseudo"])) {
                $dataA= $this->driver->listePanier(0, $_SESSION["Auth"]["numero"], $_SESSION["Auth"]["role"], 0, "-1", 0, 0, "-1", 0, 0, "-1", 0, "", 0);
              } else {
                $dataA= $this->driver->listePanier(0, $_SESSION["AuthClt"]["numero"], 0, 0, "-1", 0, 0, "-1", 0, 0, "-1", 0, "", 0);
              }
              $_SESSION['dataA']= $dataA;
              if (count($dataA) > 0) {header('location: ./index.php?action=voirpanier&voirpanier');}
              else {
                $_SESSION['panier']=0;
                $dataA= $this->driver->listeLivreRevue(0,"","","",-1,0,"toutes");
                $_SESSION["dataA"]=$dataA;$_COOKIE['total']=count($dataA);setcookie('total', count($dataA));
                $this->ctrPub->accueilLivreRevue($dataA); 
              }  
            }

            if (isset($_GET['augmenternbrepanier'])) {

              $dataA= $_SESSION["dataA"][0];

              if ($_GET['pos']=='0') 
                {$dataA->setNbre1((int)$dataA->getNbre1()+1); $nbre=$dataA->getNbre1();} 

              if ($_GET['pos']=='1') 
                {$dataA->setNbre2((int)$dataA->getNbre2()+1); $nbre=$dataA->getNbre2();} 
    
              if ($_GET['pos']=='2') 
                {$dataA->setNbre3((int)$dataA->getNbre3()+1); $nbre=$dataA->getNbre3();} 

              $nbPan= $dataA->getNbre1()+$dataA->getNbre2()+$dataA->getNbre3();
              $this->driver->modifierPanier($dataA ,$dataA->getId_panier(), $nbre, $nbPan, $_GET['pos']+1);

              if (isset($_SESSION["Auth"]["pseudo"])) {
                $dataA= $this->driver->listePanier(0, $_SESSION["Auth"]["numero"], $_SESSION["Auth"]["role"], 0, "-1", 0, 0, "-1", 0, 0, "-1", 0, "", 0);
              } else {
                $dataA= $this->driver->listePanier(0, $_SESSION["AuthClt"]["numero"], 0, 0, "-1", 0, 0, "-1", 0, 0, "-1", 0, "", 0);
              }
              $_SESSION['dataA']= $dataA;
              $_SESSION['panier']= $nbPan;
              header('location: ./index.php?action=voirpanier&voirpanier');
            }

          }     

          if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="reservArticlePanier" ) {

            if (!isset($_SESSION)) {
              session_start();
              $_SESSION['id']=$_GET['t'];     
            }
            $_SESSION['id']=$_GET['t'];

            $this->ctrPan->enregistrArticlePanier();

            $this->ctrArt->tableauRecapArticle($_SESSION['dataA'], $_SESSION['dataSupp']);
 
          }


          if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="reservPanier" ) {

            if (!isset($_SESSION)) {
              session_start();
              $_SESSION['id']=$_GET['t'];     
            }
            $_SESSION['id']=$_GET['t'];

            $this->ctrPan->enregistrLivreRevuePanier();
            if  (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] < 5) {
              $this->ctrLvR->tableauRecapLivreRevue($_SESSION['dataA']);
            } else {
              $this->ctrPub->accueilLivreRevue($_SESSION["dataA"]); 
            }
             
          }



          if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="tableau" ) {
            if(AuthController::isLogged()){
              if ($_GET['tableau']=="tableauLvR") {
                  $dataA= $this->driver->listeLivreRevue(0,"","","",-1,0,"toutes");
                  $_SESSION['dataA']=$dataA;
                  $this->ctrLvR->tableauRecapLivreRevue($_SESSION['dataA']);

                }       
              if ($_GET['tableau']=="tableauAut") {
                $dataA= $this->driver->listeAuteur(0,"","","",0);
                $_SESSION['dataA']=$dataA;
                $this->ctrAut->tableauRecapAuteur($_SESSION['dataA']);
              }
              if ($_GET['tableau']=="tableauEdt") {
                $dataA= $this->driver->listeEditeur(0,"","","",0);
                $_SESSION['dataA']=$dataA;
                $this->ctrEdt->tableauRecapEditeur($_SESSION['dataA']);
              }
              if ($_GET['tableau']=="tableauMon") {
                $dataA= $this->driver->listeMonnaie(0,"");
                $_SESSION['dataA']=$dataA;
                $this->ctrMon->tableauRecapMonnaie($_SESSION['dataA']);
              }
              if ($_GET['tableau']=="tableauMbr") {
                $dataA= $this->driver->listeClientMembre(0,"","","",0,0,"","2100",0);
                $_SESSION['dataA']=$dataA;
                $this->ctrMbr->tableauRecapClientMembre($_SESSION['dataA']);
              }
              if ($_GET['tableau']=="tableauCti") {
                $dataA= $this->driver->listeClientInvite(0,0,"","","",0);
                $_SESSION['dataA']=$dataA;
                $this->ctrCti->tableauRecapClientInvite($_SESSION['dataA']);
              }
              if ($_GET['tableau']=="tableauAbn") {
                $dataA= $this->driver->listeAbonnement(0, "", 0, 0, 0, 0);
                $_SESSION['dataA']=$dataA;
                $this->ctrAbn->tableauRecapAbonnement($_SESSION['dataA']);
              }
              if ($_GET['tableau']=="tableauCmd") {
                $dataA= $this->driver->listeCommande(0, "", "", -1, 0, 0, "-1", 0, 0, "", "2100-01-01", "2100-01-01", "2100-01-01","", 0);
                $_SESSION['dataA']=$dataA;
                $this->ctrCmd->tableauRecapCommande($_SESSION['dataA']);
              }

              if ($_GET['tableau']=="tableauPan") {                           
                $dataA= $this->driver->listePanier( 0, 0, -1, 0, "-1", 0, 0, "-1", 0, 0, "-1", 0, "",0);
                $_SESSION['dataA']=$dataA;

                $this->ctrPan->tableauRecapPanier($_SESSION['dataA']);
              }

              if ($_GET['tableau']=="tableauMcl") {
                $dataA= $this->driver->listeMotsCles(0,"",0,0,0);
                $_SESSION['dataA']=$dataA;
                $this->ctrMcl->tableauRecapMotcle($_SESSION['dataA']);
              }

              if ($_GET['tableau']=="tableauRev") {
                $dataA= $this->driver->listeSpecifRevue(0,"",0,0);
                $_SESSION['dataA']=$dataA;

                $this->ctrRev->tableauRecapSpecifRevue($_SESSION['dataA']);
              }

              if ($_GET['tableau']=="tableauThm") {
                $dataA= $this->driver->listeTheme(0, "", 0);
                $_SESSION['dataA']=$dataA;
                $this->ctrThm->tableauRecapTheme($_SESSION['dataA']);
              }

              if ($_GET['tableau']=="tableauArt") {
                $dataA= $this->driver->listeArticle(0, "", "", "", "", 0, -1, "", "", 0);
                $dataSupp= $this->driver->listeMotsCles(0,"",0,0,0);
                $_SESSION['dataA']=$dataA;
                $_SESSION['dataSupp']=$dataSupp;
                $this->ctrArt->tableauRecapArticle($_SESSION['dataA'], $_SESSION['dataSupp']);
              }

              if ($_GET['tableau']=="tableauUsr") {
                $dataA= $this->driver->listeUser(0,"","",0,"1970-01-01",0);
                $_SESSION['dataA']=$dataA;
                $this->ctrUsr->tableauRecapUser($_SESSION['dataA']);
              }
              /*
              if ($_GET['tableau']=="tableauPln") {
                $dataA= $this->driver->listePlan(0,"","","",0);
                $_SESSION['dataA']=$dataA;
                $this->ctrPln->tableauRecapPlan($_SESSION['dataA']);
              }
              */
            } else{
              header('location: ./index.php?action=login');
          }

          }


              if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="searchCrit" ) {
                if ($_GET['searchCrit'] === "searchAccCritLvR" || $_GET['searchCrit'] === "searchTabCritLvR") {                 
                  $idsearch= (int)($_POST['idsearch']); $refsearch= $_POST['refsearch'];
                  $ntsearch= $_POST['ntsearch']; $autsearch= $_POST['autsearch'];      

                  if (isset($_POST['anneesearch'])) {
                    if ($_POST['anneesearch']=="") { $anneesearch= "toutes";  
                    } else { $anneesearch= $_POST['anneesearch']; }
                  } else { $anneesearch= "toutes"; }

                  if ((!isset($_POST['dispo']) && !isset($_POST['nonDispo'])) || (isset($_POST['dispo']) && isset($_POST['nonDispo']))) {                    
                    $dataA= $this->driver->listeLivreRevue($idsearch,$refsearch,$ntsearch,$autsearch,-1,0,$anneesearch);
                  } else {
                    if (!isset($_POST['dispo']) && isset($_POST['nonDispo'])) {                     
                      $dataA= $this->driver->listeLivreRevue($idsearch,$refsearch,$ntsearch,$autsearch,0,0,$anneesearch);
                    } else {
                      $dataA= $this->driver->listeLivreRevue($idsearch,$refsearch,$ntsearch,$autsearch,1,0,$anneesearch);
                    }
                  }
                  $_SESSION["dataA"]= $dataA;$_COOKIE['total']=count($dataA);setcookie('total', count($dataA));
                  if ($_GET['searchCrit'] === "searchAccCritLvR") {
                    $this->ctrPub->accueilLivreRevue($_SESSION["dataA"]);               // exec 19
                  } else {
                    $this->ctrLvR->tableauRecapLivreRevue($_SESSION['dataA']);
                  }
                }        
                
                if ($_GET['searchCrit'] === "searchAccCritAut" || $_GET['searchCrit'] === "searchTabCritAut") {                 
                  $idsearch= (int)($_POST['idsearch']); $nsearch= $_POST['nsearch'];
                  $inssearch= $_POST['inssearch']; $psearch= $_POST['psearch'];      
              
                  $dataA= $this->driver->listeAuteur($idsearch, $nsearch, $psearch, $inssearch,0);

                  $_SESSION["dataA"]= $dataA;$_COOKIE['total']=count($dataA);setcookie('total', count($dataA));

                  if ($_GET['searchCrit'] === "searchAccCritAut") {
                    $this->ctrPub->accueilAuteur($_SESSION["dataA"]);                // exec 19
                  } else {
                    $this->ctrAut->tableauRecapAuteur($_SESSION['dataA']);
                  }
                  
                }        

                if ($_GET['searchCrit'] === "searchAccCritEdt" || $_GET['searchCrit'] === "searchTabCritEdt") {                 
                  $idsearch= (int)($_POST['idsearch']); $nesearch= $_POST['nesearch'];
                  $cdesearch= $_POST['cdesearch']; $locsearch= $_POST['locsearch'];
      
                  $dataA= $this->driver->listeEditeur($idsearch, $cdesearch, $nesearch, $locsearch,0);
                  $_SESSION["dataA"]= $dataA;$_COOKIE['total']=count($dataA);setcookie('total', count($dataA));

                  if ($_GET['searchCrit'] === "searchAccCritEdt") {
                    $this->ctrPub->accueilEditeur($_SESSION["dataA"]);               // exec 19
                  } else {
                    $this->ctrEdt->tableauRecapEditeur($_SESSION['dataA']);
                  }
                  
                }        

                if ($_GET['searchCrit'] === "searchTabCritMbr") {  

                  $idsearch= (int)($_POST['idsearch']); $nomsearch= $_POST['nomsearch']; $prenom="";
                  $emlsearch= $_POST['emlsearch']; $locsearch= $_POST['locsearch'];   
                  $tymbsearch= $_POST['tymbsearch']; $sttsearch= $_POST['sttsearch'];
                  $ddebsearch= $_POST['ddebsearch'];
              
                  $dataA= $this->driver->listeClientMembre($idsearch, $nomsearch, $prenom, $locsearch, $tymbsearch, $sttsearch, $emlsearch, $ddebsearch, 0);
                  $_SESSION["dataA"]= $dataA;
                  $this->ctrMbr->tableauRecapClientMembre($_SESSION['dataA']);               // exec 19
                }        


                if ($_GET['searchCrit'] === "searchTabCritCti") {  

                  $idsearch= (int)($_POST['idsearch']); $nomsearch= $_POST['nomsearch']; $prenom="";
                  $emlsearch= $_POST['emlsearch']; $locsearch= $_POST['locsearch'];   
                  $refsearch= $_POST['refsearch'];
              
                  $dataA= $this->driver->listeClientInvite($idsearch, $refsearch, $nomsearch, "", $locsearch, 0);
                  $_SESSION["dataA"]= $dataA;
                  $this->ctrCti->tableauRecapClientInvite($_SESSION['dataA']);               // exec 19
                }        


                if ($_GET['searchCrit'] === "searchTabCritAbn") {  

                  $idsearch= (int)($_POST['idsearch']); $nomsearch= $_POST['nomsearch']; 
                  $idcsearch= $_POST['idcsearch']; $tymbsearch= $_POST['tymbsearch'];   
                  $sttsearch= $_POST['sttsearch'];
              
                  $dataA= $this->driver->listeAbonnement($idsearch, $nomsearch, $idcsearch, $tymbsearch, $sttsearch, 0);
                  $_SESSION["dataA"]= $dataA;
                  $this->ctrAbn->tableauRecapAbonnement($_SESSION['dataA']);               // exec 19
                }        

                if ($_GET['searchCrit'] === "searchTabCritCmd") {  

                  $idsearch= (int)($_POST['idsearch']); 
                  $refsearch= $_POST['refsearch']; 
                  $trscsearch= $_POST['trscsearch']; 
                  $typsearch= (int)$_POST['typsearch']; 
                  $idcsearch= (int)$_POST['idcsearch']; 
                  $idlsearch= (int)$_POST['idlsearch']; 
                  $rflsearch= $_POST['rflsearch'];
                  $idasearch= (int)$_POST['idasearch'];
                  $ttrsearch= $_POST['ttrsearch']; 
                  $str1= $ttrsearch;
                  $str = str_replace("$", " ", $str1, $count);$str1=$str;$ttrsearch=$str1;
                  $modsearch= $_POST['modsearch'];
                  $locsearch= $_POST['locsearch'];
                  $str1= $locsearch;
                  $str = str_replace("$", " ", $str1, $count);$str1=$str;$locsearch=$str1;
              
                  $dataA= $this->driver->listeCommande($idsearch, $refsearch, $trscsearch, $typsearch, $idcsearch, $idlsearch, $rflsearch, $idasearch, $modsearch, $locsearch, "2100-01-01", "2100-01-01", "2100-01-01", $ttrsearch, 0);
                  $_SESSION["dataA"]= $dataA;
                  
                  $this->ctrCmd->tableauRecapCommande($_SESSION['dataA']);               // exec 19
                }        
           
                if ($_GET['searchCrit'] === "searchTabCritPan") {  

                  $idsearch= (int)($_POST['idsearch']); 
                  $typsearch= (int)$_POST['typsearch']; 
                  $idcsearch= (int)$_POST['idcsearch']; 
                  $idlsearch= (int)$_POST['idlsearch']; 
                  $rflsearch= $_POST['rflsearch'];
                  $idasearch= (int)$_POST['idasearch'];
                  $ttrsearch= $_POST['ttrsearch'];   

                  $dataA= $this->driver->listePanier($idsearch, $idcsearch, $typsearch, $idlsearch, $rflsearch, $idasearch, $idlsearch, $rflsearch, $idasearch, $idlsearch, $rflsearch, $idasearch, $ttrsearch, 0);
                  $_SESSION["dataA"]= $dataA;
                  $this->ctrPan->tableauRecapPanier($_SESSION['dataA']);               // exec 19
                }        
                

                if ($_GET['searchCrit'] === "searchTabCritMcl") {  

                  $idsearch= (int)($_POST['idsearch']); 
                  $nomsearch= $_POST['nomsearch']; 
                  $idlsearch= (int)$_POST['idlsearch']; 
                  $idasearch= (int)$_POST['idasearch'];

                  $dataA= $this->driver->listeMotsCles($idsearch, $nomsearch,$idlsearch, $idasearch,0);
                  $_SESSION['dataA']=$dataA;
                  $this->ctrMcl->tableauRecapMotcle($_SESSION['dataA']);
                }        

                if ($_GET['searchCrit'] === "searchTabCritRev") {  

                  $idsearch= (int)($_POST['idsearch']); 
                  $nomsearch= $_POST['nomsearch']; 
                  $persearch= (int)$_POST['persearch']; 

                  $dataA= $this->driver->listeSpecifRevue($idsearch, $nomsearch, $persearch, 0);
                  $_SESSION['dataA']=$dataA;
                  $this->ctrRev->tableauRecapSpecifRevue($_SESSION['dataA']);
                  }        

                  if ($_GET['searchCrit'] === "searchTabCritThm") {  

                    $idsearch= (int)($_POST['idsearch']); 
                    $nomsearch= $_POST['nomsearch'];  
                    $dataA= $this->driver->listeTheme($idsearch, $nomsearch, 0);
                    $_SESSION['dataA']=$dataA;
                    $this->ctrThm->tableauRecapTheme($_SESSION['dataA']);
                  }        
  

                  if ($_GET['searchCrit'] === "searchTabCritArt") {  

                    $idsearch= (int)($_POST['idsearch']); 
                    $ttrsearch= ($_POST['ttrsearch']);
                    $thmsearch= ($_POST['thmsearch']);
                    $idlsearch= ($_POST['idlsearch']);
                    $rflsearch= ($_POST['rflsearch']);

                    $nomsearch= $_POST['nomsearch'];
                    $prnsearch= ($_POST['nomsearch']);  // prenom = nom dans la recherche
                    $mclsearch= ($_POST['mclsearch']);

                    $dataA= $this->driver->listeArticle($idsearch, $ttrsearch, $thmsearch, $nomsearch, $rflsearch, $idlsearch, -1, $prnsearch, $mclsearch, 0);
                    $_SESSION['dataA']=$dataA;
                    $dataSupp= $this->driver->listeMotsCles(0,"",0,$idsearch,0);
                    $_SESSION['dataSupp']=$dataSupp;
                    $this->ctrArt->tableauRecapArticle($_SESSION['dataA'], $_SESSION['dataSupp']);
                  }        

                  if ($_GET['searchCrit'] === "searchTabCritUsr") {  

                    $idsearch= (int)($_POST['idsearch']); 
                    $nomsearch= $_POST['nomsearch'];  
                    $mailsearch= $_POST['mailsearch'];  
                    $rolsearch= $_POST['rolsearch'];  

                    $dtesearch="1970-01-01";  
                    $dataA= $this->driver->listeUser($idsearch, $nomsearch,$mailsearch, $rolsearch, $dtesearch, 0);
                    $_SESSION['dataA']=$dataA;
                    $this->ctrUsr->tableauRecapUser($_SESSION['dataA']);
                  }         
                
              }



              if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="search" ) {
                if ($_GET['search'] === "searchAccLvR" || $_GET['search'] === "searchTabLvR") {
                  if (isset($_POST['sequence'])) {
                    $dataA= $this->driver->listeLivreRevue(0,"",$_POST['sequence'],"",-1,0,"toutes");
                  } else {
                    $dataA= $this->driver->listeLivreRevue(0,"","","",-1,0,"toutes");
                  }
                  
                  $_SESSION["dataA"]= $dataA;$_COOKIE['total']=count($dataA);setcookie('total', count($dataA));
                  if ($_GET['search'] === "searchAccLvR") {
                    $_SESSION['iindex']=0;$_SESSION ["icompteur"]=0;
                    $this->ctrPub->accueilLivreRevue($_SESSION["dataA"]);               // exec 19
                  }
                  if ($_GET['search'] === "searchTabLvR") {
                    $this->ctrLvR->tableauRecapLivreRevue($_SESSION['dataA']);              // exec 19
                  }
                }         
                if ($_GET['search'] === "searchAccAut" || $_GET['search'] === "searchTabAut") {
                  $dataA= $this->driver->listeAuteur(0,$_POST['sequence'],"","",0);
                  $_SESSION["dataA"]= $dataA;$_COOKIE['total']=count($dataA);setcookie('total', count($dataA));
                  if ($_GET['search'] === "searchAccAut") {
                    $_SESSION['iindex']=0;$_SESSION ["icompteur"]=0;
                    $this->ctrPub->accueilAuteur($_SESSION["dataA"]);               // exec 19
                  }
                  if ($_GET['search'] === "searchTabAut") {
                    $this->ctrAut->tableauRecapAuteur($_SESSION['dataA']);              // exec 19
                  }
                }         

                if ($_GET['search'] === "searchAccEdt" || $_GET['search'] === "searchTabEdt") {
                  $dataA= $this->driver->listeEditeur(0,"",$_POST['sequence'],"",0);
                  $_SESSION["dataA"]= $dataA;$_COOKIE['total']=count($dataA);setcookie('total', count($dataA));
                  if ($_GET['search'] === "searchAccEdt") {
                    $_SESSION['iindex']=0;$_SESSION ["icompteur"]=0;
                    $this->ctrPub->accueilEditeur($_SESSION["dataA"]);               // exec 19
                  }
                  if ($_GET['search'] === "searchTabEdt") {
                    $this->ctrEdt->tableauRecapEditeur($_SESSION['dataA']);              // exec 19
                  }
                }         

                if ($_GET['search'] === "searchTabMon") {
                  $dataA= $this->driver->listeMonnaie(0,$_POST['sequence']);
                  $_SESSION["dataA"]= $dataA;
                    $this->ctrMon->tableauRecapMonnaie($_SESSION['dataA']);              // exec 19
                }         

                if ($_GET['search'] === "searchTabMbr") {
                  $dataA= $this->driver->listeClientMembre(0,$_POST['sequence'],"","",0,0,"","2100",0);
                  $_SESSION["dataA"]= $dataA;
                    $this->ctrMbr->tableauRecapClientMembre($_SESSION['dataA']);              // exec 19
                }         

                if ($_GET['search'] === "searchTabCti") {
                  $dataA= $this->driver->listeClientInvite(0,0,$_POST['sequence'],"","",0);
                  $_SESSION["dataA"]= $dataA;
                    $this->ctrCti->tableauRecapClientInvite($_SESSION['dataA']);              // exec 19
                }      
                
                if ($_GET['search'] === "searchTabAbn") {
                  $dataA= $this->driver->listeAbonnement(0,$_POST['sequence'],0,0,0,0);
                  $_SESSION["dataA"]= $dataA;
                    $this->ctrAbn->tableauRecapAbonnement($_SESSION['dataA']);              // exec 19
                }      

                if ($_GET['search'] === "searchTabCmd") {
                  $dataA= $this->driver->listeCommande(0,"","",-1,0,0,"-1",0,0,"","2100-01-01", "2100-01-01", "2100-01-01",$_POST['sequence'],0);
                  $_SESSION["dataA"]= $dataA;
                    $this->ctrCmd->tableauRecapCommande($_SESSION['dataA']);              // exec 19
                }      
               
                if ($_GET['search'] === "searchTabPan") {
                  $dataA= $this->driver->listePanier(0, 0, -1, 0, "-1", 0, 0, "-1", 0, 0, "-1", 0, $_POST['sequence'], 0);
                  $_SESSION["dataA"]= $dataA;
                  $this->ctrPan->tableauRecapPanier($_SESSION['dataA']);               // exec 19
                  }      

                  if ($_GET['search'] === "searchTabMcl") {
                    $dataA= $this->driver->listeMotsCles(0, $_POST['sequence'], 0, 0, 0);
                    $_SESSION['dataA']=$dataA;
                    $this->ctrMcl->tableauRecapMotcle($_SESSION['dataA']);
                  }      
  
                  if ($_GET['search'] === "searchTabRev") {
                    $dataA= $this->driver->listeSpecifRevue(0, $_POST['sequence'], 0, 0);
                    $_SESSION['dataA']=$dataA;
                    $this->ctrRev->tableauRecapSpecifRevue($_SESSION['dataA']);
                  }      

                  if ($_GET['search'] === "searchTabThm") {
                    $dataA= $this->driver->listeTheme(0, $_POST['sequence'], 0);
                    $_SESSION['dataA']=$dataA;
                    $this->ctrThm->tableauRecapTheme($_SESSION['dataA']);
                  }      

                  if ($_GET['search'] === "searchTabArt") {
                    $dataA= $this->driver->listeArticle(0, $_POST['sequence'], "", "", "", 0, -1, "", "",0);
                    $_SESSION['dataA']=$dataA;
                    $dataSupp= $this->driver->listeMotsCles(0,"",0,0,0);
                    $_SESSION['dataSupp']=$dataSupp;
                    $this->ctrArt->tableauRecapArticle($_SESSION['dataA'], $_SESSION['dataSupp']);
                  }      

                  if ($_GET['search'] === "searchTabUsr") {  
                    $dataA= $this->driver->listeUser(0, $_POST['sequence'], "", 0, "1970-01-01", 0);
                    $_SESSION['dataA']=$dataA;
                    $this->ctrUsr->tableauRecapUser($_SESSION['dataA']);
                  }      

              }
    
    
              if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="securite" ) {

                $dataA= $this->driver->listeLivreRevue(0,"","","",-1,0,"toutes");
                $_SESSION['dataA']=$dataA;$_COOKIE['total']=count($dataA);setcookie('total', count($dataA));
                $this->ctrPub->accueilLivreRevue($_SESSION['dataA']);    

              }

              if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="ajouterArt" ) {
    
                if (isset($_POST['ajoutart'])) {
    
                  $ajoutArt= new Article();
                  $ajoutArt= $this->ctrArt->verifierArticle();
                  
                  if ($ajoutArt->getTitre() == "-1") {
                    require_once('./views/backend/nonsuccesAjoutArticle.php');
                  } else {
                        $this->driver->ajouterArticle($ajoutArt);
                        require_once('./views/backend/succesAjoutArticle.php');
                  }
                 
                } 
              }

    

              if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="ajouterEdt" ) {

                  
                if (isset($_POST['ajoutedt'])) {
    
                  $ajoutEdt= new Editeur();
    
                  $ajoutEdt= $this->ctrEdt->verifierEditeur();
    
                  if ($ajoutEdt->getNom_editeur() == "-1") {
                      require_once('./views/backend/nonsuccesAjoutEditeur.php');
                  } else {
                        $this->driver->ajouterEditeur($ajoutEdt, 1);
                        require_once('./views/backend/succesAjoutEditeur.php');
                    
                  }
                }
              }
    


              if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="ajouterAut" ) {

                if (isset($_POST['ajoutaut'])) {
    
                  $ajoutAut= new Auteur();
    
                  $ajoutAut= $this->ctrAut->verifierAuteur();
    
                  if ($ajoutAut->getNom() == "-1") {
                      require_once('./views/backend/nonsuccesAjoutAuteur.php');
                  } else {
                        $this->driver->ajouterAuteur($ajoutAut, 1);
                        require_once('./views/backend/succesAjoutAuteur.php');
                    
                  }
                }
              }
    


              if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="ajouterThm" ) {

                if (isset($_POST['ajoutthm'])) {
    
                  $ajoutThm= new Theme();
    
                  $ajoutThm= $this->ctrThm->verifierTheme();
    
                  if ($ajoutThm->getNom_theme() == "-1") {
                      require_once('./views/backend/nonsuccesAjoutTheme.php');
                  } else {
                        $this->driver->ajouterTheme($ajoutThm);
                        require_once('./views/backend/succesAjoutTheme.php');
                    
                  }
                }
              }
    


              if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="ajouterMcl" ) {

                if (isset($_POST['ajoutmcl'])) {
    
                  $ajoutMcl= new MotCle();
    
                  $ajoutMcl= $this->ctrMcl->verifierMotcle();
    
                  if ($ajoutMcl->getNom_motcle() == "-1") {
                      require_once('./views/backend/nonsuccesAjoutMotcle.php');
                  } else {
                        $this->driver->ajouterMotcle($ajoutMcl);
                        require_once('./views/backend/succesAjoutMotcle.php');
                    
                  }
                }
              }
    


              if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="ajouterLvR" ) {
    
                if (isset($_POST['ajoutlvrv'])) {

                  $ajoutLvR= new LivreRevue();
                  $ajoutLvR= $this->ctrLvR->verifierLivre();

                  if  (isset($_POST['type']) && $_POST['type'] == "2") {
                    if ($ajoutLvR->getNom_specif_revue() == "-1") {
                      require_once('./views/backend/nonsuccesAjoutLivre.php');
                    } else {
                          $this->driver->ajouterLivre($ajoutLvR, 1);
                          require_once('./views/backend/succesAjoutLivre.php');
                    }
                  }

                  if  (isset($_POST['type']) && $_POST['type'] == "1") {
                    if ($ajoutLvR->getTitre() == "-1") {
                      require_once('./views/backend/nonsuccesAjoutLivre.php');
                    } else {
                          $this->driver->ajouterLivre($ajoutLvR, 1);
                          require_once('./views/backend/succesAjoutLivre.php');
                    }
                  }
                }
              }

              

              if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="ajouterRev" ) {
   
                if (isset($_POST['ajoutrev'])) {
    
                  $ajoutRev= new Specif_revue();
    
                  $ajoutRev= $this->ctrRev->verifierRevue();
    
                  if ($ajoutRev->getNom_specif_revue() == "-1") {
                      require_once('./views/backend/nonsuccesAjoutRevue.php');
                  } else {
                        $this->driver->ajouterRevue($ajoutRev);
                        require_once('./views/backend/succesAjoutRevue.php');
                    
                  }
       
                }
              }
    
            



          if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="ajouterMbr" ) {

            if (isset($_POST['ajoutclmb'])) {

              $ajoutMbr= new ClientMembre();

              $ajoutMbr= $this->ctrMbr->verifierClientMembre();

              if ($ajoutMbr->getType_membre() == -1) {
                  require_once('./views/backend/nonsuccesAdhesion.php');
              } else {
                  if ($ajoutMbr->getPrix_membre() == 0 ) {
                    $this->driver->ajouterMembre($ajoutMbr);
                    require_once('./views/backend/succesAdhesion.php');
                  } else {
                    $_SESSION['mbr1']= new ClientMembre();
                    $_SESSION['mbr1']= $ajoutMbr;
                    require_once('./views/backend/paiementmembrecotis.php');
                  }
                
              }
            }
          }


          if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="paiementacceptAdherCherch" ) {
            
            if ($_SERVER['REQUEST_METHOD']==='POST') {
              $this->ctrPub->paiement($_SESSION['mbr1']->getMonnaie(), 'Adhésion');
              
              $this->driver->ajouterMembre($_SESSION['mbr1']);
            } else {
              $this->ctrPub->checkout();
            }
                                
            require_once('./views/backend/succesAdhesion.php');

          }
         

          if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="suivant" ) {
            if (!isset($_SESSION)) {
              session_start();
              $_SESSION['iindex']=0;
              $_SESSION['icompteur']=$_COOKIE['icompteur'];     // exec 18
            }

            if ($_GET['suivant'] === "suivantLvR") {

              $this->ctrPub->accueilLivreRevue($_SESSION["dataA"]);               // exec 19
            }         
            if ($_GET['suivant'] === "suivantAut") {

              $this->ctrPub->accueilAuteur($_SESSION["dataA"]);               // exec 19
            }         
            if ($_GET['suivant'] === "suivantEdt") {

              $this->ctrPub->accueilEditeur($_SESSION["dataA"]);               // exec 19
            }         
            
          }

          if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="precedent" ) {
            if (!isset($_SESSION)) {
              session_start();
              $_SESSION['iindex']=0;
              if ($_COOKIE['iindex']>0) {
                $_SESSION['icompteur']=$_COOKIE['iindex'];        // exec 18
              } else {
                $_SESSION['icompteur']=$_COOKIE['icompteur'];     // exec 18
              }
            } else {
                if (isset($_COOKIE['total']) && ($_COOKIE['total'] % 2 == 1)) {
                  if (isset($_SESSION['icompteur'])) {
                    if ($_SESSION['icompteur']+1>=$_COOKIE['total']) {

                      $_SESSION['iindex']=0;
                    }
                  }
                }
            }
            if ($_GET['precedent']=="precedentLvR") {
              $this->ctrPub->accueilLivreRevue($_SESSION["dataA"]);
            }        
            if ($_GET['precedent']=="precedentAut") {
              $this->ctrPub->accueilAuteur($_SESSION["dataA"]);
            }        
            if ($_GET['precedent']=="precedentEdt") {
              $this->ctrPub->accueilEditeur($_SESSION["dataA"]);
            }        
          }


          if (isset($_GET) && isset($_GET['action']) && $_GET['action']=='login') {

            $this->ctrUsr->login();

          } elseif (isset($_GET) && isset($_GET['action']) && $_GET['action']=='logout') {
            
              $this->ctrUsr->logout();
              $_SESSION['iindex']=0;
              $_SESSION['panier']=0;
              $dataA= $this->driver->listeLivreRevue(0,"","","",-1,0,"toutes");
              $_SESSION["dataA"]=$dataA;$_COOKIE['total']=count($dataA);setcookie('total', count($dataA));
              $this->ctrPub->accueilLivreRevue($dataA); 
          }

          if (isset($_GET) && isset($_GET['action']) && $_GET['action']=='register') {

              if ($_GET['register']=="article") {

                $donnLvR= $this->driver->listeLivreRevue(0, "", "", "", -1, 1,"toutes");
                $i=0;
                foreach ($donnLvR as $livre) {
                  $str = str_replace(" ", "$", $livre->getTitre(), $count);$livre->setTitre($str);
                  if ($livre->getNom_specif_revue() <> "") {
                      //***************supprimer les revues**************** 
                      array_splice($donnLvR, $i, 1);
                      $i--;
                  }
                  $i++;
                }

                $donnAut1= $this->driver->listeAuteur(0, "", "", "", 0);
                foreach ($donnAut1 as $auteur) {
                  $str = str_replace(" ", "$", $auteur->getNom(), $count);$auteur->setNom($str);
                  $str = str_replace(" ", "$", $auteur->getPrenom(), $count);$auteur->setPrenom($str);
                  $str = str_replace(" ", "$", $auteur->getInstitution(), $count);$auteur->setInstitution($str);
                }
   
                $donnAut2= $this->driver->listeAuteur(0, "", "", "", 1);
                foreach ($donnAut2 as $auteur) {
                  $str = str_replace(" ", "$", $auteur->getNom(), $count);$auteur->setNom($str);
                  $str = str_replace(" ", "$", $auteur->getPrenom(), $count);$auteur->setPrenom($str);
                  $str = str_replace(" ", "$", $auteur->getInstitution(), $count);$auteur->setInstitution($str);
                }
  
                $donnThm= $this->driver->listeTheme(0, "", 1);
                foreach ($donnThm as $theme) {
                  $str = str_replace(" ", "$", $theme->getNom_theme(), $count);$theme->setNom_theme($str);
                }

                $this->ctrArt->registerArt($donnLvR, $donnAut1, $donnAut2, $donnThm);

              }    


              if ($_GET['register']=="editeur") {
                $this->ctrEdt->registerEdt();

              }    
  

              if ($_GET['register']=="auteur") {
                $this->ctrAut->registerAut();

              }    
  

              if ($_GET['register']=="motcle") {

                $donnArt= $this->driver->listeArticle(0, "", "", "", "", 0, -1, "", "", 0);
                foreach ($donnArt as $article) {
                  $str = str_replace(" ", "$", $article->getTitre(), $count);$article->setTitre($str);
                }
                $this->ctrMcl->registerMcl($donnArt);

              }    
  

              if ($_GET['register']=="theme") {
                $this->ctrThm->registerThm();

              }    
  

              
            if ($_GET['register']=="livre") {
              $donnRev= $this->driver->listeSpecifRevue(0, "", 0, 0);
              $donnAut1= $this->driver->listeAuteur(0, "", "", "", 0);
              foreach ($donnAut1 as $auteur) {
                $str = str_replace(" ", "$", $auteur->getNom(), $count);$auteur->setNom($str);
                $str = str_replace(" ", "$", $auteur->getPrenom(), $count);$auteur->setPrenom($str);
                $str = str_replace(" ", "$", $auteur->getInstitution(), $count);$auteur->setInstitution($str);
              }
 
              $donnAut2= $this->driver->listeAuteur(0, "", "", "", 1);
              foreach ($donnAut2 as $auteur) {
                $str = str_replace(" ", "$", $auteur->getNom(), $count);$auteur->setNom($str);
                $str = str_replace(" ", "$", $auteur->getPrenom(), $count);$auteur->setPrenom($str);
                $str = str_replace(" ", "$", $auteur->getInstitution(), $count);$auteur->setInstitution($str);
              }

              $donnEdt1= $this->driver->listeEditeur(0, "", "", "", 0);
              foreach ($donnEdt1 as $auteur) {
                $str = str_replace(" ", "$", $auteur->getNom_editeur(), $count);$auteur->setNom_editeur($str);
              }

              $donnEdt2= $this->driver->listeEditeur(0, "", "", "", 1);
              foreach ($donnEdt2 as $auteur) {
                $str = str_replace(" ", "$", $auteur->getNom_editeur(), $count);$auteur->setNom_editeur($str);
              }

              $this->ctrLvR->registerLvR($donnRev, $donnAut1, $donnAut2, $donnEdt1, $donnEdt2);

            }    

            if ($_GET['register']=="revue") {
              $this->ctrRev->registerRev();

            }    

            if ($_GET['register']=="user") {
              $this->ctrUsr->registerUsr();

            }    

            if ($_GET['register']=="client") {
              $donnMonn= $this->driver->listeMonnaie(0,"");
              $this->ctrUsr->registerClt($donnMonn);

            }     
          }


          if (isset($_GET) && isset($_GET['action']) && $_GET['action']=="initial" ) {

            if (!isset($_SESSION ["icompteur"])) {
              if (!(isset($_SESSION))) { session_start();}
              $_SESSION ["icompteur"]=2;$_SESSION['iindex']=0;$_COOKIE['icompteur']=2;
            }
            if (!isset($_SESSION  ["Auth"])) {session_unset();} 
              else {$_SESSION ["icompteur"]=2;$_SESSION['iindex']=0;$_COOKIE['icompteur']=2;}

            $_SESSION['iindex']=0;

            if ($_GET['initial']=="initialLvR") {
              $dataA= $this->driver->listeLivreRevue(0, "", "", "", -1, 0,"toutes");
              $_SESSION["dataA"]=$dataA;$_COOKIE['total']=count($dataA);setcookie('total', count($dataA));
              $this->ctrPub->accueilLivreRevue($dataA);
            }       
            if ($_GET['initial']=="initialAut") {
              if(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] < 5){              
                $dataA= $this->driver->listeAuteur(0, "", "", "", 0);
                $_SESSION["dataA"]= $dataA;$_COOKIE['total']=count($dataA);setcookie('total', count($dataA));
                $this->ctrPub->accueilAuteur($_SESSION["dataA"]);
              }
            }
            if ($_GET['initial']=="initialEdt") {
              if(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] < 5){              
                $dataA= $this->driver->listeEditeur(0, "", "", "", 0);
                $_SESSION["dataA"]= $dataA;$_COOKIE['total']=count($dataA);setcookie('total', count($dataA));
                $this->ctrPub->accueilEditeur($_SESSION["dataA"]);
              }
            }
          }

       
      } else {                        // Fin if action

        if (!isset($_SESSION)) {
            session_start();$_SESSION['iindex']=0;$_SESSION ["icompteur"]=0;
           
        } else {
              $_SESSION['iindex']=0;$_SESSION ["icompteur"]=0;
        }
         
        if (!isset($_SESSION  ["Auth"]) && !isset($_SESSION  ["AuthClt"])) {
          session_unset();$_SESSION ["icompteur"]=0;
          // Générer un numéro provisoire
          
          $num= $this->ctrPub->genererNumCompteur();
          $_SESSION['AuthClt'] = array('pseudo'=>$num[0]->id_compteur,'numero'=>$num[0]->id_compteur,'pass'=>"",'role'=>6);
          $_SESSION["panier"]= 0;
          
        }
        $_SESSION['iindex']=0;
        
        $dataA= $this->driver->listeLivreRevue(0,"","","",-1,0,"toutes");
        $_SESSION['dataA']=$dataA; $_COOKIE['total']=count($dataA); setcookie('total', count($dataA));
        $this->ctrPub->accueilLivreRevue($_SESSION["dataA"]);   

      }
    } catch (Exception $e) {
          echo $e->getMessage();
          $this->page404($e->getMessage());
    }

  }

  private function page404($msgerreur) {
    //require_once('./views/page404.php');
  }


}





