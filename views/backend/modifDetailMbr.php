<?php  $contenu = ob_start(); ?>
<link rel="stylesheet" href="./assets/css/telephone.css">
<script src="./assets/js/formulaireClient.js"></script>


    <div class="container text-center bg-warning" style="position:relative;  ">
        <br><br><br>   
        <h1 class="h2">Modification de données Client</h1>
        <form action="./index.php?action=modifierMbr&modifierMbr" method="POST" enctype="multipart/form-data">
            <div class="form-row align-items-center">
                <div class="col-sm-1 my-1">
                    <label for="idl">Id Membre</label>
                    <input type="text" class="form-control" id="idl" name="idl" 
                           placeholder="<?=(int)$donnMbr[0]->getId_clt_membre()?>"
                           value="<?=(int)$donnMbr[0]->getId_clt_membre();?>"
                    readonly>
                </div>
                <div class="col-2 my-1">
                    <label for="selectMbre">Type membre</label>
                    <input type="text" class="form-control" id="selectMbre" name="selectMbre" 
                           placeholder="<?php if (isset($_POST['selectMbre'])) { echo $_POST['selectMbre']; } else { echo $donnMbr[0]->getType_membre();}?>"
                           value="<?php if (isset($_POST['selectMbre'])) { echo $_POST['selectMbre']; } else { echo $donnMbr[0]->getType_membre();}?>"
                    readonly>
                </div>
                <div class="col-1 my-1">
                    <label for="selectStt">Statut</label>
                    <input type="text" class="form-control" id="selectStt" name="selectStt" 
                           placeholder="<?php if (isset($_POST['selectStt'])) { echo $_POST['selectStt']; } else { echo $donnMbr[0]->getStatut_client();}?>"
                           value="<?php if (isset($_POST['selectStt'])) { echo $_POST['selectStt']; } else { echo $donnMbr[0]->getStatut_client();}?>"
                    readonly>
                </div>

                <div class="col-sm-4 my-1">
                    <label for="nom">Nom membre ou représentant</label>
                    <input type="text" class="form-control" id="nom" name="nom" 
                           placeholder="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo $donnMbr[0]->getNom_membre();}?>"
                           value="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo $donnMbr[0]->getNom_membre();}?>"
                    readonly>
                </div>

                <div class="col-sm-4 my-1">
                    <label for="prenom">Prénom</label>
                    <input type="text" class="form-control" id="prenom" name="prenom" 
                           placeholder="<?php if (isset($_POST['prenom'])) { echo $_POST['prenom']; } else { echo $donnMbr[0]->getPrenom_membre();}?>"
                           value="<?php if (isset($_POST['prenom'])) { echo $_POST['prenom']; } else { echo $donnMbr[0]->getPrenom_membre();}?>"
                    readonly>
                </div>
            </div>
            <div class="form-row align-items-center">
                <div class="col-sm-4 my-1">
                    <label for="instit">Nom Institution ou Entreprise</label>
                    <input type="text" class="form-control" id="instit" name="institution" 
                           placeholder="<?php if (isset($_POST['institution'])) { echo $_POST['institution']; } else { echo $donnMbr[0]->getInstitution();}?>"
                           value="<?php if (isset($_POST['institution'])) { echo $_POST['institution']; } else { echo $donnMbr[0]->getInstitution();}?>"
                    readonly>
                </div>
                <div class="col-sm-4 my-1">
                    <label for="fonction">Fonction</label>
                    <input type="text" class="form-control" id="fonction" name="fonction" 
                           placeholder="<?php if (isset($_POST['fonction'])) { echo $_POST['fonction']; } else { echo $donnMbr[0]->getFonction();}?>"
                           value="<?php if (isset($_POST['fonction'])) { echo $_POST['fonction']; } else { echo $donnMbr[0]->getFonction();}?>">
                </div>
                <div class="form-group col-sm-4 my-1">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control text-center" id="email" name="email" aria-describedby="emailHelp" 
                        placeholder="<?php if (isset($_POST['email'])) { echo $_POST['email']; } else { echo $donnMbr[0]->getEmail();}?>"
                        value="<?php if (isset($_POST['email'])) { echo $_POST['email']; } else { echo $donnMbr[0]->getEmail();}?>"  
                    readonly>
                </div>
            </div>
            <div class="form-row align-items-center">
                <div class="form-group col-sm-2 my-1 border border-primary" id="offresatrs">
                    <div class="checkbox text-left">
                        <label><input id="offrpartner" name="offrpartner" type="checkbox" 
                        <?php if (isset($_POST['offrpartner'])) { if ((int)$_POST['offrpartner']==1) {echo "checked";} } else { if ((int)$donnMbr[0]->getOffre_partenaire()==1) {echo "checked";}}?>
                        style="margin-left: 0px">Offres partenaires</label>
                    </div>
                    <div class="checkbox text-left">
                        <label><input id="offrnwlett" name="offrnwlett" type="checkbox" 
                        <?php if (isset($_POST['offrnwlett'])) { if ((int)$_POST['offrnwlett']==1) {echo "checked";} } else { if ((int)$donnMbr[0]->getOffre_newsletter()==1) {echo "checked";}}?> 
                        style="margin-left: 0px">Offre newsletter</label>
                    </div>
                </div>
                <div class="col-sm-5 my-1">
                    <label for="inputAddress">Adresse de livraison</label>
                    <input type="text" class="form-control" id="inputAddress" name="inputAddress" 
                    placeholder="Apartment, studio, or floor"
                    value="<?php if (isset($_POST['inputAddress'])) { echo $_POST['inputAddress']; } else { 
                        $str=explode('//', $donnMbr[0]->getadr_livrais());
                        echo $str[0];
                        }?>"
                    required>
                </div>
                <div class="col-sm-3 my-1">
                    <label for="inputAddress2">Adresse complémentaire</label>
                    <input type="text" class="form-control" id="inputAddress2" name="inputAddress2" 
                    placeholder="<?php if (isset($_POST['inputAddress2'])) { echo $_POST['inputAddress2']; } else { 
                        $str=explode('//', $donnMbr[0]->getadr_livrais());
                        if (count($str)>1) {echo $str[1];} else {echo "";}
                        }?>"
                    value="<?php if (isset($_POST['inputAddress2'])) { echo $_POST['inputAddress2']; } else { 
                        $str=explode('//', $donnMbr[0]->getadr_livrais());
                        if (count($str)>1) {echo $str[1];} else {echo "";}
                        }?>">
                    </div>
                <div class="col-sm-2 my-1">
                    <label for="inputCity">Ville</label>
                    <input type="text" class="form-control" id="inputCity" name="inputCity"
                        value="<?php if (isset($_POST['inputCity'])) { echo $_POST['inputCity']; } else { echo $donnMbr[0]->getVille();}?>"
                required>
                </div>
                </div>

            <div class="form-row align-items-center">

                <div class="col-sm-2 my-1">
                    <label for="inputEtat">Pays</label>
                    <input type="text" class="form-control" id="inputEtat" name="inputEtat"
                        value="<?php if (isset($_POST['inputState'])) { echo $_POST['inputState']; } else { echo $donnMbr[0]->getPays();}?>"
                    readonly>
                    <input type="text" class="form-control" id="inputZipEtat" name="inputZipEtat"
                        value="<?php if (isset($_POST['inputZipEtat'])) { echo $_POST['inputZipEtat']; } else { echo $donnMbr[0]->getZip_etat();}?>"
                    readonly>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="inputState">Changer Pays</label>
                    <select id="inputState" name="inputState" class="countrypicker form-control"></select>
                    <input type="hidden" id="nometat" name="nometat"/>
                </div>


                <div class="col-sm-2 my-1">
                    <label for="inputZip">Code Postal</label>
                    <input type="text" class="form-control" id="inputZip" name="inputZip"
                       value="<?php if (isset($_POST['inputZip'])) { echo $_POST['inputZip']; } else { echo $donnMbr[0]->getCode_postal();}?>"
                    required>
                </div>
            

          
                <div id="teldiv" name="teldiv" class="col-sm-6 my-1 input-group">
                  
                    <input type="text" class="form-control" id="indicatif" name="indicatif" 
                        placeholder="<?php if (isset($_POST['indicatif'])) { echo $_POST['indicatif']; } else { echo $donnMbr[0]->getIndicatif();}?>" 
                        value="<?php if (isset($_POST['indicatif'])) { echo $_POST['indicatif']; } else { echo $donnMbr[0]->getIndicatif();}?>"
                    required>
                    <input type="tel" id="teleph1" name="telClt" class="form-control"
                     value="<?php if (isset($_POST['telClt'])) { echo $_POST['telClt']; } else { echo $donnMbr[0]->getTelephone();}?>"
                    required>
                    <span class="btn btn-info">Tel</span>
               
                </div>
                </div>
            
                <div class="col-auto my-1">
                </div>
                <div class="container bg-info text-center">
                <button type="submit" class="btn btn-primary" id="modifmbr" name="modifmbr">Enregistrer</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauMbr">Retour au tableau</a>
                </div>
        </form>
</div>
<script> $('.countrypicker').countrypicker(); </script>
<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




