<?php  $contenu = ob_start(); ?>

<?php 
    require_once('./communs/connect.php');
    
    ?>
    <br><br><br><br>
    <div class="container bg-success">
        <br><br>
        <h1 class="bg-success" style="text-align: center">Les données du client invité ont été modifiées</h1> 
       
        <br><br>
    </div>

    <div class="container bg-secondary text-center">
        <br><br>
        <a class="btn btn-danger" href="./index.php">Retour à l'accueil</a>
        <?php if ($_SESSION['Auth']['role']<3) {?>
        <a class="btn btn-danger" href="./index.php?action=tableau&tableau=tableauCti">Retour au tableau</a>
        <?php }?>
        <br><br>
    </div>
    <br><br>
    <?php

$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>