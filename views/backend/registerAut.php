<?php  $contenu = ob_start(); ?>
<script src="./assets/js/formulaireAuteur.js"></script>

    <div class="container text-center bg-warning" style="position:relative;  ">
        <br><br><br>   
        <h1 class="h2">Formulaire d'ajout d'un auteur ou rédacteur</h1>
        <form action="./index.php?action=ajouterAut&ajouterAut" method="POST" enctype="multipart/form-data">
            <div class="form-row align-items-center">
                <div class="offset-1 col-sm-3 my-1">
                    <label for="nom">Nom</label>
                    <input type="text" class="form-control" id="nom" name="nom" 
                           placeholder="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo "Entrer la référence";}?>"
                           value="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo "";}?>"
                           required>
                </div>

                <div class="col-sm-3 my-1">
                    <label for="prenom">Prénom</label>
                    <input type="text" class="form-control" id="prenom" name="prenom" 
                           placeholder="<?php if (isset($_POST['prenom'])) { echo $_POST['prenom']; } else { echo "Entrer la référence";}?>"
                           value="<?php if (isset($_POST['prenom'])) { echo $_POST['prenom']; } else { echo "";}?>"
                           required>
                </div>

                <div class="col-sm-4 my-1">
                    <label for="institution">Institution</label>
                    <input type="text" class="form-control" id="institution" name="institution" 
                           placeholder="<?php if (isset($_POST['institution'])) { echo $_POST['institution']; } else { echo "Entrer la institution";}?>"
                           value="<?php if (isset($_POST['institution'])) { echo $_POST['institution']; } else { echo "";}?>"
                           >
                </div>
            </div>

            <div class="form-row align-items-center">
                <div class="offset-1 col-sm-5">
                    <small id="imgHelp" class="form-text text-muted">Les fichiers acceptés sont de type .png, .bmp ou .jpg</small>
                    <input type="hidden" name="max_file_size" value="1000000000" />
                    <input type="file" class="form-control" id="image1" name="image1"
                    placeholder="<?php if (isset($_POST['image1'])) { echo $_POST['image1']; } else { echo "Entrer votre fichier image ( .png, .jpg, .bmp )";}?>"
                    value="<?php if (isset($_POST['image1'])) { echo $_POST['image1']; } else { echo "";}?>">                    
                </div>

                <div class="offset-1 col-sm-4 my-1">
                    <label for="fonction">Fonction</label>
                    <input type="text" class="form-control" id="fonction" name="fonction" 
                           placeholder="<?php if (isset($_POST['fonction'])) { echo $_POST['fonction']; } else { echo "Entrer la fonction";}?>"
                           value="<?php if (isset($_POST['fonction'])) { echo $_POST['fonction']; } else { echo "";}?>"
                           >
                </div>
            </div>
 
                <div class="container bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="ajoutaut" name="ajoutaut">Enregistrer</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauAut">Retour au tableau</a>
                </div>
        </form>
</div>
<script> $('.countrypicker').countrypicker(); </script>
<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




