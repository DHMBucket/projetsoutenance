<?php 
    $contenu= ob_start();
    require_once('./communs/connect.php');
    require_once('./controllers/backend/ClientInviteController.php');

?>

<script type="text/javascript" src="./assets/js/affichetabselectionScript.js">
</script>


<br><br>
<h1 class="h2" style="text-align: center;caption-side: top; font-weight: bolder">Situation des clients anonymes</h1>
    <br>

<div id="navBoard" class="row border " style="margin: auto; align-items: center; justify-content: center; ">
    <div class="col-sm-3 m-0 p-0 border">
        <div class="container">
            <nav class="navbar navbar-light bg-light m-0 p-0 ">
                <form action="./index.php?action=search&search=searchTabCti" class="form-inline" method="POST">
                    <input class="form-control mr-sm-2" type="search" id="sequence" name="sequence" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" id="seqrech" name="seqrech" type="submit">Recherche dans titre</button>
                </form>
            </nav>
        </div>
    </div>
    <?php
        
        if (isset($_SESSION['Auth']) && isset($_SESSION['Auth']['role']) && (int)$_SESSION['Auth']['role']<5) {
    ?>
    <div class="col-sm-9 m-0 p-0 border">
    
        
            <form action="./index.php?action=searchCrit&searchCrit=searchTabCritCti" class="form-inline col" method="POST">
            <div class="container row border" style="margin: auto;"> 
                <div class="col-sm-1 m-0 p-0 text-center">
                    <label for="idsearch">par Id</label>
                    <select class="form-control" id="idsearch" name="idsearch">
                        <option value=0>0</option>
                        <?php $i=1; foreach ($idData as $idVal) {?>
                            <option value=<?php echo $idVal;?>><?php echo $idVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($idData as $idVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="refsearch">référence</label>
                    <select class="form-control" id="refsearch" name="refsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($refData as $refVal) {?>
                            <option value=<?php echo $refVal;?>><?php echo $refVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($refData as $refVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$refVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="nomsearch">nom</label>
                    <select class="form-control" id="nomsearch" name="nomsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($nomData as $nomVal) {?>
                            <option value=<?php echo '"'.$nomVal.'"';?>><?php echo $nomVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($nomData as $nomVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$nomVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-3 m-0 p-0 text-center" hidden>
                    <label for="emlsearch">email</label>
                    <select class="form-control" id="emlsearch" name="emlsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($emlData as $emlVal) {?>
                            <option value=<?php echo '"'.$emlVal.'"';?>><?php echo $emlVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($emlData as $emlVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$emlVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="locsearch">localisation</label>
                    <select class="form-control" id="locsearch" name="locsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($locData as $locVal) {?>
                            <option value=<?php echo '"'.$locVal.'"';?>><?php echo $locVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($locData as $locVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$locVal; $i++;?></p>
                    <?php } ?>
                </div>

                <div class="col-sm-2 m-0 p-0 bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="filtrsearchCti" name="filtrsearchCti">Filtrage</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauCti">Réinitialiser</a>
                </div>
                </div>
            </form>
        
        
        </div>
        <?php
        }
        ?>

</div>


<table class="table table-striped table-bordered">
          
            <thead>
                <tr>
                    <th class="text-center">Numéro</th>
                    <th class="text-center">Référence</th>
                    <th class="text-center">Nom + Prénom + Instit + Fction</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Offres</th>
                    <th class="text-center">Adresse + téléphone</th>
                    <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] == 1) { ?>
                        <th class="text-center" colspan="3">Actions</th><!--colspan="3"-->
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
            <?php
                    foreach ($dataA as $donCti) {
                        
                        
                    ?>
                    <tr>
                    <td class="text-center"><?php echo $donCti->getId_clt_anonym();?></td>
                    <td class="text-center"><?php echo $donCti->getNum_compteur();?></td>
                    <td class="text-center"><?php 
                        if ($donCti->getCivilite() == 1) {echo "Monsieur ";}
                        if ($donCti->getCivilite() == 2) {echo "Madame ";}
                        echo $donCti->getNom_destin()." ".$donCti->getPrenom_destin()." ".$donCti->getInstitution()." ".$donCti->getFonction();?>
                    </td>
                        <td class="text-center"><?php echo $donCti->getEmail();?></td>

                        <td class="text-center"><?php 
                            if ($donCti->getOffre_partenaire() == 1) echo "partenaire ";
                            if ($donCti->getOffre_newsletter() == 1) echo "newsletter";
                            ?>
                        </td>
                        <td class="text-center"><?php echo $donCti->getAdr_livrais().", ".$donCti->getCode_postal().", ".$donCti->getVille().", ".$donCti->getPays()."(".$donCti->getZip_etat()."), Tél : (".$donCti->getIndicatif().")".$donCti->getTelephone();?></td>

                        <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] == 1) { ?>
                        <td class="text-center">
                            <a href='./index.php?action=detail&id=<?=$donCti->getId_clt_anonym();?>&detail=cltinv' class="btn btn-primary"><i class="fas fa-info"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                        </td>
                        <td hidden class="text-center">
                            <a href='./index.php?action=modifier&id=<?=$donCti->getId_clt_anonym();?>&modifier=cltinv' class="btn btn-success" style="display: none; background-color: aqua; color: red; pointer-events: none; cursor: default;"><i class="fas fa-pen"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                        </td>
                        <td class="text-center">
                            <a onclick="return confirm('Etes-vous sûr de vouloir supprimer cette ligne ?')" href="./index.php?action=supprimer&id=<?=$donCti->getId_clt_anonym();?>&supprimer=cltinv" class="btn btn-danger"><i class="fas fa-trash"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                        </td>
                        <?php } ?>
                    </tr>
                    <?php }
            ?>

            </tbody>
        </table>

               
        <?php
$contenu= ob_get_clean();

?>