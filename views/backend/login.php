<?php $contenu = ob_start();
 ?>

<div class=" offset-3 col-6">
<div class="card">

  <div class="card-header text-center">Page d'authentification</div>
  <div class="card-body">
    <form action="" method="post">
    <div class="form-group">
        <label for="pseudo">Pseudo ou email(au moins 4 caractères)*</label>
        <input type="text" class="form-control" placeholder="Entrer votre pseudo" id="pseudo" name="pseudo">
    </div>
    <div class="form-group">
        <label for="pass">Mot de passe*:</label>
        <input type="password" class="form-control" placeholder="Entrer votre mot de passe" id="pass" name="pass">
    </div>
    
    
    <button type="submit" class="btn btn btn-secondary btn-block" name="soumis">Soumettre</button>
    </form>
    </div>
    
    <?php if(isset($erreur)){echo $erreur;} ?>
    
    </div>
    <br>
</div>
<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php')
?>