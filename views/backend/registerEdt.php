<?php  $contenu = ob_start(); ?>
<script src="./assets/js/formulaireEditeur.js"></script>


    <div class="container text-center bg-warning" style="position:relative;  ">
        <br><br><br>   
        <h1 class="h2">Formulaire d'ajout d'un éditeur</h1>
        <form action="./index.php?action=ajouterEdt&ajouterEdt" method="POST">
            <div class="form-row align-items-center">
                <div class="col-sm-3 my-1">
                    <label for="code">Code éditeur</label>
                    <input type="text" class="form-control" id="code" name="code" 
                           placeholder="<?php if (isset($_POST['code'])) { echo $_POST['code']; } else { echo "Entrer la référence";}?>"
                           value="<?php if (isset($_POST['code'])) { echo $_POST['code']; } else { echo "";}?>"
                           required>
                </div>

                <div class="col-sm-3 my-1">
                    <label for="nom">Nom éditeur</label>
                    <input type="text" class="form-control" id="nom" name="nom" 
                           placeholder="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo "Entrer la référence";}?>"
                           value="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo "";}?>"
                           required>
                </div>

                <div class="col-sm-3 my-1">
                    <label for="localite">localité</label>
                    <input type="text" class="form-control" id="localite" name="localite" 
                           placeholder="<?php if (isset($_POST['localite'])) { echo $_POST['localite']; } else { echo "Entrer la localité";}?>"
                           value="<?php if (isset($_POST['localite'])) { echo $_POST['localite']; } else { echo "";}?>"
                           required>
                </div>

                <div class="col-sm-3 my-1">
                    <label for="pays">Pays</label>
                    <input type="text" class="form-control" id="pays" name="pays" 
                           placeholder="<?php if (isset($_POST['pays'])) { echo $_POST['pays']; } else { echo "Entrer le pays";}?>"
                           value="<?php if (isset($_POST['pays'])) { echo $_POST['pays']; } else { echo "";}?>"
                           required>
                </div>

                <div class="col-sm-2 my-1">
                    <label for="frslivr">Frais de livraison en Euro</label>
                    <input type="number" class="form-control" id="frslivr" name="frslivr" 
                            placeholder="<?php if (isset($_POST['frslivr'])) { echo $_POST['frslivr']; } else { echo "Frais livraison";}?>"
                            value="<?php if (isset($_POST['frslivr'])) {
                                                if ((int)$_POST['frslivr'] >= 0) {
                                                    echo $_POST['frslivr'];
                                                } else { echo 0;}  
                                        } else { echo 0;}?>"
                            required>
                </div>

                <div class="container bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="ajoutedt" name="ajoutedt">Enregistrer</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauEdt">Retour au tableau</a>
                </div>
        </form>
</div>
<script> $('.countrypicker').countrypicker(); </script>
<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




