<?php 
    $contenu= ob_start();
    require_once('./communs/connect.php');
    require_once('./controllers/backend/MotCleController.php');

?>

<script type="text/javascript" src="./assets/js/affichetabselectionScript.js">
</script>


<br><br>
<h1 class="h2" style="text-align: center;caption-side: top; font-weight: bolder">Situation des administrateurs</h1>
    <br>

<div id="navBoard" class="row border " style="margin: auto; align-items: center; justify-content: center; ">
    <div class="col-sm-4 m-0 p-0 border">
    <div class="container">
        <nav class="navbar navbar-light bg-light m-0 p-0 ">
            <form action="./index.php?action=search&search=searchTabUsr" class="form-inline" method="POST">
                <input class="form-control mr-sm-2" type="search" id="sequence" name="sequence" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" id="seqrech" name="seqrech" type="submit">Recherche dans mot</button>
            </form>
        </nav>
        </div>
        <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] == 1) {?>
        <div class="container">
            <form action='./index.php?action=register&register=user' method="post">
                <button class="btn btn-info" type="submit" name="ajoutUsr" id="ajoutUsr">Ajouter un gestionnaire</button>
            </form>    
        </div>
        <?php } ?>
    </div>
    <?php
        
        if (isset($_SESSION['Auth']) && isset($_SESSION['Auth']['role']) && (int)$_SESSION['Auth']['role']<5) {
    ?>
    <div class="col-sm-8 m-0 p-0 border">
        
        <form action="./index.php?action=searchCrit&searchCrit=searchTabCritUsr" class="form-inline col" method="POST">
            <div class="container row border" style="margin: auto;">               
                <div class="col-sm-2 m-0 p-0 text-center" >
                    <label for="idsearch">par Id</label>
                    <select class="form-control" id="idsearch" name="idsearch">
                        <option value=0>0</option>
                        <?php $i=1; foreach ($idData as $idVal) {?>
                            <option value=<?php echo $idVal;?>><?php echo $idVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($idData as $idVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="nomsearch">Noms / Pseudo</label>
                    <select class="form-control" id="nomsearch" name="nomsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($nomData as $nomVal) {?>
                            <option value=<?php echo '"'.$nomVal.'"';?>><?php echo $nomVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($nomData as $nomVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$nomVal; $i++;?></p>
                    <?php } ?>
                </div>
    
                <div class="col-sm-4 m-0 p-0 text-center" >
                    <label for="mailsearch">Email</label>
                    <select class="form-control" id="mailsearch" name="mailsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($mailData as $mailVal) {?>
                            <option value=<?php echo '"'.$mailVal.'"';?>><?php echo $mailVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($mailData as $mailVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$mailVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-1 m-0 p-0 text-center" >
                    <label for="rolsearch">Rôle</label>
                    <select class="form-control" id="rolsearch" name="rolsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($rolData as $rolVal) {?>
                            <option value=<?php echo $rolVal;?>><?php echo $rolVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($rolData as $rolVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$rolVal; $i++;?></p>
                    <?php } ?>
                </div>
                

                <div class="col-sm-2 m-0 p-0 bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="filtrsearchUsr" name="filtrsearchUsr">Filtrage</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauUsr">Réinitialiser</a>
                </div>

            </div>
        </form>
        
        </div>
        <?php
        }
        ?>

</div>



<table class="table table-striped table-bordered">
          
            <thead>
                <tr>

                    <th class="text-center">Numéro</th>
                    <th class="text-center">Nom</th>
                    <th class="text-center">Prénom</th>
                    <th class="text-center">Pseudo</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Rôle</th>
                    <th class="text-center">Date de création</th>
                    <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] == 1) { ?>
                        <th class="text-center">Actions</th><!--colspan="3"-->
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
            <?php
                foreach ($dataA as $donUsr) {
                    
                ?>
                    <tr>
                        <td class="text-center"><?php echo $donUsr->getId_util();?></td>
                        <td class="text-center"><?php echo $donUsr->getNom();?></td>
                        <td class="text-center"><?php echo $donUsr->getPrenom();?></td>
                        <td class="text-center"><?php echo $donUsr->getPseudo();?></td>
                        <td class="text-center"><?php echo $donUsr->getEmail();?></td>
                        <td class="text-center"><?php echo $donUsr->getRole();?></td>
                        <td class="text-center"><?php echo $donUsr->getDate_created();?></td>

                        <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] == 1) { ?>
                        <td class="text-center">
                            <a href='./index.php?action=detail&id=<?=$donUsr->getId_util();?>&detail=user' class="btn btn-primary" style="display: none; background-color: aqua; color: red; pointer-events: none; cursor: default;"><i class="fas fa-info"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                            <a href='./index.php?action=modifier&id=<?=$donUsr->getId_util();?>&modifier=user' class="btn btn-success"><i class="fas fa-pen"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                            <?php
                                if ($donUsr->getRole()>1) { 
                            ?>
                            <a onclick="return confirm('Etes-vous sûr de vouloir supprimer cette ligne ?')" href="./index.php?action=supprimer&id=<?=$donUsr->getId_util();?>&supprimer=user" class="btn btn-danger"><i class="fas fa-trash"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                            <?php }?>
                        </td>
                        <?php } ?>
                    </tr>
                    <?php }
            ?>

            </tbody>
        </table>

               
        <?php
$contenu= ob_get_clean();

?>