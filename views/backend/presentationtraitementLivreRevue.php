<?php
$contenu= ob_start();
require_once('./communs/connect.php');
require_once('./controllers/frontend/PresentationtraitementController.php');

?>
<div id="navBoard" class="row border " style="margin: auto; align-items: center; justify-content: center; ">
    <div class="col-sm-3 m-0 p-0 border">
        <nav class="navbar navbar-light bg-light m-0 p-0 ">
            <form action="./index.php?action=search&search=searchAccLvR" class="form-inline" method="POST">
                <input class="form-control mr-sm-2" type="search" id="sequence" name="sequence" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" id="seqrech" name="seqrech" type="submit">Recherche dans titre</button>
            </form>
        </nav>
    </div>
    <?php
        
        if (isset($_SESSION['Auth']) && isset($_SESSION['Auth']['role']) && (int)$_SESSION['Auth']['role']<5) {
    ?>
    <div class="col-sm-9 m-0 p-0 border">
    <div class="container row border" style="margin: auto;"> 
        
            <form action="./index.php?action=searchCrit&searchCrit=searchAccCritLvR" class="form-inline col-sm" method="POST">
              
                <div class="col-sm-1 m-0 p-0 text-center" >
                    <label for="idsearch">par Id</label>
                    <select class="form-control" id="idsearch" name="idsearch">
                        <option value=0>0</option>
                        <?php $i=1; foreach ($idData as $idVal) {?>
                            <option value=<?php echo $idVal;?>><?php echo $idVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($idData as $idVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-2 m-0 p-0 text-center" >
                    <label for="refsearch">référence</label>
                    <select class="form-control" id="refsearch" name="refsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($refData as $refVal) {?>
                            <option value=<?php echo $refVal;?>><?php echo $refVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($refData as $refVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$refVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-1 m-0 p-0 text-center" >
                    <label for="anneesearch">année</label>
                    <select class="form-control" id="anneesearch" name="anneesearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($anneeData as $anneeVal) {?>
                            <option value=<?php echo $anneeVal;?>><?php echo $anneeVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($anneeData as $anneeVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$anneeVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="ntsearch">Nom-Titre</label>
                    <select class="form-control" id="ntsearch" name="ntsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($nomData as $nomVal) {?>
                            <option value=<?php echo '"'.$nomVal.'"';?>><?php echo $nomVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($nomData as $nomVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$nomVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="autsearch">auteur,editeur,rédact.</label>
                    <select class="form-control" id="autsearch" name="autsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($autData as $autVal) {?>
                            <option value=<?php echo '"'.$autVal.'"';?>><?php echo $autVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($autData as $autVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$autVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="form-group col-sm-1 m-0 p-0" id="stocks">
                    <div class="checkbox text-left">
                        <label><input id="dispo" name="dispo" type="checkbox" value="" style="margin-left: 0px">Dispo</label>
                    </div>
                    <div class="checkbox text-left">
                        <label><input id="nonDispo" name="nonDispo" type="checkbox" value="" style="margin-left: 0px">Indispo</label>
                    </div>
                </div>
                <div class="col-sm-1 m-0 p-0 bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="filtrsearchLvR" name="filtrsearchLvR">Filtrage</button>
                    <a class="btn btn-info" href="./index.php?action=initial&initial=initialLvR">Réinitialiser</a>
                </div>
            </form>
        
        </div>
        </div>
        <?php
        }
        ?>

</div>

<p id=<?php echo "sp" ?> style="display: none"><?php echo "";?></p>

    <h3 style="text-align: center;caption-side: top;">Les livres / revues de notre site</h3>
    <br>
    <div class="container row" style="margin: auto;">
        <div class="col-auto" style="margin: auto;" >
                <button type="button" name="precedentLvR" id="precedent">Précédent</button>
                <button type="button" name="suivantLvR" id="suivant">Suivant</button>
        </div>
    </div>

<br>

<div class="row">               

<?php
    if ($numDim > 0) { 
  
        if (isset($_SESSION)) {
            if (isset($_COOKIE['iindex']) && $_COOKIE['iindex']>0) {
                $_SESSION['icompteur']=$_COOKIE['iindex'];
            }
            
?>
            <input hidden type="number" id="nbligne1" value="<?=$_SESSION['icompteur']?>">

            
            <div id="fixsz1" style="border: 1px solid #ced4da;">
                <div class="row">
                    <div class="col-sm-4" style="height:100%;">
                        <img id="ph1" src="<?='./assets/images/'.$dataA[$_SESSION['icompteur']-2]->getImage1();?>" style="width:400px;" alt="<?=$dataA[$_SESSION['icompteur']-2]->getImage1();?>">
                    </div>
                    <div class="col-sm-8">
                        <ul class="list-group list-group-flush">
                        <li class="list-group-item"><p><span class="font-weight-bolder">Référence&emsp;&nbsp;&nbsp;:&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-2]->getReference();?></p></li>
                        <?php if ($dataA[$_SESSION['icompteur']-2]->getNom_specif_revue()<>"") { ?>
                            <li class="list-group-item"><p><span class="font-weight-bolder">&nbsp;</span><b>Revue</b><?=" ".$dataA[$_SESSION['icompteur']-2]->getNom_specif_revue();?></p></li>
                        <?php } ?>
                        <li class="list-group-item"><p><span class="font-weight-bolder">Titre&emsp;&emsp;&emsp;&emsp;:&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-2]->getTitre();?></p></li>
                        <li class="list-group-item"><p><span class="font-weight-bolder">Auteur(s)&emsp;&emsp;:&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-2]->getAuteur_princ1_nom()." ".$dataA[$_SESSION['icompteur']-2]->getAuteur_princ1_prenom()." ".$dataA[$_SESSION['icompteur']-2]->getAuteur_princ1_institution();?></p></li>
                        <?php if ($dataA[$_SESSION['icompteur']-2]->getAuteur_princ2_nom()<>"" || $dataA[$_SESSION['icompteur']-2]->getAuteur_princ2_institution()<>"") { ?>
                            <li class="list-group-item"><p><span class="font-weight-bolder">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-2]->getAuteur_princ2_nom()." ".$dataA[$_SESSION['icompteur']-2]->getAuteur_princ2_prenom()." ".$dataA[$_SESSION['icompteur']-2]->getAuteur_princ2_institution();?></p></li>
                        <?php } ?>
                        <li class="list-group-item"><p><span class="font-weight-bolder">Edition&emsp;&emsp;&emsp;:&nbsp;&nbsp;</span><?php echo " ".$dataA[$_SESSION['icompteur']-2]->getEditeur1(); if ($dataA[$_SESSION['icompteur']-2]->getEditeur2()<>'') {" / ".$dataA[$_SESSION['icompteur']-2]->getEditeur2();}?></p></li>
                        
                        <li class="list-group-item"><p><span class="font-weight-bolder">Prix&emsp;&emsp;&emsp;&emsp;:&nbsp;&nbsp;</span><?php echo " ".$dataA[$_SESSION['icompteur']-2]->getPrix()." ";?>&euro;</p></li>
                       
                        <li class="list-group-item"><p><span class="font-weight-bolder">Date de parution&nbsp;&nbsp;&emsp; :&nbsp;&nbsp;</span><?=$dataA[$_SESSION['icompteur']-2]->getDate_parution();?></p></li>
                        <li class="list-group-item"><p><span class="font-weight-bolder">Format&emsp;&emsp;&emsp;:&nbsp;&nbsp;&nbsp;</span><?=$dataA[$_SESSION['icompteur']-2]->getFormat_livre()." ".$dataA[$_SESSION['icompteur']-2]->getType_support();?></p></li>
                        <?php if (($dataA[$_SESSION['icompteur']-2]->getStock_iris() + $dataA[$_SESSION['icompteur']-2]->getStock_editeur())<>0) { ?>
                            <li class="list-group-item"><h3 class="h4 font-weight-bolder" style="color:green">Disponible</h3>
                        <?php } else { ?>
                            <li class="list-group-item"><h3 class="h4 font-weight-bolder" style="color:red">Indisponible</h3>
                        <?php }?>

                        <li class="list-group-item"><p><span class="font-weight-bolder">Nombre de pages&emsp;&emsp;:&nbsp;&nbsp;&nbsp;</span><?=$dataA[$_SESSION['icompteur']-2]->getNbre_pages();?></p></li>
                        <li class="list-group-item"><h4 class="h5 font-weight-bolder">Description :</h4><p id="descript1"><?=$dataA[$_SESSION['icompteur']-2]->getDescription();?></p></li>
                        <?php if ($dataA[$_SESSION['icompteur']-2]->getMorceau_choisi()<>"") { ?>
                            <li class="list-group-item"><h4 class="h5 font-weight-bolder">Passage :</h4><p id="morcechsi1"><?=$dataA[$_SESSION['icompteur']-2]->getMorceau_choisi();?></p></li>
                        <?php } ?>


                            <li class="list-group-item">
                                <button class="btn btn-success" type="button" id="details1">détail</button> 
                                <a class="btn btn-secondary" 
                                <?php if (($dataA[$_SESSION['icompteur']-2]->getStock_iris() + $dataA[$_SESSION['icompteur']-2]->getStock_editeur())<>0) { 
                                        echo 'style="background-color: yellow; color: green;"';
                                      } else {
                                        echo 'style="background-color: aqua; color: red; pointer-events: none; cursor: default;"';
                                      }  
                                ?>
                                href="./index.php?t=<?=$_SESSION['icompteur']-2?>&action=reservPanier" id="reserv1">Ajouter au panier</a>
                                <?php if ($dataA[$_SESSION['icompteur']-2]->getNom_specif_revue()<>"" && isset($_SESSION['Auth'])) { ?>
                                    <a class="btn btn-secondary" 
                                    <?php if (($dataA[$_SESSION['icompteur']-2]->getStock_iris() + $dataA[$_SESSION['icompteur']-2]->getStock_editeur())<>0) { 
                                        echo 'style="background-color: brown; color: white;"';
                                      } else {
                                        echo 'style="background-color: aqua; color: red; pointer-events: none; cursor: default;"';
                                      }  
                                ?>
                                    href="./index.php?t=<?=$_SESSION['icompteur']-2?>&nomrevue=<?=$dataA[$_SESSION['icompteur']-2]->getNom_specif_revue();?>&action=abonnement" id="abonnemt1">S'abonner</a>
                                <?php } ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        <?php
        }
        if (
            (isset($_SESSION) && isset($_COOKIE['iindex']) && $_COOKIE['iindex']==0  && count($dataA)>1) 
            || 
            (isset($_SESSION) && $_SESSION['icompteur']==2 && count($dataA)>1)
            ) {
        ?>
        <input hidden type="number" id="nbligne2" value="<?=$_SESSION['icompteur']?>">

        <div id="fixsz2"  style="border: 1px solid #ced4da;">
                <div class="row">
                    <div class="col-sm-4" style="height:100%;">
                        <img id="ph2" src="<?='./assets/images/'.$dataA[$_SESSION['icompteur']-1]->getImage1();?>" style="width:400px;" alt="<?=$dataA[$_SESSION['icompteur']-1]->getImage1();?>">
                    </div>
                    <div class="col-sm-8">
                        <ul class="list-group list-group-flush">
                        <li class="list-group-item"><p><span class="font-weight-bolder">Référence&emsp;&nbsp;&nbsp;:&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-1]->getReference();?></p></li>
                        <?php if ($dataA[$_SESSION['icompteur']-1]->getNom_specif_revue()<>"") { ?>
                            <li class="list-group-item"><p><span class="font-weight-bolder">&nbsp;</span><b>Revue</b><?=" ".$dataA[$_SESSION['icompteur']-1]->getNom_specif_revue();?></p></li>
                        <?php } ?>
                        <li class="list-group-item"><p><span class="font-weight-bolder">Titre&emsp;&emsp;&emsp;&emsp;:&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-1]->getTitre();?></p></li>
                        <li class="list-group-item"><p><span class="font-weight-bolder">Auteur(s)&emsp;&emsp;:&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-1]->getAuteur_princ1_nom()." ".$dataA[$_SESSION['icompteur']-1]->getAuteur_princ1_prenom()." ".$dataA[$_SESSION['icompteur']-1]->getAuteur_princ1_institution();?></p></li>
                        <?php if ($dataA[$_SESSION['icompteur']-1]->getAuteur_princ2_nom()<>"" || $dataA[$_SESSION['icompteur']-1]->getAuteur_princ2_institution()<>"") { ?>
                            <li class="list-group-item"><p><span class="font-weight-bolder">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-1]->getAuteur_princ2_nom()." ".$dataA[$_SESSION['icompteur']-1]->getAuteur_princ2_prenom()." ".$dataA[$_SESSION['icompteur']-1]->getAuteur_princ2_institution();?></p></li>
                        <?php } ?>
                        <li class="list-group-item"><p><span class="font-weight-bolder">Edition&emsp;&emsp;&emsp;:&nbsp;&nbsp;</span><?php echo " ".$dataA[$_SESSION['icompteur']-1]->getEditeur1(); if ($dataA[$_SESSION['icompteur']-1]->getEditeur2()<>'') {" / ".$dataA[$_SESSION['icompteur']-1]->getEditeur2();}?></p></li>

                        <li class="list-group-item"><p><span class="font-weight-bolder">Prix&emsp;&emsp;&emsp;&emsp;:&nbsp;&nbsp;</span><?php echo " ".$dataA[$_SESSION['icompteur']-1]->getPrix()." ";?>&euro;</p></li>
                        
                        <li class="list-group-item"><p><span class="font-weight-bolder">Date de parution&nbsp;&nbsp;&emsp; :&nbsp;&nbsp;</span><?=$dataA[$_SESSION['icompteur']-1]->getDate_parution();?></p></li>
                        <li class="list-group-item"><p><span class="font-weight-bolder">Format&emsp;&emsp;&emsp;:&nbsp;&nbsp;&nbsp;</span><?=$dataA[$_SESSION['icompteur']-1]->getFormat_livre()." ".$dataA[$_SESSION['icompteur']-1]->getType_support();?></p></li>
                        <?php if (($dataA[$_SESSION['icompteur']-1]->getStock_iris() + $dataA[$_SESSION['icompteur']-1]->getStock_editeur())<>0) { ?>
                            <li class="list-group-item"><h3 class="h4 font-weight-bolder" style="color:green">Disponible</h3>
                        <?php } else { ?>
                            <li class="list-group-item"><h3 class="h4 font-weight-bolder" style="color:red">Indisponible</h3>
                        <?php }?>

                        <li class="list-group-item"><p><span class="font-weight-bolder">Nombre de pages&emsp;&emsp;:&nbsp;&nbsp;&nbsp;</span><?=$dataA[$_SESSION['icompteur']-1]->getNbre_pages();?></p></li>
                        <li class="list-group-item"><h4 class="h5 font-weight-bolder">Description :</h4><p id="descript2"><?=$dataA[$_SESSION['icompteur']-1]->getDescription();?></p></li>
                        <?php if ($dataA[$_SESSION['icompteur']-1]->getMorceau_choisi()<>"") { ?>
                            <li class="list-group-item"><h4 class="h5 font-weight-bolder">Passage :</h4><p id="morcechsi2"><?=$dataA[$_SESSION['icompteur']-1]->getMorceau_choisi();?></p></li>
                        <?php } ?>

                        <li class="list-group-item">
                            <button class="btn btn-success" type="button" id="details2">détail</button>
                            <a class="btn btn-secondary" 
                            <?php if (($dataA[$_SESSION['icompteur']-1]->getStock_iris() + $dataA[$_SESSION['icompteur']-1]->getStock_editeur())<>0) { 
                                        echo 'style="background-color: yellow; color: green;"';
                                      } else {
                                        echo 'style="background-color: aqua; color: red; pointer-events: none; cursor: default;"';
                                      }  
                            ?> 
                            href="./index.php?t=<?=$_SESSION['icompteur']-1?>&action=reservPanier" id="reserv2">Ajouter au panier</a>
                            <?php if ($dataA[$_SESSION['icompteur']-1]->getNom_specif_revue()<>"" && isset($_SESSION['Auth'])) { ?>
                                    <a class="btn btn-secondary" 
                                    <?php if (($dataA[$_SESSION['icompteur']-1]->getStock_iris() + $dataA[$_SESSION['icompteur']-1]->getStock_editeur())<>0) { 
                                        echo 'style="background-color: brown; color: white;"';
                                      } else {
                                        echo 'style="background-color: aqua; color: red; pointer-events: none; cursor: default;"';
                                      }  
                                ?>
                                    href="./index.php?t=<?=$_SESSION['icompteur']-1?>&nomrevue=<?=$dataA[$_SESSION['icompteur']-1]->getNom_specif_revue();?>&action=abonnement" id="abonnemt2">S'abonner</a>
                                <?php } ?>

                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <?php
        }
    } else {
        ?>
            <br>
            <div class="container row" style="margin: auto;">
                <div class="col-auto" style="margin: auto;" >
                    <h1>Pas de livres ou revues avec ces critères, appuyer sur la touche Filtrage ou Réinitialiser</h1>
                </div>
            </div>
            <br>
        <?php
    }

    
?>

</div>

<br><br>
<script src="./assets/js/presentationScript.js"> // exec 14

</script>   

<?php

    $contenu= ob_get_clean();
    
?>    

