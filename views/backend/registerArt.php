<?php  $contenu = ob_start(); ?>
<link rel="stylesheet" href="./assets/css/telephone.css">
<script src="./assets/js/formulaireArticle.js"></script>


    <div class="container text-center bg-warning" style="position:relative;  ">
        <br><br><br>   
        <h1 class="h2">Formulaire d'ajout d'article</h1>
        <form action="./index.php?action=ajouterArt&ajouterArt" method="POST" enctype="multipart/form-data">
            <div class="form-row align-items-center">
 
                <div class="col-sm-3 my-1">
                    <label for="titre">Titre</label>
                    <input type="text" class="form-control" id="titre" name="titre" 
                           placeholder="<?php if (isset($_POST['titre'])) { echo $_POST['titre']; } else { echo "Entrer le titre";}?>"
                           value="<?php if (isset($_POST['titre'])) { echo $_POST['titre']; } else { echo "";}?>"
                           required>
                </div>

                <div class="col-sm-3 my-1">
                    <label for="theme">Thème</label>
                    <select class="form-control" id="theme" name="theme">
                        <?php $i=1; foreach ($donnThm as $theme) { 
                            $str1= $theme->getNom_theme();
                            $str = str_replace("$", " ", $str1, $count);$str1=$str;
                            ?>
                            <option value=<?php echo $theme->getNom_theme();?>><?php echo $str1; $i++?></option>
                        <?php } ?>
                    </select>
                </div>
  
                <div class="col-sm-4 my-1">
                    <label for="livre">Livre</label>
                    <select class="form-control" id="livre" name="livre" value="" placeholder="">
                        <?php $i=1; foreach ($donnLvR as $livre) {
                            $str1= $livre->getId_livre()." - ".$livre->getReference()." - ".$livre->getTitre();
                            $str = str_replace("$", " ", $str1, $count);$str1=$str;
                            ?>
                            <option value=<?php echo $livre->getId_livre()."//".$livre->getReference()."//".$livre->getTitre();?>><?php echo $str1; $i++?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="col-sm-2 my-1">
                    <label for="rang">Emplacement</label>
                    <input type="number" class="form-control" id="rang" name="rang" 
                            placeholder="<?php if (isset($_POST['rang'])) { echo $_POST['rang']; } else { echo "Entrer rang";}?>"
                            value="<?php if (isset($_POST['rang'])) {
                                                if ((int)$_POST['rang'] >= 0) {
                                                    echo $_POST['rang'];
                                                } else { echo 0;}  
                                        } else { echo 0;}?>"
                            required>
                </div>

                <div class="col-sm-4 my-1">
                    <label id="filelab" for="ficjustif">Fichier ebook</label>
                    <input type="hidden" name="max_file_size" value="1000000000" />
                    <input type="file" class="form-control" id="ficjustif" name="ficjustif"
                    placeholder="<?php if (isset($_POST['ficjustif'])) { echo $_POST['ficjustif']; } else { echo "Entrer votre fichier justificatif ( .pdf, .docx, .jpg )";}?>"
                    value="<?php if (isset($_POST['ficjustif'])) { echo $_POST['ficjustif']; } else { echo "";}?>">
                    <small id="fileHelp" class="form-text text-muted">Les fichiers acceptés sont de type .pdf, .docx ou .jpg</small>
                </div>

                <div class="col-sm-4 my-1">
                    <label for="nauteur1">Nom auteur1</label>
                    <select class="form-control" id="nauteur1" name="nauteur1" required>
                        <?php $i=1; foreach ($donnAut1 as $auteur) { 
                            $str1= $auteur->getNom()." - ".$auteur->getPrenom()." - ".$auteur->getInstitution();
                            $str = str_replace("$", " ", $str1, $count);$str1=$str;
                            ?>
                            <option value=<?php echo $auteur->getNom()."//".$auteur->getPrenom()."//".$auteur->getInstitution();?>><?php echo $str1; $i++?></option>
                        <?php } ?>
                    </select>
                    <small id="fileHelp" class="form-text text-muted">Premier Auteur</small>
                </div>

                <div class="col-sm-4 my-1">
                    <label for="nauteur2">Nom auteur2</label>
                    <select class="form-control" id="nauteur2" name="nauteur2" value="" placeholder="">
                        <?php $i=1; foreach ($donnAut2 as $auteur) {
                            $str1= $auteur->getNom()." - ".$auteur->getPrenom()." - ".$auteur->getInstitution();
                            $str = str_replace("$", " ", $str1, $count);$str1=$str;
                            ?>
                            <option value=<?php echo $auteur->getNom()."//".$auteur->getPrenom()."//".$auteur->getInstitution();?>><?php echo $str1; $i++?></option>
                        <?php } ?>
                    </select>
                    <small id="fileHelp" class="form-text text-muted">Deuxième Auteur</small>
                </div>

                <div class="offset-3 col-sm-2 my-1">
                    <label for="nbpage">Nombre de pages</label>
                    <input type="number" class="form-control" id="nbpage" name="nbpage" 
                            placeholder="<?php if (isset($_POST['nbpage'])) { echo $_POST['nbpage']; } else { echo 1;}?>"
                            value="<?php if (isset($_POST['nbpage'])) {
                                                if ((int)$_POST['nbpage'] > 0) {
                                                    echo $_POST['nbpage'];
                                                } else { echo 1;}  
                                        } else { echo 1;}?>"
                            required>
                </div>

                <div class="col-sm-2 my-1">
                    <label for="mpert">Nbre mots-clés</label>
                    <input type="number" class="form-control" id="mpert" name="mpert" 
                            placeholder="<?php if (isset($_POST['mpert'])) { echo $_POST['mpert']; } else { echo 0;}?>"
                            value="<?php if (isset($_POST['mpert'])) {
                                                if ((int)$_POST['mpert'] >= 0) {
                                                    echo $_POST['mpert'];
                                                } else { echo 0;}  
                                        } else { echo 0;}?>"
                            required>
                </div>

                <div class="col-sm-2 my-1">
                    <label for="prix">Prix en Euro</label>
                    <input type="number" class="form-control" id="prix" name="prix" 
                            placeholder="<?php if (isset($_POST['prix'])) { echo $_POST['prix']; } else { echo "Entrer prix";}?>"
                            value="<?php if (isset($_POST['prix'])) {
                                                if ((int)$_POST['prix'] >= 0) {
                                                    echo $_POST['prix'];
                                                } else { echo 0;}  
                                        } else { echo 0;}?>"
                            required>
                </div>

                <div class="container bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="ajoutart" name="ajoutart">Enregistrer</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauArt">Retour au tableau</a>
                </div>
        </form>
</div>

<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




