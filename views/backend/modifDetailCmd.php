<?php  $contenu = ob_start(); ?>
<link rel="stylesheet" href="./assets/css/telephone.css">
<script src="./assets/js/formulaireClientcoord.js"></script>

<br><br><br>
    <div class="container text-center bg-warning">
        <h1 class="h2">Modification de commande</h1>
        <form action="./index.php?action=modifierCmd&modifierCmd" method="POST">
            <div class="form-row align-items-center">
                <div class="col-sm-1 my-1">
                    <label for="idcmd">Id Cmde</label>
                    <input type="text" class="form-control" id="idcmd" name="idcmd" 
                            placeholder="<?=(int)$donnCmd[0]->getId_commande()?>"
                            value="<?=(int)$donnCmd[0]->getId_commande();?>"
                            readonly>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="ref">Référence</label>
                    <input type="text" class="form-control" id="ref" name="ref" 
                           placeholder="<?php echo $donnCmd[0]->getRef_commande();?>"
                           value="<?php echo $donnCmd[0]->getRef_commande();?>"
                           readonly>
                </div>
                <div class="col-sm-1 my-1">
                    <label for="tpc">Type Client</label>
                    <input type="text" class="form-control" id="tpc" name="tpc" 
                            placeholder="<?=(int)$donnCmd[0]->getType_client()?>"
                            value="<?=(int)$donnCmd[0]->getType_client();?>"
                            readonly>
                </div>
                <div class="col-sm-1 my-1">
                    <label for="idc">Id Client</label>
                    <input type="text" class="form-control" id="idc" name="idc" 
                            placeholder="<?=(int)$donnCmd[0]->getId_client()?>"
                            value="<?=(int)$donnCmd[0]->getId_client();?>"
                            readonly>
                </div>
                <div class="col-sm-1 my-1">
                    <label for="idl">Id livre</label>
                    <input type="text" class="form-control" id="idl" name="idl" 
                            placeholder="<?=(int)$donnCmd[0]->getId_livre()?>"
                            value="<?=(int)$donnCmd[0]->getId_livre();?>"
                            readonly>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="refl">Réf. livre</label>
                    <input type="text" class="form-control" id="refl" name="refl" 
                           placeholder="<?php echo $donnCmd[0]->getRef_livre();?>"
                           value="<?php echo $donnCmd[0]->getRef_livre();?>"
                           readonly>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="ttrl">Titre livre</label>
                    <input type="text" class="form-control" id="ttrl" name="ttrl" 
                           placeholder="<?php echo $donnCmd[0]->getTtr_livre();?>"
                           value="<?php echo $donnCmd[0]->getTtr_livre();?>"
                           readonly>
                </div>
                <div class="col-sm-1 my-1">
                    <label for="ida">Id article</label>
                    <input type="text" class="form-control" id="ida" name="ida" 
                            placeholder="<?=(int)$donnCmd[0]->getId_artcl()?>"
                            value="<?=(int)$donnCmd[0]->getId_artcl();?>"
                            readonly>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="ttra">Titre article</label>
                    <input type="text" class="form-control" id="ttra" name="ttra" 
                           placeholder="<?php echo $donnCmd[0]->getTtr_artcl();?>"
                           value="<?php echo $donnCmd[0]->getTtr_artcl();?>"
                           readonly>
                </div>
                <div class="col-sm-1 my-1">
                    <label for="nbr">Nombre</label>
                    <input type="text" class="form-control" id="nbr" name="nbr" 
                            placeholder="<?=(int)$donnCmd[0]->getNombre()?>"
                            value="<?=(int)$donnCmd[0]->getNombre();?>"
                            readonly>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="tarif1">Prix de la commande</label>
                    <input type="number" name="prixeuro" id="prixeuro" value="<?php echo (int)$donnCmd[0]->getPx_ttc();?>" hidden>
                    <input type="number" class="form-control" id="tarif1" name="tarif1" 
                           placeholder=""
                           value="<?php if (isset($_POST['tarif1'])) { echo $_POST['tarif1']; } else { echo (int)$donnCmd[0]->getPx_ttc();}?>" readonly>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="frs">Frais livr.</label>
                    <input type="number" name="frseuro" id="frseuro" value="<?php echo (int)$donnCmd[0]->getFrs_livrais();?>" hidden>
                    <input type="number" class="form-control" id="frs" name="frs" 
                           placeholder=""
                           value="<?php echo (int)$donnCmd[0]->getFrs_livrais();?>" readonly>
                </div>
                <div class="col-1 my-1">
                    <label for="monnaie1">Libellé</label>
                    <select class="form-control" id="monnaie1" name="monnaie1" readonly>
                        <?php $i=1; foreach ($donnMonn as $monn) {
                            if ($monn->getNom_monnaie() == $donnCmd[0]->getMonnaie()) {?>
                            <option value=<?php echo $i."-".$monn->getSymb_monnaie();?>><?php echo $i."-".$monn->getSymb_monnaie(); $i++?></option>
                        <?php } } ?>
                    </select>
                    <?php $i=1; foreach ($donnMonn as $monn) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$monn->getSymb_monnaie()."--".$monn->getNom_monnaie()."--".$monn->getVal_commerc_en_euro()."--".$monn->getId_monnaie(); $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="mpy">Mode Paiement</label>
                    <input type="number" name="mpyeuro" id="mpyeuro" value="<?php echo (int)$donnCmd[0]->getMode_payment();?>" hidden>
                    <input type="number" class="form-control" id="mpy" name="mpy" 
                           placeholder=""
                           value="<?php echo (int)$donnCmd[0]->getMode_payment();?>" readonly>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="reft">Réf. Transaction</label>
                    <input type="text" class="form-control" id="reft" name="reft" 
                           placeholder="<?php echo $donnCmd[0]->getRef_transaction();?>"
                           value="<?php echo $donnCmd[0]->getRef_transaction();?>"
                           readonly>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="dteods">Date ordre livr.</label>
                    <input type="date" class="form-control" id="dteods" name="dteods" 
                           placeholder="<?php echo $donnCmd[0]->getDate_ordre_livrais();?>"
                           value="<?php echo $donnCmd[0]->getDate_ordre_livrais();?>"
                    readonly>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="dtelpv">Date livr. prev</label>
                    <input type="date" class="form-control" id="dtelpv" name="dtelpv" 
                           placeholder="<?php echo $donnCmd[0]->getDate_livrais_prev();?>"
                           value="<?php echo $donnCmd[0]->getDate_livrais_prev();?>"
                    readonly>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="dtelvr">Date livr. real</label>
                    <input type="date" class="form-control" style="background-color: aqua" id="dtelvr" name="dtelvr" 
                           placeholder="<?php if (isset($_POST['dtelvr'])) { echo $_POST['dtelvr']; } else { echo $donnCmd[0]->getDate_livrais_real();}?>"
                           value="<?php if (isset($_POST['dtelvr'])) { echo $_POST['dtelvr']; } else { echo $donnCmd[0]->getDate_livrais_real();}?>"
                    required>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="nom">Nom</label>
                    <input type="text" class="form-control" id="nom" name="nom" 
                           placeholder="<?php echo $donnCmd[0]->getNom_destin();?>"
                           value="<?php echo $donnCmd[0]->getNom_destin();?>"
                           readonly>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="prenom">Prénom</label>
                    <input type="text" class="form-control" id="prenom" name="prenom" 
                           placeholder="<?php echo $donnCmd[0]->getPrenom_destin();?>"
                           value="<?php echo $donnCmd[0]->getPrenom_destin();?>"
                           readonly>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="inst">Institution</label>
                    <input type="text" class="form-control" id="inst" name="inst" 
                           placeholder="<?php echo $donnCmd[0]->getInstitution_destin();?>"
                           value="<?php echo $donnCmd[0]->getInstitution_destin();?>"
                           readonly>
                </div>

                <div class="col-sm-2 my-1">
                    <label for="mdlv">Mode livr.</label>
                    <input type="text" class="form-control" id="mdlv" name="mdlv" 
                           placeholder="<?php echo $donnCmd[0]->getMode_livrais();?>"
                           value="<?php echo $donnCmd[0]->getMode_livrais();?>"
                           readonly>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="msg">Message Client</label>
                    <input type="text" class="form-control" id="msg" name="msg" 
                           placeholder="<?php echo $donnCmd[0]->getMsg_livrais();?>"
                           value="<?php echo $donnCmd[0]->getMsg_livrais();?>"
                           readonly>
                </div>

                <div class="col-sm-2 my-1">
                    <label for="adr">Adresse</label>
                    <input type="text" class="form-control" id="adr" name="adr" 
                           placeholder="<?php echo $donnCmd[0]->getAdr_livrais();?>"
                           value="<?php echo $donnCmd[0]->getAdr_livrais();?>"
                           readonly>
                </div>
                
                    <div class="col-sm-4 my-1">
                    <label for="inputCity">Ville</label>
                    <input type="text" class="form-control" id="inputCity" name="inputCity"
                    value="<?php echo $donnCmd[0]->getVille();?>"  
                    readonly>
                </div>
                <div class="col-sm-4 my-1">
                    <label for="inputState">Etat</label>
                    <input id="inputState" name="inputState" class="countrypicker form-control"
                    value="<?php echo $donnCmd[0]->getPays();?>"  
                    readonly>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="inputZip">Code Postal</label>
                    <input type="text" class="form-control" id="inputZip" name="inputZip"
                    value="<?php echo $donnCmd[0]->getCode_postal();?>"  
                    readonly>
                </div>
            
                <div class="col-sm-4 my-1">
                    <label for="teleph1" style="display:block;">Téléphone</label>
                    <input type="tel" id="teleph1" name="telClt" class="form-control"
                    value="<?php echo $donnCmd[0]->getTelephone();?>"  
                    readonly>
                    
                </div>
            
                <div class="col-auto my-1">
                </div>
                <div class="container bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="modifcmd" name="modifcmd">Enregistrer</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauCmd">Retour au tableau</a>
                </div>
        </form>
</div>
<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




