<?php  $contenu = ob_start(); ?>

<script src="./assets/js/formulairelivre.js"></script>


    <div class="container text-center bg-warning" style="position:relative;  ">
        <br><br><br>   
        <h1 class="h2">Modification d'article</h1>
        <form action="./index.php?action=modifierArt&modifierArt" method="POST" enctype="multipart/form-data">
            <div class="form-row align-items-center">

                <div class="col-sm-1 my-1">
                    <label for="ida">Id article</label>
                    <input type="text" class="form-control" id="ida" name="ida" 
                           placeholder="<?=(int)$donnArt[0]->getId_article()?>"
                           value="<?=(int)$donnArt[0]->getId_article();?>"
                           readonly>
                </div>
                <div class="col-sm-3 my-1">
                    <label for="titre">Titre</label>
                    <input type="text" class="form-control" id="titre" name="titre" 
                           placeholder="<?php if (isset($_POST['titre'])) { echo $_POST['titre']; } else { echo $donnArt[0]->getTitre();}?>"
                           value="<?php if (isset($_POST['titre'])) { echo $_POST['titre']; } else { echo $donnArt[0]->getTitre();}?>"
                    required>
                </div>

                <div class="col-sm-3 my-1">
                    <label for="theme">Thème</label>
                    <select class="form-control" id="theme" name="theme">
                        <option value=<?php 
                            if (isset($_POST['theme'])) { echo '"'.$_POST['theme'].'"';} else
                            { echo '"'.$donnArt[0]->getTheme_article().'"';} ?>>
                            <?php
                            if (isset($_POST['theme'])) { echo '"'.$_POST['theme'].'"';} else
                            { echo '"'.$donnArt[0]->getTheme_article().'"';} 
                            ?>
                        </option>
                        <?php $i=1; foreach ($donnThm as $theme) { ?>
                            <option value=<?php echo '"'.$theme->getNom_theme().'"';?>><?php echo $theme->getNom_theme(); $i++?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-sm-1 my-1">
                    <label for="idl">Id livre</label>
                    <input type="text" class="form-control" id="idl" name="idl" 
                           placeholder="<?=(int)$donnArt[0]->getId_livre()?>"
                           value="<?=(int)$donnArt[0]->getId_livre();?>"
                           readonly>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="ref">Référence</label>
                    <input type="text" class="form-control" id="ref" name="ref" 
                           placeholder="<?php echo $donnArt[0]->getRef_livre();?>"
                           value="<?php echo $donnArt[0]->getRef_livre();?>"
                           readonly>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="empl">Emplacement</label>
                    <input type="text" class="form-control" id="empl" name="empl" 
                           placeholder="<?php echo $donnArt[0]->getEmplacement();?>"
                           value="<?php echo $donnArt[0]->getEmplacement();?>"
                           readonly>
                </div>
                <div class="col-sm-4 my-1">
                    <label id="filelab" for="ficjustif">Fichier ebook</label>
                    <input type="hidden" name="max_file_size" value="1000000000" />
                    <input type="file" class="form-control" id="ficjustif" name="ficjustif"
                    placeholder="<?php if (isset($_FILES['ficjustif']['name']) && (!empty($_FILES['ficjustif']['name']))) { echo $_FILES['ficjustif']['name']; } else { echo $donnArt[0]->getFichier_earticle();}?>"
                    value="<?php if (isset($_FILES['ficjustif']['name']) && (!empty($_FILES['ficjustif']['name']))) { echo $_FILES['ficjustif']['name']; } else { echo $donnArt[0]->getFichier_earticle();}?>">
                    <small id="fileHelp" class="form-text text-muted">Les fichiers acceptés sont de type .pdf, .docx ou .jpg</small>
                </div>

                <div class="col-sm-4 my-1">
                    <label for="nauteur1">Nom auteur1</label>
                    <?php
                        $str1= $donnArt[0]->getAuteur1_nom()."//".$donnArt[0]->getAuteur1_prenom()."//".$donnArt[0]->getAuteur1_institution();
                        $str2= $donnArt[0]->getAuteur1_nom()." - ".$donnArt[0]->getAuteur1_prenom()." - ".$donnArt[0]->getAuteur1_institution();
                        if (isset($_POST['nauteur1'])) { $str1=str_replace('"', '', $_POST['nauteur1'], $count); $str2 = str_replace("//", " - ", $str1, $count);}
                    ?>
                    <select class="form-control" id="nauteur1" name="nauteur1" required>
                        <option value=<?='"'.$str1.'"'?>><?=$str2?></option>
                        <?php $i=1; foreach ($donnAut1 as $auteur) { 
                            $str1= $auteur->getNom()."//".$auteur->getPrenom()."//".$auteur->getInstitution();
                            $str2= $auteur->getNom()." - ".$auteur->getPrenom()." - ".$auteur->getInstitution();
                        ?>
                            <option value=<?php echo '"'.$str1.'"';?>><?php echo $str2; $i++?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="col-sm-4 my-1">
                    <label for="nauteur2">Nom auteur2</label>
                    <?php
                        $str1= $donnArt[0]->getAuteur2_nom()."//".$donnArt[0]->getAuteur2_prenom()."//".$donnArt[0]->getAuteur2_institution();
                        $str2= $donnArt[0]->getAuteur2_nom()." - ".$donnArt[0]->getAuteur2_prenom()." - ".$donnArt[0]->getAuteur2_institution();
                        if (isset($_POST['nauteur2'])) { $str1=str_replace('"', '', $_POST['nauteur2'], $count); $str2 = str_replace("//", " - ", $str1, $count);}
                    ?>
                    <select class="form-control" id="nauteur2" name="nauteur2" value="" placeholder="">
                        <option value=<?='"'.$str1.'"'?>><?=$str2?></option>
                        <?php $i=1; foreach ($donnAut2 as $auteur) {
                            $str1= $auteur->getNom()."//".$auteur->getPrenom()."//".$auteur->getInstitution();
                            $str2= $auteur->getNom()." - ".$auteur->getPrenom()." - ".$auteur->getInstitution();
                        ?>
                            <option value=<?php echo '"'.$str1.'"';?>><?php echo $str2; $i++?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="col-sm-2 my-1">
                    <label for="nbpage">Nombre de pages</label>
                    <input type="number" class="form-control" id="nbpage" name="nbpage" 
                            placeholder="<?php if (isset($_POST['nbpage'])) { echo $_POST['nbpage']; } else { echo $donnArt[0]->getNbre_pages();}?>"
                            value="<?php if (isset($_POST['nbpage'])) {
                                                if ((int)$_POST['nbpage'] > 0) {
                                                    echo $_POST['nbpage'];
                                                } else { echo 1;}  
                                        } else { echo $donnArt[0]->getNbre_pages();}?>"
                    required>
                </div>
                <div class="col-sm-2">
                    <label for="prix">Prix en Euro</label>
                    <input type="number" class="form-control" id="prix" name="prix" 
                            placeholder="<?php if (isset($_POST['prix'])) { echo $_POST['prix']; } else { echo $donnArt[0]->getPrix();}?>"
                            value="<?php if (isset($_POST['prix'])) {
                                                if ((int)$_POST['prix'] >= 0) {
                                                    echo $_POST['prix'];
                                                } else { echo 0;}  
                                        } else { echo (int)$donnArt[0]->getPrix();}?>"
                            required>
                </div>

                <div class="container bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="modifart" name="modifart">Enregistrer</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauArt">Retour au tableau</a>
                </div>
            </div>
        </form>
</div>

<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




