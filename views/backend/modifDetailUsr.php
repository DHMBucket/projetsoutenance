<?php  $contenu = ob_start(); ?>
<link rel="stylesheet" href="./assets/css/telephone.css">
<script src="./assets/js/formulaireClient.js"></script>


    <div class="container text-center bg-warning" style="position:relative;  ">
        <br><br><br>   
        <h1 class="h2">Modification de données Gestionnaire</h1>
        <form action="./index.php?action=modifierUsr&modifierUsr" method="POST" enctype="multipart/form-data">
            <div class="form-row align-items-center">
                <div class="col-sm-1 my-1">
                    <label for="idl">Id User</label>
                    <input type="text" class="form-control" id="idl" name="idl" 
                           placeholder="<?=(int)$donnUsr[0]->getId_util()?>"
                           value="<?=(int)$donnUsr[0]->getId_util();?>"
                    readonly>
                </div>

                <div class="col-sm-4 my-1">
                    <label for="nom">Nom</label>
                    <input type="text" class="form-control" id="nom" name="nom" 
                           placeholder="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo $donnUsr[0]->getNom();}?>"
                           value="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo $donnUsr[0]->getNom();}?>"
                    required>
                </div>

                <div class="col-sm-4 my-1">
                    <label for="prenom">Prénom</label>
                    <input type="text" class="form-control" id="prenom" name="prenom" 
                           placeholder="<?php if (isset($_POST['prenom'])) { echo $_POST['prenom']; } else { echo $donnUsr[0]->getPrenom();}?>"
                           value="<?php if (isset($_POST['prenom'])) { echo $_POST['prenom']; } else { echo $donnUsr[0]->getPrenom();}?>"
                    required>
                </div>
                <div class="col-sm-2 m-0 p-0 text-center" >
                    <label for="rolsearch">Rôle</label>
                    <select class="form-control" id="rolsearch" name="rolsearch">
                        <option value="<?php if ($donnUsr[0]->getRole()==2) { echo 2;} else { echo 1;} ?>">
                            <?php if (isset($_POST['rolsearch'])) { if ($_POST['rolsearch'] == "1") { echo "1-Administrateur";} 
                                                                    else { echo "2-Gestionnaire";}
                            } else { if ($donnUsr[0]->getRole() == 2) { echo "2-Gestionnaire";} else { echo "1-Administrateur";} }
                            ?>        
                    </option>
                            <option value=1>1-Administrateur</option>
                            <?php if ($chg1nochg0 == 1) { ?>
                                <option value=2>2-Gestionnaire</option>
                            <?php } ?>
                    </select>
                </div>

            </div>


            <div class="form-row align-items-center">
                <div class="col-sm-4 my-1">
                    <label for="pseudo">Pseudo</label>
                    <input type="text" class="form-control" id="pseudo" name="pseudo" 
                           placeholder="<?php if (isset($_POST['pseudo'])) { echo $_POST['pseudo']; } else { echo $donnUsr[0]->getPseudo();}?>"
                           value="<?php if (isset($_POST['pseudo'])) { echo $_POST['pseudo']; } else { echo $donnUsr[0]->getPseudo();}?>"
                    required>
                </div>
               
                <div class="form-group col-sm-4 my-1">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control text-center" id="email" name="email" aria-describedby="emailHelp" 
                        placeholder="<?php if (isset($_POST['email'])) { echo $_POST['email']; } else { echo $donnUsr[0]->getEmail();}?>"
                        value="<?php if (isset($_POST['email'])) { echo $_POST['email']; } else { echo $donnUsr[0]->getEmail();}?>"  
                    required>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="datecreat">Date de création</label>
                    <input type="date" class="form-control" id="datecreat" name="datecreat" 
                           placeholder="<?php if (isset($_POST['datecreat'])) { echo $_POST['datecreat']; } else { echo $donnUsr[0]->getDate_created();}?>"
                           value="<?php if (isset($_POST['datecreat'])) { echo $_POST['datecreat']; } else { echo $donnUsr[0]->getDate_created();}?>"
                    required>
                </div>

            </div>
           
                <div class="container bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="modifusr" name="modifusr">Enregistrer</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauUsr">Retour au tableau</a>
                </div>
        </form>
</div>
<script> $('.countrypicker').countrypicker(); </script>
<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');



?>




