<?php  $contenu = ob_start(); 
if (!(isset($_SESSION['Auth']) && $_SESSION['Auth']['role'] == "1"  && $_SESSION['Auth']['operationnel'] == "1")) {
    header('location: ../page404.php');
}
?>

    <div class="container text-center" style="position:relative; background-color:lightgreen ">
        <br><br><br>   
        <h1 class="h2">Edition du livre ou revue (id = <?= $donnLvR[0]->getId_livre(); ?>)</h1>
        <form action="" method="GET" enctype="multipart/form-data">
            <div class="form-row align-items-center">
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php if ($donnLvR[0]->getNom_specif_revue() <> '') { echo 'Revue '.$donnLvR[0]->getNom_specif_revue();} else { echo 'Type : Livre';}?>"
                    value="<?php if ($donnLvR[0]->getNom_specif_revue() <> '') { echo 'Revue '.$donnLvR[0]->getNom_specif_revue();} else { echo 'Type : Livre';}?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Réf : '.$donnLvR[0]->getReference();?>"
                    value="<?php echo 'Réf : '.$donnLvR[0]->getReference();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Titre : '.$donnLvR[0]->getTitre();?>"
                    value="<?php echo 'Titre : '.$donnLvR[0]->getTitre();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Prix : '.$donnLvR[0]->getPrix()." ".$donnLvR[0]->getMonnaie();?>"
                    value="<?php echo 'Prix : '.$donnLvR[0]->getPrix()." ".$donnLvR[0]->getMonnaie();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Nbre Pages : '.$donnLvR[0]->getNbre_pages();?>"
                    value="<?php echo 'Nbre Pages : '.$donnLvR[0]->getNbre_pages();?>">
                </div>
                <div class="col-sm-2 my-1">
                <img src="<?='./assets/images/'.$donnLvR[0]->getImage1();?>" style="width:25%;" alt="<?=$donnLvR[0]->getImage1();?>">
                </div>
                <div class="col-sm-2 my-1">
                <img src="<?='./assets/images/'.$donnLvR[0]->getImage2();?>" style="width:25%;" alt="<?=$donnLvR[0]->getImage2();?>">
                </div>
                <div class="col-sm-2 my-1">
                <img src="<?='./assets/images/'.$donnLvR[0]->getImage3();?>" style="width:25%;" alt="<?=$donnLvR[0]->getImage3();?>">
                </div>
                <div class="col-sm-2 my-1">
                <img src="<?='./assets/images/'.$donnLvR[0]->getImage4();?>" style="width:25%;" alt="<?=$donnLvR[0]->getImage4();?>">
                </div>
                <div class="col-sm-2 my-1">
                <img src="<?='./assets/images/'.$donnLvR[0]->getImage5();?>" style="width:25%;" alt="<?=$donnLvR[0]->getImage5();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Ebook : '.$donnLvR[0]->getFichier_ebook();?>"
                    value="<?php echo 'Ebook : '.$donnLvR[0]->getFichier_ebook();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Auteur1 : '.$donnLvR[0]->getAuteur_princ1_nom()." ".$donnLvR[0]->getAuteur_princ1_prenom()." ".$donnLvR[0]->getAuteur_princ1_institution();?>"
                    value="<?php echo 'Auteur1 : '.$donnLvR[0]->getAuteur_princ1_nom()." ".$donnLvR[0]->getAuteur_princ1_prenom()." ".$donnLvR[0]->getAuteur_princ1_institution();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Auteur2 : '.$donnLvR[0]->getAuteur_princ2_nom()." ".$donnLvR[0]->getAuteur_princ2_prenom()." ".$donnLvR[0]->getAuteur_princ2_institution();?>"
                    value="<?php echo 'Auteur2 : '.$donnLvR[0]->getAuteur_princ2_nom()." ".$donnLvR[0]->getAuteur_princ2_prenom()." ".$donnLvR[0]->getAuteur_princ2_institution();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Rédacteur : '.$donnLvR[0]->getredac_chef_nom()." ".$donnLvR[0]->getredac_chef_prenom()." ".$donnLvR[0]->getredac_chef_institution();?>"
                    value="<?php echo 'Rédacteur : '.$donnLvR[0]->getredac_chef_nom()." ".$donnLvR[0]->getredac_chef_prenom()." ".$donnLvR[0]->getredac_chef_institution();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Editeur1 : '.$donnLvR[0]->getEditeur1();?>"
                    value="<?php echo 'Editeur1 : '.$donnLvR[0]->getEditeur1();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Editeur2 : '.$donnLvR[0]->getEditeur2();?>"
                    value="<?php echo 'Editeur2 : '.$donnLvR[0]->getEditeur2();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Format : '.$donnLvR[0]->getFormat_livre();?>"
                    value="<?php echo 'Format : '.$donnLvR[0]->getFormat_livre();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Parution: '.date("d-m-Y", strtotime($donnLvR[0]->getDate_parution()));?>"
                    value="<?php echo 'Parution: '.date("d-m-Y", strtotime($donnLvR[0]->getDate_parution()));?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Support : '.$donnLvR[0]->getType_support();?>"
                    value="<?php echo 'Support : '.$donnLvR[0]->getType_support();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Stock IR : '.$donnLvR[0]->getStock_iris();?>"
                    value="<?php echo 'Stock IR : '.$donnLvR[0]->getStock_iris();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Stock Edt : '.$donnLvR[0]->getStock_editeur();?>"
                    value="<?php echo 'Stock Edt : '.$donnLvR[0]->getStock_editeur();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Périodicité : '.$donnLvR[0]->getPeriodicite();?>"
                    value="<?php echo 'Périodicité : '.$donnLvR[0]->getPeriodicite();?>">
                </div>

                <div class="col-sm-12 my-1">
                    <div class="row my-1">
                        <textarea class="col-sm-12 my-1">
                            <?php echo 'Description : '.$donnLvR[0]->getDescription();?>
                        </textarea>
                    </div>
                </div>
                <div class="col-sm-12 my-1">
                    <div class="row my-1">
                        <textarea class="col-sm-12 my-1">
                            <?php echo 'Morceau choisi : '.$donnLvR[0]->getMorceau_choisi();?>
                        </textarea>
                    </div>
                </div>
                <div class="col-sm-12 my-1">
                    <div class="row my-1">
                        <textarea class="col-sm-12 my-1">
                            <?php echo 'Critique : '.$donnLvR[0]->getCritique();?>
                        </textarea>
                    </div>
                </div>
                <div class="col-sm-12 my-1">
                    <div class="row my-1">
                        <textarea class="col-sm-12 my-1">
                            <?php echo 'Consigne de l\'auteur : '.$donnLvR[0]->getConsign_auteur();?>
                        </textarea>
                    </div>
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'isbn : '.$donnLvR[0]->getIsbn();?>"
                    value="<?php echo 'isbn : '.$donnLvR[0]->getIsbn();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'doi : '.$donnLvR[0]->getDoi();?>"
                    value="<?php echo 'doi : '.$donnLvR[0]->getDoi();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'issn : '.$donnLvR[0]->getIssn();?>"
                    value="<?php echo 'issn : '.$donnLvR[0]->getIssn();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'issn_online : '.$donnLvR[0]->getIssn_online();?>"
                    value="<?php echo 'issn_online : '.$donnLvR[0]->getIssn_online();?>">
                </div>

                <div class="container bg-info text-center">
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauLvR">Retour au tableau</a>
                </div>
        </form>
</div>

<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




