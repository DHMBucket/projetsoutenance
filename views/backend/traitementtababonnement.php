<?php 
    $contenu= ob_start();
    require_once('./communs/connect.php');
    require_once('./controllers/backend/AbonnementController.php');

?>

<script type="text/javascript" src="./assets/js/affichetabselectionScript.js">
</script>


<br><br>
<h1 class="h2" style="text-align: center;caption-side: top; font-weight: bolder">Situation des abonnements clients</h1>
    <br>

<div id="navBoard" class="row border " style="margin: auto; align-items: center; justify-content: center; ">
    <div class="col-sm-3 m-0 p-0 border">
        <div class="container">
            <nav class="navbar navbar-light bg-light m-0 p-0 ">
                <form action="./index.php?action=search&search=searchTabAbn" class="form-inline" method="POST">
                    <input class="form-control mr-sm-2" type="search" id="sequence" name="sequence" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" id="seqrech" name="seqrech" type="submit">Recherche dans titre</button>
                </form>
            </nav>
        </div>
    </div>
    <?php
        
        if (isset($_SESSION['Auth']) && isset($_SESSION['Auth']['role']) && (int)$_SESSION['Auth']['role']<5) {
    ?>
    <div class="col-sm-9 m-0 p-0 border">
    
        
        <form action="./index.php?action=searchCrit&searchCrit=searchTabCritAbn" class="form-inline col" method="POST">
            <div class="container row border" style="margin: auto;"> 
                <div class="col-sm-1 m-0 p-0 text-center">
                    <label for="idsearch">par Id</label>
                    <select class="form-control" id="idsearch" name="idsearch">
                        <option value=0>0</option>
                        <?php $i=1; foreach ($idData as $idVal) {?>
                            <option value=<?php echo $idVal;?>><?php echo $idVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($idData as $idVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-4 m-0 p-0 text-center" >
                    <label for="nomsearch">nom</label>
                    <select class="form-control" id="nomsearch" name="nomsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($nomData as $nomVal) {?>
                            <option value=<?php echo '"'.$nomVal.'"';?>><?php echo $nomVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($nomData as $nomVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$nomVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-1 m-0 p-0 text-center">
                        <label for="idcsearch">Id client</label>
                        <select class="form-control" id="idcsearch" name="idcsearch">
                            <option value=0>0</option>
                            <?php $i=1; foreach ($idcData as $idcVal) {?>
                                <option value=<?php echo $idcVal;?>><?php echo $idcVal; $i++?></option>
                            <?php } ?>
                        </select>
                        <?php $i=1; foreach ($idcData as $idcVal) {?>
                            <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idcVal; $i++;?></p>
                        <?php } ?>
                    </div>
                    <div class="col-sm-2 m-0 p-0 text-center" >
                        <label for="tymbsearch">type membre</label>
                        <select class="form-control" id="tymbsearch" name="tymbsearch">
                            <option value=""></option>
                            <?php $i=1; foreach ($tymbData as $tymbVal) {?>
                                <option value=<?php echo '"'.$tymbVal.'"';?>><?php 
                                    if ($tymbVal == 5) {echo "Membre(gratuit)";}
                                    if ($tymbVal == 4) {echo "Adhérent";}
                                    if ($tymbVal == 3) {echo "Chercheur";}
                                    if ($tymbVal == 2) {echo "Gestionnaire";}
                                    if ($tymbVal == 1) {echo "Administrateur";}
                                    $i++;?>
                                </option>
                            <?php } ?>
                        </select>
                        <?php $i=1; foreach ($tymbData as $tymbVal) {?>
                            <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$tymbVal; $i++;?></p>
                        <?php } ?>
                    </div>

                    <div class="col-sm-2 m-0 p-0 text-center" >
                        <label for="sttsearch">statut client</label>
                        <select class="form-control" id="sttsearch" name="sttsearch">
                            <option value=""></option>
                            <?php $i=1; foreach ($sttData as $sttVal) {?>
                                <option value=<?php echo '"'.$sttVal.'"';?>><?php 
                                    if ($sttVal == 1) {echo "Individuel";}
                                    if ($sttVal == 2) {echo "Etudiant";}
                                    if ($sttVal == 3) {echo "Sans emploi";}
                                    if ($sttVal == 4) {echo "Institution";}
                                    $i++;?>
                                </option>
                            <?php } ?>
                        </select>
                        <?php $i=1; foreach ($sttData as $sttVal) {?>
                            <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$sttVal; $i++;?></p>
                        <?php } ?>
                    </div>

                    <div class="col-sm-2 m-0 p-0 bg-info text-center">
                        <button type="submit" class="btn btn-primary" id="filtrsearchAbn" name="filtrsearchAbn">Filtrage</button>
                        <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauAbn">Réinitialiser</a>
                    </div>
                </div>
            </form>
        
        
        </div>
        <?php
        }
        ?>

</div>

<table class="table table-striped table-bordered">
          
            <thead>
                <tr>
                    <th class="text-center">Numéro</th>
                    <th class="text-center">Nom de la revue</th>
                    <th class="text-center">Id Client</th>
                    <th class="text-center">Type membre</th>
                    <th class="text-center">Statut membre</th>
                    <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] == 1) { ?>
                        <th class="text-center">Actions</th><!--colspan="3"-->
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
            <?php
                    foreach ($dataA as $donAbn) {
                        
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $donAbn->getId_abonnement();?></td>
                        <td class="text-center"><?php echo $donAbn->getNom_specif_revue();?></td>
                        <td class="text-center"><?=$donAbn->getId_client();?></td>
                        <td class="text-center"><?php 
                            if ($donAbn->getType_client() == 5) {echo "Membre(gratuit)";}
                            if ($donAbn->getType_client() == 4) {echo "Adhérent";}
                            if ($donAbn->getType_client() == 3) {echo "Chercheur";}
                            if ($donAbn->getType_client() == 2) {echo "Gestionnaire";}
                            if ($donAbn->getType_client() == 1) {echo "Administrateur";}
                            ?>
                        </td>    
                        <td class="text-center"><?php 
                            if ($donAbn->getStatut_client() == 1) {echo "Individuel";}
                            if ($donAbn->getStatut_client() == 2) {echo "Etudiant";}
                            if ($donAbn->getStatut_client() == 3) {echo "Sans emploi";}
                            if ($donAbn->getStatut_client() == 4) {echo "Institution";}
                            ?>
                        </td>    


                        <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] == 1) { ?>
                        <td class="text-center">
                            <a href='./index.php?action=detail&id=<?=$donAbn->getId_abonnement();?>&detail=abonnement' class="btn btn-primary"><i class="fas fa-info"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                            <a href='./index.php?action=modifier&id=<?=$donAbn->getId_abonnement();?>&modifier=abonnement' class="btn btn-success" style="display: none; background-color: aqua; color: red; pointer-events: none; cursor: default;"><i class="fas fa-pen"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                            <a onclick="return confirm('Etes-vous sûr de vouloir supprimer cette ligne ?')" href="./index.php?action=supprimer&id=<?=$donAbn->getId_abonnement();?>&supprimer=abonnement" class="btn btn-danger"><i class="fas fa-trash"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                        </td>
                        <?php } ?>
                    </tr>
                    <?php }
            ?>

            </tbody>
        </table>

               
        <?php
$contenu= ob_get_clean();

?>