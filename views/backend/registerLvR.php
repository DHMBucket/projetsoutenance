<?php  $contenu = ob_start(); ?>
<link rel="stylesheet" href="./assets/css/telephone.css">
<script src="./assets/js/formulaireLivre.js"></script>


    <div class="container text-center bg-warning" style="position:relative;  ">
        <br><br><br>   
        <h1 class="h2">Formulaire d'ajout de livre ou revue</h1>
        <form action="./index.php?action=ajouterLvR&ajouterLvR" method="POST" enctype="multipart/form-data">
            <div class="form-row align-items-center">
                <div class="col-sm-2 my-1">
                    <label for="selectType">Type</label>
                    <select class="form-control" id="selectType" name="type" required>
                        <option value=1>1-Livre</option>
                        <option value=2>2-Revue</option>
                    </select>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="ref">Référence</label>
                    <input type="text" class="form-control" id="ref" name="ref" 
                           placeholder="<?php if (isset($_POST['ref'])) { echo $_POST['ref']; } else { echo "Entrer la référence";}?>"
                           value="<?php if (isset($_POST['ref'])) { echo $_POST['ref']; } else { echo "";}?>"
                           required>
                </div>

                <div class="col-sm-3 my-1">
                    <label id="nrv" for="nomrev">Nom de la revue (si requis)</label>
                    <select class="form-control" id="nomrev" name="nomrev" required>
                        <?php $i=1; foreach ($donnRev as $revue) { ?>
                            <option value=<?php echo '"'.$revue->getNom_specif_revue()."//".$revue->getPeriodicite().'"';?>><?php echo $revue->getNom_specif_revue(); $i++?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="col-sm-3 my-1">
                    <label for="titre">Titre</label>
                    <input type="text" class="form-control" id="titre" name="titre" 
                           placeholder="<?php if (isset($_POST['titre'])) { echo $_POST['titre']; } else { echo "Entrer le titre";}?>"
                           value="<?php if (isset($_POST['titre'])) { echo $_POST['titre']; } else { echo "";}?>"
                           required>
                </div>

                <div class="col-sm-2 my-1">
                    <label for="prix">Prix en Euro</label>
                    <input type="number" class="form-control" id="prix" name="prix" 
                            placeholder="<?php if (isset($_POST['prix'])) { echo $_POST['prix']; } else { echo "Entrer prix";}?>"
                            value="<?php if (isset($_POST['prix'])) {
                                                if ((int)$_POST['prix'] >= 0) {
                                                    echo $_POST['prix'];
                                                } else { echo 0;}  
                                        } else { echo 0;}?>"
                            required>
                </div>

                <div class="offset-1 col-sm-2">
                    <input type="hidden" name="max_file_size" value="1000000000" />
                    <input type="file" class="form-control" id="image1" name="image1"
                    placeholder="<?php if (isset($_POST['image1'])) { echo $_POST['image1']; } else { echo "Entrer votre fichier image ( .png, .jpg, .bmp )";}?>"
                    value="<?php if (isset($_POST['image1'])) { echo $_POST['image1']; } else { echo "";}?>">
                </div>

                <div class="col-sm-2">
                    <input type="hidden" name="max_file_size" value="1000000000" />
                    <input type="file" class="form-control" id="image2" name="image2"
                    placeholder="<?php if (isset($_POST['image2'])) { echo $_POST['image2']; } else { echo "Entrer votre fichier image ( .png, .jpg, .bmp )";}?>"
                    value="<?php if (isset($_POST['image2'])) { echo $_POST['image2']; } else { echo "";}?>">
                </div>

                <div class="col-sm-2">
                    <input type="hidden" name="max_file_size" value="1000000000" />
                    <input type="file" class="form-control" id="image3" name="image3"
                    placeholder="<?php if (isset($_POST['image3'])) { echo $_POST['image3']; } else { echo "Entrer votre fichier image ( .png, .jpg, .bmp )";}?>"
                    value="<?php if (isset($_POST['image3'])) { echo $_POST['image3']; } else { echo "";}?>">
                </div>

                <div class="col-sm-2">
                    <input type="hidden" name="max_file_size" value="1000000000" />
                    <input type="file" class="form-control" id="image4" name="image4"
                    placeholder="<?php if (isset($_POST['image4'])) { echo $_POST['image4']; } else { echo "Entrer votre fichier image ( .png, .jpg, .bmp )";}?>"
                    value="<?php if (isset($_POST['image4'])) { echo $_POST['image4']; } else { echo "";}?>">
                </div>

                <div class="col-sm-2">
                    <input type="hidden" name="max_file_size" value="1000000000" />
                    <input type="file" class="form-control" id="image5" name="image5"
                    placeholder="<?php if (isset($_POST['image5'])) { echo $_POST['image5']; } else { echo "Entrer votre fichier image ( .png, .jpg, .bmp )";}?>"
                    value="<?php if (isset($_POST['image5'])) { echo $_POST['image5']; } else { echo "";}?>">
                </div>
                <div class="col-sm-12">
                    <small id="imgHelp" class="form-text text-muted">Les fichiers acceptés sont de type .png, .bmp ou .jpg</small>                    
                </div>

                <div class="col-sm-4 my-1">
                    <label for="nauteur1">Nom auteur1</label>
                    <select class="form-control" id="nauteur1" name="nauteur1" required>
                        <?php $i=1; foreach ($donnAut1 as $auteur) { 
                            $str1= $auteur->getNom()." - ".$auteur->getPrenom()." - ".$auteur->getInstitution();
                            $str = str_replace("$", " ", $str1, $count);$str1=$str;
                            ?>
                            <option value=<?php echo $auteur->getNom()."//".$auteur->getPrenom()."//".$auteur->getInstitution();?>><?php echo $str1; $i++?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="col-sm-4 my-1">
                    <label for="nauteur2">Nom auteur2</label>
                    <select class="form-control" id="nauteur2" name="nauteur2" value="" placeholder="">
                        <?php $i=1; foreach ($donnAut2 as $auteur) {
                            $str1= $auteur->getNom()." - ".$auteur->getPrenom()." - ".$auteur->getInstitution();
                            $str = str_replace("$", " ", $str1, $count);$str1=$str;
                            ?>
                            <option value=<?php echo $auteur->getNom()."//".$auteur->getPrenom()."//".$auteur->getInstitution();?>><?php echo $str1; $i++?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="col-sm-4 my-1">
                    <label for="nredac">Nom redacteur</label>
                    <select class="form-control" id="nredac" name="nredac" value="" placeholder="">
                        <?php $i=1; foreach ($donnAut2 as $auteur) { 
                            $str1= $auteur->getNom()." - ".$auteur->getPrenom()." - ".$auteur->getInstitution();
                            $str = str_replace("$", " ", $str1, $count);$str1=$str;
                            ?>
                            <option value=<?php echo $auteur->getNom()."//".$auteur->getPrenom()."//".$auteur->getInstitution();?>><?php echo $str1; $i++?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="col-sm-3 my-1">
                    <label for="edit1">Nom éditeur1</label>
                    <select class="form-control" id="edit1" name="edit1" required>
                        <?php $i=1; foreach ($donnEdt1 as $editeur) { 
                            $str1= $editeur->getNom_editeur();
                            $str = str_replace("$", " ", $str1, $count);$str1=$str;
                            ?>
                            <option value=<?php echo $editeur->getNom_editeur();?>><?php echo $str1; $i++?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="col-sm-3 my-1">
                    <label for="edit2">Nom éditeur2</label>
                    <select class="form-control" id="edit2" name="edit2" value="" placeholder="">
                        <?php $i=1; foreach ($donnEdt2 as $editeur) { 
                           $str1= $editeur->getNom_editeur();
                           $str = str_replace("$", " ", $str1, $count);$str1=$str;
                            ?>
                            <option value=<?php echo $editeur->getNom_editeur();?>><?php echo $str1; $i++?></option>
                        <?php } ?>
                    </select>
                </div>


                <div class="col-sm-2 my-1">
                    <label for="dateparut">Date de parution</label>
                    <input type="date" class="form-control" id="dateparut" name="dateparut" 
                           placeholder="<?php if (isset($_POST['dateparut'])) { echo $_POST['dateparut']; } else { echo "";}?>"
                           value="<?php if (isset($_POST['dateparut'])) { echo $_POST['dateparut']; } else { echo date("Y-m-d");}?>">
                </div>

                <div class="col-sm-2 my-1">
                    <label for="nbpage">Nombre de pages</label>
                    <input type="number" class="form-control" id="nbpage" name="nbpage" 
                            placeholder="<?php if (isset($_POST['nbpage'])) { echo $_POST['nbpage']; } else { echo 1;}?>"
                            value="<?php if (isset($_POST['nbpage'])) {
                                                if ((int)$_POST['nbpage'] > 0) {
                                                    echo $_POST['nbpage'];
                                                } else { echo 1;}  
                                        } else { echo 1;}?>"
                            required>
                </div>
                <div class="col-sm-6 my-1">
                    <label for="descript">Description</label>
                    <input type="text" class="form-control" id="descript" name="descript" 
                           placeholder="<?php if (isset($_POST['descript'])) { echo $_POST['descript']; } else { echo "Entrer votre description";}?>"
                           value="<?php if (isset($_POST['descript'])) { echo $_POST['descript']; } else { echo "";}?>"
                           required>
                </div>
                <div class="col-sm-6 my-1">
                    <label for="mchoisi">Morceau choisi</label>
                    <input type="text" class="form-control" id="mchoisi" name="mchoisi" 
                           placeholder="<?php if (isset($_POST['mchoisi'])) { echo $_POST['mchoisi']; } else { echo "Entrer votre morceau choisi";}?>"
                           value="<?php if (isset($_POST['mchoisi'])) { echo $_POST['mchoisi']; } else { echo "";}?>"
                           required>
                </div>
                <div class="col-sm-6 my-1">
                    <label for="critique">Critique</label>
                    <input type="text" class="form-control" id="critique" name="critique" 
                           placeholder="<?php if (isset($_POST['critique'])) { echo $_POST['critique']; } else { echo "Texte de la critique";}?>"
                           value="<?php if (isset($_POST['critique'])) { echo $_POST['critique']; } else { echo "";}?>"
                           required>
                </div>
                <div class="col-sm-6 my-1">
                    <label for="csgnaut">Consigne auteur</label>
                    <input type="text" class="form-control" id="csgnaut" name="csgnaut" 
                           placeholder="<?php if (isset($_POST['csgnaut'])) { echo $_POST['csgnaut']; } else { echo "Entrer la consigne auteur";}?>"
                           value="<?php if (isset($_POST['csgnaut'])) { echo $_POST['csgnaut']; } else { echo "";}?>"
                           required>
                </div>

                <div class="col-sm-4 my-1">
                    <label id="filelab" for="ficjustif">Fichier ebook</label>
                    <input type="hidden" name="max_file_size" value="1000000000" />
                    <input type="file" class="form-control" id="ficjustif" name="ficjustif"
                    placeholder="<?php if (isset($_POST['ficjustif'])) { echo $_POST['ficjustif']; } else { echo "Entrer votre fichier justificatif ( .pdf, .docx, .jpg )";}?>"
                    value="<?php if (isset($_POST['ficjustif'])) { echo $_POST['ficjustif']; } else { echo "";}?>">
                    <small id="fileHelp" class="form-text text-muted">Les fichiers acceptés sont de type .pdf, .docx ou .jpg</small>
                </div>

                <div class="col-sm-2 my-1">
                    <label for="selectFrmt">Format</label>
                    <select class="form-control" id="selectFrmt" name="format" required>
                        <option value="Normal">1-Normal</option>
                        <option value="Poche">2-Poche</option>
                    </select>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="selectSprt">Support</label>
                    <select class="form-control" id="selectSprt" name="support" required>
                        <option value="Papier">1-Papier</option>
                        <option value="Ebook">2-Ebook</option>
                    </select>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="stkirs">Stock IRIS</label>
                    <input type="number" class="form-control" id="stkirs" name="stkirs" 
                            placeholder="<?php if (isset($_POST['stkirs'])) { echo $_POST['stkirs']; } else { echo "Stock Iris";}?>"
                            value="<?php if (isset($_POST['stkirs'])) {
                                                if ((int)$_POST['stkirs'] >= 0) {
                                                    echo (int)$_POST['stkirs'];
                                                } else { echo 0;}  
                                        } else { echo 0;}?>"
                            required>
                </div>
                
                <div class="col-sm-2 my-1">
                    <label for="stkedt">Stock Editeur</label>
                    <input type="number" class="form-control" id="stkedt" name="stkedt" 
                            placeholder="<?php if (isset($_POST['stkedt'])) { echo $_POST['stkedt']; } else { echo "stock éditeur";}?>"
                            value="<?php if (isset($_POST['stkedt'])) {
                                                if ((int)$_POST['stkedt'] >= 0) {
                                                    echo (int)$_POST['stkedt'];
                                                } else { echo 0;}  
                                        } else { echo 0;}?>"
                            required>
                </div>


                <div class="col-sm-3 my-1">
                    <label for="isbn">isbn</label>
                    <input type="text" class="form-control" id="isbn" name="isbn" 
                           placeholder="<?php if (isset($_POST['isbn'])) { echo $_POST['isbn']; } else { echo "isbn";}?>"
                           value="<?php if (isset($_POST['isbn'])) { echo $_POST['isbn']; } else { echo "";}?>"
                           >
                </div>
 
                <div class="col-sm-3 my-1">
                    <label for="doi">doi</label>
                    <input type="text" class="form-control" id="doi" name="doi" 
                           placeholder="<?php if (isset($_POST['doi'])) { echo $_POST['doi']; } else { echo "doi";}?>"
                           value="<?php if (isset($_POST['doi'])) { echo $_POST['doi']; } else { echo "";}?>"
                           >
                </div>
 
                <div class="col-sm-3 my-1">
                    <label for="issn">issn</label>
                    <input type="text" class="form-control" id="issn" name="issn" 
                           placeholder="<?php if (isset($_POST['issn'])) { echo $_POST['issn']; } else { echo "issn";}?>"
                           value="<?php if (isset($_POST['issn'])) { echo $_POST['issn']; } else { echo "";}?>"
                           >
                </div>
 
                <div class="col-sm-3 my-1">
                    <label for="issn_online">issn_online</label>
                    <input type="text" class="form-control" id="issn_online" name="issn_online" 
                           placeholder="<?php if (isset($_POST['issn_online'])) { echo $_POST['issn_online']; } else { echo "issn_online";}?>"
                           value="<?php if (isset($_POST['issn_online'])) { echo $_POST['issn_online']; } else { echo "";}?>"
                           >
                </div>
 
                <div class="container bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="ajoutlvrv" name="ajoutlvrv">Enregistrer</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauLvR">Retour au tableau</a>
                </div>
        </form>
</div>
<script> $('.countrypicker').countrypicker(); </script>
<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




