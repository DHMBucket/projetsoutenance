<?php 
    $contenu= ob_start();
    require_once('./communs/connect.php');
    require_once('./controllers/backend/ClientMembreController.php');

?>

<script type="text/javascript" src="./assets/js/affichetabselectionScript.js">
</script>


<br><br>
<h1 class="h2" style="text-align: center;caption-side: top; font-weight: bolder">Situation des membres</h1>
    <br>

<div id="navBoard" class="row border " style="margin: auto; align-items: center; justify-content: center; ">
    <div class="col-sm-3 m-0 p-0 border">
        <div class="container">
            <nav class="navbar navbar-light bg-light m-0 p-0 ">
                <form action="./index.php?action=search&search=searchTabMbr" class="form-inline" method="POST">
                    <input class="form-control mr-sm-2" type="search" id="sequence" name="sequence" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" id="seqrech" name="seqrech" type="submit">Recherche dans titre</button>
                </form>
            </nav>
        </div>
        <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role']<3) {?>
        <div class="container">
            <form action='./index.php?action=register&register=client' method="post">
                <button class="btn btn-info" type="submit" name="ajoutmbr" id="ajoutmbr">Ajouter un membre</button>
            </form>    
        </div>
        <?php } ?>
    </div>
    <?php
        
        if (isset($_SESSION['Auth']) && isset($_SESSION['Auth']['role']) && (int)$_SESSION['Auth']['role']<5) {
    ?>
    <div class="col-sm-9 m-0 p-0 border">
    
        
            <form action="./index.php?action=searchCrit&searchCrit=searchTabCritMbr" class="form-inline col" method="POST">
            <div class="container row border" style="margin: auto;"> 
                <div class="col-sm-1 m-0 p-0 text-center">
                    <label for="idsearch">par Id</label>
                    <select class="form-control" id="idsearch" name="idsearch">
                        <option value=0>0</option>
                        <?php $i=1; foreach ($idData as $idVal) {?>
                            <option value=<?php echo $idVal;?>><?php echo $idVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($idData as $idVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-4 m-0 p-0 text-center" >
                    <label for="nomsearch">nom</label>
                    <select class="form-control" id="nomsearch" name="nomsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($nomData as $nomVal) {?>
                            <option value=<?php echo '"'.$nomVal.'"';?>><?php echo $nomVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($nomData as $nomVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$nomVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-3 m-0 p-0 text-center" hidden>
                    <label for="ddebsearch">date inscrip.</label>
                    <select class="form-control" id="ddebsearch" name="ddebsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($ddebData as $ddebVal) {?>
                            <option value=<?php echo $ddebVal;?>><?php echo $ddebVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($ddebData as $ddebVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$ddebVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-4 m-0 p-0 text-center" >
                    <label for="emlsearch">email</label>
                    <select class="form-control" id="emlsearch" name="emlsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($emlData as $emlVal) {?>
                            <option value=<?php echo '"'.$emlVal.'"';?>><?php echo $emlVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($emlData as $emlVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$emlVal; $i++;?></p>
                    <?php } ?>
                </div>
                </div>
                <br>
                <div class="container row border" style="margin: auto;"> 
                <div class="col-sm-4 m-0 p-0 text-center" >
                    <label for="locsearch">localisation</label>
                    <select class="form-control" id="locsearch" name="locsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($locData as $locVal) {?>
                            <option value=<?php echo '"'.$locVal.'"';?>><?php echo $locVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($locData as $locVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$locVal; $i++;?></p>
                    <?php } ?>
                </div>


                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="tymbsearch">type membre</label>
                    <select class="form-control" id="tymbsearch" name="tymbsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($tymbData as $tymbVal) {?>
                            <option value=<?php echo '"'.$tymbVal.'"';?>><?php 
                                if ($tymbVal == 5) {echo "Membre(gratuit)";}
                                if ($tymbVal == 4) {echo "Adhérent";}
                                if ($tymbVal == 3) {echo "Chercheur";}
                                $i++;?>
                            </option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($tymbData as $tymbVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$tymbVal; $i++;?></p>
                    <?php } ?>
                </div>

                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="sttsearch">statut client</label>
                    <select class="form-control" id="sttsearch" name="sttsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($sttData as $sttVal) {?>
                            <option value=<?php echo '"'.$sttVal.'"';?>><?php 
                                if ($sttVal == 1) {echo "Individuel";}
                                if ($sttVal == 2) {echo "Etudiant";}
                                if ($sttVal == 3) {echo "Sans emploi";}
                                if ($sttVal == 4) {echo "Institution";}
                                $i++;?>
                            </option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($sttData as $sttVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$sttVal; $i++;?></p>
                    <?php } ?>
                </div>

                <div class="col-sm-2 m-0 p-0 bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="filtrsearchMbr" name="filtrsearchMbr">Filtrage</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauMbr">Réinitialiser</a>
                </div>
                </div>
            </form>
        
        
        </div>
        <?php
        }
        ?>

</div>


<table class="table table-striped table-bordered">
          
            <thead>
                <tr>
                    <th class="text-center">Numéro</th>
                    <th class="text-center">Type + statut</th>
                    <th class="text-center">Nom + Prénom + Instit + Fction</th>
                    <th class="text-center">Justificatif</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Offres</th>
                    <th class="text-center">Tarif</th>
                    <th class="text-center">Date d'inscription</th>
                    <th class="text-center">Adresse + téléphone</th>
                    <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] == 1) { ?>
                        <th class="text-center">Actions</th><!--colspan="3"-->
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
            <?php
                    foreach ($dataA as $donMbr) {
                        
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $donMbr->getId_clt_membre();?></td>
                        <td class="text-center"><?php 
                        if ($donMbr->getType_membre() == 5) {echo "Membre(gratuit) / ";}
                        if ($donMbr->getType_membre() == 4) {echo "Adhérent / ";}
                        if ($donMbr->getType_membre() == 3) {echo "Chercheur / ";}
                        if ($donMbr->getStatut_client() == 1) {echo "Individuel";}
                        if ($donMbr->getStatut_client() == 2) {echo "Etudiant";}
                        if ($donMbr->getStatut_client() == 3) {echo "Sans emploi";}
                        if ($donMbr->getStatut_client() == 4) {echo "Institution";}
                        ?>
                    </td>    
                        <td class="text-center"><?php 
                            if ($donMbr->getCivilite() == 1) {echo "Monsieur ";}
                            if ($donMbr->getCivilite() == 2) {echo "Madame ";}
                            echo $donMbr->getNom_membre()." ".$donMbr->getPrenom_membre()." ".$donMbr->getInstitution()." ".$donMbr->getFonction();?></td>
                        <td class="text-center"><?=$donMbr->getJustif_etud_ou_ssemploi_instit();?></td>
                        <td class="text-center"><?php echo $donMbr->getEmail();?></td>

                        <td class="text-center"><?php 
                            if ($donMbr->getOffre_partenaire() == 1) echo "partenaire ";
                            if ($donMbr->getOffre_newsletter() == 1) echo "newsletter";
                            ?>
                        </td>
                        <td class="text-center"><?php echo $donMbr->getPrix_membre()." ".$donMbr->getMonnaie();?></td>
                        <td class="text-center"><?php echo $donMbr->getDate_debut();?></td>
                        <td class="text-center"><?php echo $donMbr->getAdr_livrais().", ".$donMbr->getCode_postal().", ".$donMbr->getVille().", ".$donMbr->getPays()."(".$donMbr->getZip_etat()."), Tél : (".$donMbr->getIndicatif().")".$donMbr->getTelephone();?></td>

                        <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] == 1) { ?>
                        <td class="text-center">
                            <a href='./index.php?action=detail&id=<?=$donMbr->getId_clt_membre();?>&detail=cltmbr' class="btn btn-primary"><i class="fas fa-info"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                            <a href='./index.php?action=modifier&id=<?=$donMbr->getId_clt_membre();?>&modifier=cltmbr' class="btn btn-success"><i class="fas fa-pen"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                            <a onclick="return confirm('Etes-vous sûr de vouloir supprimer cette ligne ?')" href="./index.php?action=supprimer&id=<?=$donMbr->getId_clt_membre();?>&supprimer=cltmbr" class="btn btn-danger"><i class="fas fa-trash"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                        </td>
                        <?php } ?>
                    </tr>
                    <?php }
            ?>

            </tbody>
        </table>
               
        <?php
$contenu= ob_get_clean();

?>