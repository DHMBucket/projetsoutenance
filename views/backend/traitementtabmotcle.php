<?php 
    $contenu= ob_start();
    require_once('./communs/connect.php');
    require_once('./controllers/backend/MotCleController.php');

?>

<script type="text/javascript" src="./assets/js/affichetabselectionScript.js">
</script>


<br><br>
<h1 class="h2" style="text-align: center;caption-side: top; font-weight: bolder">Situation des Mots-clés</h1>
    <br>

<div id="navBoard" class="row border " style="margin: auto; align-items: center; justify-content: center; ">
    <div class="col-sm-4 m-0 p-0 border">
        <div class="container">
            <nav class="navbar navbar-light bg-light m-0 p-0 ">
                <form action="./index.php?action=search&search=searchTabMcl" class="form-inline" method="POST">
                    <input class="form-control mr-sm-2" type="search" id="sequence" name="sequence" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" id="seqrech" name="seqrech" type="submit">Recherche dans mot</button>
                </form>
            </nav>
        </div>
        <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role']<3) {?>
        <div class="container">
            <form action='./index.php?action=register&register=motcle' method="post">
                <button class="btn btn-info" type="submit" name="ajoutMcl" id="ajoutMcl">Ajouter un mot-clé</button>
            </form>    
        </div>
        <?php } ?>
    </div>
    <?php
        
        if (isset($_SESSION['Auth']) && isset($_SESSION['Auth']['role']) && (int)$_SESSION['Auth']['role']<5) {
    ?>
    <div class="col-sm-8 m-0 p-0 border">
        
        <form action="./index.php?action=searchCrit&searchCrit=searchTabCritMcl" class="form-inline col" method="POST">
            <div class="container row border" style="margin: auto;">               
                <div class="col-sm-2 m-0 p-0 text-center" >
                    <label for="idsearch">par Id</label>
                    <select class="form-control" id="idsearch" name="idsearch">
                        <option value=0>0</option>
                        <?php $i=1; foreach ($idData as $idVal) {?>
                            <option value=<?php echo $idVal;?>><?php echo $idVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($idData as $idVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-4 m-0 p-0 text-center" >
                    <label for="nomsearch">Mot-clé</label>
                    <select class="form-control" id="nomsearch" name="nomsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($nomData as $nomVal) {?>
                            <option value=<?php echo '"'.$nomVal.'"';?>><?php echo $nomVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($nomData as $nomVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$nomVal; $i++;?></p>
                    <?php } ?>
                </div>
    
                <div class="col-sm-2 m-0 p-0 text-center" >
                    <label for="idlsearch">Id Livre</label>
                    <select class="form-control" id="idlsearch" name="idlsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($idlData as $idlVal) {?>
                            <option value=<?php echo $idlVal;?>><?php echo $idlVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($idlData as $idlVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idlVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-2 m-0 p-0 text-center" >
                    <label for="idasearch">Id article</label>
                    <select class="form-control" id="idasearch" name="idasearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($idaData as $idaVal) {?>
                            <option value=<?php echo $idaVal;?>><?php echo $idaVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($idaData as $idaVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idaVal; $i++;?></p>
                    <?php } ?>
                </div>
                

                <div class="col-sm-2 m-0 p-0 bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="filtrsearchMcl" name="filtrsearchMcl">Filtrage</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauMcl">Réinitialiser</a>
                </div>

            </div>
        </form>
        
        </div>
        <?php
        }
        ?>

</div>


<table class="table table-striped table-bordered">
          
            <thead>
                <tr>

                    <th class="text-center">Numéro</th>
                    <th class="text-center">Mot-clé</th>
                    <th class="text-center">Id livre</th>
                    <th class="text-center">Id Article</th>
                    <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] == 1) { ?>
                        <th class="text-center">Actions</th><!--colspan="3"-->
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
            <?php
                foreach ($dataA as $donMcl) {
                    
                    
                ?>
                    <tr>
                        <td class="text-center"><?php echo $donMcl->getId_motcle();?></td>
                        <td class="text-center"><?php echo $donMcl->getNom_motcle();?></td>

                        <td class="text-center"><?php 
                            if ($donMcl->getId_livre() > 0) {echo $donMcl->getId_livre();}
                            ?>
                        </td>
                        <td class="text-center"><?php 
                            if ($donMcl->getId_artcl() > 0) {echo $donMcl->getId_artcl();}
                            ?>
                        </td>

                        <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] == 1) { ?>
                        <td class="text-center">
                            <a href='./index.php?action=detail&id=<?=$donMcl->getId_motcle();?>&detail=motcle' class="btn btn-primary" style="display: none; background-color: aqua; color: red; pointer-events: none; cursor: default;"><i class="fas fa-info"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                            <a href='./index.php?action=modifier&id=<?=$donMcl->getId_motcle();?>&modifier=motcle' class="btn btn-success" style="display: none; background-color: aqua; color: red; pointer-events: none; cursor: default;"><i class="fas fa-pen"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                            <a onclick="return confirm('Etes-vous sûr de vouloir supprimer cette ligne ?')" href="./index.php?action=supprimer&id=<?=$donMcl->getId_motcle();?>&supprimer=motcle" class="btn btn-danger"><i class="fas fa-trash"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                        </td>
                        <?php } ?>
                    </tr>
                    <?php }
            ?>

            </tbody>
        </table>

               
        <?php
$contenu= ob_get_clean();

?>