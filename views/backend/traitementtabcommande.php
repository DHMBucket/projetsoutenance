<?php 
    $contenu= ob_start();
    require_once('./communs/connect.php');
    require_once('./controllers/backend/CommandeController.php');

?>

<script type="text/javascript" src="./assets/js/affichetabselectionScript.js">
</script>


<br><br>
<h1 class="h2" style="text-align: center;caption-side: top; font-weight: bolder">Situation des Commandes</h1>
    <br>

<div id="navBoard" class="row border " style="margin: auto; align-items: center; justify-content: center; ">
    <div class="col-sm-4 m-0 p-0 border">
        <div class="container">
            <nav class="navbar navbar-light bg-light m-0 p-0 ">
                <form action="./index.php?action=search&search=searchTabCmd" class="form-inline" method="POST">
                    <input class="form-control mr-sm-2" type="search" id="sequence" name="sequence" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" id="seqrech" name="seqrech" type="submit">Recherche dans titre</button>
                </form>
            </nav>
        </div>
    </div>
    <?php

        if (isset($_SESSION['Auth']) && isset($_SESSION['Auth']['role']) && (int)$_SESSION['Auth']['role']<5) {
    ?>
    <div class="col-sm-8 m-0 p-0 border">
        
        <form action="./index.php?action=searchCrit&searchCrit=searchTabCritCmd" class="form-inline col" method="POST">
            <div class="container row border" style="margin: auto;">               
                <div class="col-sm-1 m-0 p-0 text-center" >
                    <label for="idsearch">par Id</label>
                    <select class="form-control" id="idsearch" name="idsearch">
                        <option value=0>0</option>
                        <?php $i=1; foreach ($idData as $idVal) {?>
                            <option value=<?php echo $idVal;?>><?php echo $idVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($idData as $idVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-2 m-0 p-0 text-center" >
                    <label for="refsearch">référence</label>
                    <select class="form-control" id="refsearch" name="refsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($refData as $refVal) {?>
                            <option value=<?php echo $refVal;?>><?php echo $refVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($refData as $refVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$refVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="trscsearch">réf. transact</label>
                    <select class="form-control" id="trscsearch" name="trscsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($trscData as $trscVal) {?>
                            <option value=<?php echo $trscVal;?>><?php echo $trscVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($trscData as $trscVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$trscVal; $i++;?></p>
                    <?php } ?>
                </div>

                <div class="col-sm-2 m-0 p-0 text-center" >
                    <label for="typsearch">client ?</label>
                    <select class="form-control" id="typsearch" name="typsearch">
                        <option value="-1"></option>
                        <?php $i=1; foreach ($typData as $typVal) {?>
                            <option value=<?php echo $typVal;?>><?php 
                                if ($typVal == "0") {echo $typVal."- non inscrit";}
                                if ($typVal == "1") {echo $typVal."- administr.";}
                                if ($typVal == "2") {echo $typVal."- Gestionn.";}
                                if ($typVal == "3") {echo $typVal."- Chercheur";}
                                if ($typVal == "4") {echo $typVal."- Adhérent";}
                                if ($typVal == "5") {echo $typVal."- Membre";}
                                 
                                 $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($typData as $typVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$typVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-2 m-0 p-0 text-center" >
                    <label for="idcsearch">Id client</label>
                    <select class="form-control" id="idcsearch" name="idcsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($idcData as $idcVal) {?>
                            <option value=<?php echo $idcVal;?>><?php echo $idcVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($idcData as $idcVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idcVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-2 m-0 p-0 text-center" >
                    <label for="locsearch">Pays</label>
                    <select class="form-control" id="locsearch" name="locsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($locData as $locVal) {
                            $str1= $locVal;
                            $str = str_replace(" ", "$", $str1, $count);$str1=$str;
                            ?>
                            <option value=<?php echo $str1;?>><?php echo $locVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($locData as $locVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$locVal; $i++;?></p>
                    <?php } ?>
                </div>

                </div>
                <div class="container row border" style="margin: auto;">               
                <div class="col-sm-1 m-0 p-0 text-center" >
                    <label for="idlsearch">Id Livre</label>
                    <select class="form-control" id="idlsearch" name="idlsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($idlData as $idlVal) {?>
                            <option value=<?php echo $idlVal;?>><?php echo $idlVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($idlData as $idlVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idlVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="rflsearch">Réf. Livre</label>
                    <select class="form-control" id="rflsearch" name="rflsearch">
                        <option value="-1"></option>
                        <?php $i=1; foreach ($rflData as $rflVal) {?>
                            <option value=<?php echo $rflVal;?>><?php echo $rflVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($rflData as $rflVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$rflVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-1 m-0 p-0 text-center" >
                    <label for="idasearch">Id article</label>
                    <select class="form-control" id="idasearch" name="idasearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($idaData as $idaVal) {?>
                            <option value=<?php echo $idaVal;?>><?php echo $idaVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($idaData as $idaVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idaVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="ttrsearch">Titre</label>
                    <select class="form-control" id="ttrsearch" name="ttrsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($ttrData as $ttrVal) {
                             $str1= $ttrVal;
                             $str = str_replace(" ", "$", $str1, $count);$str1=$str;
                            ?>
                            <option value=<?php echo $str1;?>><?php echo $ttrVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($ttrData as $ttrVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$ttrVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-2 m-0 p-0 text-center" >
                    <label for="modsearch">Mode pay</label>
                    <select class="form-control" id="modsearch" name="modsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($modData as $modVal) {?>
                            <option value=<?php echo $modVal;?>><?php 
                                if ($modVal == 1) {echo $modVal."- Carte";} else {echo $modVal."- Chèque";}
                                $i++;?>
                            </option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($modData as $modVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$modVal; $i++;?></p>
                    <?php } ?>
                </div>

                <div class="col-sm-2 m-0 p-0 bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="filtrsearchCmd" name="filtrsearchCmd">Filtrage</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauCmd">Réinitialiser</a>
                </div>

            </div>
        </form>
        
        </div>
        <?php
        }
        ?>

</div>


<table class="table table-striped table-bordered">
          
            <thead>
                <tr>

                    <th class="text-center">Id & référence</th>
                    <th class="text-center">Type & Id Client</th>
                    <th class="text-center">Id + Réf + Titre livre/Article</th>
                    <th class="text-center">Nombre</th>
                    <th class="text-center">Prix ttc + frs livr. + monnaie</th>
                    <th class="text-center">Mode de payment + Réf transac.</th>
                    <th class="text-center">date ordre livr.</th>
                    <th class="text-center">nom + prenom + instit.</th>
                    <th class="text-center">Adresse & téléphone</th>
                    <th class="text-center">mode livr. + message clt</th>
                    <th class="text-center">date livr. prévue + réalisée</th>
                    <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] == 1) { ?>
                        <th class="text-center">Actions</th><!--colspan="3"-->
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
            <?php
                foreach ($dataA as $donCmd) {
                    
                ?>
                    <tr>
                        <td class="text-center"><?php echo $donCmd->getId_commande()." / ".$donCmd->getRef_commande();?></td>
                        <td class="text-center"><?php 
                            if ($donCmd->getType_client() == 0 || $donCmd->getType_client() == 6) {echo "Non inscrit / ";}
                            if ($donCmd->getType_client() == 1) {echo "Administr. / ";}
                            if ($donCmd->getType_client() == 2) {echo "Gestionn. / ";}
                            if ($donCmd->getType_client() == 3) {echo "Chercheur / ";}
                            if ($donCmd->getType_client() == 4) {echo "Adhérent / ";}
                            if ($donCmd->getType_client() == 5) {echo "Membre / ";}
                            
                            echo $donCmd->getId_client();
                            ?>
                        </td>
                        <td class="text-center"><?php 
                            if ($donCmd->getTtr_livre() <> "") {echo "<b>Livre :</b><br>".$donCmd->getId_livre()." / ".$donCmd->getRef_livre()." /<br>".$donCmd->getTtr_livre();}
                            if ($donCmd->getTtr_artcl() <> "") {echo "<br><b>Article :</b><br>".$donCmd->getId_artcl()." /<br>".$donCmd->getTtr_artcl();}                      
                            ?>
                        </td>
                        <td class="text-center"><?php echo $donCmd->getNombre() ?></td>
                        <td class="text-center"><?php echo (int)$donCmd->getPx_ttc()*(int)$donCmd->getNombre()." ".$donCmd->getMonnaie()." /<br>"
                            .$donCmd->getFrs_livrais()." ".$donCmd->getMonnaie();
                            ?>
                        </td>
                        <td class="text-center"><?php 
                            if ($donCmd->getMode_payment() == 1) {echo "Carte";} else {echo "Chèque";}
                            echo " / ".$donCmd->getRef_transaction();
                            ?>
                        </td>
                        <td class="text-center"><?php echo $donCmd->getDate_ordre_livrais() ?></td>
                        <td class="text-center"><?php echo $donCmd->getNom_destin()." / ".$donCmd->getPrenom_destin()
                            ." / <br>".$donCmd->getInstitution_destin();
                            ?>
                        </td>
                        <td class="text-center"><?php echo $donCmd->getAdr_livrais().", ".$donCmd->getCode_postal()
                            .", ".$donCmd->getVille().", ".$donCmd->getPays()
                            ."<br> Tél : ".$donCmd->getTelephone();
                            ?>
                        </td>
                        <td class="text-center"><?php 
                            echo $donCmd->getMode_livrais();
                            if ($donCmd->getMsg_livrais() <> "") {echo " / ".$donCmd->getMsg_livrais();}
                            ?>
                        </td>
                        <td class="text-center"><?php echo $donCmd->getDate_livrais_prev()." / ".$donCmd->getDate_livrais_real();?></td>

                        <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] == 1) { ?>
                        <td class="text-center">
                            <a href='./index.php?action=detail&id=<?=$donCmd->getId_commande();?>&detail=commande' class="btn btn-primary"><i class="fas fa-info"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                            <a href='./index.php?action=modifier&id=<?=$donCmd->getId_commande();?>&modifier=commande' class="btn btn-success"><i class="fas fa-pen"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                            <a onclick="return confirm('Etes-vous sûr de vouloir supprimer cette ligne ?')" href="./index.php?action=supprimer&id=<?=$donCmd->getId_commande();?>&supprimer=commande" class="btn btn-danger"><i class="fas fa-trash"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                        </td>
                        <?php } ?>
                    </tr>
                    <?php }
            ?>

            </tbody>
        </table>
               
        <?php
$contenu= ob_get_clean();

?>