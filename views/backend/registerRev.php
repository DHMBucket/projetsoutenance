<?php  $contenu = ob_start(); ?>

<script src="./assets/js/formulaireRevue.js"></script>


    <div class="container text-center bg-warning" style="position:relative;  ">
        <br><br><br>   
        <h1 class="h2">Formulaire d'inscription Revue</h1>
        <form action="./index.php?action=ajouterRev&ajouterRev" method="POST" enctype="multipart/form-data">
            <div class="form-row align-items-center">

            <div class="col-sm-4 my-1">
                <label for="nom">Nom de la revue</label>
                <input type="text" class="form-control" id="nom" name="nom" 
                        placeholder="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo "Entrer votre nom";}?>"
                        value="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo "";}?>"
                        required>
            </div>

            <div class="col-sm-3 my-1">
                <label for="period">Periodicité</label>
                <input type="number" class="form-control" id="period" name="period" 
                        placeholder="<?php if (isset($_POST['period'])) { echo $_POST['period']; } else { echo "Entrer nombre";}?>"
                        value="<?php if (isset($_POST['period'])) {
                                            if ((int)$_POST['period'] == 4) {
                                                echo $_POST['period'];
                                            } else { echo 4;}  
                                     } else { echo 4;}?>"
                        required>
            </div>

            <div class="checkbox text-left col-sm-2 my-1">
                <label><input id="horsserie" name="horsserie" type="checkbox" value="" style="margin-left: 0px">Hors serie</label>
            </div>

            <div class="col-sm-3 my-1">
                <label for="prix">Prix en Euro</label>
                <input type="number" class="form-control" id="prix" name="prix" 
                        placeholder="<?php if (isset($_POST['prix'])) { echo $_POST['prix']; } else { echo "Entrer prix";}?>"
                        value="<?php if (isset($_POST['prix'])) {
                                            if ((int)$_POST['prix'] >= 0) {
                                                echo $_POST['prix'];
                                            } else { echo 0;}  
                                     } else { echo 0;}?>"
                        required>
            </div>
            </div>
            <div class="form-row align-items-center">
            </div>
            <br><br><br>
                <div class="container bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="ajoutrev" name="ajoutrev">Enregistrer</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauRev">Retour au tableau</a>
                </div>
        </form>
</div>
<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




