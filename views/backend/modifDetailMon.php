<?php  $contenu = ob_start(); ?>

<script src="./assets/js/formulaireLivre.js"></script>


    <div class="container text-center bg-warning" style="position:relative;  ">
        <br><br><br>   
        <h1 class="h2">Modification de taux de la monnaie</h1>
        <form action="./index.php?action=modifierMon&modifierMon" method="POST" enctype="multipart/form-data">
            <div class="form-row align-items-center">

                <div class="col-sm-1 my-1">
                    <label for="idm">Id Monnaie</label>
                    <input type="text" class="form-control" id="idm" name="idm" 
                           placeholder="<?=(int)$donnMon[0]->getId_monnaie()?>"
                           value="<?=(int)$donnMon[0]->getId_monnaie();?>"
                           readonly>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="nom">Nom</label>
                    <input type="text" class="form-control" id="nom" name="nom" 
                           placeholder="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo $donnMon[0]->getNom_monnaie();}?>"
                           value="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo $donnMon[0]->getNom_monnaie();}?>"
                           readonly>
                </div>

                <div class="col-sm-3 my-1">
                    <label for="symb">Symbole</label>
                    <input type="text" class="form-control" id="symb" name="symb" 
                           placeholder="<?php if (isset($_POST['symb'])) { echo $_POST['symb']; } else { echo $donnMon[0]->getSymb_monnaie();}?>"
                           value="<?php if (isset($_POST['symb'])) { echo $_POST['symb']; } else { echo $donnMon[0]->getSymb_monnaie();}?>"
                           readonly>
                </div>

                <div class="col-sm-2">
                    <label for="prix">Prix en Euro</label>
                    <input type="text" class="form-control" id="prix" name="prix" 
                            placeholder="<?php if (isset($_POST['prix'])) { echo $_POST['prix']; } else { echo $donnMon[0]->getVal_commerc_en_euro();}?>"
                            value="<?php if (isset($_POST['prix'])) {
                                                if ((int)$_POST['prix'] > 0) {
                                                    echo $_POST['prix'];
                                                } else { echo 1;}  
                                        } else { echo $donnMon[0]->getVal_commerc_en_euro();}?>"
                            required>
                </div>

                <div class="container bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="modifmon" name="modifmon">Enregistrer</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauMon">Retour au tableau</a>
                </div>
            </div>
        </form>
</div>

<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




