<?php  $contenu = ob_start(); ?>

    <div class="container text-center" style="position:relative; background-color:lightgreen ">
        <br><br><br>   
        <h1 class="h2">Edition de la commande (id = <?= $donnCmd[0]->getId_commande(); ?>)</h1>
        <form action="" method="GET" enctype="multipart/form-data">
            <div class="form-row align-items-center">
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Réf : '.$donnCmd[0]->getRef_commande();?>"
                    value="<?php echo 'Réf : '.$donnCmd[0]->getRef_commande();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Id_client : '.$donnCmd[0]->getId_client();?>"
                    value="<?php echo 'Id_client : '.$donnCmd[0]->getId_client();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php 
                        if ($donnCmd[0]->getType_client() == 0 || $donnCmd[0]->getType_client() == 6) {echo 'Type client : Non inscrit';}
                        if ($donnCmd[0]->getType_client() == 1) {echo 'Type client : Administrateur ';}
                        if ($donnCmd[0]->getType_client() == 2) {echo 'Type client : Gestionnaire ';}
                        if ($donnCmd[0]->getType_client() == 3) {echo 'Type client : Chercheur ';}
                        if ($donnCmd[0]->getType_client() == 4) {echo 'Type client : Adhérent ';}
                        if ($donnCmd[0]->getType_client() == 5) {echo 'Type client : Membre(gratuit) ';}
                    ?>"
                    value="<?php 
                        if ($donnCmd[0]->getType_client() == 0 || $donnCmd[0]->getType_client() == 6) {echo 'Type client : Non inscrit';}
                        if ($donnCmd[0]->getType_client() == 1) {echo 'Type client : Administrateur ';}
                        if ($donnCmd[0]->getType_client() == 2) {echo 'Type client : Gestionnaire ';}
                        if ($donnCmd[0]->getType_client() == 3) {echo 'Type client : Chercheur ';}
                        if ($donnCmd[0]->getType_client() == 4) {echo 'Type client : Adhérent ';}
                        if ($donnCmd[0]->getType_client() == 5) {echo 'Type client : Membre(gratuit) ';}
                    ?>">
                </div>

                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Id_livre : '.$donnCmd[0]->getId_livre();?>"
                    value="<?php echo 'Id_livre : '.$donnCmd[0]->getId_livre();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Ref_livre : '.$donnCmd[0]->getRef_livre();?>"
                    value="<?php echo 'Ref_livre : '.$donnCmd[0]->getRef_livre();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Titre livre : '.$donnCmd[0]->getTtr_livre();?>"
                    value="<?php echo 'Titre livre : '.$donnCmd[0]->getTtr_livre();?>">
                </div>

                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Id_article : '.$donnCmd[0]->getId_artcl();?>"
                    value="<?php echo 'Id_article : '.$donnCmd[0]->getId_artcl();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Titre article : '.$donnCmd[0]->getTtr_artcl();?>"
                    value="<?php echo 'Titre article : '.$donnCmd[0]->getTtr_artcl();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Nombre : '.$donnCmd[0]->getNombre();?>"
                    value="<?php echo 'Nombre : '.$donnCmd[0]->getNombre();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Prix HT : '.$donnCmd[0]->getPx_ht()." ".$donnCmd[0]->getMonnaie();?>"
                    value="<?php echo 'Prix HT : '.$donnCmd[0]->getPx_ht()." ".$donnCmd[0]->getMonnaie();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php $tx= floatval($donnCmd[0]->getTva_percent()*100); echo 'TVA : '.$tx." %";?>"
                    value="<?php $tx= floatval($donnCmd[0]->getTva_percent()*100); echo 'TVA : '.$tx." %";?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Prix TTC : '.$donnCmd[0]->getPx_ttc()." ".$donnCmd[0]->getMonnaie();?>"
                    value="<?php echo 'Prix TTC : '.$donnCmd[0]->getPx_ttc()." ".$donnCmd[0]->getMonnaie();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Frais livraison : '.$donnCmd[0]->getFrs_livrais()." ".$donnCmd[0]->getMonnaie();?>"
                    value="<?php echo 'Frais livraison : '.$donnCmd[0]->getFrs_livrais()." ".$donnCmd[0]->getMonnaie();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php if ($donnCmd[0]->getMode_payment() == 1) {echo 'Paiement : Carte';} else {echo 'Paiement : Chèque';}?>"
                    value="<?php if ($donnCmd[0]->getMode_payment() == 1) {echo 'Paiement : Carte';} else {echo 'Paiement : Chèque';}?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Réf. Trsct : '.$donnCmd[0]->getRef_transaction();?>"
                    value="<?php echo 'Réf. Trsct : '.$donnCmd[0]->getRef_transaction();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'ODS : '.date("d-m-Y", strtotime($donnCmd[0]->getDate_ordre_livrais()));?>"
                    value="<?php echo 'ODS : '.date("d-m-Y", strtotime($donnCmd[0]->getDate_ordre_livrais()));?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'livr. prévue : '.date("d-m-Y", strtotime($donnCmd[0]->getDate_livrais_prev()));?>"
                    value="<?php echo 'livr. prévue : '.date("d-m-Y", strtotime($donnCmd[0]->getDate_livrais_prev()));?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Nom destin. : '.$donnCmd[0]->getNom_destin();?>"
                    value="<?php echo 'Nom destin. : '.$donnCmd[0]->getNom_destin();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Prénom destin. : '.$donnCmd[0]->getPrenom_destin();?>"
                    value="<?php echo 'Prénom destin. : '.$donnCmd[0]->getPrenom_destin();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Instit. destin. : '.$donnCmd[0]->getInstitution_destin();?>"
                    value="<?php echo 'Instit. destin. : '.$donnCmd[0]->getInstitution_destin();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Message Client : '.$donnCmd[0]->getMsg_livrais();?>"
                    value="<?php echo 'Message Client : '.$donnCmd[0]->getMsg_livrais();?>">
                </div>
                <div class="col-sm-5 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Adresse : '.$donnCmd[0]->getAdr_livrais();?>"
                    value="<?php echo 'Adresse : '.$donnCmd[0]->getAdr_livrais();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Code Postal : '.$donnCmd[0]->getCode_Postal();?>"
                    value="<?php echo 'Code Postal : '.$donnCmd[0]->getCode_Postal();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Ville : '.$donnCmd[0]->getVille();?>"
                    value="<?php echo 'Ville : '.$donnCmd[0]->getVille();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Pays : '.$donnCmd[0]->getPays();?>"
                    value="<?php echo 'Pays : '.$donnCmd[0]->getPays();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Téléphone : '.$donnCmd[0]->getTelephone();?>"
                    value="<?php echo 'Téléphone : '.$donnCmd[0]->getTelephone();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Mode livrais : '.$donnCmd[0]->getMode_livrais();?>"
                    value="<?php echo 'Mode livrais : '.$donnCmd[0]->getMode_livrais();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'livr. réalisée : '.date("d-m-Y", strtotime($donnCmd[0]->getDate_livrais_real()));?>"
                    value="<?php echo 'livr. réalisée : '.date("d-m-Y", strtotime($donnCmd[0]->getDate_livrais_real()));?>">
                </div>
 
                <div class="container bg-info text-center">
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauCmd">Retour au tableau</a>
                </div>
        </form>
</div>

<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




