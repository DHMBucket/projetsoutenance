<?php 
    $contenu= ob_start();
    require_once('./communs/connect.php');
    require_once('./controllers/backend/MonnaieController.php');

?>

<script type="text/javascript" src="./assets/js/affichetabselectionScript.js">
</script>

<br><br>
<h1 class="h2" style="text-align: center;caption-side: top; font-weight: bolder">Situation des monnaies</h1>
<br>

<div id="navBoard" class="row border " style="margin: auto; align-items: center; justify-content: center; ">
    <div class="col-sm-6 m-0 p-0 border">
        <div class="container">
            <nav class="navbar navbar-light bg-light m-0 p-0 ">
                <form action="./index.php?action=search&search=searchTabMon" class="form-inline" method="POST">
                    <input class="form-control mr-sm-2" type="search" id="sequence" name="sequence" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" id="seqrech" name="seqrech" type="submit">Recherche par nom</button>
                </form>
            </nav>
        </div>
    </div>
</div>
    

<table class="table table-striped table-bordered">
          
            <thead>
                <tr>
                    <th class="text-center">numéro</th>
                    <th class="text-center">nom</th>
                    <th class="text-center">symbole</th>
                    <th class="text-center">valeur commerciale</th>
                    <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] < 1) { ?>
                        <th class="text-center" colspan="3">Actions</th>
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
            <?php
                    foreach ($dataA as $donMon) {
                        
                        
                    ?>
                        <tr>
                        <td class="text-center"><?php echo $donMon->getId_monnaie(); ?></td>
                        <td class="text-center"><?=$donMon->getNom_monnaie(); ?></td>       <!-- ou bien -->
                        <td class="text-center"><?=$donMon->getSymb_monnaie(); ?></td>
                        <td class="text-center"><?=$donMon->getVal_commerc_en_euro(); ?></td>

                        <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] < 1) { ?>
                        <td hidden class="text-center">
                            <a href='./index.php?action=detail&id=<?=$donMon->getId_monnaie();?>&detail=monnaie' class="btn btn-primary"><i class="fas fa-info"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                        </td>
                        <td class="text-center">
                            <a href='./index.php?action=modifier&id=<?=$donMon->getId_monnaie();?>&modifier=monnaie' class="btn btn-success"><i class="fas fa-pen"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                        </td>
                        <td class="text-center">
                            <a onclick="return confirm('Etes-vous sûr de vouloir supprimer cette ligne ?')" href="./index.php?action=supprimerMon&id=<?=$donMon->getId_monnaie();?>&supprimerMon=" class="btn btn-danger"><i class="fas fa-trash"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                        </td>
                        <?php } ?>
                    </tr>
                    <?php }
            ?>

            </tbody>
        </table>

               
        <?php
$contenu= ob_get_clean();

?>