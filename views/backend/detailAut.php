<?php  $contenu = ob_start(); ?>

    <div class="container text-center" style="position:relative; background-color:lightgreen ">
        <br><br><br>   
        <h1 class="h2">Edition de l'auteur (id = <?= $donnAut[0]->getId_auteur(); ?>)</h1>
        <form action="" method="GET" enctype="multipart/form-data">
            <div class="form-row align-items-center">
                <div class="col-sm-2 my-1">
                    <img src="<?='./assets/images/'.$donnAut[0]->getPhoto();?>" style="width:50%;" alt="<?=$donnAut[0]->getPhoto();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Nom : '.$donnAut[0]->getNom();?>"
                    value="<?php echo 'Nom : '.$donnAut[0]->getNom();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Prénom : '.$donnAut[0]->getPrenom();?>"
                    value="<?php echo 'Prénom : '.$donnAut[0]->getPrenom();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Institution : '.$donnAut[0]->getInstitution();?>"
                    value="<?php echo 'Institution : '.$donnAut[0]->getInstitution();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Fonction : '.$donnAut[0]->getFonction();?>"
                    value="<?php echo 'Fonction : '.$donnAut[0]->getFonction();?>">
                </div>

                <div class="container bg-info text-center">
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauAut">Retour au tableau</a>
                </div>
        </form>
</div>

<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




