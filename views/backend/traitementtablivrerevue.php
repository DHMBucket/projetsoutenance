<?php 
    $contenu= ob_start();
    require_once('./communs/connect.php');
    require_once('./controllers/backend/LivreRevueController.php');

?>

<script type="text/javascript" src="./assets/js/affichetabselectionScript.js">
</script>


<h1 class="h2" style="text-align: center;caption-side: top; font-weight: bolder">Situation des livres / revues</h1>
    <br>

<div id="navBoard" class="row border " style="margin: auto; align-items: center; justify-content: center; ">
    <div class="col-sm-3 m-0 p-0 border">
        <div class="container">
            <nav class="navbar navbar-light bg-light m-0 p-0 ">
                <form action="./index.php?action=search&search=searchTabLvR" class="form-inline" method="POST">
                    <input class="form-control mr-sm-2" type="search" id="sequence" name="sequence" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" id="seqrech" name="seqrech" type="submit">Recherche dans titre</button>
                </form>
            </nav>
        </div>
        <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role']<3) {?>
        <div class="container">
            <form action='./index.php?action=register&register=livre' method="post">
                <button class="btn btn-info" type="submit" name="ajoutLvR" id="ajoutLvR">Ajouter un livre ou une revue</button>
            </form>    
        </div>
        <?php } ?>
    </div>
    <?php
        
        if (isset($_SESSION['Auth']) && isset($_SESSION['Auth']['role']) && (int)$_SESSION['Auth']['role']<5) {
    ?>
    <div class="col-sm-9 m-0 p-0 border">
    <div class="container row border" style="margin: auto;"> 
        
            <form action="./index.php?action=searchCrit&searchCrit=searchTabCritLvR" class="form-inline col" method="POST">
              
                <div class="col-sm-1 m-0 p-0 text-center" >
                    <label for="idsearch">par Id</label>
                    <select class="form-control" id="idsearch" name="idsearch">
                        <option value=0>0</option>
                        <?php $i=1; foreach ($idData as $idVal) {?>
                            <option value=<?php echo $idVal;?>><?php echo $idVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($idData as $idVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-2 m-0 p-0 text-center" >
                    <label for="refsearch">référence</label>
                    <select class="form-control" id="refsearch" name="refsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($refData as $refVal) {?>
                            <option value=<?php echo $refVal;?>><?php echo $refVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($refData as $refVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$refVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-1 m-0 p-0 text-center" >
                    <label for="anneesearch">année</label>
                    <select class="form-control" id="anneesearch" name="anneesearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($anneeData as $anneeVal) {?>
                            <option value=<?php echo $anneeVal;?>><?php echo $anneeVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($anneeData as $anneeVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$anneeVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="ntsearch">Nom-Titre</label>
                    <select class="form-control" id="ntsearch" name="ntsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($nomData as $nomVal) {?>
                            <option value=<?php echo '"'.$nomVal.'"';?>><?php echo $nomVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($nomData as $nomVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$nomVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="autsearch">auteur,editeur,rédact.</label>
                    <select class="form-control" id="autsearch" name="autsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($autData as $autVal) {?>
                            <option value=<?php echo '"'.$autVal.'"';?>><?php echo $autVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($autData as $autVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$autVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="form-group col-sm-1 m-0 p-0" id="stocks">
                    <div class="checkbox text-left">
                        <label><input id="dispo" name="dispo" type="checkbox" value="" style="margin-left: 0px">Dispo</label>
                    </div>
                    <div class="checkbox text-left">
                        <label><input id="nonDispo" name="nonDispo" type="checkbox" value="" style="margin-left: 0px">Indispo</label>
                    </div>
                </div>
                <div class="col-sm-1 m-0 p-0 bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="filtrsearchLvR" name="filtrsearchLvR">Filtrage</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauLvR">Réinitialiser</a>
                </div>
            </form>
        
        </div>
        </div>
        <?php
        }
        ?>

</div>


<table class="table table-striped table-bordered">
          
            <thead>
                <tr>
                    <th class="text-center">référence</th>
                    <th class="text-center">image + fichierebook</th>
                    <th class="text-center">titre + nomspecif</th>
                    <th class="text-center">auteur1 + auteur2</th>
                    <th class="text-center">redacteur + éditeur1 + éditeur2</th>
                    <th class="text-center">nombre de pages</th>
                    <th class="text-center">description + morceau + critique + consign</th>
                    <th class="text-center">date de parution</th>
                    <th class="text-center">prix + monnaie</th>
                    <th class="text-center">format & support & périodicité</th>
                    <th class="text-center">stocks iris + editeurs</th>
                    <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] == 1) { ?>
                        <th class="text-center">Actions</th><!--colspan="3"-->
                    <?php } ?>
                </tr>
            </thead>
            
            <tbody>
            <?php
                    $ilvrrev=-1;
                    foreach ($dataA as $donLvR) {
                        $ilvrrev++;
                    ?>
                        <tr>
                        <td class="text-center"><?php echo $donLvR->getReference() ?>
                            <?php if ($_SESSION['Auth']['role'] < 5) { ?>
                                <br>
                                <a class="btn btn-secondary" 
                                <?php if (($donLvR->getStock_iris() + $donLvR->getStock_editeur())<>0) { 
                                        echo 'style="background-color: yellow; color: green;"';
                                      } else {
                                        echo 'style="background-color: aqua; color: red; pointer-events: none; cursor: default;"';
                                      }  
                                ?>
                                href="./index.php?t=<?=$ilvrrev;?>&action=reservPanier" id="reserv1">Ajouter au panier</a>
                                <?php if ($donLvR->getNom_specif_revue()<>"" && isset($_SESSION['Auth'])) { ?>
                                    <a class="btn btn-secondary" 
                                    <?php if (($donLvR->getStock_iris() + $donLvR->getStock_editeur())<>0) { 
                                        echo 'style="background-color: brown; color: white;"';
                                      } else {
                                        echo 'style="background-color: aqua; color: red; pointer-events: none; cursor: default;"';
                                      }  
                                ?>
                                    href="./index.php?t=<?=$ilvrrev;?>&nomrevue=<?=$donLvR->getNom_specif_revue();?>&action=abonnement" id="abonnemt1">S'abonner</a>
                                <?php } ?>
                            <?php } ?>                                
                        </td>
                        <td class="text-center"><img src="<?="./assets/images/".$donLvR->getImage1(); ?>" alt="<?="E_Book : ".$donLvR->getFichier_ebook();?>" style="width:100px;height:100px;">
                                                <p><?="E_Book : ".$donLvR->getFichier_ebook();?></p></td>
                        <td class="text-center"><?php if ($donLvR->getNom_specif_revue()=="") {
                                                          echo $donLvR->getTitre();
                                                      } else {
                                                          echo $donLvR->getNom_specif_revue();
                                                          echo "<br>"; 
                                                          echo $donLvR->getTitre();
                                                      } ?></td>       
                        <td class="text-center"><?php 
                                    echo $donLvR->getAuteur_princ1_nom()." ".$donLvR->getAuteur_princ1_prenom()." ".$donLvR->getAuteur_princ1_institution();
                                    if ($donLvR->getAuteur_princ2_nom() <> "" || $donLvR->getAuteur_princ2_institution() <> "") {
                                        echo "<br>".$donLvR->getAuteur_princ2_nom()." ".$donLvR->getAuteur_princ2_prenom()." ".$donLvR->getAuteur_princ2_institution();
                                    }
                                    ?></td>
                        <td class="text-center"><?php 
                                    if ($donLvR->getRedac_chef_nom() <> "" || $donLvR->getRedac_chef_institution() <> "") {
                                        echo "<br>".$donLvR->getRedac_chef_nom()." ".$donLvR->getRedac_chef_prenom()." ".$donLvR->getRedac_chef_institution();
                                    }
                                    if ($donLvR->getEditeur1() <> "") {
                                        echo "<br> éditeurs : ".$donLvR->getEditeur1();
                                    }
                                    if ($donLvR->getEditeur2() <> "") {
                                        echo "<br> / ".$donLvR->getEditeur2();
                                    }
                                    ?></td>
                    <td class="text-center"><?=$donLvR->getNbre_pages(); ?></td>
                    <td class="text-center"><?php 
                                if ($donLvR->getDescription() <> "") {
                                    echo "<br> Description : ".$donLvR->getDescription();
                                }
                                if ($donLvR->getMorceau_choisi() <> "") {
                                    echo "<br> Passage choisi : ".$donLvR->getMorceau_choisi();
                                }
                                if ($donLvR->getCritique() <> "") {
                                    echo "<br> Critique : ".$donLvR->getCritique();
                                }
                                if ($donLvR->getConsign_auteur() <> "") {
                                    echo "<br> Consigne de l'auteur : ".$donLvR->getConsign_auteur();
                                }
                                ?></td>
                    <td class="text-center"><?=$donLvR->getDate_parution(); ?></td>

                  <td class="text-center"><?=$donLvR->getPrix()." ".$donLvR->getMonnaie(); ?></td>
                    <td class="text-center"><?php 
                                if ($donLvR->getFormat_livre() <> "") {
                                    echo "<br> Format : ".$donLvR->getFormat_livre();
                                }
                                if ($donLvR->getType_support() <> "") {
                                    echo "<br> Support : ".$donLvR->getType_support();
                                }
                                if ($donLvR->getPeriodicite() <> "") {
                                    echo "<br> Périodicité : ".$donLvR->getPeriodicite();
                                }
                                ?></td>
                    <td class="text-center"><?=$donLvR->getStock_iris()+$donLvR->getStock_editeur();?></td>

                        <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] == 1) { ?>
                        <td class="text-center">
                            <a href='./index.php?action=detail&id=<?=$donLvR->getId_livre();?>&detail=livre' class="btn btn-primary"><i class="fas fa-info"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                            <a href='./index.php?action=modifier&id=<?=$donLvR->getId_livre();?>&modifier=livre' class="btn btn-success"><i class="fas fa-pen"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                            <a onclick="return confirm('Etes-vous sûr de vouloir supprimer cette ligne ?')" href="./index.php?action=supprimer&id=<?=$donLvR->getId_livre();?>&supprimer=livre" class="btn btn-danger"><i class="fas fa-trash"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                        </td>
                        <?php } ?>
                    </tr>
                    <?php }
            ?>

            </tbody>
        </table>

               
        <?php
$contenu= ob_get_clean();

?>