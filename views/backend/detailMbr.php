<?php  $contenu = ob_start(); ?>

    <div class="container text-center" style="position:relative; background-color:lightgreen ">
        <br><br><br>   
        <h1 class="h2">Edition du client membre (id = <?= $donnMbr[0]->getId_clt_membre(); ?>)</h1>
        <form aMbron="" method="GET" enctype="multipart/form-data">
            <div class="form-row align-items-center">
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php 
                        if ($donnMbr[0]->getType_membre() == 3) {echo 'Type membre : Chercheur ';}
                        if ($donnMbr[0]->getType_membre() == 4) {echo 'Type membre : Adhérent ';}
                        if ($donnMbr[0]->getType_membre() == 5) {echo 'Type membre : Membre(gratuit) ';}
                    ?>"
                    value="<?php 
                        if ($donnMbr[0]->getType_membre() == 3) {echo 'Type membre : Chercheur ';}
                        if ($donnMbr[0]->getType_membre() == 4) {echo 'Type membre : Adhérent ';}
                        if ($donnMbr[0]->getType_membre() == 5) {echo 'Type membre : Membre(gratuit) ';}
                    ?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php 
                        if ($donnMbr[0]->getCivilite() == 1) { echo 'Civilité : Monsieur';} 
                            else { echo 'Civilité : Madame';} ?>"
                    value="<?php 
                        if ($donnMbr[0]->getCivilite() == 1) { echo 'Civilité : Monsieur';} 
                            else { echo 'Civilité : Madame';} ?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Nom membre : '.$donnMbr[0]->getNom_membre();?>"
                    value="<?php echo 'Nom membre : '.$donnMbr[0]->getNom_membre();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Prénom membre : '.$donnMbr[0]->getPrenom_membre();?>"
                    value="<?php echo 'Prénom membre : '.$donnMbr[0]->getPrenom_membre();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Institution : '.$donnMbr[0]->getInstitution();?>"
                    value="<?php echo 'Institution : '.$donnMbr[0]->getInstitution();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Fonction : '.$donnMbr[0]->getFonction();?>"
                    value="<?php echo 'Fonction : '.$donnMbr[0]->getFonction();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php 
                        if ($donnMbr[0]->getStatut_client() == 1) {echo 'Statut : Individuel';}
                        if ($donnMbr[0]->getStatut_client() == 2) {echo 'Statut : Etudiant';}
                        if ($donnMbr[0]->getStatut_client() == 3) {echo 'Statut : Sans emploi';}
                        if ($donnMbr[0]->getStatut_client() == 4) {echo 'Statut : Institution/Entreprise';}
                    ?>"
                    value="<?php 
                        if ($donnMbr[0]->getStatut_client() == 1) {echo 'Statut : Individuel';}
                        if ($donnMbr[0]->getStatut_client() == 2) {echo 'Statut : Etudiant';}
                        if ($donnMbr[0]->getStatut_client() == 3) {echo 'Statut : Sans emploi';}
                        if ($donnMbr[0]->getStatut_client() == 4) {echo 'Statut : Institution/Entreprise';}
                    ?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Justificatif : '.$donnMbr[0]->getJustif_etud_ou_ssemploi_instit();?>"
                    value="<?php echo 'Justificatif : '.$donnMbr[0]->getJustif_etud_ou_ssemploi_instit();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Email : '.$donnMbr[0]->getEmail();?>"
                    value="<?php echo 'Email : '.$donnMbr[0]->getEmail();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php 
                        if ($donnMbr[0]->getOffre_partenaire() == 1) { echo 'Offre parten. : Oui';} 
                        else { echo 'Offre parten. : Non';} ?>"
                value="<?php 
                        if ($donnMbr[0]->getOffre_partenaire() == 1) { echo 'Offre parten. : Oui';} 
                            else { echo 'Offre parten. : Non';} ?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php 
                        if ($donnMbr[0]->getOffre_newsletter() == 1) { echo 'Newsletter : Oui';} 
                        else { echo 'Newsletter : Non';} ?>"
                value="<?php 
                        if ($donnMbr[0]->getOffre_newsletter() == 1) { echo 'Newsletter : Oui';} 
                            else { echo 'Newsletter : Non';} ?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Tarif Mbre : '.$donnMbr[0]->getPrix_membre()." ".$donnMbr[0]->getMonnaie();?>"
                    value="<?php echo 'Tarif Mbre : '.$donnMbr[0]->getPrix_membre()." ".$donnMbr[0]->getMonnaie();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Date création : '.date("d-m-Y", strtotime($donnMbr[0]->getDate_debut()));?>"
                    value="<?php echo 'Date création : '.date("d-m-Y", strtotime($donnMbr[0]->getDate_debut()));?>">
                </div>
                <div class="col-sm-6 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Adresse : '.$donnMbr[0]->getAdr_livrais();?>"
                    value="<?php echo 'Adresse : '.$donnMbr[0]->getAdr_livrais();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Code Postal : '.$donnMbr[0]->getCode_Postal();?>"
                    value="<?php echo 'Code Postal : '.$donnMbr[0]->getCode_Postal();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Ville : '.$donnMbr[0]->getVille();?>"
                    value="<?php echo 'Ville : '.$donnMbr[0]->getVille();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Pays : '.$donnMbr[0]->getPays();?>"
                    value="<?php echo 'Pays : '.$donnMbr[0]->getPays();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Zip état : '.$donnMbr[0]->getZip_etat();?>"
                    value="<?php echo 'Zip état : '.$donnMbr[0]->getZip_etat();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Téléphone : '.$donnMbr[0]->getIndicatif()." ".$donnMbr[0]->getTelephone();?>"
                    value="<?php echo 'Téléphone : '.$donnMbr[0]->getIndicatif()." ".$donnMbr[0]->getTelephone();?>">
                </div>
              
 
                <div class="container bg-info text-center">
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauMbr">Retour au tableau</a>
                </div>
        </form>
</div>

<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




