<?php  $contenu = ob_start(); ?>
<link rel="stylesheet" href="./assets/css/telephone.css">
<script src="./assets/js/formulaireClient.js"></script>


    <div class="container text-center bg-warning" style="position:relative;  ">
        <br><br><br>   
        <h1 class="h2">Formulaire d'inscription Client</h1>
        <form action="./index.php?action=ajouterMbr&ajouterMbr" method="POST" enctype="multipart/form-data">
            <div class="form-row align-items-center">
                <div class="offset-1 col-3 my-1">
                    <label for="selectMbre">Type membre</label>
                    <select class="form-control" id="selectMbre" name="typembre" required>
                        <option value=5>1-Membre (gratuit)</option>
                        <option value=4>2-Adhérent</option>
                        <option value=3>3-Chercheur (avec justificatif)</option>
                    </select>
                </div>
                <div class="col-4 my-1">
                    <label for="selectStt">Statut Adhérent</label>
                    <select class="form-control" id="selectStt" name="statutmbre" required>
                        <option value=1>1-Individuel</option>
                        <option value=2>2-Etudiant (avec justificatif)</option>
                        <option value=3>3-Sans emploi (avec justificatif)</option>
                        <option value=4>4-Institution / Entreprise (avec justificatif)</option>
                    </select>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="tarif">Tarif</label>
                    <input type="number" class="form-control" id="tarif" name="tarif" 
                           placeholder=""
                           value="<?php if (isset($_POST['tarif'])) { echo $_POST['tarif']; } else { echo "0";}?>" readonly>
                </div>
                <div class="col-1 my-1">
                    <label for="monnaie">Libellé</label>
                    <select class="form-control" id="monnaie" name="monnaie" required>
                        <?php $i=1; foreach ($donnMonn as $monn) {?>
                            <option value=<?php echo $i."-".$monn->getSymb_monnaie();?>><?php echo $i."-".$monn->getSymb_monnaie(); $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($donnMonn as $monn) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$monn->getSymb_monnaie()."--".$monn->getNom_monnaie()."--".$monn->getVal_commerc_en_euro()."--".$monn->getId_monnaie(); $i++;?></p>
                    <?php } ?>
                </div>
            </div>
            <div class="form-row align-items-center">
                <div class="offset-1 col-2 my-1">
                    <label for="selectCivil">Civilité</label>
                    <select class="form-control" id="selectCivil" name="typecivil" required>
                        <option value=1>1-Monsieur</option>
                        <option value=2>2-Madame</option>
                    </select>
                </div>
                <div class="col-sm-4 my-1">
                    <label for="nom">Nom membre ou représentant</label>
                    <input type="text" class="form-control" id="nom" name="nom" 
                           placeholder="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo "Entrer votre nom";}?>"
                           value="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo "";}?>"
                           required>
                </div>
                <div class="col-sm-4 my-1">
                    <label for="prenom">Prénom ( facultatif )</label>
                    <input type="text" class="form-control" id="prenom" name="prenom" 
                           placeholder="<?php if (isset($_POST['prenom'])) { echo $_POST['prenom']; } else { echo "Entrer votre prénom";}?>"
                           value="<?php if (isset($_POST['prenom'])) { echo $_POST['prenom']; } else { echo "";}?>">
                </div>
            </div>
            <div class="form-row align-items-center">
                <div class="offset-1 col-sm-6 my-1">
                    <label for="instit">Nom Institution ou Entreprise (non requis pour les particuliers)</label>
                    <input type="text" class="form-control" id="instit" name="institution" 
                           placeholder="<?php if (isset($_POST['institution'])) { echo $_POST['institution']; } else { echo "Entrer le nom de votre instituion/entreprise";}?>"
                           value="<?php if (isset($_POST['institution'])) { echo $_POST['institution']; } else { echo "";}?>">
                </div>
                <div class="col-sm-4 my-1">
                    <label for="fonction">Fonction du représentant (facultatif)</label>
                    <input type="text" class="form-control" id="fonction" name="fonction" 
                           placeholder="<?php if (isset($_POST['fonction'])) { echo $_POST['fonction']; } else { echo "Entrer votre fonction";}?>"
                           value="<?php if (isset($_POST['fonction'])) { echo $_POST['fonction']; } else { echo "";}?>">
                </div>

            </div>
    
            <div class="form-row align-items-center">
                <div class="offset-1 col-sm-5 my-1">
                    <label id="filelab" for="fichier">Fichier justificatif</label>
                    <input type="hidden" name="max_file_size" value="1000000000" />
                    <input type="file" class="form-control" id="fichier" name="ficjustif"
                    placeholder="<?php if (isset($_POST['ficjustif'])) { echo $_POST['ficjustif']; } else { echo "Entrer votre fichier justificatif ( .pdf, .docx, .jpg )";}?>"
                    value="<?php if (isset($_POST['ficjustif'])) { echo $_POST['ficjustif']; } else { echo "";}?>">
                    <small id="fileHelp" class="form-text text-muted">Les fichiers acceptés sont de type .pdf, .docx ou .jpg</small>
                </div>

                <div class="form-group col-sm-5 my-1">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control text-center" id="email" name="email" aria-describedby="emailHelp" 
                            placeholder="<?php if (isset($_POST['email'])) { echo $_POST['email']; } else { echo "Entrer votre email";}?>"
                            value="<?php if (isset($_POST['email'])) { echo $_POST['email']; } else { echo "@";}?>"  
                        required>
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
            </div>

            <div class="form-row align-items-center">
                <div class="form-group offset-1 col-sm-4 my-1">
                    <label for="mdp">Password</label>
                    <input type="password" class="form-control" id="mdp" name="mdp" 
                        placeholder="<?php if (isset($_POST['mdp'])) { echo $_POST['mdp']; } else { echo "";}?>"
                        value="<?php if (isset($_POST['mdp'])) { echo $_POST['mdp']; } else { echo "";}?>"  
                    required>
                    <small  id="passmbr" class="form-text text-muted">_</small>
                </div>
                <div class="form-group col-sm-4 my-1">
                    <label for="mdpbis">Confirmer Password</label>
                    <input type="password" class="form-control" id="mdpbis" name="mdpbis" 
                        placeholder="<?php if (isset($_POST['mdpbis'])) { echo $_POST['mdpbis']; } else { echo "";}?>"
                        value="<?php if (isset($_POST['mdpbis'])) { echo $_POST['mdpbis']; } else { echo "";}?>"  
                    required>
                    <small id="passconfirm" class="form-text text-muted">_</small>
                </div>
                <div class="form-group col-sm-2 my-1 border border-primary" id="offresatrs">
                    <div class="checkbox text-left">
                        <label><input id="offrpartner" name="offrpartner" type="checkbox" value="" style="margin-left: 0px">Offres partenaires</label>
                    </div>
                    <div class="checkbox text-left">
                        <label><input id="offrnwlett" name="offrnwlett" type="checkbox" value="" style="margin-left: 0px">Offre newsletter</label>
                    </div>
                </div>
            </div>

            <div class="form-row align-items-center">
                <div class="offset-1 col-sm-6 my-1">
                    <label for="inputAddress">Adresse de livraison</label>
                    <input type="text" class="form-control" id="inputAddress" name="inputAddress" placeholder="1234 Main St">
                </div>
                <div class="col-sm-4 my-1">
                    <label for="inputAddress2">Adresse complémentaire</label>
                    <input type="text" class="form-control" id="inputAddress2" name="inputAddress2" placeholder="Apartment, studio, or floor">
                </div>
            </div>

            <div class="form-row align-items-center">
                <div class="offset-1 col-sm-4 my-1">
                    <label for="inputCity">Ville</label>
                    <input type="text" class="form-control" id="inputCity" name="inputCity">
                </div>
                <div class="col-sm-4 my-1">
                    <label for="inputState">Etat</label>
                    <select id="inputState" name="inputState" class="countrypicker form-control"></select>
                    <input type="hidden" id="nometat" name="nometat"/>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="inputZip">Code Postal</label>
                    <input type="text" class="form-control" id="inputZip" name="inputZip">
                </div>
            </div>

            <div class="form-row align-items-center">
                <div id="teldiv" name="teldiv" class="offset-1 col-sm-6 my-1 input-group">
                    <label for="indicatif">indicatif</label>
                    <input type="text" class="form-control" id="indicatif" name="indicatif" placeholder="+33" value="+33">
                    <input type="tel" id="teleph1" name="telClt" class="form-control">
                    <span class="btn btn-info">Tel</span>
                </div>
            </div>
            
                <div class="col-auto my-1">
                </div>
                <div class="container bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="ajoutclmb" name="ajoutclmb">S'inscrire</button>
                    <?php
                    if (isset($_SESSION['Auth']) && isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] < 3 && $_SESSION['Auth']['operationnel'] == 1) { ?>
                        <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauMbr">Retour au tableau</a>
                    <?php    
                    } else { ?>
                        <a class="btn btn-info" href="./index.php">Retour à la page d'accueil</a>
                    <?php } ?>
                </div>
        </form>
</div>
<script> $('.countrypicker').countrypicker(); </script>
<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




