<?php
$contenu= ob_start();
require_once('./communs/connect.php');
require_once('./controllers/frontend/PresentationtraitementController.php');
?>

<div id="navBoard" class="row border " style="margin: auto; align-items: center; justify-content: center; ">
    <div class="col-sm-3 m-0 p-0 border">
        <nav class="navbar navbar-light bg-light m-0 p-0 ">
            <form action="./index.php?action=search&search=searchAccEdt" class="form-inline" method="POST">
                <input class="form-control mr-sm-2" type="search" id="sequence" name="sequence" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" id="seqrech" name="seqrech" type="submit">Recherche par nom</button>
            </form>
        </nav>
    </div>
    <?php
        
        if (isset($_SESSION['Auth']) && isset($_SESSION['Auth']['role']) && (int)$_SESSION['Auth']['role']<5) {
    ?>
    <div class="col-sm-9 m-0 p-0 border">
    <div class="container row border" style="margin: auto;"> 
        
            <form action="./index.php?action=searchCrit&searchCrit=searchAccCritEdt" class="form-inline col" method="POST">
              
            <div class="col-sm-1 m-0 p-0 text-center" >
                    <label for="idsearch">par Id</label>
                    <select class="form-control" id="idsearch" name="idsearch">
                        <option value=0>0</option>
                        <?php $i=1; foreach ($idData as $idVal) {?>
                            <option value=<?php echo $idVal;?>><?php echo $idVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($idData as $idVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="nesearch">Nom</label>
                    <select class="form-control" id="nesearch" name="nesearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($nomData as $nomVal) {?>
                            <option value=<?php echo '"'.$nomVal.'"';?>><?php echo $nomVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($nomData as $nomVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$nomVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-2 m-0 p-0 text-center" >
                    <label for="cdesearch">Code</label>
                    <select class="form-control" id="cdesearch" name="cdesearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($cdeData as $cdeVal) {?>
                            <option value=<?php echo '"'.$cdeVal.'"';?>><?php echo $cdeVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($cdeData as $cdeVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$cdeVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="locsearch">Localisation</label>
                    <select class="form-control" id="locsearch" name="locsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($locData as $locVal) {?>
                            <option value=<?php echo '"'.$locVal.'"';?>><?php echo $locVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($locData as $locVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$locVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-2 m-0 p-0 text-center" hidden>
                    <label for="frlsearch">Frais livr.</label>
                    <select class="form-control" id="frlsearch" name="frlsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($frlData as $frlVal) {?>
                            <option value=<?php echo '"'.$frlVal.'"';?>><?php echo $frlVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($frlData as $frlVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$frlVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-2 m-0 p-0 bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="filtrsearchAut" name="filtrsearchAut">Filtrage</button>
                    <a class="btn btn-info" href="./index.php?action=initial&initial=initialEdt">Réinitialiser</a>
                </div>
            </form>
        
        </div>
        </div>
        <?php
        }
        ?>

</div>

 <h3 style="text-align: center;caption-side: top;">Les éditeurs de notre site</h3>
 <br>
<div class="container row" style="margin: auto;">
    <div class="col-auto" style="margin: auto;" >
            <button type="button" name="precedentEdt" id="precedent">Précédent</button>
            <button type="button" name="suivantEdt" id="suivant">Suivant</button>
    </div>
</div>
<br>

<div class="row">               

<?php
if ($numDim > 0) { 
        if (isset($_SESSION)) {
            if (isset($_COOKIE['iindex']) && $_COOKIE['iindex']>0) {
                $_SESSION['icompteur']=$_COOKIE['iindex'];
            }
?>
            <input hidden type="number" id="nbligne1" value="<?=$_SESSION['icompteur']?>">
            
            <div id="fixsz1" class="container col-5" style="border: 1px solid #ced4da;">
                <div class="row">
                    <div class="col">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><p><span class="font-weight-bolder">Nom&emsp;&emsp;&emsp;:&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-2]->getNom_editeur();?></p></li>
                            <li class="list-group-item"><p><span class="font-weight-bolder">Pays&emsp;&emsp;:&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-2]->getPays_editeur();?></p></li>
                            <li class="list-group-item"><p><span class="font-weight-bolder">Localité&emsp;:&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-2]->getLocalite_editeur();?></p></li>
                            <li class="list-group-item"><p><span class="font-weight-bolder">Frais Livr.&emsp;:&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-2]->getFrs_livrais()." ";?>&euro;</p></li>
                            <li class="list-group-item"><p><span class="font-weight-bolder">Code&emsp;&emsp;:&nbsp;&nbsp;</span></p><p id="descript1"><?=$dataA[$_SESSION['icompteur']-2]->getCode_editeur();?></p></li>
                            <li class="list-group-item">
                                <button class="btn btn-success" type="button" id="details1">détail code</button> 
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        <?php
        }
        if (
            (isset($_SESSION) && isset($_COOKIE['iindex']) && $_COOKIE['iindex']==0 && $_SESSION['icompteur']>0  && count($dataA)>1) 
            || 
            (isset($_SESSION) && $_SESSION['icompteur']==2 && count($dataA)>1)
            ) {
        ?>
        <input hidden type="number" id="nbligne2" value="<?=$_SESSION['icompteur']?>">

        <div id="fixsz2" class="container col-5" style="border: 1px solid #ced4da;">
            <div class="row">
                <div class="col">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><p><span class="font-weight-bolder">Nom&emsp;&emsp;&emsp;:&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-1]->getNom_editeur();?></p></li>
                        <li class="list-group-item"><p><span class="font-weight-bolder">Pays&emsp;&emsp;:&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-1]->getPays_editeur();?></p></li>
                        <li class="list-group-item"><p><span class="font-weight-bolder">Localité&emsp;:&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-1]->getLocalite_editeur();?></p></li>
                        <li class="list-group-item"><p><span class="font-weight-bolder">Frais Livr.&emsp;:&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-1]->getFrs_livrais()." ";?>&euro;</p></li>
                        <li class="list-group-item"><p><span class="font-weight-bolder">Code&emsp;&emsp;:&nbsp;&nbsp;</span></p><p id="descript2"><?=" ".$dataA[$_SESSION['icompteur']-1]->getCode_editeur();?></p></li>
                        <li class="list-group-item">
                            <button class="btn btn-success" type="button" id="details2">détail code</button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <?php
        } 
    } else {
        ?>
            <br>
            <div class="container row" style="margin: auto;">
                <div class="col-auto" style="margin: auto;" >
                    <h1>Pas de nom d'éditeur avec ces critères, appuyer sur la touche Filtrage ou Réinitialiser</h1>
                </div>
            </div>
            <br>
        <?php
    }
    
?>

</div>

<br><br>
<script src="./assets/js/presentationScript.js"> 

</script>   

<?php

    $contenu= ob_get_clean();
    
?>    

