<?php  $contenu = ob_start(); ?>

<?php 
    require_once('./communs/connect.php');
    
    ?>
    <br><br><br><br>
    <div class="container bg-success">
        <br><br>
        <h1 class="bg-success" style="text-align: center">Votre adhésion a été prise en compte,</h1> 
        <h1 class="bg-success" style="text-align: center">nous vous remercions pour votre fidélité</h1>
       
        <br><br>
    </div>

    <div class="container bg-secondary text-center">
        <br><br>
        <a class="btn btn-danger" href="./index.php">Retour à l'accueil</a>
        <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role']<3) {?>
        <a class="btn btn-danger" href="./index.php?action=tableau&tableau=tableauMbr">Retour au tableau</a>
        <?php }?>
        <br><br>
    </div>
    <br><br>
    <?php

$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>