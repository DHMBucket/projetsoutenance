<?php  $contenu = ob_start(); ?>

<?php 
    require_once('./communs/connect.php');    
    ?>


<br><br><br><br>
    <div class="container bg-warning">
        <br><br>
        <h1 class="bg-warning" style="text-align: center">Erreur de création de livre !</h1> 
        <h1 class="bg-warning" style="text-align: center">Nom Livre ou Revue déjà enregistrée</h1>
        <br><br>
    </div>

    <div class="container bg-secondary text-center">
        <br><br>
        <?php 
        $_GET['action']="register";
        $_GET['register']="livre";
        ?>
        <a class="btn btn-danger" href="./index.php?action=<?=$_GET['action']?>&register=<?=$_GET['register']?>">Revenir à la page d'enregistrement</a>
        <br><br>
    </div>

<?php
    echo "<br>";
    $contenu = ob_get_clean();
    require_once('./views/gabarit.php');

?>
