<?php  $contenu = ob_start(); ?>

    <div class="container text-center" style="position:relative; background-color:lightgreen ">
        <br><br><br>   
        <h1 class="h2">Edition du Panier (id = <?= $donnPan[0]->getId_panier(); ?>)</h1>
        <form action="" method="GET" enctype="multipart/form-data">
            <div class="form-row align-items-center">
            <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Date : '.date("d-m-Y", strtotime($donnPan[0]->getDate_created()));?>"
                    value="<?php echo 'Date : '.date("d-m-Y", strtotime($donnPan[0]->getDate_created()));?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Id_client : '.$donnPan[0]->getId_client();?>"
                    value="<?php echo 'Id_client : '.$donnPan[0]->getId_client();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php 
                        if ($donnPan[0]->getType_client() == 0 || $donnPan[0]->getType_client() == 6) {echo 'Type client : Non inscrit';}
                        if ($donnPan[0]->getType_client() == 1) {echo 'Type client : Administrateur ';}
                        if ($donnPan[0]->getType_client() == 2) {echo 'Type client : Gestionnaire ';}
                        if ($donnPan[0]->getType_client() == 3) {echo 'Type client : Chercheur ';}
                        if ($donnPan[0]->getType_client() == 4) {echo 'Type client : Adhérent ';}
                        if ($donnPan[0]->getType_client() == 5) {echo 'Type client : Membre(gratuit) ';}
                    ?>"
                    value="<?php 
                        if ($donnPan[0]->getType_client() == 0 || $donnPan[0]->getType_client() == 6) {echo 'Type client : Non inscrit';}
                        if ($donnPan[0]->getType_client() == 1) {echo 'Type client : Administrateur ';}
                        if ($donnPan[0]->getType_client() == 2) {echo 'Type client : Gestionnaire ';}
                        if ($donnPan[0]->getType_client() == 3) {echo 'Type client : Chercheur ';}
                        if ($donnPan[0]->getType_client() == 4) {echo 'Type client : Adhérent ';}
                        if ($donnPan[0]->getType_client() == 5) {echo 'Type client : Membre(gratuit) ';}
                    ?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Id_livre1 : '.$donnPan[0]->getId_livre1();?>"
                    value="<?php echo 'Id_livre1 : '.$donnPan[0]->getId_livre1();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Ref_livre1 : '.$donnPan[0]->getRef_livre1();?>"
                    value="<?php echo 'Ref_livre1 : '.$donnPan[0]->getRef_livre1();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <img src="<?='./assets/images/'.$donnPan[0]->getImg_livre1();?>" style="width:25%;" alt="<?=$donnPan[0]->getImg_livre1();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Titre livre1 : '.$donnPan[0]->getTtr_livre1();?>"
                    value="<?php echo 'Titre livre1 : '.$donnPan[0]->getTtr_livre1();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Id_article1 : '.$donnPan[0]->getId_artcl1();?>"
                    value="<?php echo 'Id_article1 : '.$donnPan[0]->getId_artcl1();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Titre article1 : '.$donnPan[0]->getTtr_artcl1();?>"
                    value="<?php echo 'Titre article1 : '.$donnPan[0]->getTtr_artcl1();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php $px= $donnPan[0]->getPxttc_livre1() + $donnPan[0]->getPxttc_artcl1();
                    echo 'Prix1 TTC : '.$px." ".$donnPan[0]->getMonnaie();?>"
                    value="<?php $px= $donnPan[0]->getPxttc_livre1() + $donnPan[0]->getPxttc_artcl1();
                    echo 'Prix1 TTC : '.$px." ".$donnPan[0]->getMonnaie();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Nombre1 : '.$donnPan[0]->getNbre1();?>"
                    value="<?php echo 'Nombre1 : '.$donnPan[0]->getNbre1();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Frais livraison1 : '.$donnPan[0]->getFrs_livrais1()." ".$donnPan[0]->getMonnaie();?>"
                    value="<?php echo 'Frais livraison1 : '.$donnPan[0]->getFrs_livrais1()." ".$donnPan[0]->getMonnaie();?>">
                </div>



                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Id_livre2 : '.$donnPan[0]->getId_livre2();?>"
                    value="<?php echo 'Id_livre2 : '.$donnPan[0]->getId_livre2();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Ref_livre2 : '.$donnPan[0]->getRef_livre2();?>"
                    value="<?php echo 'Ref_livre2 : '.$donnPan[0]->getRef_livre2();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <img src="<?='./assets/images/'.$donnPan[0]->getImg_livre2();?>" style="width:25%;" alt="<?=$donnPan[0]->getImg_livre2();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Titre livre2 : '.$donnPan[0]->getTtr_livre2();?>"
                    value="<?php echo 'Titre livre2 : '.$donnPan[0]->getTtr_livre2();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Id_article2 : '.$donnPan[0]->getId_artcl2();?>"
                    value="<?php echo 'Id_article2 : '.$donnPan[0]->getId_artcl2();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Titre article2 : '.$donnPan[0]->getTtr_artcl2();?>"
                    value="<?php echo 'Titre article2 : '.$donnPan[0]->getTtr_artcl2();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php $px= $donnPan[0]->getPxttc_livre2() + $donnPan[0]->getPxttc_artcl2();
                    echo 'Prix1 TTC : '.$px." ".$donnPan[0]->getMonnaie();?>"
                    value="<?php $px= $donnPan[0]->getPxttc_livre2() + $donnPan[0]->getPxttc_artcl2();
                    echo 'Prix1 TTC : '.$px." ".$donnPan[0]->getMonnaie();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Nombre2 : '.$donnPan[0]->getNbre2();?>"
                    value="<?php echo 'Nombre2 : '.$donnPan[0]->getNbre2();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Frais livraison1 : '.$donnPan[0]->getFrs_livrais2()." ".$donnPan[0]->getMonnaie();?>"
                    value="<?php echo 'Frais livraison1 : '.$donnPan[0]->getFrs_livrais2()." ".$donnPan[0]->getMonnaie();?>">
                </div>




                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Id_livre3 : '.$donnPan[0]->getId_livre3();?>"
                    value="<?php echo 'Id_livre3 : '.$donnPan[0]->getId_livre3();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Ref_livre3 : '.$donnPan[0]->getRef_livre3();?>"
                    value="<?php echo 'Ref_livre3 : '.$donnPan[0]->getRef_livre3();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <img src="<?='./assets/images/'.$donnPan[0]->getImg_livre3();?>" style="width:25%;" alt="<?=$donnPan[0]->getImg_livre3();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Titre livre3 : '.$donnPan[0]->getTtr_livre3();?>"
                    value="<?php echo 'Titre livre3 : '.$donnPan[0]->getTtr_livre3();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Id_article3 : '.$donnPan[0]->getId_artcl3();?>"
                    value="<?php echo 'Id_article3 : '.$donnPan[0]->getId_artcl3();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Titre article3 : '.$donnPan[0]->getTtr_artcl3();?>"
                    value="<?php echo 'Titre article3 : '.$donnPan[0]->getTtr_artcl3();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php $px= $donnPan[0]->getPxttc_livre3() + $donnPan[0]->getPxttc_artcl3();
                    echo 'Prix1 TTC : '.$px." ".$donnPan[0]->getMonnaie();?>"
                    value="<?php $px= $donnPan[0]->getPxttc_livre3() + $donnPan[0]->getPxttc_artcl3();
                    echo 'Prix1 TTC : '.$px." ".$donnPan[0]->getMonnaie();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Nombre3 : '.$donnPan[0]->getNbre3();?>"
                    value="<?php echo 'Nombre3 : '.$donnPan[0]->getNbre3();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Frais livraison1 : '.$donnPan[0]->getFrs_livrais3()." ".$donnPan[0]->getMonnaie();?>"
                    value="<?php echo 'Frais livraison1 : '.$donnPan[0]->getFrs_livrais3()." ".$donnPan[0]->getMonnaie();?>">
                </div>

                <div class="container bg-info text-center">
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauPan">Retour au tableau</a>
                </div>
        </form>
</div>

<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




