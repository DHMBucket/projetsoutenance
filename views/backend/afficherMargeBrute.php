<?php  $contenu = ob_start(); ?>
<link rel="stylesheet" href="./assets/css/telephone.css">
<script src="./assets/js/formulaireClient.js"></script>


    <div class="container text-center bg-warning" style="position:relative;  ">
        <br><br><br>   
        <h1 class="h2">Marge Brute et Prestations fournies</h1>
        <form action="./index.php?action=synthese&synthese=margebrute" method="POST">

            <div class="form-row align-items-center">
                <div class="col-sm-2 my-1">
                    <label for="datedebut">Date de début</label>
                    <input type="date" class="form-control" id="datedebut" name="datedebut" 
                           placeholder="<?php if (isset($_POST['datedebut'])) { echo $_POST['datedebut']; } else { echo $deb;}?>"
                           value="<?php if (isset($_POST['datedebut'])) { echo $_POST['datedebut']; } else { echo $deb;}?>"
                    required>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="datefin">Date de fin</label>
                    <input type="date" class="form-control" id="datefin" name="datefin" 
                           placeholder="<?php if (isset($_POST['datefin'])) { if (strtotime($_POST['datefin']) < strtotime($_POST['datedebut'])) 
                                                                                  {echo $_POST['datedebut']; 
                                                                                  } else {echo $_POST['datefin']; } 
                                              } else { echo $fn;}?>"
                           value="<?php if (isset($_POST['datefin'])) { if (strtotime($_POST['datefin']) < strtotime($_POST['datedebut'])) 
                                                                                  {echo $_POST['datedebut']; 
                                                                                  } else {echo $_POST['datefin']; }
                                        } else { echo $fn;}?>"
                    required>
                </div>
                <div class="col-sm-4 my-1">
                    <label for="mbCmd">Solde total</label>
                    <input type="text" class="form-control" id="mbCmd" name="mbCmd" 
                           placeholder="<?php 
                                            $solde= $donnMB->getMbCmd()+$donnMB->getMbfrslivr()+$donnMB->getMbAdh();
                                            echo number_format($solde, 2, ',', ' ')." Є";
                                        ?>"
                           value="<?php echo number_format($solde, 2, ',', ' ')." Є";?>"
                    readonly>
                </div>


            </div>
            <div class="form-row align-items-center">
                <div class="col-sm-4 my-1">
                    <label for="mbCmd">Marge Brute des commandes</label>
                    <input type="text" class="form-control" id="mbCmd" name="mbCmd" 
                           placeholder="<?php echo number_format($donnMB->getMbCmd(), 2, ',', ' ')." Є";?>"
                           value="<?php echo number_format($donnMB->getMbCmd(), 2, ',', ' ')." Є";?>"
                    readonly>
                </div>
                <div class="col-sm-4 my-1">
                    <label for="mbFrslivr">Prestations des frais de livraison</label>
                    <input type="text" class="form-control" id="mbFrslivr" name="mbFrslivr" 
                           placeholder="<?php echo number_format($donnMB->getMbfrslivr(), 2, ',', ' ')." Є";?>"
                           value="<?php echo number_format($donnMB->getMbfrslivr(), 2, ',', ' ')." Є";?>"
                    readonly>
                </div>
                <div class="col-sm-4 my-1">
                    <label for="mbMbr">Marge Brute Inscription de membres</label>
                    <input type="text" class="form-control" id="mbMbr" name="mbMbr" 
                           placeholder="<?php echo number_format($donnMB->getMbAdh(), 2, ',', ' ')." Є";?>"
                           value="<?php echo number_format($donnMB->getMbAdh(), 2, ',', ' ')." Є";?>"
                    readonly>
                </div>


            </div>
           
                <div class="container bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="calculMB" name="calculMB">Calculer la Marge Brute</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauLvR">Retour au tableau</a>
                </div>
        </form>
</div>
<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




