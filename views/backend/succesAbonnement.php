<?php  $contenu = ob_start(); ?>

<?php 
    require_once('./communs/connect.php');
    
    ?>
    <br><br><br><br>
    <div class="container bg-success">
        <br><br>
        <h1 class="bg-success" style="text-align: center">Votre abonnement a été prise en compte,</h1> 
        <h1 class="bg-success" style="text-align: center">l'ordre de livraison a été effectué pour 4 revues</h1>
        <h1 class="bg-success" style="text-align: center">nous vous remercions pour votre fidélité</h1>
       
        <br><br>
    </div>

    <div class="container bg-secondary text-center">
        <br><br>
        <a class="btn btn-danger" href="./index.php">Retour à l'accueil</a>
        <br><br>
    </div>
    <br><br>
    <?php

$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>