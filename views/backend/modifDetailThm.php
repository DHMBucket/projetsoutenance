<?php  $contenu = ob_start(); ?>

    <div class="container text-center bg-warning" style="position:relative;  ">
        <br><br><br>   
        <h1 class="h2">Modification de thème</h1>
        <form action="./index.php?action=modifierThm&modifierThm" method="POST" enctype="multipart/form-data">
            <div class="form-row align-items-center">
            <div class="col-sm-1 my-1">
                <label for="idl">Id Theme</label>
                <input type="text" class="form-control" id="idl" name="idl" 
                        placeholder="<?=(int)$donnThm[0]->getId_theme()?>"
                        value="<?=(int)$donnThm[0]->getId_theme();?>"
                readonly>
            </div>
            <div class="col-sm-4 my-1">
                <label for="nom">Nom de la Theme</label>
                <input type="text" class="form-control" id="nom" name="nom" 
                        placeholder="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo $donnThm[0]->getNom_theme();}?>"
                        value="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo $donnThm[0]->getNom_theme();}?>"
                required>
            </div>
           
            </div>
            <div class="form-row align-items-center">
            </div>
            <br><br><br>
                <div class="container bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="modifthm" name="modifthm">Enregistrer</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauThm">Retour au tableau</a>
                </div>
        </form>
</div>
<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




