<?php  $contenu = ob_start(); ?>

    <div class="container text-center" style="position:relative; background-color:lightgreen ">
        <br><br><br>   
        <h1 class="h2">Edition du client anonyme (id = <?= $donnCti[0]->getId_clt_anonym(); ?>)</h1>
        <form action="" method="GET" enctype="multipart/form-data">
            <div class="form-row align-items-center">
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Num client : '.$donnCti[0]->getNum_compteur();?>"
                    value="<?php echo 'Num client : '.$donnCti[0]->getNum_compteur();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php 
                        if ($donnCti[0]->getCivilite() == 1) { echo 'Civilité : Monsieur';} 
                            else { echo 'Civilité : Madame';} ?>"
                    value="<?php 
                        if ($donnCti[0]->getCivilite() == 1) { echo 'Civilité : Monsieur';} 
                            else { echo 'Civilité : Madame';} ?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Nom destin. : '.$donnCti[0]->getNom_destin();?>"
                    value="<?php echo 'Nom destin. : '.$donnCti[0]->getNom_destin();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Prénom destin. : '.$donnCti[0]->getPrenom_destin();?>"
                    value="<?php echo 'Prénom destin. : '.$donnCti[0]->getPrenom_destin();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Institution : '.$donnCti[0]->getInstitution();?>"
                    value="<?php echo 'Institution : '.$donnCti[0]->getInstitution();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Fonction : '.$donnCti[0]->getFonction();?>"
                    value="<?php echo 'Fonction : '.$donnCti[0]->getFonction();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Email : '.$donnCti[0]->getEmail();?>"
                    value="<?php echo 'Email : '.$donnCti[0]->getEmail();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php 
                        if ($donnCti[0]->getOffre_partenaire() == 1) { echo 'Offre parten. : Oui';} 
                        else { echo 'Offre parten. : Non';} ?>"
                value="<?php 
                        if ($donnCti[0]->getOffre_partenaire() == 1) { echo 'Offre parten. : Oui';} 
                            else { echo 'Offre parten. : Non';} ?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php 
                        if ($donnCti[0]->getOffre_newsletter() == 1) { echo 'Newsletter : Oui';} 
                        else { echo 'Newsletter : Non';} ?>"
                value="<?php 
                        if ($donnCti[0]->getOffre_newsletter() == 1) { echo 'Newsletter : Oui';} 
                            else { echo 'Newsletter : Non';} ?>">
                </div>

                <div class="col-sm-6 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Adresse : '.$donnCti[0]->getAdr_livrais();?>"
                    value="<?php echo 'Adresse : '.$donnCti[0]->getAdr_livrais();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Code Postal : '.$donnCti[0]->getCode_Postal();?>"
                    value="<?php echo 'Code Postal : '.$donnCti[0]->getCode_Postal();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Ville : '.$donnCti[0]->getVille();?>"
                    value="<?php echo 'Ville : '.$donnCti[0]->getVille();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Pays : '.$donnCti[0]->getPays();?>"
                    value="<?php echo 'Pays : '.$donnCti[0]->getPays();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Zip état : '.$donnCti[0]->getZip_etat();?>"
                    value="<?php echo 'Zip état : '.$donnCti[0]->getZip_etat();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Téléphone : '.$donnCti[0]->getIndicatif()." ".$donnCti[0]->getTelephone();?>"
                    value="<?php echo 'Téléphone : '.$donnCti[0]->getIndicatif()." ".$donnCti[0]->getTelephone();?>">
                </div>
              
 
                <div class="container bg-info text-center">
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauCti">Retour au tableau</a>
                </div>
        </form>
</div>

<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




