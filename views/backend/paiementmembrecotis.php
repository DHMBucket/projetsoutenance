<?php  $contenu = ob_start(); ?>
<?php 
    require_once('./communs/connect.php');
           
    ?>

<div class="container bg-secondary text-center">
        
        <h1 class="h2">Procédez au paiement de votre cotisation annuelle</h1>
        
        <p>
                        <span class="font-weight-bolder">Montant de la cotisation&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;:&nbsp;&nbsp;</span>&emsp;&nbsp;
                        <span class="btn btn-success" style="font-size: xx-large;"><?=" ".$ajoutMbr->getPrix_membre()." ".$ajoutMbr->getMonnaie();?></span>
                    </p>
        
</div>

    <br>
    <div class="container bg-secondary text-center">
    <div class="row">
        <div class="col-12 btn border border-secondary btn-warning">
        <form action="./index.php?action=paiementacceptAdherCherch&paiementacceptAdherCherch" method="post">
            <input type="hidden" name="px" value="<?=$ajoutMbr->getPrix_membre()?>">
            <script
                src="https://checkout.stripe.com/checkout.js"
                class="stripe-button"
                data-key="pk_test_5aWar4ue4dUmOTHpA23NSoIh00CIWzuAC6"
                data-name="boutiqueiris"
                data-description="Adhésion"
                data-amount=<?=($ajoutMbr->getPrix_membre())*100  // montant à débiter ?>     
                data-currency=<?php if ($ajoutMbr->getMonnaie()=="Є") { echo "'eur'";} else { if ($ajoutMbr->getMonnaie()=="$") { echo "'usd'";} else {echo "'gbp'";} } ?> 
                data-locale= "auto"
                data-label="Paiement par carte">
            </script>
        </form>
    </div>
    </div>
    </div>

    <div class="container bg-secondary text-center">
        <br>
        <a class="btn btn-danger" href="./index.php">Retour à l'accueil</a>
        <br>
    </div>

<?php

$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>
