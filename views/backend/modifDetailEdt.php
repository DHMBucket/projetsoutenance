<?php  $contenu = ob_start(); ?>
<script src="./assets/js/formulaireEditeur.js"></script>


    <div class="container text-center bg-warning" style="position:relative;  ">
        <br><br><br>   
        <h1 class="h2">Formulaire d'ajout d'un éditeur</h1>
        <form action="./index.php?action=modifierEdt&modifierEdt" method="POST">
            <div class="form-row align-items-center">
                <div class="col-sm-1 my-1">
                    <label for="idl">Id Editeur</label>
                    <input type="text" class="form-control" id="idl" name="idl" 
                           placeholder="<?=(int)$donnEdt[0]->getId_editeur()?>"
                           value="<?=(int)$donnEdt[0]->getId_editeur();?>"
                           readonly>
                </div>
                <div class="col-sm-3 my-1">
                    <label for="code">Code éditeur</label>
                    <input type="text" class="form-control" id="code" name="code" 
                           placeholder="<?php if (isset($_POST['code'])) { echo $_POST['code']; } else { echo $donnEdt[0]->getCode_editeur();}?>"
                           value="<?php if (isset($_POST['code'])) { echo $_POST['code']; } else { echo $donnEdt[0]->getCode_editeur();}?>"
                           readonly>
                </div>

                <div class="col-sm-3 my-1">
                    <label for="nom">Nom éditeur</label>
                    <input type="text" class="form-control" id="nom" name="nom" 
                           placeholder="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo $donnEdt[0]->getNom_editeur();}?>"
                           value="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo $donnEdt[0]->getNom_editeur();}?>"
                           required>
                </div>

                <div class="col-sm-3 my-1">
                    <label for="localite">localité</label>
                    <input type="text" class="form-control" id="localite" name="localite" 
                           placeholder="<?php if (isset($_POST['localite'])) { echo $_POST['localite']; } else { echo $donnEdt[0]->getLocalite_editeur();}?>"
                           value="<?php if (isset($_POST['localite'])) { echo $_POST['localite']; } else { echo $donnEdt[0]->getLocalite_editeur();}?>"
                           required>
                </div>

                <div class="col-sm-3 my-1">
                    <label for="pays">Pays</label>
                    <input type="text" class="form-control" id="pays" name="pays" 
                           placeholder="<?php if (isset($_POST['pays'])) { echo $_POST['pays']; } else { echo $donnEdt[0]->getPays_editeur();}?>"
                           value="<?php if (isset($_POST['pays'])) { echo $_POST['pays']; } else { echo $donnEdt[0]->getPays_editeur();}?>"
                           required>
                </div>

                <div class="col-sm-2 my-1">
                    <label for="frslivr">Frais de livraison en Euro</label>
                    <input type="number" class="form-control" id="frslivr" name="frslivr" 
                            placeholder="<?php if (isset($_POST['frslivr'])) { echo $_POST['frslivr']; } else { echo (int)$donnEdt[0]->getFrs_livrais();}?>"
                            value="<?php if (isset($_POST['frslivr'])) {
                                                if ((int)$_POST['frslivr'] >= 0) {
                                                    echo $_POST['frslivr'];
                                                } else { echo 0;}  
                                        } else { echo (int)$donnEdt[0]->getFrs_livrais();}?>"
                            required>
                </div>


 
                <div class="container bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="modifedt" name="modifedt">Enregistrer</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauEdt">Retour au tableau</a>
                </div>
        </form>
</div>
<script> $('.countrypicker').countrypicker(); </script>
<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




