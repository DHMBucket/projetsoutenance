<?php  $contenu = ob_start(); ?>
<script src="./assets/js/formulaireAuteur.js"></script>

    <div class="container text-center bg-warning" style="position:relative;  ">
        <br><br><br>   
        <h1 class="h2">Formulaire d'ajout d'un auteur ou rédacteur</h1>
        <form action="./index.php?action=modifierAut&modifierAut" method="POST" enctype="multipart/form-data">
            <div class="form-row align-items-center">
                <div class="col-sm-1 my-1">
                    <label for="idl">Id Livre</label>
                    <input type="text" class="form-control" id="idl" name="idl" 
                           placeholder="<?=(int)$donnAut[0]->getId_auteur()?>"
                           value="<?=(int)$donnAut[0]->getId_auteur();?>"
                           readonly>
                </div>
                <div class="offset-1 col-sm-3 my-1">
                    <label for="nom">Nom</label>
                    <input type="text" class="form-control" id="nom" name="nom" 
                           placeholder="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo $donnAut[0]->getNom();}?>"
                           value="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo $donnAut[0]->getNom();}?>"
                           readonly>
                </div>

                <div class="col-sm-3 my-1">
                    <label for="prenom">Prénom</label>
                    <input type="text" class="form-control" id="prenom" name="prenom" 
                           placeholder="<?php if (isset($_POST['prenom'])) { echo $_POST['prenom']; } else { echo $donnAut[0]->getPrenom();}?>"
                           value="<?php if (isset($_POST['prenom'])) { echo $_POST['prenom']; } else { echo $donnAut[0]->getPrenom();}?>"
                           readonly>
                </div>

                <div class="col-sm-4 my-1">
                    <label for="institution">Institution</label>
                    <input type="text" class="form-control" id="institution" name="institution" 
                           placeholder="<?php if (isset($_POST['institution'])) { echo $_POST['institution']; } else { echo $donnAut[0]->getInstitution();}?>"
                           value="<?php if (isset($_POST['institution'])) { echo $_POST['institution']; } else { echo $donnAut[0]->getInstitution();}?>"
                           >
                </div>
            </div>

            <div class="form-row align-items-center">
                <div class="offset-1 col-sm-5">
                    <small id="imgHelp" class="form-text text-muted">Les fichiers acceptés sont de type .png, .bmp ou .jpg</small>
                    <input type="hidden" name="max_file_size" value="1000000000" />
                    <input type="file" class="form-control" id="image1" name="image1"
                    placeholder="<?php if (isset($_FILES['image1']['name'])) { echo $_FILES['image1']['name']; } else { echo $donnAut[0]->getPhoto();}?>"
                    value="<?php if (isset($_FILES['image1']['name'])) { echo $_FILES['image1']['name']; } else { echo $donnAut[0]->getPhoto();}?>">                    
                </div>

                <div class="offset-1 col-sm-4 my-1">
                    <label for="fonction">Fonction</label>
                    <input type="text" class="form-control" id="fonction" name="fonction" 
                           placeholder="<?php if (isset($_POST['fonction'])) { echo $_POST['fonction']; } else { echo $donnAut[0]->getFonction();}?>"
                           value="<?php if (isset($_POST['fonction'])) { echo $_POST['fonction']; } else { echo $donnAut[0]->getFonction();}?>"
                           >
                </div>
            </div>
 
                <div class="container bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="modifaut" name="modifaut">Enregistrer</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauAut">Retour au tableau</a>
                </div>
        </form>
</div>
<script> $('.countrypicker').countrypicker(); </script>
<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




