<?php 
    $contenu= ob_start();
    require_once('./communs/connect.php');
    require_once('./controllers/backend/PanierController.php');

?>

<script type="text/javascript" src="./assets/js/affichetabselectionScript.js">
</script>


<br><br>
<h1 class="h2" style="text-align: center;caption-side: top; font-weight: bolder">Votre Panier</h1>
    <br>

<div id="navBoard" class="row border " style="margin: auto; align-items: center; justify-content: center; ">
    <div class="col-sm-6 m-0 p-0 border">        
        <form action="
        <?php
        if (isset($_SESSION['Auth']['pseudo'])) {
            echo "./index.php?action=passercommande&passercommande";
        } else {
            echo "./index.php?action=passercommande&enregistrcoordinv";
        }
        ?>
        " class="form-inline col" method="POST">
            <button type="submit" class="btn btn-primary" id="passercommande" name="passercommande">Passer la commande</button>   
        </form>    
    </div>
    <div class="col-sm-6 m-0 p-0 border">    
    <?php if (count($dataA) > 0) { ?>    
        <h3 class="h4">Prix du panier&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;= <?php 
            
            $px1=0;$px2=0;$px3=0;$pxtt=0;$pxtaxe=0;$frstt=0;
            
            if ((int)$dataA[0]->getId_livre1() > 1) { if ((int)$dataA[0]->getId_artcl1() > 1) {$px1= (int)$dataA[0]->getPxttc_artcl1()*(int)$dataA[0]->getNbre1();}
                                                else {$px1= (int)$dataA[0]->getPxttc_livre1()*(int)$dataA[0]->getNbre1();}
            } else { if ((int)$dataA[0]->getId_artcl1() > 1) {$px1= (int)$dataA[0]->getPxttc_artcl1()*(int)$dataA[0]->getNbre1();}}
            
            if ((int)$dataA[0]->getId_livre2() > 1) { if ((int)$dataA[0]->getId_artcl2() > 1) {$px2= (int)$dataA[0]->getPxttc_artcl2()*(int)$dataA[0]->getNbre2();}
                                                else {$px2= (int)$dataA[0]->getPxttc_livre2()*(int)$dataA[0]->getNbre2();}
            } else { if ((int)$dataA[0]->getId_artcl2() > 1) {$px2= (int)$dataA[0]->getPxttc_artcl2()*(int)$dataA[0]->getNbre2();}}

            if ((int)$dataA[0]->getId_livre3() > 1) { if ((int)$dataA[0]->getId_artcl3() > 1) {$px3= (int)$dataA[0]->getPxttc_artcl3()*(int)$dataA[0]->getNbre3();}
                                                else {$px3= (int)$dataA[0]->getPxttc_livre3()*(int)$dataA[0]->getNbre3();}
            } else { if ((int)$dataA[0]->getId_artcl3() > 1) {$px3= (int)$dataA[0]->getPxttc_artcl3()*(int)$dataA[0]->getNbre3();}}
            
            $pxtt=$px1+$px2+$px3; $pxtaxe=$pxtt*.05; 
            
            $frstt=(int)$dataA[0]->getFrs_livrais1()+(int)$dataA[0]->getFrs_livrais2()+(int)$dataA[0]->getFrs_livrais3();

            echo ($pxtt+$frstt)." ".$dataA[0]->getMonnaie(); $_SESSION['pxtotal']=$pxtt+$frstt;
        ?></h3>
        <h3 class="h4">dont taxes&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;= <?php echo $pxtaxe." ".$dataA[0]->getMonnaie(); ?> </h3>  
        <h3 class="h4">dont frais de livraison&nbsp;&nbsp;&nbsp;&nbsp;= <?php echo $frstt." ".$dataA[0]->getMonnaie(); ?> </h3>  
    <?php } ?>
    </div>
</div>


<table class="table table-striped table-bordered">
          
            <thead>
                <tr>
                    <th class="text-center">Référence</th>
                    <th class="text-center">Titre</th>
                    <th class="text-center">Image</th>
                    <th class="text-center">Prix</th>
                    <th class="text-center">Nombre</th>
                    <th class="text-center">Frais de livraison</th>
                    <th class="text-center">Actions</th><!--colspan="3"-->
                </tr>
            </thead>
            <tbody>
            <?php
             if (count($dataA) > 0) { 
                for ($i=0; $i<3; $i++) {
                    if ($i==0) {
                        $id_livre   = (int)$dataA[0]->getId_livre1();
                        $ref_livre  = $dataA[0]->getRef_livre1();
                        $ttr_livre  = $dataA[0]->getTtr_livre1();
                        $img_livre  = $dataA[0]->getImg_livre1();
                        $pxttc_livre= (int)$dataA[0]->getPxttc_livre1();
                        $id_artcl   = (int)$dataA[0]->getId_artcl1();
                        $ttr_artcl  = $dataA[0]->getTtr_artcl1();
                        $pxttc_artcl= (int)$dataA[0]->getPxttc_artcl1();
                        if ((count($dataL1) > 0) && $dataA[0]->getNbre1() == ($dataL1[0]->getStock_iris()+$dataL1[0]->getStock_editeur()))
                            {$plus=0;} else {$plus=1;}
                        $nbre       = (int)$dataA[0]->getNbre1();
                        $frs_livrais= (int)$dataA[0]->getFrs_livrais1();
                    }
                    if ($i==1) {
                        $id_livre   = (int)$dataA[0]->getId_livre2();
                        $ref_livre  = $dataA[0]->getRef_livre2();
                        $ttr_livre  = $dataA[0]->getTtr_livre2();
                        $img_livre  = $dataA[0]->getImg_livre2();
                        $pxttc_livre= (int)$dataA[0]->getPxttc_livre2();
                        $id_artcl   = (int)$dataA[0]->getId_artcl2();
                        $ttr_artcl  = $dataA[0]->getTtr_artcl2();
                        $pxttc_artcl= (int)$dataA[0]->getPxttc_artcl2();
                        if ((count($dataL2) > 0) && $dataA[0]->getNbre2() == ($dataL2[0]->getStock_iris()+$dataL2[0]->getStock_editeur()))
                            {$plus=0;} else {$plus=1;}
                        $nbre       = (int)$dataA[0]->getNbre2();
                        $frs_livrais= (int)$dataA[0]->getFrs_livrais2();
                    }
                    if ($i==2) {
                        $id_livre   = (int)$dataA[0]->getId_livre3();
                        $ref_livre  = $dataA[0]->getRef_livre3();
                        $ttr_livre  = $dataA[0]->getTtr_livre3();
                        $img_livre  = $dataA[0]->getImg_livre3();
                        $pxttc_livre= (int)$dataA[0]->getPxttc_livre3();
                        $id_artcl   = (int)$dataA[0]->getId_artcl3();
                        $ttr_artcl  = $dataA[0]->getTtr_artcl3();
                        $pxttc_artcl= (int)$dataA[0]->getPxttc_artcl3();
                        if ((count($dataL3) > 0) && $dataA[0]->getNbre3() == ($dataL3[0]->getStock_iris()+$dataL3[0]->getStock_editeur()))
                            {$plus=0;} else {$plus=1;}
                        $nbre       = (int)$dataA[0]->getNbre3();
                        $frs_livrais= (int)$dataA[0]->getFrs_livrais3();
                    }
                    if ($id_livre > 1 || $id_artcl > 1) {
                    
                ?>
                    <tr>
                    <td class="text-center"><?php if ($id_livre > 1) {echo $ref_livre;} ?></td>
                    <td class="text-center">
                        <?php 
                            if ($id_livre > 1) {
                                echo $ttr_livre;
                                if ($id_artcl > 1) {echo " / ".$ttr_artcl;}
                            } 
                            else {
                                if ($id_artcl > 1) {echo $ttr_artcl;}
                            }
                        ?>
                    </td>
                    <td class="text-center">
                        <?php
                            if ( $img_livre <> "" ) {
                        ?>
                        <img src="<?php echo "./assets/images/".$img_livre;?>" style="width:100px; height: 100px" alt="<?php echo $img_livre;?>">
                        <?php } ?>
                    </td>
                    <td class="text-center">
                        <?php 
                            if ($id_livre > 1) {
                                if ($id_artcl > 1) 
                                    {echo $pxttc_artcl;}
                                else {echo $pxttc_livre;}
                            } 
                            else {
                                if ($id_artcl > 1) {echo $pxttc_artcl;}
                            }
                        ?>
                    </td>
                    <td class="text-center"><?php echo $nbre; ?></td>
                    <td class="text-center"><?php echo $frs_livrais; ?></td>
                        <td class="text-center">
                            <a href='./index.php?action=changernbrepanier&pos=<?=$i;?>&diminuernbrepanier=""' class="btn btn-primary"><i class="fas fa-minus-square"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                            <a onclick="return confirm('Etes-vous sûr de vouloir supprimer cette ligne ?')" href="./index.php?action=changernbrepanier&pos=<?=$i;?>&supprimerPan='0'" class="btn btn-danger"><i class="fas fa-trash"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                            <a  
                            <?php 
                                if ($plus==1) { 
                                        echo 'style="background-color: green;"';
                                      } else {
                                        echo 'style="background-color: aqua; color: red; pointer-events: none; cursor: default;"';
                                      }  
                            ?> 
                            href='./index.php?action=changernbrepanier&pos=<?=$i;?>&augmenternbrepanier=""' class="btn btn-success"><i class="fas fa-plus-square"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                        </td>
                    </tr>
                    <?php } } }
            ?>

            </tbody>
        </table>

               
        <?php
$contenu= ob_get_clean();

?>