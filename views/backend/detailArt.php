<?php  $contenu = ob_start(); ?>

    <div class="container text-center" style="position:relative; background-color:lightgreen ">
        <br><br><br>   
        <h1 class="h2">Edition de l'article (id = <?= $donnArt[0]->getId_article(); ?>)</h1>
        <form action="" method="GET" enctype="multipart/form-data">
            <div class="form-row align-items-center">
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Titre : '.$donnArt[0]->getTitre();?>"
                    value="<?php echo 'Titre : '.$donnArt[0]->getTitre();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Thème : '.$donnArt[0]->getTheme_article();?>"
                    value="<?php echo 'Thème : '.$donnArt[0]->getTheme_article();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Id livre : '.$donnArt[0]->getId_livre();?>"
                    value="<?php echo 'Id livre : '.$donnArt[0]->getId_livre();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Réf. livre : '.$donnArt[0]->getRef_livre();?>"
                    value="<?php echo 'Réf. livre : '.$donnArt[0]->getRef_livre();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Emplacement article : '.$donnArt[0]->getEmplacement();?>"
                    value="<?php echo 'Emplacement article : '.$donnArt[0]->getEmplacement();?>">
                </div>
                <div class="col-sm-5 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Auteur1 : '.$donnArt[0]->getAuteur1_nom()." ".$donnArt[0]->getAuteur1_prenom()." ".$donnArt[0]->getAuteur1_institution();?>"
                    value="<?php echo 'Auteur1 : '.$donnArt[0]->getAuteur1_nom()." ".$donnArt[0]->getAuteur1_prenom()." ".$donnArt[0]->getAuteur1_institution();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Auteur2 : '.$donnArt[0]->getAuteur2_nom()." ".$donnArt[0]->getAuteur2_prenom()." ".$donnArt[0]->getAuteur2_institution();?>"
                    value="<?php echo 'Auteur2 : '.$donnArt[0]->getAuteur2_nom()." ".$donnArt[0]->getAuteur2_prenom()." ".$donnArt[0]->getAuteur2_institution();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Earticle : '.$donnArt[0]->getFichier_earticle();?>"
                    value="<?php echo 'Earticle : '.$donnArt[0]->getFichier_earticle();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Nbre Pages : '.$donnArt[0]->getNbre_pages();?>"
                    value="<?php echo 'Nbre Pages : '.$donnArt[0]->getNbre_pages();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Nbre mots-clés : '.$donnArt[0]->getNbre_mots_pert();?>"
                    value="<?php echo 'Nbre mots-clés : '.$donnArt[0]->getNbre_mots_pert();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Prix : '.$donnArt[0]->getPrix()." ".$donnArt[0]->getMonnaie();?>"
                    value="<?php echo 'Prix : '.$donnArt[0]->getPrix()." ".$donnArt[0]->getMonnaie();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Support : '.$donnArt[0]->getType_support();?>"
                    value="<?php echo 'Support : '.$donnArt[0]->getType_support();?>">
                </div>

                <div class="container bg-info text-center">
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauArt">Retour au tableau</a>
                </div>
        </form>
</div>

<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




