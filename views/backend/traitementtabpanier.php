<?php 
    $contenu= ob_start();
    require_once('./communs/connect.php');
    require_once('./controllers/backend/PanierController.php');

?>

<script type="text/javascript" src="./assets/js/affichetabselectionScript.js">
</script>


<br><br>
<h1 class="h2" style="text-align: center;caption-side: top; font-weight: bolder">Situation des Paniers</h1>
    <br>

<div id="navBoard" class="row border " style="margin: auto; align-items: center; justify-content: center; ">
    <div class="col-sm-4 m-0 p-0 border">
        <div class="container">
            <nav class="navbar navbar-light bg-light m-0 p-0 ">
                <form action="./index.php?action=search&search=searchTabPan" class="form-inline" method="POST">
                    <input class="form-control mr-sm-2" type="search" id="sequence" name="sequence" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" id="seqrech" name="seqrech" type="submit">Recherche dans titre</button>
                </form>
            </nav>
        </div>
    </div>
    <?php
        
        if (isset($_SESSION['Auth']) && isset($_SESSION['Auth']['role']) && (int)$_SESSION['Auth']['role']<5) {
    ?>
    <div class="col-sm-8 m-0 p-0 border">
        
        <form action="./index.php?action=searchCrit&searchCrit=searchTabCritPan" class="form-inline col" method="POST">
            <div class="container row border" style="margin: auto;">               
                <div class="col-sm-2 m-0 p-0 text-center" >
                    <label for="idsearch">par Id</label>
                    <select class="form-control" id="idsearch" name="idsearch">
                        <option value=0>0</option>
                        <?php $i=1; foreach ($idData as $idVal) {?>
                            <option value=<?php echo $idVal;?>><?php echo $idVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($idData as $idVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="typsearch">client ?</label>
                    <select class="form-control" id="typsearch" name="typsearch">
                        <option value="-1"></option>
                        <?php $i=1; foreach ($typData as $typVal) {?>
                            <option value=<?php echo $typVal;?>><?php 
                                if ($typVal == "0") {echo $typVal."- non inscrit";}
                                if ($typVal == "1") {echo $typVal."- administr.";}
                                if ($typVal == "2") {echo $typVal."- Gestionn.";}
                                if ($typVal == "3") {echo $typVal."- Chercheur";}
                                if ($typVal == "4") {echo $typVal."- Adhérent";}
                                if ($typVal == "5") {echo $typVal."- Membre";}
                                $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($typData as $typVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$typVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-2 m-0 p-0 text-center" >
                    <label for="idcsearch">Id client</label>
                    <select class="form-control" id="idcsearch" name="idcsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($idcData as $idcVal) {?>
                            <option value=<?php echo $idcVal;?>><?php echo $idcVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($idcData as $idcVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idcVal; $i++;?></p>
                    <?php } ?>
                </div>
                </div>
                <div class="container row border" style="margin: auto;">               
                <div class="col-sm-2 m-0 p-0 text-center" >
                    <label for="idlsearch">Id Livre</label>
                    <select class="form-control" id="idlsearch" name="idlsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($idlData as $idlVal) {?>
                            <option value=<?php echo $idlVal;?>><?php echo $idlVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($idlData as $idlVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idlVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="rflsearch">Réf. Livre</label>
                    <select class="form-control" id="rflsearch" name="rflsearch">
                        <option value="-1"></option>
                        <?php $i=1; foreach ($rflData as $rflVal) {?>
                            <option value=<?php echo $rflVal;?>><?php echo $rflVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($rflData as $rflVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$rflVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-2 m-0 p-0 text-center" >
                    <label for="idasearch">Id article</label>
                    <select class="form-control" id="idasearch" name="idasearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($idaData as $idaVal) {?>
                            <option value=<?php echo $idaVal;?>><?php echo $idaVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($idaData as $idaVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idaVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="ttrsearch">Titre</label>
                    <select class="form-control" id="ttrsearch" name="ttrsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($ttrData as $ttrVal) {?>
                            <option value=<?php echo '"'.$ttrVal.'"';?>><?php echo $ttrVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($ttrData as $ttrVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$ttrVal; $i++;?></p>
                    <?php } ?>
                </div>

                <div class="col-sm-2 m-0 p-0 bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="filtrsearchPan" name="filtrsearchPan">Filtrage</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauPan">Réinitialiser</a>
                </div>

            </div>
        </form>
        
        </div>
        <?php
        }
        ?>

</div>


<table class="table table-striped table-bordered">
          
            <thead>
                <tr>

                    <th class="text-center">Numéro</th>
                    <th class="text-center">Type & Id Client</th>
                    <th class="text-center">Id + Réf + Titre + Nbre livr1/Artcl1</th>
                    <th class="text-center">Id + Réf + Titre + Nbre livr2/Artcl2</th>
                    <th class="text-center">Id + Réf + Titre + Nbre livr3/Artcl3</th>
                    <th class="text-center">Prix ttc + frs livr.</th>
                    <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] == 1) { ?>
                        <th class="text-center">Actions</th><!--colspan="3"-->
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
            <?php
                foreach ($dataA as $donPan) {
                    
                    
                ?>
                    <tr>
                        <td class="text-center"><?php echo $donPan->getId_panier();?></td>
                        <td class="text-center"><?php 
                            if ($donPan->getType_client() == 0 || $donPan->getType_client() == 6) {echo "Non inscrit / ";}
                            if ($donPan->getType_client() == 1) {echo "Administr. / ";}
                            if ($donPan->getType_client() == 2) {echo "Gestionn. / ";}
                            if ($donPan->getType_client() == 3) {echo "Chercheur / ";}
                            if ($donPan->getType_client() == 4) {echo "Adhérent / ";}
                            if ($donPan->getType_client() == 5) {echo "Membre / ";}
                            echo $donPan->getId_client();
                            ?>
                        </td>
                        <td class="text-center"><?php 
                            if ($donPan->getTtr_livre1() <> "") {echo "<b>Livre :</b><br>".$donPan->getId_livre1()." / ".$donPan->getRef_livre1()." /<br>".$donPan->getTtr_livre1();}
                            if ($donPan->getTtr_artcl1() <> "") {echo "<br><b>Article :</b><br>".$donPan->getId_artcl1()." /<br>".$donPan->getTtr_artcl1();}
                            if ($donPan->getNbre1() <> 0) {echo "<br>Nbre = ".$donPan->getNbre1();}                      
                            ?>
                        </td>
                        <td class="text-center"><?php 
                            if ($donPan->getTtr_livre2() <> "") {echo "<b>Livre :</b><br>".$donPan->getId_livre2()." / ".$donPan->getRef_livre2()." /<br>".$donPan->getTtr_livre2();}
                            if ($donPan->getTtr_artcl2() <> "") {echo "<br><b>Article :</b><br>".$donPan->getId_artcl2()." /<br>".$donPan->getTtr_artcl2();}
                            if ($donPan->getNbre2() <> 0) {echo "<br>Nbre = ".$donPan->getNbre2();}                      
                            ?>
                        </td>
                        <td class="text-center"><?php 
                            if ($donPan->getTtr_livre3() <> "") {echo "<b>Livre :</b><br>".$donPan->getId_livre3()." / ".$donPan->getRef_livre3()." /<br>".$donPan->getTtr_livre3();}
                            if ($donPan->getTtr_artcl3() <> "") {echo "<br><b>Article :</b><br>".$donPan->getId_artcl3()." /<br>".$donPan->getTtr_artcl3();}
                            if ($donPan->getNbre3() <> 0) {echo "<br>Nbre = ".$donPan->getNbre3();}                      
                            ?>
                        </td>
                        <td class="text-center"><?php 
                            $prix=0;
                            if ($donPan->getId_artcl1() > 1) {$prix=$donPan->getPxttc_artcl1()*$donPan->getNbre1();}
                                else {$prix= $donPan->getPxttc_livre1()*$donPan->getNbre1();} 
                                if ($donPan->getId_artcl2() > 1) {$prix+=$donPan->getPxttc_artcl2()*$donPan->getNbre2();}
                                else {$prix+= $donPan->getPxttc_livre2()*$donPan->getNbre2();} 
                                if ($donPan->getId_artcl3() > 1) {$prix+=$donPan->getPxttc_artcl3()*$donPan->getNbre3();}
                                else {$prix+= $donPan->getPxttc_livre3()*$donPan->getNbre3();} 

                                $fraislivr= $donPan->getFrs_livrais1()+ $donPan->getFrs_livrais2()+ $donPan->getFrs_livrais3();
                            
                            echo "Prix ttc = ".$prix." ".$donPan->getMonnaie()." /<br>Frs Livr = "
                                    .$fraislivr." ".$donPan->getMonnaie();
                            ?>
                        </td>

                        <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] == 1) { ?>
                        <td class="text-center">
                            <a href='./index.php?action=detail&id=<?=$donPan->getId_panier();?>&detail=panier' class="btn btn-primary"><i class="fas fa-info"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                            <a href='./index.php?action=modifier&id=<?=$donPan->getId_panier();?>&modifier=panier' class="btn btn-success" style="display: none; background-color: aqua; color: red; pointer-events: none; cursor: default;"><i class="fas fa-pen"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                            <a onclick="return confirm('Etes-vous sûr de vouloir supprimer cette ligne ?')" href="./index.php?action=supprimer&id=<?=$donPan->getId_panier();?>&supprimer=panier" class="btn btn-danger"><i class="fas fa-trash"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                        </td>
                        <?php } ?>
                    </tr>
                    <?php }
            ?>

            </tbody>
        </table>

               
        <?php
$contenu= ob_get_clean();

?>