<?php 
    $contenu= ob_start();
    require_once('./communs/connect.php');
    require_once('./controllers/backend/CommandeController.php');

?>

<script type="text/javascript" src="./assets/js/affichetabselectionScript.js">
</script>


<br><br>
<h1 class="h2" style="text-align: center;caption-side: top; font-weight: bolder">Vos Commandes</h1>
    <br>

<table class="table table-striped table-bordered">
          
            <thead>
                <tr>
                    <th class="text-center">Id & référence</th>                    
                    <th class="text-center">Id + Réf + Titre livre/Article</th>
                    <th class="text-center">Nombre</th>
                    <th class="text-center">Prix ttc + frs livr. + monnaie</th>
                    <th class="text-center">Mode de payment + Réf transac.</th>
                    <th class="text-center">date ordre livr.</th>
                    <th class="text-center">nom + prenom + instit.</th>
                    <th class="text-center">Adresse & téléphone</th>
                    <th class="text-center">mode livr. + message clt</th>
                    <th class="text-center">date livr. prévue + réalisée</th>
                </tr>
            </thead>
            <tbody>
            <?php
                foreach ($dataA as $donCmd) {
                    
                ?>
                    <tr>
                        <td class="text-center"><?php echo $donCmd->getId_commande()." / ".$donCmd->getRef_commande();?></td>
                        <td class="text-center"><?php 
                            if ($donCmd->getTtr_livre() <> "") {echo "<b>Livre :</b><br>".$donCmd->getId_livre()." / ".$donCmd->getRef_livre()." /<br>".$donCmd->getTtr_livre();}
                            if ($donCmd->getTtr_artcl() <> "") {echo "<br><b>Article :</b><br>".$donCmd->getId_artcl()." /<br>".$donCmd->getTtr_artcl();}                      
                            ?>
                        </td>
                        <td class="text-center"><?php echo $donCmd->getNombre() ?></td>
                        <td class="text-center"><?php echo (int)$donCmd->getPx_ttc()*(int)$donCmd->getNombre()." ".$donCmd->getMonnaie()." /<br>"
                            .$donCmd->getFrs_livrais()." ".$donCmd->getMonnaie();
                            ?>
                        </td>
                        <td class="text-center"><?php 
                            if ($donCmd->getMode_payment() == 1) {echo "Carte";} else {echo "Chèque";}
                            echo " / ".$donCmd->getRef_transaction();
                            ?>
                        </td>
                        <td class="text-center"><?php echo $donCmd->getDate_ordre_livrais() ?></td>
                        <td class="text-center"><?php echo $donCmd->getNom_destin()." / ".$donCmd->getPrenom_destin()
                            ." / <br>".$donCmd->getInstitution_destin();
                            ?>
                        </td>
                        <td class="text-center"><?php echo $donCmd->getAdr_livrais().", ".$donCmd->getCode_postal()
                            .", ".$donCmd->getVille().", ".$donCmd->getPays()
                            ."<br> Tél : ".$donCmd->getTelephone();
                            ?>
                        </td>
                        <td class="text-center"><?php 
                            echo $donCmd->getMode_livrais();
                            if ($donCmd->getMsg_livrais() <> "") {echo " / ".$donCmd->getMsg_livrais();}
                            ?>
                        </td>
                        <td class="text-center"><?php echo $donCmd->getDate_livrais_prev()." / ".$donCmd->getDate_livrais_real();?></td>

                    </tr>
                    <?php }
            ?>

            </tbody>
        </table>
               
        <?php
$contenu= ob_get_clean();

?>