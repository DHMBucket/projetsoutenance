<?php
$contenu= ob_start();
require_once('./communs/connect.php');
require_once('./controllers/frontend/PresentationtraitementController.php');
?>

<div id="navBoard" class="row border " style="margin: auto; align-items: center; justify-content: center; ">
    <div class="col-sm-3 m-0 p-0 border">
        <nav class="navbar navbar-light bg-light m-0 p-0 ">
            <form action="./index.php?action=search&search=searchAccAut" class="form-inline" method="POST">
                <input class="form-control mr-sm-2" type="search" id="sequence" name="sequence" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" id="seqrech" name="seqrech" type="submit">Recherche par nom</button>
            </form>
        </nav>
    </div>
    <?php
        
        if (isset($_SESSION['Auth']) && isset($_SESSION['Auth']['role']) && (int)$_SESSION['Auth']['role']<5) {
    ?>
    <div class="col-sm-9 m-0 p-0 border">
    <div class="container row border" style="margin: auto;"> 
        
            <form action="./index.php?action=searchCrit&searchCrit=searchAccCritAut" class="form-inline col" method="POST">
         
            <div class="col-sm-1 m-0 p-0 text-center" >
                <label for="idsearch">par Id</label>
                <select class="form-control" id="idsearch" name="idsearch">
                    <option value=0>0</option>
                    <?php $i=1; foreach ($idData as $idVal) {?>
                        <option value=<?php echo $idVal;?>><?php echo $idVal; $i++?></option>
                    <?php } ?>
                </select>
                <?php $i=1; foreach ($idData as $idVal) {?>
                    <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idVal; $i++;?></p>
                <?php } ?>
            </div>
            <div class="col-sm-3 m-0 p-0 text-center" >
                <label for="nsearch">Nom</label>
                <select class="form-control" id="nsearch" name="nsearch">
                    <option value=""></option>
                    <?php $i=1; foreach ($nomData as $nomVal) {?>
                        <option value=<?php echo '"'.$nomVal.'"';?>><?php echo $nomVal; $i++?></option>
                    <?php } ?>
                </select>
                <?php $i=1; foreach ($nomData as $nomVal) {?>
                    <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$nomVal; $i++;?></p>
                <?php } ?>
            </div>
            <div class="col-sm-3 m-0 p-0 text-center" >
                <label for="psearch">Prénom</label>
                <select class="form-control" id="psearch" name="psearch">
                    <option value=""></option>
                    <?php $i=1; foreach ($prnData as $prnVal) {?>
                        <option value=<?php echo '"'.$prnVal.'"';?>><?php echo $prnVal; $i++?></option>
                    <?php } ?>
                </select>
                <?php $i=1; foreach ($prnData as $prnVal) {?>
                    <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$prnVal; $i++;?></p>
                <?php } ?>
            </div>
            <div class="col-sm-3 m-0 p-0 text-center" >
                <label for="inssearch">Institution</label>
                <select class="form-control" id="inssearch" name="inssearch">
                    <option value=""></option>
                    <?php $i=1; foreach ($insData as $insVal) {?>
                        <option value=<?php echo '"'.$insVal.'"';?>><?php echo $insVal; $i++?></option>
                    <?php } ?>
                </select>
                <?php $i=1; foreach ($insData as $insVal) {?>
                    <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$insVal; $i++;?></p>
                <?php } ?>
            </div>
            <div class="col-sm-1 m-0 p-0 bg-info text-center">
                <button type="submit" class="btn btn-primary" id="filtrsearchAut" name="filtrsearchAut">Filtrage</button>
                <a class="btn btn-info" href="./index.php?action=initial&initial=initialAut">Réinitialiser</a>
            </div>
        </form>
    
    </div>
        </div>
        <?php
        }
        ?>

</div>

 <h3 style="text-align: center;caption-side: top;">Les auteurs / rédacteurs de notre site</h3>
 <br>
<div class="container row" style="margin: auto;">
    <div class="col-auto" style="margin: auto;" >
            <button type="button" name="precedentAut" id="precedent">Précédent</button>
            <button type="button" name="suivantAut" id="suivant">Suivant</button>
    </div>
</div>
<br>

<div class="row">               

<?php
if ($numDim > 0) { 
        if (isset($_SESSION)) {
            if (isset($_COOKIE['iindex']) && $_COOKIE['iindex']>0) {
                $_SESSION['icompteur']=$_COOKIE['iindex'];
            }

?>
            <input hidden type="number" id="nbligne1" value="<?=$_SESSION['icompteur']?>">
            
            <div id="fixsz1" class="container col-5" style="border: 1px solid #ced4da;">
                <div class="row">
                    <div class="col-4" style="height:100%">
                        <img id="ph1" src="<?='./assets/images/'.$dataA[$_SESSION['icompteur']-2]->getPhoto();?>" style="width:400px;" alt="<?=$dataA[$_SESSION['icompteur']-2]->getPhoto();?>">
                    </div>
                    <div class="col-8">
                        <ul class="list-group list-group-flush">
                        <li class="list-group-item"><p><span class="font-weight-bolder">Nom&emsp;&emsp;&emsp;:&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-2]->getNom();?></p></li>
                        <li class="list-group-item"><p><span class="font-weight-bolder">Prénom&emsp;&emsp;:&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-2]->getPrenom();?></p></li>
                        <li class="list-group-item"><p><span class="font-weight-bolder">Institution&emsp;:&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-2]->getInstitution();?></p></li>
                        <li class="list-group-item"><p><span class="font-weight-bolder">Fonction&emsp;&emsp;:&nbsp;&nbsp;</p><p id="descript1"><?=$dataA[$_SESSION['icompteur']-2]->getFonction();?></p></li>
                            <li class="list-group-item">
                                <button class="btn btn-success" type="button" id="details1">détail fonction</button> 
                                <a class="btn btn-secondary" style="background-color: yellow; color: red" href="" id="contacter1" hidden>Contacter</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        <?php
        }
        if (
            (isset($_SESSION) && isset($_COOKIE['iindex']) && $_COOKIE['iindex']==0 && $_SESSION['icompteur']>0  && count($dataA)>1) 
            || 
            (isset($_SESSION) && $_SESSION['icompteur']==2 && count($dataA)>1)
            ) {
        ?>
        <input hidden type="number" id="nbligne2" value="<?=$_SESSION['icompteur']?>">

        <div id="fixsz2" class="container col-5" style="border: 1px solid #ced4da;">
            <div class="row">
                <div class="col-4" style="height:100%">
                    <img id="ph2" src="<?='./assets/images/'.$dataA[$_SESSION['icompteur']-1]->getPhoto();?>" style="width:400px;" alt="<?=$dataA[$_SESSION['icompteur']-1]->getPhoto();?>">
                </div>
                <div class="col-8">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><p><span class="font-weight-bolder">Nom&emsp;&emsp;&emsp;:&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-1]->getNom();?></p></li>
                        <li class="list-group-item"><p><span class="font-weight-bolder">Prénom&emsp;&emsp;:&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-1]->getPrenom();?></p></li>
                        <li class="list-group-item"><p><span class="font-weight-bolder">Institution&emsp;:&nbsp;&nbsp;</span><?=" ".$dataA[$_SESSION['icompteur']-1]->getInstitution();?></p></li>
                        <li class="list-group-item"><p><span class="font-weight-bolder">Fonction&emsp;&emsp;:&nbsp;&nbsp;</p><p id="descript2"><?=$dataA[$_SESSION['icompteur']-1]->getFonction();?></p></li>
                        <li class="list-group-item">
                            <button class="btn btn-success" type="button" id="details2">détail fonction</button>
                            <a class="btn btn-secondary" style="background-color: yellow; color: red" href="" id="contacter2" hidden>Contacter</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <?php
        } 
    } else {
        ?>
            <br>
            <div class="container row" style="margin: auto;">
                <div class="col-auto" style="margin: auto;" >
                    <h1>Pas de nom d'auteur avec ces critères, appuyer sur la touche Filtrage ou Réinitialiser</h1>
                </div>
            </div>
            <br>
        <?php
    }
    
?>

</div>

<br><br>
<script src="./assets/js/presentationScript.js"> 

</script>   

<?php

    $contenu= ob_get_clean();
    
?>    

