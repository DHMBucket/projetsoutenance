<?php  $contenu = ob_start(); ?>
<link rel="stylesheet" href="./assets/css/telephone.css">
<script src="./assets/js/formulaireClientcoord.js"></script>

<br><br><br>
    <div class="container text-center bg-warning">
        <h1 class="h2">Formulaire d'abonnement Membre</h1>
        <form action="./index.php?action=abonnerMbr&abonnerMbr" method="POST">
            <div class="form-row align-items-center">
                <div class="col-sm-2 my-1">
                    <label for="tarif1">Prix de l'abonnement</label>
                    <input type="number" name="prixeuro" id="prixeuro" value="<?php echo $donnRevue[0]->getPrix();?>" hidden>
                    <input type="number" class="form-control" id="tarif1" name="tarif1" 
                           placeholder=""
                           value="<?php if (isset($_POST['tarif1'])) { echo $_POST['tarif1']; } else { echo $donnRevue[0]->getPrix();}?>" readonly>
                </div>
                <div class="col-1 my-1">
                    <label for="monnaie1">Libellé</label>
                    <select class="form-control" id="monnaie1" name="monnaie1" required>
                        <?php $i=1; foreach ($donnMonn as $monn) {?>
                            <option value=<?php echo $i."-".$monn->getSymb_monnaie();?>><?php echo $i."-".$monn->getSymb_monnaie(); $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($donnMonn as $monn) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$monn->getSymb_monnaie()."--".$monn->getNom_monnaie()."--".$monn->getVal_commerc_en_euro()."--".$monn->getId_monnaie(); $i++;?></p>
                    <?php } ?>
                </div>
            </div>
            <div class="form-row align-items-center">
                <div class="offset-1 col-2 my-1">
                    <label for="selectCivil">Civilité</label>
                    <select class="form-control" id="selectCivil" name="typecivil" readonly>
                        <?php if ($dataM[0]->getCivilite()=="1") { ?> 
                        <option value=1>1-Monsieur</option>
                        <?php } else {?>
                        <option value=2>2-Madame</option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-sm-4 my-1">
                    <label for="nom">Nom</label>
                    <input type="text" class="form-control" id="nom" name="nom" 
                           placeholder="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo "Entrer votre nom";}?>"
                           value="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo $dataM[0]->getNom_membre();}?>"
                           readonly>
                           <input type="text" class="form-control" id="id_clt_membre" name="id_clt_membre" 
                           value="<?php echo $dataM[0]->getId_clt_membre();?>" hidden>
                           <input type="text" class="form-control" id="type_membre" name="type_membre" 
                           value="<?php echo $dataM[0]->getType_membre();?>" hidden>
                </div>
                <div class="col-sm-4 my-1">
                    <label for="prenom">Prénom ( facultatif )</label>
                    <input type="text" class="form-control" id="prenom" name="prenom" 
                           placeholder="<?php if (isset($_POST['prenom'])) { echo $_POST['prenom']; } else { echo "Entrer votre prénom";}?>"
                           value="<?php if (isset($_POST['prenom'])) { echo $_POST['prenom']; } else { echo $dataM[0]->getPrenom_membre();}?>"
                           readonly>
                </div>
            </div>   
            <div class="form-row align-items-center">
                 <div class="offset-1 col-sm-6 my-1">
                    <label for="instit">Nom Institution ou Entreprise (non requis pour les particuliers)</label>
                    <input type="text" class="form-control" id="instit" name="institution" 
                        placeholder="<?php if (isset($_POST['institution'])) { echo $_POST['institution']; } else { echo "Entrer le nom de votre instituion/entreprise";}?>"
                        value="<?php if (isset($_POST['institution'])) { echo $_POST['institution']; } else { echo $dataM[0]->getInstitution();}?>"
                        readonly>
                        <small id="institHelp" class="form-text text-muted">Nom de l'institution</small>
                 </div>
                <div class="form-group col-sm-5 my-1">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control text-center" id="email" name="email" aria-describedby="emailHelp" 
                        placeholder="<?php if (isset($_POST['email'])) { echo $_POST['email']; } else { echo "Entrer votre email";}?>"
                        value="<?php if (isset($_POST['email'])) { echo $_POST['email']; } else { echo $dataM[0]->getEmail();}?>"  
                    readonly>
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
            </div>

            <div class="form-row align-items-center">
                <div class="offset-1 col-sm-6 my-1">
                    <label for="inputAddress">Adresse de livraison</label>
                    <input type="text" class="form-control" id="inputAddress" name="inputAddress" placeholder="1234 Main St"
                    value="<?php if (isset($_POST['inputAddress'])) { echo $_POST['inputAddress']; } 
                                    else { $adr= explode("//",$dataM[0]->getAdr_livrais()); echo $adr[0];}?>"  
                    readonly>
                </div>
                <div class="col-sm-4 my-1">
                    <label for="inputAddress2">Adresse complémentaire</label>
                    <input type="text" class="form-control" id="inputAddress2" name="inputAddress2" placeholder="Apartment, studio, or floor"
                    value="<?php if (isset($_POST['inputAddress2'])) { echo $_POST['inputAddress2']; } 
                                    else { $adr= explode("//",$dataM[0]->getAdr_livrais()); echo $adr[1];}?>"  
                    readonly>
                </div>
            </div>

            <div class="form-row align-items-center">
                <div class="offset-1 col-sm-4 my-1">
                    <label for="inputCity">Ville</label>
                    <input type="text" class="form-control" id="inputCity" name="inputCity"
                    value="<?php if (isset($_POST['inputCity'])) { echo $_POST['inputCity']; } else { echo $dataM[0]->getVille();}?>"  
                    readonly>
                </div>
                <div class="col-sm-4 my-1">
                    <label for="inputState">Etat</label>
                    <input id="inputState" name="inputState" class="countrypicker form-control"
                    value="<?php if (isset($_POST['inputState'])) { echo $_POST['inputState']; } else { echo $dataM[0]->getPays();}?>"  
                    readonly>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="inputZip">Code Postal</label>
                    <input type="text" class="form-control" id="inputZip" name="inputZip"
                    value="<?php if (isset($_POST['inputZip'])) { echo $_POST['inputZip']; } else { echo $dataM[0]->getCode_postal();}?>"  
                    readonly>
                </div>
            </div>

            <div class="form-row align-items-center">
                <div id="teldiv" name="teldiv" class="offset-1 col-sm-6 my-1 input-group">
                    <label for="indicatif">indicatif</label>
                    <input type="text" class="form-control" id="indicatif" name="indicatif" placeholder="+33"
                    value="<?php if (isset($_POST['indicatif'])) { echo $_POST['indicatif']; } else { echo $dataM[0]->getIndicatif();}?>"  
                    readonly>
                    <input type="tel" id="teleph1" name="telClt" class="form-control"
                    value="<?php if (isset($_POST['telClt'])) { echo $_POST['telClt']; } else { echo $dataM[0]->getTelephone();}?>"  
                    readonly>
                    <span class="btn btn-info">Tel</span>
                </div>
            </div>
            
                <div class="col-auto my-1">
                </div>
                <div class="container bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="abonnerMbr" name="abonnerMbr">Payer l'abonnement</button>
                    <a class="btn btn-info" href="./index.php">Retour à la page d'accueil</a>
                </div>
        </form>
</div>
<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




