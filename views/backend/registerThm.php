<?php  $contenu = ob_start(); ?>

    <div class="container text-center bg-warning" style="position:relative;  ">
        <br><br><br>   
        <h1 class="h2">Formulaire d'ajout d'un thème</h1>
        <form action="./index.php?action=ajouterThm&ajouterThm" method="POST">
            <div class="form-row align-items-center">
 
                <div class="col-sm-3 my-1">
                    <label for="nom">Nom du thème</label>
                    <input type="text" class="form-control" id="nom" name="nom" 
                           placeholder="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo "Entrer le nom du thème";}?>"
                           value="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo "";}?>"
                           required>
                </div>

  
                <div class="container bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="ajoutthm" name="ajoutthm">Enregistrer</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauThm">Retour au tableau</a>
                </div>
        </form>
</div>
<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




