<?php  $contenu = ob_start(); ?>
<link rel="stylesheet" href="./assets/css/telephone.css">
<script src="./assets/js/formulaireLivre.js"></script>


    <div class="container text-center bg-warning" style="position:relative;  ">
        <br><br><br>   
        <h1 class="h2">Formulaire de modification de livre ou revue</h1>
        <form action="./index.php?action=modifierLvR&modifierLvR" method="POST" enctype="multipart/form-data">
            <div class="form-row align-items-center">

                <div class="col-sm-1 my-1">
                    <label for="idl">Id Livre</label>
                    <input type="text" class="form-control" id="idl" name="idl" 
                           placeholder="<?=(int)$donnLvR[0]->getId_livre()?>"
                           value="<?=(int)$donnLvR[0]->getId_livre();?>"
                           readonly>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="selectType">Type</label>
                    <input type="text" class="form-control" id="tp" name="tp" 
                           placeholder="<?php if ($donnLvR[0]->getNom_specif_revue() <> "") { echo "2-Revue";} else { echo "1-Livre";}?>"
                           value="<?php if ($donnLvR[0]->getNom_specif_revue() <> "") { echo "2-Revue";} else { echo "1-Livre";}?>"
                    readonly>
                    <select class="form-control" id="selectType" name="type" hidden>
                        <option value="<?php if ($donnLvR[0]->getNom_specif_revue() <> "") { echo 2;} else { echo 1;} ?>">
                            <?php if (isset($_POST['type'])) { if ($_POST['type'] == "1") { echo "1-Livre";} 
                                                                    else { echo "2-Revue";}
                            } else { if ($donnLvR[0]->getNom_specif_revue() <> "") { echo "2-Revue";} else { echo "1-Livre";} }
                            ?>
                        </option>
                        <option value=1>1-Livre</option>
                        <option value=2>2-Revue</option>
                    </select>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="ref">Référence</label>
                    <input type="text" class="form-control" id="ref" name="ref" 
                           placeholder="<?php if (isset($_POST['ref'])) { echo $_POST['ref']; } else { echo $donnLvR[0]->getReference();}?>"
                           value="<?php if (isset($_POST['ref'])) { echo $_POST['ref']; } else { echo $donnLvR[0]->getReference();}?>"
                           readonly>
                </div>

                <div class="col-sm-3 my-1">
                    <label id="nrv" for="nomrev" <?php if ($donnLvR[0]->getNom_specif_revue() == "") { echo "hidden";}?>>Nom de la revue//périodicité</label>
                    <input type="text" class="form-control" id="nomrev" name="nomrev" 
                           placeholder="<?=$donnLvR[0]->getNom_specif_revue();?>"
                           value=<?php echo '"'.$donnLvR[0]->getNom_specif_revue()."//".$donnLvR[0]->getPeriodicite().'"';?>
                    readonly <?php if ($donnLvR[0]->getNom_specif_revue() == "") { echo "hidden";}?>>
                </div>

                <div class="col-sm-3 my-1">
                    <label for="titre">Titre</label>
                    <input type="text" class="form-control" id="titre" name="titre" 
                           placeholder="<?php if (isset($_POST['titre'])) { echo $_POST['titre']; } else { echo $donnLvR[0]->getTitre();}?>"
                           value="<?php if (isset($_POST['titre'])) { echo $_POST['titre']; } else { echo $donnLvR[0]->getTitre();}?>"
                           readonly>
                </div>

                <div class="col-sm-2">
                    <label for="prix">Prix en Euro</label>
                    <input type="number" class="form-control" id="prix" name="prix" 
                            placeholder="<?php if (isset($_POST['prix'])) { echo $_POST['prix']; } else { echo $donnLvR[0]->getPrix();}?>"
                            value="<?php if (isset($_POST['prix'])) {
                                                if ((int)$_POST['prix'] >= 0) {
                                                    echo $_POST['prix'];
                                                } else { echo 0;}  
                                        } else { echo (int)$donnLvR[0]->getPrix();}?>"
                            required>
                </div>

                <div class="col-sm-2">
                    <label for="image1">image 1</label>
                    <input type="hidden" name="max_file_size" value="1000000000" />
                    <input type="file" class="form-control" id="image1" name="image1"
                    placeholder="<?php if (isset($_FILES['image1']['name']) && (!empty($_FILES['image1']['name']))) { echo $_FILES['image1']['name']; } else { echo $donnLvR[0]->getImage1();}?>"
                    value="<?php if (isset($_FILES['image1']['name']) && (!empty($_FILES['image1']['name']))) { echo $_FILES['image1']['name']; } else { echo $donnLvR[0]->getImage1();}?>">
                </div>

                <div class="col-sm-2">
                    <label for="image2">image 2</label>
                    <input type="hidden" name="max_file_size" value="1000000000" />
                    <input type="file" class="form-control" id="image2" name="image2"
                    placeholder="<?php if (isset($_FILES['image2']['name']) && (!empty($_FILES['image2']['name']))) { echo $_FILES['image2']['name']; } else { echo $donnLvR[0]->getImage2();}?>"
                    value="<?php if (isset($_FILES['image2']['name']) && (!empty($_FILES['image2']['name']))) { echo $_FILES['image2']['name']; } else { echo $donnLvR[0]->getImage2();}?>">
                </div>

                <div class="col-sm-2">
                    <label for="image3">image 3</label>
                    <input type="hidden" name="max_file_size" value="1000000000" />
                    <input type="file" class="form-control" id="image3" name="image3"
                    placeholder="<?php if (isset($_FILES['image3']['name']) && (!empty($_FILES['image3']['name']))) { echo $_FILES['image3']['name']; } else { echo $donnLvR[0]->getImage3();}?>"
                    value="<?php if (isset($_FILES['image3']['name']) && (!empty($_FILES['image3']['name']))) { echo $_FILES['image3']['name']; } else { echo $donnLvR[0]->getImage3();}?>">
                </div>

                <div class="col-sm-2">
                    <label for="image4">image 4</label>
                    <input type="hidden" name="max_file_size" value="1000000000" />
                    <input type="file" class="form-control" id="image4" name="image4"
                    placeholder="<?php if (isset($_FILES['image4']['name']) && (!empty($_FILES['image4']['name']))) { echo $_FILES['image4']['name']; } else { echo $donnLvR[0]->getImage4();}?>"
                    value="<?php if (isset($_FILES['image4']['name']) && (!empty($_FILES['image4']['name']))) { echo $_FILES['image4']['name']; } else { echo $donnLvR[0]->getImage4();}?>">
                </div>

                <div class="col-sm-2">
                    <label for="image5">image 5</label>
                    <input type="hidden" name="max_file_size" value="1000000000" />
                    <input type="file" class="form-control" id="image5" name="image5"
                    placeholder="<?php if (isset($_FILES['image5']['name']) && (!empty($_FILES['image5']['name']))) { echo $_FILES['image5']['name']; } else { echo $donnLvR[0]->getImage5();}?>"
                    value="<?php if (isset($_FILES['image5']['name']) && (!empty($_FILES['image5']['name']))) { echo $_FILES['image5']['name']; } else { echo $donnLvR[0]->getImage5();}?>">
                </div>
                <div class="col-sm-12">
                    <small id="imgHelp" class="form-text text-muted">Les fichiers acceptés sont de type .png, .bmp ou .jpg</small>                    
                </div>

                <div class="col-sm-4 my-1">
                    <label for="nauteur1">Nom auteur1</label>
                    <?php
                        $str1= $donnLvR[0]->getAuteur_princ1_nom()."//".$donnLvR[0]->getAuteur_princ1_prenom()."//".$donnLvR[0]->getAuteur_princ1_institution();
                        $str2= $donnLvR[0]->getAuteur_princ1_nom()." - ".$donnLvR[0]->getAuteur_princ1_prenom()." - ".$donnLvR[0]->getAuteur_princ1_institution();
                        if (isset($_POST['nauteur1'])) { $str1=str_replace('"', '', $_POST['nauteur1'], $count); $str2 = str_replace("//", " - ", $str1, $count);}
                    ?>
                    <select class="form-control" id="nauteur1" name="nauteur1" required>
                        <option value=<?='"'.$str1.'"'?>><?=$str2?></option>
                        <?php $i=1; foreach ($donnAut1 as $auteur) { 
                            $str1= $auteur->getNom()."//".$auteur->getPrenom()."//".$auteur->getInstitution();
                            $str2= $auteur->getNom()." - ".$auteur->getPrenom()." - ".$auteur->getInstitution();
                        ?>
                            <option value=<?php echo '"'.$str1.'"';?>><?php echo $str2; $i++?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="col-sm-4 my-1">
                    <label for="nauteur2">Nom auteur2</label>
                    <?php
                        $str1= $donnLvR[0]->getAuteur_princ2_nom()."//".$donnLvR[0]->getAuteur_princ2_prenom()."//".$donnLvR[0]->getAuteur_princ2_institution();
                        $str2= $donnLvR[0]->getAuteur_princ2_nom()." - ".$donnLvR[0]->getAuteur_princ2_prenom()." - ".$donnLvR[0]->getAuteur_princ2_institution();
                        if (isset($_POST['nauteur2'])) { $str1=str_replace('"', '', $_POST['nauteur2'], $count); $str2 = str_replace("//", " - ", $str1, $count);}
                    ?>
                    <select class="form-control" id="nauteur2" name="nauteur2" value="" placeholder="">
                        <option value=<?='"'.$str1.'"'?>><?=$str2?></option>
                        <?php $i=1; foreach ($donnAut2 as $auteur) {
                            $str1= $auteur->getNom()."//".$auteur->getPrenom()."//".$auteur->getInstitution();
                            $str2= $auteur->getNom()." - ".$auteur->getPrenom()." - ".$auteur->getInstitution();
                        ?>
                            <option value=<?php echo '"'.$str1.'"';?>><?php echo $str2; $i++?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="col-sm-4 my-1">
                    <label for="nredac">Nom redacteur</label>
                    <?php
                        $str1= $donnLvR[0]->getRedac_chef_nom()."//".$donnLvR[0]->getRedac_chef_prenom()."//".$donnLvR[0]->getRedac_chef_institution();
                        $str2= $donnLvR[0]->getRedac_chef_nom()." - ".$donnLvR[0]->getRedac_chef_prenom()." - ".$donnLvR[0]->getRedac_chef_institution();
                        if (isset($_POST['nredac'])) { $str1=str_replace('"', '', $_POST['nredac'], $count); $str2 = str_replace("//", " - ", $str1, $count);}
                    ?>
                    <select class="form-control" id="nredac" name="nredac" value="" placeholder="">
                        <option value=<?='"'.$str1.'"'?>><?=$str2?></option>
                        <?php $i=1; foreach ($donnAut2 as $auteur) { 
                            $str1= $auteur->getNom()."//".$auteur->getPrenom()."//".$auteur->getInstitution();
                            $str2= $auteur->getNom()." - ".$auteur->getPrenom()." - ".$auteur->getInstitution();
                        ?>
                            <option value=<?php echo '"'.$str1.'"';?>><?php echo $str2; $i++?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="col-sm-3 my-1">
                    <label for="edit1">Nom éditeur1</label>
                    <?php
                        if (isset($_POST['edit1'])) { $str2= $_POST['edit1']; } else { $str2= $donnLvR[0]->getEditeur1();}
                    ?>
                    <select class="form-control" id="edit1" name="edit1" required>
                        <option value=<?=$str2;?>><?=$str2;?></option>
                        <?php $i=1; foreach ($donnEdt1 as $editeur) { 
                            $str1= $editeur->getNom_editeur();
                        ?>
                            <option value=<?php echo $str1;?>><?php echo $str1; $i++?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="col-sm-3 my-1">
                    <label for="edit2">Nom éditeur2</label>
                    <?php
                        if (isset($_POST['edit2'])) { $str2= $_POST['edit2']; } else { $str2= $donnLvR[0]->getEditeur2();}
                    ?>
                    <select class="form-control" id="edit2" name="edit2" value="" placeholder="">
                        <option value=<?=$str2;?>><?=$str2;?></option>
                        <?php $i=1; foreach ($donnEdt2 as $editeur) { 
                           $str1= $editeur->getNom_editeur();
                        ?>
                            <option value=<?php echo $str1;?>><?php echo $str1; $i++?></option>
                        <?php } ?>
                    </select>
                </div>


                <div class="col-sm-2 my-1">
                    <label for="dateparut">Date de parution</label>
                    <input type="date" class="form-control" id="dateparut" name="dateparut" 
                           placeholder="<?php if (isset($_POST['dateparut'])) { echo $_POST['dateparut']; } else { echo $donnLvR[0]->getDate_parution();}?>"
                           value="<?php if (isset($_POST['dateparut'])) { echo $_POST['dateparut']; } else { echo $donnLvR[0]->getDate_parution();}?>"
                    required>
                </div>

                <div class="col-sm-2 my-1">
                    <label for="nbpage">Nombre de pages</label>
                    <input type="number" class="form-control" id="nbpage" name="nbpage" 
                            placeholder="<?php if (isset($_POST['nbpage'])) { echo $_POST['nbpage']; } else { echo $donnLvR[0]->getNbre_pages();}?>"
                            value="<?php if (isset($_POST['nbpage'])) {
                                                if ((int)$_POST['nbpage'] > 0) {
                                                    echo $_POST['nbpage'];
                                                } else { echo 1;}  
                                        } else { echo $donnLvR[0]->getNbre_pages();}?>"
                    required>
                </div>
                <div class="col-sm-6 my-1">
                    <label for="descript">Description</label>
                    <input type="text" class="form-control" id="descript" name="descript" 
                           placeholder="<?php if (isset($_POST['descript'])) { echo $_POST['descript']; } else { echo $donnLvR[0]->getDescription();}?>"
                           value="<?php if (isset($_POST['descript'])) { echo $_POST['descript']; } else { echo $donnLvR[0]->getDescription();}?>"
                    required>
                </div>
                <div class="col-sm-6 my-1">
                    <label for="mchoisi">Morceau choisi</label>
                    <input type="text" class="form-control" id="mchoisi" name="mchoisi" 
                           placeholder="<?php if (isset($_POST['mchoisi'])) { echo $_POST['mchoisi']; } else { echo $donnLvR[0]->getMorceau_choisi();}?>"
                           value="<?php if (isset($_POST['mchoisi'])) { echo $_POST['mchoisi']; } else { echo $donnLvR[0]->getMorceau_choisi();}?>"
                    required>
                </div>
                <div class="col-sm-6 my-1">
                    <label for="critique">Critique</label>
                    <input type="text" class="form-control" id="critique" name="critique" 
                           placeholder="<?php if (isset($_POST['critique'])) { echo $_POST['critique']; } else { echo $donnLvR[0]->getCritique();}?>"
                           value="<?php if (isset($_POST['critique'])) { echo $_POST['critique']; } else { echo $donnLvR[0]->getCritique();}?>"
                    required>
                </div>
                <div class="col-sm-6 my-1">
                    <label for="csgnaut">Consigne auteur</label>
                    <input type="text" class="form-control" id="csgnaut" name="csgnaut" 
                           placeholder="<?php if (isset($_POST['csgnaut'])) { echo $_POST['csgnaut']; } else { echo $donnLvR[0]->getConsign_auteur();}?>"
                           value="<?php if (isset($_POST['csgnaut'])) { echo $_POST['csgnaut']; } else { echo $donnLvR[0]->getConsign_auteur();}?>"
                    required>
                </div>

                <div class="col-sm-4 my-1">
                    <label id="filelab" for="ficjustif">Fichier ebook</label>
                    <input type="hidden" name="max_file_size" value="1000000000" />
                    <input type="file" class="form-control" id="ficjustif" name="ficjustif"
                    placeholder="<?php if (isset($_FILES['ficjustif']['name']) && (!empty($_FILES['ficjustif']['name']))) { echo $_FILES['ficjustif']['name']; } else { echo $donnLvR[0]->getFichier_ebook();}?>"
                    value="<?php if (isset($_FILES['ficjustif']['name']) && (!empty($_FILES['ficjustif']['name']))) { echo $_FILES['ficjustif']['name']; } else { echo $donnLvR[0]->getFichier_ebook();}?>">
                    <small id="fileHelp" class="form-text text-muted">Les fichiers acceptés sont de type .pdf, .docx ou .jpg</small>
                </div>

                <div class="col-sm-2 my-1">
                    <label for="selectFrmt">Format</label>
                    <select class="form-control" id="selectFrmt" name="format" required>
                        <option value=<?php 
                            if (isset($_POST['format'])) { echo $_POST['format']; } 
                            else { echo $donnLvR[0]->getFormat_livre(); }?>>
                            <?php
                            if (isset($_POST['format'])) { 
                                if ($_POST['format'] == "Normal") { echo "1-Normal";} else {echo "2-Poche";}
                            } 
                            else {  
                                if ($donnLvR[0]->getFormat_livre() == "Normal") { echo "1-Normal";} else {echo "2-Poche";}
                            }
                            ?>
                        </option>
                        <option value="Normal">1-Normal</option>
                        <option value="Poche">2-Poche</option>
                    </select>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="selectSprt">Support</label>
                    <select class="form-control" id="selectSprt" name="support" required>
                        <option value=<?php 
                            if (isset($_POST['support'])) { echo $_POST['support']; } 
                            else { echo $donnLvR[0]->getType_support(); }?>>
                            <?php
                            if (isset($_POST['support'])) { 
                                if ($_POST['support'] == "Papier") { echo "1-Papier";} else {echo "2-Ebook";}
                            } 
                            else {  
                                if ($donnLvR[0]->getType_support() == "Papier") { echo "1-Papier";} else {echo "2-Ebook";}
                            }
                            ?>
                        </option>
                        <option value="Papier">1-Papier</option>
                        <option value="Ebook">2-Ebook</option>
                    </select>
                </div>
                <div class="col-sm-2 my-1">
                    <label for="stkirs">Stock IRIS</label>
                    <input type="number" class="form-control" id="stkirs" name="stkirs" 
                            placeholder="<?php if (isset($_POST['stkirs'])) { echo $_POST['stkirs']; } else { echo $donnLvR[0]->getStock_iris();}?>"
                            value="<?php if (isset($_POST['stkirs'])) {
                                                if ((int)$_POST['stkirs'] >= 0) {
                                                    echo (int)$_POST['stkirs'];
                                                } else { echo 0;}  
                                        } else { echo $donnLvR[0]->getStock_iris();}?>"
                            required>
                </div>
                
                <div class="col-sm-2 my-1">
                    <label for="stkedt">Stock Editeur</label>
                    <input type="number" class="form-control" id="stkedt" name="stkedt" 
                            placeholder="<?php if (isset($_POST['stkedt'])) { echo $_POST['stkedt']; } else { echo $donnLvR[0]->getStock_editeur();}?>"
                            value="<?php if (isset($_POST['stkedt'])) {
                                                if ((int)$_POST['stkedt'] >= 0) {
                                                    echo (int)$_POST['stkedt'];
                                                } else { echo 0;}  
                                        } else { echo $donnLvR[0]->getStock_editeur();}?>"
                            required>
                </div>


                <div class="col-sm-3 my-1">
                    <label for="isbn">isbn</label>
                    <input type="text" class="form-control" id="isbn" name="isbn" 
                           placeholder="<?php if (isset($_POST['isbn'])) { echo $_POST['isbn']; } else { echo $donnLvR[0]->getIsbn();}?>"
                           value="<?php if (isset($_POST['isbn'])) { echo $_POST['isbn']; } else { echo $donnLvR[0]->getIsbn();}?>"
                           >
                </div>
 
                <div class="col-sm-3 my-1">
                    <label for="doi">doi</label>
                    <input type="text" class="form-control" id="doi" name="doi" 
                           placeholder="<?php if (isset($_POST['doi'])) { echo $_POST['doi']; } else { echo $donnLvR[0]->getDoi();}?>"
                           value="<?php if (isset($_POST['doi'])) { echo $_POST['doi']; } else { echo $donnLvR[0]->getDoi();}?>"
                           >
                </div>
 
                <div class="col-sm-3 my-1">
                    <label for="issn">issn</label>
                    <input type="text" class="form-control" id="issn" name="issn" 
                           placeholder="<?php if (isset($_POST['issn'])) { echo $_POST['issn']; } else { echo $donnLvR[0]->getIssn();}?>"
                           value="<?php if (isset($_POST['issn'])) { echo $_POST['issn']; } else { echo $donnLvR[0]->getIssn();}?>"
                           >
                </div>
 
                <div class="col-sm-3 my-1">
                    <label for="issn_online">issn_online</label>
                    <input type="text" class="form-control" id="issn_online" name="issn_online" 
                           placeholder="<?php if (isset($_POST['issn_online'])) { echo $_POST['issn_online']; } else { echo $donnLvR[0]->getIssn_online();}?>"
                           value="<?php if (isset($_POST['issn_online'])) { echo $_POST['issn_online']; } else { echo $donnLvR[0]->getIssn_online();}?>"
                           >
                </div>
 
                <div class="container bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="modiflvrv" name="modiflvrv">Enregistrer</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauLvR">Retour au tableau</a>
                </div>
            </div>
        </form>
</div>
<script> $('.countrypicker').countrypicker(); </script>
<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




