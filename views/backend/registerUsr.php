<?php  $contenu = ob_start(); ?>

    <div class="container text-center bg-warning">
        <h1 class="h2">Formulaire d'inscription</h1>
        <form action="./index.php?action=register&register=user&ajoutuser" method="POST">
            <div class="form-row align-items-center">
                <div class="col-sm-3 my-1">
                    <label for="inlineFormInputGroupUsername">Username</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">@</div>
                        </div>
                        <input type="text" class="form-control" id="pseudo" name="pseudo" 
                            placeholder="<?php if (isset($_POST['pseudo'])) { echo $_POST['pseudo']; } else { echo "Entrer votre pseudo";}?>"
                            value="<?php if (isset($_POST['pseudo'])) { echo $_POST['pseudo']; } else { echo "";}?>"
                            required>
                    </div>
                </div>
                <div class="col-sm-4 my-1">
                    <label for="nom">Nom</label>
                    <input type="text" class="form-control" id="nom" name="nom" 
                           placeholder="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo "Entrer votre nom";}?>"
                           value="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo "";}?>"
                           required>
                </div>
                <div class="col-sm-4 my-1">
                    <label for="prenom">Prénom</label>
                    <input type="text" class="form-control" id="prenom" name="prenom" 
                           placeholder="<?php if (isset($_POST['prenom'])) { echo $_POST['prenom']; } else { echo "Entrer votre prénom";}?>"
                           value="<?php if (isset($_POST['prenom'])) { echo $_POST['prenom']; } else { echo "";}?>"  
                    required>
                </div>
                </div>
                <div class="form-row align-items-center">
                    <div class="form-group col-sm-4 my-1">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control text-center" id="email" name="email" aria-describedby="emailHelp" 
                            placeholder="<?php if (isset($_POST['email'])) { echo $_POST['email']; } else { echo "Entrer votre email";}?>"
                            value="<?php if (isset($_POST['email'])) { echo $_POST['email']; } else { echo "@";}?>"  
                        required>
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="form-group col-sm-3 my-1">
                        <label for="mdp">Password</label>
                        <input type="password" class="form-control" id="mdp" name="mdp" 
                            placeholder="<?php if (isset($_POST['mdp'])) { echo $_POST['mdp']; } else { echo "";}?>"
                            value="<?php if (isset($_POST['mdp'])) { echo $_POST['mdp']; } else { echo "";}?>"  
                        required>
                        <small id="emailHelp2" class="form-text text-muted">_</small>
                    </div>
                    <div class="form-group col-sm-3 my-1">
                        <label for="mdpbis">Confirmer Password</label>
                        <input type="password" class="form-control" id="mdpbis" name="mdpbis" 
                            placeholder="<?php if (isset($_POST['mdpbis'])) { echo $_POST['mdpbis']; } else { echo "";}?>"
                            value="<?php if (isset($_POST['mdpbis'])) { echo $_POST['mdpbis']; } else { echo "";}?>"  
                        required>
                        <small id="emailHelp3" class="form-text text-muted">_</small>
                    </div>

                    <div class="offset-4 col-4 my-1">
                            <label for="select1">Niveau du rôle administrateur</label>
                            
                            <select class="form-control" id="select1" name="role" required>
                                <option value=1>1-Administrateur</option>
                                <option value=2>2-Gestionnaire</option>
                            </select>
                    </div>
                </div>

                <div class="col-auto my-1">
                </div>
                <div class="container bg-info text-center">
                    <button id="ajoutUser" name="ajoutUser" type="submit" class="btn btn-primary">Inscrire</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauUsr">Retour au tableau</a>
                </div>
        </form>
</div>

<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




