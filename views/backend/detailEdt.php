<?php  $contenu = ob_start(); ?>

    <div class="container text-center" style="position:relative; background-color:lightgreen ">
        <br><br><br>   
        <h1 class="h2">Edition de l'éditeur (id = <?= $donnEdt[0]->getId_editeur(); ?>)</h1>
        <form action="" method="GET" enctype="multipart/form-data">
            <div class="form-row align-items-center">
               
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Code : '.$donnEdt[0]->getCode_editeur();?>"
                    value="<?php echo 'Code : '.$donnEdt[0]->getCode_editeur();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Nom : '.$donnEdt[0]->getNom_editeur();?>"
                    value="<?php echo 'Nom : '.$donnEdt[0]->getNom_editeur();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Localité : '.$donnEdt[0]->getLocalite_editeur();?>"
                    value="<?php echo 'Localité : '.$donnEdt[0]->getLocalite_editeur();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Pays : '.$donnEdt[0]->getPays_editeur();?>"
                    value="<?php echo 'Pays : '.$donnEdt[0]->getPays_editeur();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Frais livraison : '.$donnEdt[0]->getFrs_livrais()." Euros"?>"
                    value="<?php echo 'Frais livraison : '.$donnEdt[0]->getFrs_livrais()." Euros"?>">
                </div>
 
                <div class="container bg-info text-center">
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauEdt">Retour au tableau</a>
                </div>
        </form>
</div>

<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




