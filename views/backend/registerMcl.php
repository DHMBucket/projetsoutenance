<?php  $contenu = ob_start(); ?>
<script src="./assets/js/formulaireLivre.js"></script>


    <div class="container text-center bg-warning" style="position:relative;  ">
        <br><br><br>   
        <h1 class="h2">Formulaire d'ajout de mots-clés</h1>
        <form action="./index.php?action=ajouterMcl&ajouterMcl" method="POST"">
            <div class="form-row align-items-center">

                <div class="col-sm-2 my-1">
                    <label for="nom">Mot-clé</label>
                    <input type="text" class="form-control" id="nom" name="nom" 
                           placeholder="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo "Entrer le mot-clé";}?>"
                           value="<?php if (isset($_POST['nom'])) { echo $_POST['nom']; } else { echo "";}?>"
                           required>
                </div>

                <div class="col-sm-4 my-1">
                    <label for="idartcl">Id Article</label>
                    <select class="form-control" id="idartcl" name="idartcl" required>
                        <?php $i=1; foreach ($donnArt as $article) { 
                            $str1= $article->getId_article()." - ".$article->getId_livre()." - ".$article->getTitre();
                            $str = str_replace("$", " ", $str1, $count);$str1=$str;
                            ?>
                            <option value=<?php echo $article->getId_article()."//".$article->getId_livre()."//".$article->getTitre();?>><?php echo $str1; $i++?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="container bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="ajoutmcl" name="ajoutmcl">Enregistrer</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauMcl">Retour au tableau</a>
                </div>
        </form>
</div>
<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');

?>




