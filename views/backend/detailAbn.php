<?php  $contenu = ob_start(); ?>

    <div class="container text-center" style="position:relative; background-color:lightgreen ">
        <br><br><br>   
        <h1 class="h2">Edition de l'abonnement (id = <?= $donnAbn[0]->getId_abonnement(); ?>)</h1>
        <form action="" method="GET" enctype="multipart/form-data">
            <div class="form-row align-items-center">
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Revue '.$donnAbn[0]->getNom_specif_revue();?>"
                    value="<?php echo 'Revue '.$donnAbn[0]->getNom_specif_revue();?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Périodicité : '.$donnAbn[0]->getPeriodicite();?>"
                    value="<?php echo 'Périodicité : '.$donnAbn[0]->getPeriodicite();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Déb abonn. : '.date("d-m-Y", strtotime($donnAbn[0]->getDate_debut()));?>"
                    value="<?php echo 'Déb abonn. : '.date("d-m-Y", strtotime($donnAbn[0]->getDate_debut()));?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Fin abonn. : '.date("d-m-Y", strtotime($donnAbn[0]->getDate_fin()));?>"
                    value="<?php echo 'Fin abonn. : '.date("d-m-Y", strtotime($donnAbn[0]->getDate_fin()));?>">
                </div>
                <div class="col-sm-2 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Support : '.$donnAbn[0]->getType_papier_epapier();?>"
                    value="<?php echo 'Support : '.$donnAbn[0]->getType_papier_epapier();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Prix : '.$donnAbn[0]->getPrix()." ".$donnAbn[0]->getMonnaie();?>"
                    value="<?php echo 'Prix : '.$donnAbn[0]->getPrix()." ".$donnAbn[0]->getMonnaie();?>">
                </div>
                <div class="col-sm-3 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php if ($donnAbn[0]->getMode_payment() == 1) {echo 'Paiement : Carte';} else {echo 'Paiement : Chèque';}?>"
                    value="<?php if ($donnAbn[0]->getMode_payment() == 1) {echo 'Paiement : Carte';} else {echo 'Paiement : Chèque';}?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Réf. Trsct : '.$donnAbn[0]->getRef_transaction();?>"
                    value="<?php echo 'Réf. Trsct : '.$donnAbn[0]->getRef_transaction();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php echo 'Id_client : '.$donnAbn[0]->getId_client();?>"
                    value="<?php echo 'Id_client : '.$donnAbn[0]->getId_client();?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php 
                        if ($donnAbn[0]->getType_client() == 5) {echo 'Type client : Membre(gratuit)';}
                        if ($donnAbn[0]->getType_client() == 4) {echo 'Type client : Adhérent';}
                        if ($donnAbn[0]->getType_client() == 3) {echo 'Type client : Chercheur';}
                        if ($donnAbn[0]->getType_client() == 2) {echo 'Type client : Gestionnaire';}
                        if ($donnAbn[0]->getType_client() == 1) {echo 'Type client : Administrateur';}
                    ?>"
                    value="<?php 
                        if ($donnAbn[0]->getType_client() == 5) {echo 'Type client : Membre(gratuit)';}
                        if ($donnAbn[0]->getType_client() == 4) {echo 'Type client : Adhérent';}
                        if ($donnAbn[0]->getType_client() == 3) {echo 'Type client : Chercheur';}
                        if ($donnAbn[0]->getType_client() == 2) {echo 'Type client : Gestionnaire';}
                        if ($donnAbn[0]->getType_client() == 1) {echo 'Type client : Administrateur';}
                    ?>">
                </div>
                <div class="col-sm-4 my-1">
                    <input type="text" class="form-control" 
                    placeholder="<?php 
                        if ($donnAbn[0]->getStatut_client() == 1) {echo 'Statut client : Individuel';}
                        if ($donnAbn[0]->getStatut_client() == 2) {echo 'Statut client : Etudiant';}
                        if ($donnAbn[0]->getStatut_client() == 3) {echo 'Statut client : Sans emploi';}
                        if ($donnAbn[0]->getStatut_client() == 4) {echo 'Statut client : Institution / Entreprise';}
                    ?>"
                    value="<?php 
                        if ($donnAbn[0]->getStatut_client() == 1) {echo 'Statut client : Individuel';}
                        if ($donnAbn[0]->getStatut_client() == 2) {echo 'Statut client : Etudiant';}
                        if ($donnAbn[0]->getStatut_client() == 3) {echo 'Statut client : Sans emploi';}
                        if ($donnAbn[0]->getStatut_client() == 4) {echo 'Statut client : Institution / Entreprise';}
                    ?>">
                </div>

 
                <div class="container bg-info text-center">
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauAbn">Retour au tableau</a>
                </div>
        </form>
</div>

<?php 
$contenu = ob_get_clean();
require_once('./views/gabarit.php');



?>




