<?php 
    $contenu= ob_start();
    require_once('./communs/connect.php');
    require_once('./controllers/backend/ArticleController.php');
?>

<script type="text/javascript" src="./assets/js/affichetabselectionScript.js">
</script>


<br><br>
<h1 class="h2" style="text-align: center;caption-side: top; font-weight: bolder">Situation des Articles</h1>
    <br>

<div id="navBoard" class="row border " style="margin: auto; align-items: center; justify-content: center; ">
    <div class="col-sm-3 m-0 p-0 border">
        <div class="container">
            <nav class="navbar navbar-light bg-light m-0 p-0 ">
                <form action="./index.php?action=search&search=searchTabArt" class="form-inline" method="POST">
                    <input class="form-control mr-sm-2" type="search" id="sequence" name="sequence" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" id="seqrech" name="seqrech" type="submit">Recherche dans titre</button>
                </form>
            </nav>
        </div>
        <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role']<3) {?>
        <div class="container">
            <form action='./index.php?action=register&register=article' method="post">
                <button class="btn btn-info" type="submit" name="ajoutArt" id="ajoutArt">Ajouter un article</button>
            </form>    
        </div>
        <?php } ?>
    </div>
    <?php
        
        if (isset($_SESSION['Auth']) && isset($_SESSION['Auth']['role']) && (int)$_SESSION['Auth']['role']<5) {
    ?>
    <div class="col-sm-9 m-0 p-0 border">
        
            <form action="./index.php?action=searchCrit&searchCrit=searchTabCritArt" class="form-inline col" method="POST">

            <div class="container row border" style="margin: auto;"> 
                <div class="col-sm-2 m-0 p-0 text-center" >
                    <label for="idsearch">Par Id</label>
                    <select class="form-control" id="idsearch" name="idsearch">
                        <option value=0>0</option>
                        <?php $i=1; foreach ($idData as $idVal) {?>
                            <option value=<?php echo $idVal;?>><?php echo $idVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($idData as $idVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-4 m-0 p-0 text-center" >
                    <label for="ttrsearch">Titre</label>
                    <select class="form-control" id="ttrsearch" name="ttrsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($ttrData as $ttrVal) {?>
                            <option value=<?php echo $ttrVal;?>><?php echo $ttrVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($ttrData as $ttrVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$ttrVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="thmsearch">Thème</label>
                    <select class="form-control" id="thmsearch" name="thmsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($thmData as $thmVal) {?>
                            <option value=<?php echo $thmVal;?>><?php echo $thmVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($thmData as $thmVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$thmVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-2 m-0 p-0 text-center" >
                    <label for="idlsearch">Id livre</label>
                    <select class="form-control" id="idlsearch" name="idlsearch">
                        <option value=0>0</option>
                        <?php $i=1; foreach ($idlData as $idlVal) {?>
                            <option value=<?php echo $idlVal;?>><?php 
                                if ($idlVal == 1) {
                                    echo $idlVal."- article sans livre";    
                                } else {echo $idlVal;}
                                $i++?>
                            </option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($idlData as $idlVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idlVal; $i++;?></p>
                    <?php } ?>
                </div>
                </div>
                <div class="container row border" style="margin: auto;"> 
                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="rflsearch">Réf. livre</label>
                    <select class="form-control" id="rflsearch" name="rflsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($rflData as $rflVal) {?>
                            <option value=<?php echo $rflVal;?>><?php echo $rflVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($rflData as $rflVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$rflVal; $i++;?></p>
                    <?php } ?>
                </div>

                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="mclsearch">MotCle</label>
                    <select class="form-control" id="mclsearch" name="mclsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($mclData as $mclVal) {?>
                            <option value=<?php echo $mclVal;?>><?php echo $mclVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($mclData as $mclVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$mclVal; $i++;?></p>
                    <?php } ?>
                </div>

                <div class="col-sm-4 m-0 p-0 text-center" >
                    <label for="nomsearch">Nom/Instit.</label>
                    <select class="form-control" id="nomsearch" name="nomsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($nomData as $nomVal) {?>
                            <option value=<?php echo $nomVal;?>><?php echo $nomVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($nomData as $nomVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$nomVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-2 m-0 p-0 bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="filtrsearchArt" name="filtrsearchArt">Filtrage</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauArt">Réinitialiser</a>
                </div>
            </form>
        
        </div>
        </div>
        <?php
        }
        ?>

</div>


<table class="table table-striped table-bordered">
          
            <thead>
                <tr>
                    <th class="text-center">Numéro</th>
                    <th class="text-center">Titre</th>
                    <th class="text-center">Thème</th>
                    <th class="text-center">Id + référ. livre</th>
                    <th class="text-center">Emplacement</th>
                    <th class="text-center">Fichier associé</th>
                    <th class="text-center">auteurs</th>
                    <th class="text-center">Nbre Pages</th>
                    <th class="text-center">Nbre et mots pertinents</th>
                    <th class="text-center">Prix</th>
                    <th class="text-center">Type support</th>
                    <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] == 1) { ?>
                        <th class="text-center">Actions</th><!--colspan="3"-->
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
            <?php
                    $ipan=-1;
                    foreach ($dataA as $donArt) {
                        $ipan++;
                        
                    ?>
                        <tr>
                        <td class="text-center"><?php echo $donArt->getId_article(); ?></td>
                        <td class="text-center"><?php echo $donArt->getTitre(); ?> 
                                <?php if ($_SESSION['Auth']['role']<4) { ?>
                                    <br><a class="btn btn-secondary" href="./index.php?t=<?=$ipan?>&action=reservArticlePanier" id="reservart">Ajouter au panier</a>
                                <?php } ?>                                
                        </td>
                        <td class="text-center"><?php echo $donArt->getTheme_article(); ?></td>
                        <td class="text-center"><?php 
                            if ($donArt->getId_livre() > 0) {
                                echo $donArt->getId_livre();
                                if ($donArt->getRef_livre() == "") {
                                    echo " / article sans livre";    
                                } else {
                                    echo " / ".$donArt->getRef_livre();
                                }
                            }?>
                        </td>
                        <td class="text-center"><?php echo $donArt->getEmplacement(); ?></td>
                        <td class="text-center"><?php echo $donArt->getFichier_earticle(); ?></td>
                        <td class="text-center"><?php 
                            echo $donArt->getAuteur1_nom()." ".$donArt->getAuteur1_prenom()." ".$donArt->getAuteur1_institution();
                            if ($donArt->getAuteur2_nom() <> "" || $donArt->getAuteur2_institution() <> "") {
                                echo "<br>".$donArt->getAuteur2_nom()." ".$donArt->getAuteur2_prenom()." ".$donArt->getAuteur2_institution();
                            }?>
                        </td>
                        <td class="text-center"><?php echo $donArt->getNbre_pages(); ?></td>
                        <td class="text-center"><?php 
                            $jM=0;
                            foreach ($dataSupp as $donnSupp) {
                                    if ($donnSupp->getId_artcl() == $donArt->getId_article()) { 
                                        echo $donnSupp->getNom_motcle()." ";$jM++;
                                    } 
                            }                       
                            echo "<br>Nombre = $jM"; 
                            ?>
                        </td>
                        <td class="text-center"><?=$donArt->getPrix()." ".$donArt->getMonnaie(); ?></td>
                        <td class="text-center"><?=$donArt->getType_support(); ?></td>

                        <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] == 1) { ?>
                        <td class="text-center">
                            <a href='./index.php?action=detail&id=<?=$donArt->getId_article();?>&detail=article' class="btn btn-primary"><i class="fas fa-info"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                            <a href='./index.php?action=modifier&id=<?=$donArt->getId_article();?>&modifier=article' class="btn btn-success"><i class="fas fa-pen"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                            <a onclick="return confirm('Etes-vous sûr de vouloir supprimer cette ligne ?')" href="./index.php?action=supprimer&id=<?=$donArt->getId_article();?>&supprimer=article" class="btn btn-danger"><i class="fas fa-trash"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                        </td>
                        <?php } ?>
                    </tr>
                    <?php }
            ?>

            </tbody>
        </table>

               
        <?php
$contenu= ob_get_clean();

?>