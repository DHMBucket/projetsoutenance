<?php 
    $contenu= ob_start();
    require_once('./communs/connect.php');
    require_once('./controllers/backend/EditeurController.php');

?>

<script type="text/javascript" src="./assets/js/affichetabselectionScript.js">
</script>

<br><br>
<h1 class="h2" style="text-align: center;caption-side: top; font-weight: bolder">Situation des éditeurs</h1>
<br>

<div id="navBoard" class="row border " style="margin: auto; align-items: center; justify-content: center; ">
    <div class="col-sm-3 m-0 p-0 border">
        <div class="container">
            <nav class="navbar navbar-light bg-light m-0 p-0 ">
                <form action="./index.php?action=search&search=searchTabEdt" class="form-inline" method="POST">
                    <input class="form-control mr-sm-2" type="search" id="sequence" name="sequence" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" id="seqrech" name="seqrech" type="submit">Recherche par nom</button>
                </form>
            </nav>
        </div>
        <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role']<3) {?>
        <div class="container">
            <form action='./index.php?action=register&register=editeur' method="post">
                <button class="btn btn-info" type="submit" name="ajoutEdt" id="ajoutEdt">Ajouter un éditeur</button>
            </form>    
        </div>
        <?php } ?>
    </div>
    <?php

        if (isset($_SESSION['Auth']) && isset($_SESSION['Auth']['role']) && (int)$_SESSION['Auth']['role']<5) {
    ?>
    <div class="col-sm-9 m-0 p-0 border">
    <div class="container row border" style="margin: auto;"> 
        
            <form action="./index.php?action=searchCrit&searchCrit=searchTabCritEdt" class="form-inline col" method="POST">
              
                <div class="col-sm-1 m-0 p-0 text-center" >
                    <label for="idsearch">par Id</label>
                    <select class="form-control" id="idsearch" name="idsearch">
                        <option value=0>0</option>
                        <?php $i=1; foreach ($idData as $idVal) {?>
                            <option value=<?php echo $idVal;?>><?php echo $idVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($idData as $idVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$idVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="nesearch">Nom</label>
                    <select class="form-control" id="nesearch" name="nesearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($nomData as $nomVal) {?>
                            <option value=<?php echo '"'.$nomVal.'"';?>><?php echo $nomVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($nomData as $nomVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$nomVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-2 m-0 p-0 text-center" >
                    <label for="cdesearch">Code</label>
                    <select class="form-control" id="cdesearch" name="cdesearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($cdeData as $cdeVal) {?>
                            <option value=<?php echo '"'.$cdeVal.'"';?>><?php echo $cdeVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($cdeData as $cdeVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$cdeVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-3 m-0 p-0 text-center" >
                    <label for="locsearch">Localisation</label>
                    <select class="form-control" id="locsearch" name="locsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($locData as $locVal) {?>
                            <option value=<?php echo '"'.$locVal.'"';?>><?php echo $locVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($locData as $locVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$locVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-2 m-0 p-0 text-center" hidden>
                    <label for="frlsearch">Frais livr.</label>
                    <select class="form-control" id="frlsearch" name="frlsearch">
                        <option value=""></option>
                        <?php $i=1; foreach ($frlData as $frlVal) {?>
                            <option value=<?php echo '"'.$frlVal.'"';?>><?php echo $frlVal; $i++?></option>
                        <?php } ?>
                    </select>
                    <?php $i=1; foreach ($frlData as $frlVal) {?>
                        <p id=<?php echo "sp".$i ?> style="display: none"><?php echo $i."--".$frlVal; $i++;?></p>
                    <?php } ?>
                </div>
                <div class="col-sm-2 m-0 p-0 bg-info text-center">
                    <button type="submit" class="btn btn-primary" id="filtrsearchAut" name="filtrsearchAut">Filtrage</button>
                    <a class="btn btn-info" href="./index.php?action=tableau&tableau=tableauEdt">Réinitialiser</a>
                </div>
            </form>
        
        </div>
        </div>
        <?php
        }
        ?>

</div>


<table class="table table-striped table-bordered">
          
            <thead>
                <tr>
                    <th class="text-center">numéro</th>
                    <th class="text-center">code</th>
                    <th class="text-center">nom</th>
                    <th class="text-center">Pays</th>
                    <th class="text-center">Localité</th>
                    <th class="text-center">Frais de livr.</th>
                    <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] == 1) { ?>
                        <th class="text-center" colspan="3">Actions</th>
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
            <?php
                    foreach ($dataA as $donEdt) {
                        
                    ?>
                        <tr>
                        <td class="text-center"><?php echo $donEdt->getId_editeur() ?></td>
                        <td class="text-center"><?=$donEdt->getCode_editeur(); ?></td>
                        <td class="text-center"><?=$donEdt->getNom_editeur(); ?></td>       <!-- ou bien -->
                        <td class="text-center"><?=$donEdt->getLocalite_editeur(); ?></td>
                        <td class="text-center"><?=$donEdt->getPays_editeur(); ?></td>
                        <td class="text-center"><?=$donEdt->getFrs_livrais()." "; ?>&euro;</td>

                        <?php if (isset($_SESSION['Auth']['role']) && $_SESSION['Auth']['role'] == 1) { ?>
                        <td class="text-center">
                            <a href='./index.php?action=detail&id=<?=$donEdt->getId_editeur();?>&detail=editeur' class="btn btn-primary"><i class="fas fa-info"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                        </td>
                        <td class="text-center">
                            <a href='./index.php?action=modifier&id=<?=$donEdt->getId_editeur();?>&modifier=editeur' class="btn btn-success"><i class="fas fa-pen"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                        </td>
                        <td class="text-center">
                            <a onclick="return confirm('Etes-vous sûr de vouloir supprimer cette ligne ?')" href="./index.php?action=supprimer&id=<?=$donEdt->getId_editeur();?>&supprimer=editeur" class="btn btn-danger"><i class="fas fa-trash"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                            </i></a>
                        </td>
                        <?php } ?>
                    </tr>
                    <?php }
            ?>

            </tbody>
        </table>

               
        <?php
$contenu= ob_get_clean();

?>