<?php

class Auteur
{
    private $id_auteur;
    private $nom;
    private $prenom;
    private $institution;
    private $fonction;
    private $photo;
 
    /**
     * Get the value of id_auteur
     */ 
    public function getId_auteur()
    {
        return $this->id_auteur;
    }

    /**
     * Set the value of id_auteur
     *
     * @return  self
     */ 
    public function setId_auteur($id_auteur)
    {
        $this->id_auteur = $id_auteur;

        return $this;
    }

    /**
     * Get the value of nom
     */ 
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */ 
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get the value of prenom
     */ 
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set the value of prenom
     *
     * @return  self
     */ 
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get the value of institution
     */ 
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * Set the value of institution
     *
     * @return  self
     */ 
    public function setInstitution($institution)
    {
        $this->institution = $institution;

        return $this;
    }

    /**
     * Get the value of fonction
     */ 
    public function getFonction()
    {
        return $this->fonction;
    }

    /**
     * Set the value of fonction
     *
     * @return  self
     */ 
    public function setFonction($fonction)
    {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * Get the value of photo
     */ 
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set the value of photo
     *
     * @return  self
     */ 
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }
}    

?>