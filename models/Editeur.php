<?php

class Editeur
{
    private $id_editeur;
    private $code_editeur;
    private $nom_editeur;
    private $localite_editeur;
    private $pays_editeur;
    private $frs_livrais;
    

    /**
     * Get the value of id_editeur
     */ 
    public function getId_editeur()
    {
        return $this->id_editeur;
    }

    /**
     * Set the value of id_editeur
     *
     * @return  self
     */ 
    public function setId_editeur($id_editeur)
    {
        $this->id_editeur = $id_editeur;

        return $this;
    }

    /**
     * Get the value of code_editeur
     */ 
    public function getCode_editeur()
    {
        return $this->code_editeur;
    }

    /**
     * Set the value of code_editeur
     *
     * @return  self
     */ 
    public function setCode_editeur($code_editeur)
    {
        $this->code_editeur = $code_editeur;

        return $this;
    }

    /**
     * Get the value of nom_editeur
     */ 
    public function getNom_editeur()
    {
        return $this->nom_editeur;
    }

    /**
     * Set the value of nom_editeur
     *
     * @return  self
     */ 
    public function setNom_editeur($nom_editeur)
    {
        $this->nom_editeur = $nom_editeur;

        return $this;
    }

    /**
     * Get the value of localite_editeur
     */ 
    public function getLocalite_editeur()
    {
        return $this->localite_editeur;
    }

    /**
     * Set the value of localite_editeur
     *
     * @return  self
     */ 
    public function setLocalite_editeur($localite_editeur)
    {
        $this->localite_editeur = $localite_editeur;

        return $this;
    }

    /**
     * Get the value of pays_editeur
     */ 
    public function getPays_editeur()
    {
        return $this->pays_editeur;
    }

    /**
     * Set the value of pays_editeur
     *
     * @return  self
     */ 
    public function setPays_editeur($pays_editeur)
    {
        $this->pays_editeur = $pays_editeur;

        return $this;
    }

    /**
     * Get the value of frs_livrais
     */ 
    public function getFrs_livrais()
    {
        return $this->frs_livrais;
    }

    /**
     * Set the value of frs_livrais
     *
     * @return  self
     */ 
    public function setFrs_livrais($frs_livrais)
    {
        $this->frs_livrais = $frs_livrais;

        return $this;
    }
}    

?>