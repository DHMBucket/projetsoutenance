<?php

class Monnaie
{
    private $id_monnaie;
    private $nom_monnaie;
    private $symb_monnaie; 
    private $val_commerc_en_euro; 
    

    /**
     * Get the value of id_monnaie
     */ 
    public function getId_monnaie()
    {
        return $this->id_monnaie;
    }

    /**
     * Set the value of id_monnaie
     *
     * @return  self
     */ 
    public function setId_monnaie($id_monnaie)
    {
        $this->id_monnaie = $id_monnaie;

        return $this;
    }

    /**
     * Get the value of nom_monnaie
     */ 
    public function getNom_monnaie()
    {
        return $this->nom_monnaie;
    }

    /**
     * Set the value of nom_monnaie
     *
     * @return  self
     */ 
    public function setNom_monnaie($nom_monnaie)
    {
        $this->nom_monnaie = $nom_monnaie;

        return $this;
    }

    /**
     * Get the value of symb_monnaie
     */ 
    public function getSymb_monnaie()
    {
        return $this->symb_monnaie;
    }

    /**
     * Set the value of symb_monnaie
     *
     * @return  self
     */ 
    public function setSymb_monnaie($symb_monnaie)
    {
        $this->symb_monnaie = $symb_monnaie;

        return $this;
    }

    /**
     * Get the value of val_commerc_en_euro
     */ 
    public function getVal_commerc_en_euro()
    {
        return $this->val_commerc_en_euro;
    }

    /**
     * Set the value of val_commerc_en_euro
     *
     * @return  self
     */ 
    public function setVal_commerc_en_euro($val_commerc_en_euro)
    {
        $this->val_commerc_en_euro = $val_commerc_en_euro;

        return $this;
    }
}    

?>