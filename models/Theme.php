<?php

class Theme
{
    private $id_theme;
    private $nom_theme;
    
    /**
     * Get the value of id_theme
     */ 
    public function getId_theme()
    {
        return $this->id_theme;
    }

    /**
     * Set the value of id_theme
     *
     * @return  self
     */ 
    public function setId_theme($id_theme)
    {
        $this->id_theme = $id_theme;

        return $this;
    }

    /**
     * Get the value of nom_theme
     */ 
    public function getNom_theme()
    {
        return $this->nom_theme;
    }

    /**
     * Set the value of nom_theme
     *
     * @return  self
     */ 
    public function setNom_theme($nom_theme)
    {
        $this->nom_theme = $nom_theme;

        return $this;
    }

}    

?>