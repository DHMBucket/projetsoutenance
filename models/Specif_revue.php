<?php

class Specif_revue
{
    private $id_specif_revue;
    private $nom_specif_revue;
    private $periodicite;
    private $hors_serie;
    private $prix;
    private $monnaie;


    /**
     * Get the value of id_specif_revue
     */ 
    public function getId_specif_revue()
    {
        return $this->id_specif_revue;
    }

    /**
     * Set the value of id_specif_revue
     *
     * @return  self
     */ 
    public function setId_specif_revue($id_specif_revue)
    {
        $this->id_specif_revue = $id_specif_revue;

        return $this;
    }

    /**
     * Get the value of nom_specif_revue
     */ 
    public function getNom_specif_revue()
    {
        return $this->nom_specif_revue;
    }

    /**
     * Set the value of nom_specif_revue
     *
     * @return  self
     */ 
    public function setNom_specif_revue($nom_specif_revue)
    {
        $this->nom_specif_revue = $nom_specif_revue;

        return $this;
    }

    /**
     * Get the value of periodicite
     */ 
    public function getPeriodicite()
    {
        return $this->periodicite;
    }

    /**
     * Set the value of periodicite
     *
     * @return  self
     */ 
    public function setPeriodicite($periodicite)
    {
        $this->periodicite = $periodicite;

        return $this;
    }

    /**
     * Get the value of hors_serie
     */ 
    public function getHors_serie()
    {
        return $this->hors_serie;
    }

    /**
     * Set the value of hors_serie
     *
     * @return  self
     */ 
    public function setHors_serie($hors_serie)
    {
        $this->hors_serie = $hors_serie;

        return $this;
    }

    /**
     * Get the value of prix
     */ 
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set the value of prix
     *
     * @return  self
     */ 
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get the value of monnaie
     */ 
    public function getMonnaie()
    {
        return $this->monnaie;
    }

    /**
     * Set the value of monnaie
     *
     * @return  self
     */ 
    public function setMonnaie($monnaie)
    {
        $this->monnaie = $monnaie;

        return $this;
    }
}    

?>