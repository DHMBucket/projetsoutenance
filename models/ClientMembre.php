<?php

class ClientMembre
{
    private $id_clt_membre;
    private $type_membre;
    private $civilite; 
    private $nom_membre; 
    private $prenom_membre; 
    private $institution;
    private $fonction;
    private $statut_client;
    private $justif_etud_ou_ssemploi_instit;
    private $email;
    private $mdp;
    private $offre_partenaire;  //
    private $offre_newsletter; //
    private $prix_membre;
    private $monnaie;
    private $date_debut;  //
    private $adr_livrais;  //
    private $code_postal; //
    private $ville; //
    private $pays; //
    private $telephone; //
    private $indicatif; //
    private $zip_etat; //

/**
     * Get the value of id_clt_membre
     */ 
    public function getId_clt_membre()
    {
        return $this->id_clt_membre;
    }

    /**
     * Set the value of id_clt_membre
     *
     * @return  self
     */ 
    public function setId_clt_membre($id_clt_membre)
    {
        $this->id_clt_membre = $id_clt_membre;

        return $this;
    }

    /**
     * Get the value of type_membre
     */ 
    public function getType_membre()
    {
        return $this->type_membre;
    }

    /**
     * Set the value of type_membre
     *
     * @return  self
     */ 
    public function setType_membre($type_membre)
    {
        $this->type_membre = $type_membre;

        return $this;
    }

    /**
     * Get the value of civilite
     */ 
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * Set the value of civilite
     *
     * @return  self
     */ 
    public function setCivilite($civilite)
    {
        $this->civilite = $civilite;

        return $this;
    }

    /**
     * Get the value of nom_membre
     */ 
    public function getNom_membre()
    {
        return $this->nom_membre;
    }

    /**
     * Set the value of nom_membre
     *
     * @return  self
     */ 
    public function setNom_membre($nom_membre)
    {
        $this->nom_membre = $nom_membre;

        return $this;
    }

    /**
     * Get the value of prenom_membre
     */ 
    public function getPrenom_membre()
    {
        return $this->prenom_membre;
    }

    /**
     * Set the value of prenom_membre
     *
     * @return  self
     */ 
    public function setPrenom_membre($prenom_membre)
    {
        $this->prenom_membre = $prenom_membre;

        return $this;
    }

    /**
     * Get the value of institution
     */ 
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * Set the value of institution
     *
     * @return  self
     */ 
    public function setInstitution($institution)
    {
        $this->institution = $institution;

        return $this;
    }

    /**
     * Get the value of fonction
     */ 
    public function getFonction()
    {
        return $this->fonction;
    }

    /**
     * Set the value of fonction
     *
     * @return  self
     */ 
    public function setFonction($fonction)
    {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * Get the value of statut_client
     */ 
    public function getStatut_client()
    {
        return $this->statut_client;
    }

    /**
     * Set the value of statut_client
     *
     * @return  self
     */ 
    public function setStatut_client($statut_client)
    {
        $this->statut_client = $statut_client;

        return $this;
    }

    /**
     * Get the value of justif_etud_ou_ssemploi_instit
     */ 
    public function getJustif_etud_ou_ssemploi_instit()
    {
        return $this->justif_etud_ou_ssemploi_instit;
    }

    /**
     * Set the value of justif_etud_ou_ssemploi_instit
     *
     * @return  self
     */ 
    public function setJustif_etud_ou_ssemploi_instit($justif_etud_ou_ssemploi_instit)
    {
        $this->justif_etud_ou_ssemploi_instit = $justif_etud_ou_ssemploi_instit;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

      /**
     * Get the value of mdp
     */ 
    public function getMdp()
    {
        return $this->mdp;
    }

    /**
     * Set the value of mdp
     *
     * @return  self
     */ 
    public function setMdp($mdp)
    {
        $this->mdp = $mdp;

        return $this;
    }

    /**
     * Get the value of offre_partenaire
     */ 
    public function getOffre_partenaire()
    {
        return $this->offre_partenaire;
    }

    /**
     * Set the value of offre_partenaire
     *
     * @return  self
     */ 
    public function setOffre_partenaire($offre_partenaire)
    {
        $this->offre_partenaire = $offre_partenaire;

        return $this;
    }

    /**
     * Get the value of offre_newsletter
     */ 
    public function getOffre_newsletter()
    {
        return $this->offre_newsletter;
    }

    /**
     * Set the value of offre_newsletter
     *
     * @return  self
     */ 
    public function setOffre_newsletter($offre_newsletter)
    {
        $this->offre_newsletter = $offre_newsletter;

        return $this;
    }

    /**
     * Get the value of prix_membre
     */ 
    public function getPrix_membre()
    {
        return $this->prix_membre;
    }

    /**
     * Set the value of prix_membre
     *
     * @return  self
     */ 
    public function setPrix_membre($prix_membre)
    {
        $this->prix_membre = $prix_membre;

        return $this;
    }

    /**
     * Get the value of monnaie
     */ 
    public function getMonnaie()
    {
        return $this->monnaie;
    }

    /**
     * Set the value of monnaie
     *
     * @return  self
     */ 
    public function setMonnaie($monnaie)
    {
        $this->monnaie = $monnaie;

        return $this;
    }

    /**
     * Get the value of date_debut
     */ 
    public function getDate_debut()
    {
        return $this->date_debut;
    }

    /**
     * Set the value of date_debut
     *
     * @return  self
     */ 
    public function setDate_debut($date_debut)
    {
        $this->date_debut = $date_debut;

        return $this;
    }

    /**
     * Get the value of adr_livrais
     */ 
    public function getAdr_livrais()
    {
        return $this->adr_livrais;
    }

    /**
     * Set the value of adr_livrais
     *
     * @return  self
     */ 
    public function setAdr_livrais($adr_livrais)
    {
        $this->adr_livrais = $adr_livrais;

        return $this;
    }

    /**
     * Get the value of code_postal
     */ 
    public function getCode_postal()
    {
        return $this->code_postal;
    }

    /**
     * Set the value of code_postal
     *
     * @return  self
     */ 
    public function setCode_postal($code_postal)
    {
        $this->code_postal = $code_postal;

        return $this;
    }

    /**
     * Get the value of ville
     */ 
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set the value of ville
     *
     * @return  self
     */ 
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get the value of pays
     */ 
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set the value of pays
     *
     * @return  self
     */ 
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get the value of telephone
     */ 
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set the value of telephone
     *
     * @return  self
     */ 
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

  /**
     * Get the value of indicatif
     */ 
    public function getIndicatif()
    {
        return $this->indicatif;
    }

    /**
     * Set the value of indicatif
     *
     * @return  self
     */ 
    public function setIndicatif($indicatif)
    {
        $this->indicatif = $indicatif;

        return $this;
    }

    /**
     * Get the value of zip_etat
     */ 
    public function getZip_etat()
    {
        return $this->zip_etat;
    }

    /**
     * Set the value of zip_etat
     *
     * @return  self
     */ 
    public function setZip_etat($zip_etat)
    {
        $this->zip_etat = $zip_etat;

        return $this;
    }

}    

?>