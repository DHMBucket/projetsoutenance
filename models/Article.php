<?php

class Article
{
    private $id_article;
    private $titre;  //
    private $theme_article;  //
    private $id_livre;
    private $ref_livre;
    private $emplacement; 
    private $fichier_earticle; //
    private $auteur1_nom; //
    private $auteur1_prenom; //
    private $auteur1_institution; //
    private $auteur2_nom; //
    private $auteur2_prenom; //
    private $auteur2_institution; //
    private $nbre_pages; //
    private $nbre_mots_pert;
    private $prix; //
    private $monnaie;
    private $type_support;


    /**
     * Get the value of id_article
     */ 
    public function getId_article()
    {
        return $this->id_article;
    }

    /**
     * Set the value of id_article
     *
     * @return  self
     */ 
    public function setId_article($id_article)
    {
        $this->id_article = $id_article;

        return $this;
    }

    /**
     * Get the value of titre
     */ 
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set the value of titre
     *
     * @return  self
     */ 
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get the value of theme_article
     */ 
    public function getTheme_article()
    {
        return $this->theme_article;
    }

    /**
     * Set the value of theme_article
     *
     * @return  self
     */ 
    public function setTheme_article($theme_article)
    {
        $this->theme_article = $theme_article;

        return $this;
    }

    /**
     * Get the value of id_livre
     */ 
    public function getId_livre()
    {
        return $this->id_livre;
    }

    /**
     * Set the value of id_livre
     *
     * @return  self
     */ 
    public function setId_livre($id_livre)
    {
        $this->id_livre = $id_livre;

        return $this;
    }

    /**
     * Get the value of ref_livre
     */ 
    public function getRef_livre()
    {
        return $this->ref_livre;
    }

    /**
     * Set the value of ref_livre
     *
     * @return  self
     */ 
    public function setRef_livre($ref_livre)
    {
        $this->ref_livre = $ref_livre;

        return $this;
    }

    /**
     * Get the value of emplacement
     */ 
    public function getEmplacement()
    {
        return $this->emplacement;
    }

    /**
     * Set the value of emplacement
     *
     * @return  self
     */ 
    public function setEmplacement($emplacement)
    {
        $this->emplacement = $emplacement;

        return $this;
    }

    /**
     * Get the value of fichier_earticle
     */ 
    public function getFichier_earticle()
    {
        return $this->fichier_earticle;
    }

    /**
     * Set the value of fichier_earticle
     *
     * @return  self
     */ 
    public function setFichier_earticle($fichier_earticle)
    {
        $this->fichier_earticle = $fichier_earticle;

        return $this;
    }

    /**
     * Get the value of auteur1_nom
     */ 
    public function getAuteur1_nom()
    {
        return $this->auteur1_nom;
    }

    /**
     * Set the value of auteur1_nom
     *
     * @return  self
     */ 
    public function setAuteur1_nom($auteur1_nom)
    {
        $this->auteur1_nom = $auteur1_nom;

        return $this;
    }

    /**
     * Get the value of auteur1_prenom
     */ 
    public function getAuteur1_prenom()
    {
        return $this->auteur1_prenom;
    }

    /**
     * Set the value of auteur1_prenom
     *
     * @return  self
     */ 
    public function setAuteur1_prenom($auteur1_prenom)
    {
        $this->auteur1_prenom = $auteur1_prenom;

        return $this;
    }

    /**
     * Get the value of auteur1_institution
     */ 
    public function getAuteur1_institution()
    {
        return $this->auteur1_institution;
    }

    /**
     * Set the value of auteur1_institution
     *
     * @return  self
     */ 
    public function setAuteur1_institution($auteur1_institution)
    {
        $this->auteur1_institution = $auteur1_institution;

        return $this;
    }

    /**
     * Get the value of auteur2_nom
     */ 
    public function getAuteur2_nom()
    {
        return $this->auteur2_nom;
    }

    /**
     * Set the value of auteur2_nom
     *
     * @return  self
     */ 
    public function setAuteur2_nom($auteur2_nom)
    {
        $this->auteur2_nom = $auteur2_nom;

        return $this;
    }

    /**
     * Get the value of auteur2_prenom
     */ 
    public function getAuteur2_prenom()
    {
        return $this->auteur2_prenom;
    }

    /**
     * Set the value of auteur2_prenom
     *
     * @return  self
     */ 
    public function setAuteur2_prenom($auteur2_prenom)
    {
        $this->auteur2_prenom = $auteur2_prenom;

        return $this;
    }

    /**
     * Get the value of auteur2_institution
     */ 
    public function getAuteur2_institution()
    {
        return $this->auteur2_institution;
    }

    /**
     * Set the value of auteur2_institution
     *
     * @return  self
     */ 
    public function setAuteur2_institution($auteur2_institution)
    {
        $this->auteur2_institution = $auteur2_institution;

        return $this;
    }

    /**
     * Get the value of nbre_pages
     */ 
    public function getNbre_pages()
    {
        return $this->nbre_pages;
    }

    /**
     * Set the value of nbre_pages
     *
     * @return  self
     */ 
    public function setNbre_pages($nbre_pages)
    {
        $this->nbre_pages = $nbre_pages;

        return $this;
    }

    /**
     * Get the value of nbre_mots_pert
     */ 
    public function getNbre_mots_pert()
    {
        return $this->nbre_mots_pert;
    }

    /**
     * Set the value of nbre_mots_pert
     *
     * @return  self
     */ 
    public function setNbre_mots_pert($nbre_mots_pert)
    {
        $this->nbre_mots_pert = $nbre_mots_pert;

        return $this;
    }

    /**
     * Get the value of prix
     */ 
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set the value of prix
     *
     * @return  self
     */ 
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get the value of monnaie
     */ 
    public function getMonnaie()
    {
        return $this->monnaie;
    }

    /**
     * Set the value of monnaie
     *
     * @return  self
     */ 
    public function setMonnaie($monnaie)
    {
        $this->monnaie = $monnaie;

        return $this;
    }

    /**
     * Get the value of type_support
     */ 
    public function getType_support()
    {
        return $this->type_support;
    }

    /**
     * Set the value of type_support
     *
     * @return  self
     */ 
    public function setType_support($type_support)
    {
        $this->type_support = $type_support;

        return $this;
    }
}    

?>