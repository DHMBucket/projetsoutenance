<?php

class Commande
{
    private $id_commande;
    private $ref_commande;
    private $type_client;
    private $id_client;
    private $id_livre;
    private $ref_livre;
    private $ttr_livre;
    private $id_artcl;
    private $ttr_artcl;
    private $nombre;
    private $px_ht;
    private $tva_percent;
    private $px_ttc;
    private $frs_livrais;
    private $monnaie;
    private $mode_payment;
    private $ref_transaction;
    private $date_ordre_livrais;
    private $nom_destin;
    private $prenom_destin;
    private $institution_destin;
    private $adr_livrais;
    private $code_postal;
    private $ville;
    private $pays;
    private $telephone;
    private $mode_livrais;
    private $msg_livrais;
    private $date_livrais_prev;
    private $date_livrais_real;
    


    /**
     * Get the value of id_commande
     */ 
    public function getId_commande()
    {
        return $this->id_commande;
    }

    /**
     * Set the value of id_commande
     *
     * @return  self
     */ 
    public function setId_commande($id_commande)
    {
        $this->id_commande = $id_commande;

        return $this;
    }

    /**
     * Get the value of ref_commande
     */ 
    public function getRef_commande()
    {
        return $this->ref_commande;
    }

    /**
     * Set the value of ref_commande
     *
     * @return  self
     */ 
    public function setRef_commande($ref_commande)
    {
        $this->ref_commande = $ref_commande;

        return $this;
    }

    /**
     * Get the value of type_client
     */ 
    public function getType_client()
    {
        return $this->type_client;
    }

    /**
     * Set the value of type_client
     *
     * @return  self
     */ 
    public function setType_client($type_client)
    {
        $this->type_client = $type_client;

        return $this;
    }

    /**
     * Get the value of id_client
     */ 
    public function getId_client()
    {
        return $this->id_client;
    }

    /**
     * Set the value of id_client
     *
     * @return  self
     */ 
    public function setId_client($id_client)
    {
        $this->id_client = $id_client;

        return $this;
    }

    /**
     * Get the value of id_livre
     */ 
    public function getId_livre()
    {
        return $this->id_livre;
    }

    /**
     * Set the value of id_livre
     *
     * @return  self
     */ 
    public function setId_livre($id_livre)
    {
        $this->id_livre = $id_livre;

        return $this;
    }

    /**
     * Get the value of ref_livre
     */ 
    public function getRef_livre()
    {
        return $this->ref_livre;
    }

    /**
     * Set the value of ref_livre
     *
     * @return  self
     */ 
    public function setRef_livre($ref_livre)
    {
        $this->ref_livre = $ref_livre;

        return $this;
    }

    /**
     * Get the value of ttr_livre
     */ 
    public function getTtr_livre()
    {
        return $this->ttr_livre;
    }

    /**
     * Set the value of ttr_livre
     *
     * @return  self
     */ 
    public function setTtr_livre($ttr_livre)
    {
        $this->ttr_livre = $ttr_livre;

        return $this;
    }

    /**
     * Get the value of id_artcl
     */ 
    public function getId_artcl()
    {
        return $this->id_artcl;
    }

    /**
     * Set the value of id_artcl
     *
     * @return  self
     */ 
    public function setId_artcl($id_artcl)
    {
        $this->id_artcl = $id_artcl;

        return $this;
    }

    /**
     * Get the value of ttr_artcl
     */ 
    public function getTtr_artcl()
    {
        return $this->ttr_artcl;
    }

    /**
     * Set the value of ttr_artcl
     *
     * @return  self
     */ 
    public function setTtr_artcl($ttr_artcl)
    {
        $this->ttr_artcl = $ttr_artcl;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of px_ht
     */ 
    public function getPx_ht()
    {
        return $this->px_ht;
    }

    /**
     * Set the value of px_ht
     *
     * @return  self
     */ 
    public function setPx_ht($px_ht)
    {
        $this->px_ht = $px_ht;

        return $this;
    }

    /**
     * Get the value of tva_percent
     */ 
    public function getTva_percent()
    {
        return $this->tva_percent;
    }

    /**
     * Set the value of tva_percent
     *
     * @return  self
     */ 
    public function setTva_percent($tva_percent)
    {
        $this->tva_percent = $tva_percent;

        return $this;
    }

    /**
     * Get the value of px_ttc
     */ 
    public function getPx_ttc()
    {
        return $this->px_ttc;
    }

    /**
     * Set the value of px_ttc
     *
     * @return  self
     */ 
    public function setPx_ttc($px_ttc)
    {
        $this->px_ttc = $px_ttc;

        return $this;
    }

    /**
     * Get the value of frs_livrais
     */ 
    public function getFrs_livrais()
    {
        return $this->frs_livrais;
    }

    /**
     * Set the value of frs_livrais
     *
     * @return  self
     */ 
    public function setFrs_livrais($frs_livrais)
    {
        $this->frs_livrais = $frs_livrais;

        return $this;
    }

    /**
     * Get the value of monnaie
     */ 
    public function getMonnaie()
    {
        return $this->monnaie;
    }

    /**
     * Set the value of monnaie
     *
     * @return  self
     */ 
    public function setMonnaie($monnaie)
    {
        $this->monnaie = $monnaie;

        return $this;
    }

    /**
     * Get the value of mode_payment
     */ 
    public function getMode_payment()
    {
        return $this->mode_payment;
    }

    /**
     * Set the value of mode_payment
     *
     * @return  self
     */ 
    public function setMode_payment($mode_payment)
    {
        $this->mode_payment = $mode_payment;

        return $this;
    }

    /**
     * Get the value of ref_transaction
     */ 
    public function getRef_transaction()
    {
        return $this->ref_transaction;
    }

    /**
     * Set the value of ref_transaction
     *
     * @return  self
     */ 
    public function setRef_transaction($ref_transaction)
    {
        $this->ref_transaction = $ref_transaction;

        return $this;
    }

    /**
     * Get the value of date_ordre_livrais
     */ 
    public function getDate_ordre_livrais()
    {
        return $this->date_ordre_livrais;
    }

    /**
     * Set the value of date_ordre_livrais
     *
     * @return  self
     */ 
    public function setDate_ordre_livrais($date_ordre_livrais)
    {
        $this->date_ordre_livrais = $date_ordre_livrais;

        return $this;
    }

    /**
     * Get the value of nom_destin
     */ 
    public function getNom_destin()
    {
        return $this->nom_destin;
    }

    /**
     * Set the value of nom_destin
     *
     * @return  self
     */ 
    public function setNom_destin($nom_destin)
    {
        $this->nom_destin = $nom_destin;

        return $this;
    }

    /**
     * Get the value of prenom_destin
     */ 
    public function getPrenom_destin()
    {
        return $this->prenom_destin;
    }

    /**
     * Set the value of prenom_destin
     *
     * @return  self
     */ 
    public function setPrenom_destin($prenom_destin)
    {
        $this->prenom_destin = $prenom_destin;

        return $this;
    }

    /**
     * Get the value of institution_destin
     */ 
    public function getInstitution_destin()
    {
        return $this->institution_destin;
    }

    /**
     * Set the value of institution_destin
     *
     * @return  self
     */ 
    public function setInstitution_destin($institution_destin)
    {
        $this->institution_destin = $institution_destin;

        return $this;
    }

    /**
     * Get the value of adr_livrais
     */ 
    public function getAdr_livrais()
    {
        return $this->adr_livrais;
    }

    /**
     * Set the value of adr_livrais
     *
     * @return  self
     */ 
    public function setAdr_livrais($adr_livrais)
    {
        $this->adr_livrais = $adr_livrais;

        return $this;
    }

    /**
     * Get the value of code_postal
     */ 
    public function getCode_postal()
    {
        return $this->code_postal;
    }

    /**
     * Set the value of code_postal
     *
     * @return  self
     */ 
    public function setCode_postal($code_postal)
    {
        $this->code_postal = $code_postal;

        return $this;
    }

    /**
     * Get the value of ville
     */ 
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set the value of ville
     *
     * @return  self
     */ 
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get the value of pays
     */ 
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set the value of pays
     *
     * @return  self
     */ 
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get the value of telephone
     */ 
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set the value of telephone
     *
     * @return  self
     */ 
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get the value of mode_livrais
     */ 
    public function getMode_livrais()
    {
        return $this->mode_livrais;
    }

    /**
     * Set the value of mode_livrais
     *
     * @return  self
     */ 
    public function setMode_livrais($mode_livrais)
    {
        $this->mode_livrais = $mode_livrais;

        return $this;
    }

    /**
     * Get the value of msg_livrais
     */ 
    public function getMsg_livrais()
    {
        return $this->msg_livrais;
    }

    /**
     * Set the value of msg_livrais
     *
     * @return  self
     */ 
    public function setMsg_livrais($msg_livrais)
    {
        $this->msg_livrais = $msg_livrais;

        return $this;
    }

    /**
     * Get the value of date_livrais_prev
     */ 
    public function getDate_livrais_prev()
    {
        return $this->date_livrais_prev;
    }

    /**
     * Set the value of date_livrais_prev
     *
     * @return  self
     */ 
    public function setDate_livrais_prev($date_livrais_prev)
    {
        $this->date_livrais_prev = $date_livrais_prev;

        return $this;
    }

    /**
     * Get the value of date_livrais_real
     */ 
    public function getDate_livrais_real()
    {
        return $this->date_livrais_real;
    }

    /**
     * Set the value of date_livrais_real
     *
     * @return  self
     */ 
    public function setDate_livrais_real($date_livrais_real)
    {
        $this->date_livrais_real = $date_livrais_real;

        return $this;
    }

}    

?>