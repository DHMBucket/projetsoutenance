<?php

class ClientInvite
{
    private $id_clt_anonym;
    private $num_compteur;
    private $civilite; 
    private $nom_destin; 
    private $prenom_destin; 
    private $institution;
    private $fonction;
    private $email;
    private $offre_partenaire;
    private $offre_newsletter;
    private $adr_livrais;
    private $code_postal;
    private $ville;
    private $pays;
    private $zip_etat;
    private $indicatif;
    private $telephone;



    /**
     * Get the value of id_clt_anonym
     */ 
    public function getId_clt_anonym()
    {
        return $this->id_clt_anonym;
    }

    /**
     * Set the value of id_clt_anonym
     *
     * @return  self
     */ 
    public function setId_clt_anonym($id_clt_anonym)
    {
        $this->id_clt_anonym = $id_clt_anonym;

        return $this;
    }

    /**
     * Get the value of num_compteur
     */ 
    public function getNum_compteur()
    {
        return $this->num_compteur;
    }

    /**
     * Set the value of num_compteur
     *
     * @return  self
     */ 
    public function setNum_compteur($num_compteur)
    {
        $this->num_compteur = $num_compteur;

        return $this;
    }

    /**
     * Get the value of civilite
     */ 
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * Set the value of civilite
     *
     * @return  self
     */ 
    public function setCivilite($civilite)
    {
        $this->civilite = $civilite;

        return $this;
    }

    /**
     * Get the value of nom_destin
     */ 
    public function getNom_destin()
    {
        return $this->nom_destin;
    }

    /**
     * Set the value of nom_destin
     *
     * @return  self
     */ 
    public function setNom_destin($nom_destin)
    {
        $this->nom_destin = $nom_destin;

        return $this;
    }

    /**
     * Get the value of prenom_destin
     */ 
    public function getPrenom_destin()
    {
        return $this->prenom_destin;
    }

    /**
     * Set the value of prenom_destin
     *
     * @return  self
     */ 
    public function setPrenom_destin($prenom_destin)
    {
        $this->prenom_destin = $prenom_destin;

        return $this;
    }

    /**
     * Get the value of institution
     */ 
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * Set the value of institution
     *
     * @return  self
     */ 
    public function setInstitution($institution)
    {
        $this->institution = $institution;

        return $this;
    }

    /**
     * Get the value of fonction
     */ 
    public function getFonction()
    {
        return $this->fonction;
    }

    /**
     * Set the value of fonction
     *
     * @return  self
     */ 
    public function setFonction($fonction)
    {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of offre_partenaire
     */ 
    public function getOffre_partenaire()
    {
        return $this->offre_partenaire;
    }

    /**
     * Set the value of offre_partenaire
     *
     * @return  self
     */ 
    public function setOffre_partenaire($offre_partenaire)
    {
        $this->offre_partenaire = $offre_partenaire;

        return $this;
    }

    /**
     * Get the value of offre_newsletter
     */ 
    public function getOffre_newsletter()
    {
        return $this->offre_newsletter;
    }

    /**
     * Set the value of offre_newsletter
     *
     * @return  self
     */ 
    public function setOffre_newsletter($offre_newsletter)
    {
        $this->offre_newsletter = $offre_newsletter;

        return $this;
    }

    /**
     * Get the value of adr_livrais
     */ 
    public function getAdr_livrais()
    {
        return $this->adr_livrais;
    }

    /**
     * Set the value of adr_livrais
     *
     * @return  self
     */ 
    public function setAdr_livrais($adr_livrais)
    {
        $this->adr_livrais = $adr_livrais;

        return $this;
    }

    /**
     * Get the value of code_postal
     */ 
    public function getCode_postal()
    {
        return $this->code_postal;
    }

    /**
     * Set the value of code_postal
     *
     * @return  self
     */ 
    public function setCode_postal($code_postal)
    {
        $this->code_postal = $code_postal;

        return $this;
    }

    /**
     * Get the value of ville
     */ 
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set the value of ville
     *
     * @return  self
     */ 
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get the value of pays
     */ 
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set the value of pays
     *
     * @return  self
     */ 
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get the value of zip_etat
     */ 
    public function getZip_etat()
    {
        return $this->zip_etat;
    }

    /**
     * Set the value of zip_etat
     *
     * @return  self
     */ 
    public function setZip_etat($zip_etat)
    {
        $this->zip_etat = $zip_etat;

        return $this;
    }

    /**
     * Get the value of indicatif
     */ 
    public function getIndicatif()
    {
        return $this->indicatif;
    }

    /**
     * Set the value of indicatif
     *
     * @return  self
     */ 
    public function setIndicatif($indicatif)
    {
        $this->indicatif = $indicatif;

        return $this;
    }

    /**
     * Get the value of telephone
     */ 
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set the value of telephone
     *
     * @return  self
     */ 
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }
}    

?>