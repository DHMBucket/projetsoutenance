<?php

class Abonnement
{
    private $id_abonnement;
    private $nom_specif_revue;
    private $periodicite;
    private $type_papier_epapier;
    private $date_debut;
    private $date_fin;
    private $prix;
    private $monnaie;
    private $mode_payment;
    private $ref_transaction;
    private $id_client;
    private $type_client;
    private $statut_client;


    /**
     * Get the value of id_abonnement
     */ 
    public function getId_abonnement()
    {
        return $this->id_abonnement;
    }

    /**
     * Set the value of id_abonnement
     *
     * @return  self
     */ 
    public function setId_abonnement($id_abonnement)
    {
        $this->id_abonnement = $id_abonnement;

        return $this;
    }

    /**
     * Get the value of nom_specif_revue
     */ 
    public function getNom_specif_revue()
    {
        return $this->nom_specif_revue;
    }

    /**
     * Set the value of nom_specif_revue
     *
     * @return  self
     */ 
    public function setNom_specif_revue($nom_specif_revue)
    {
        $this->nom_specif_revue = $nom_specif_revue;

        return $this;
    }

    /**
     * Get the value of periodicite
     */ 
    public function getPeriodicite()
    {
        return $this->periodicite;
    }

    /**
     * Set the value of periodicite
     *
     * @return  self
     */ 
    public function setPeriodicite($periodicite)
    {
        $this->periodicite = $periodicite;

        return $this;
    }

    /**
     * Get the value of type_papier_epapier
     */ 
    public function getType_papier_epapier()
    {
        return $this->type_papier_epapier;
    }

    /**
     * Set the value of type_papier_epapier
     *
     * @return  self
     */ 
    public function setType_papier_epapier($type_papier_epapier)
    {
        $this->type_papier_epapier = $type_papier_epapier;

        return $this;
    }

    /**
     * Get the value of date_debut
     */ 
    public function getDate_debut()
    {
        return $this->date_debut;
    }

    /**
     * Set the value of date_debut
     *
     * @return  self
     */ 
    public function setDate_debut($date_debut)
    {
        $this->date_debut = $date_debut;

        return $this;
    }

    /**
     * Get the value of date_fin
     */ 
    public function getDate_fin()
    {
        return $this->date_fin;
    }

    /**
     * Set the value of date_fin
     *
     * @return  self
     */ 
    public function setDate_fin($date_fin)
    {
        $this->date_fin = $date_fin;

        return $this;
    }

    /**
     * Get the value of prix
     */ 
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set the value of prix
     *
     * @return  self
     */ 
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get the value of monnaie
     */ 
    public function getMonnaie()
    {
        return $this->monnaie;
    }

    /**
     * Set the value of monnaie
     *
     * @return  self
     */ 
    public function setMonnaie($monnaie)
    {
        $this->monnaie = $monnaie;

        return $this;
    }


    /**
     * Get the value of mode_payment
     */ 
    public function getMode_payment()
    {
        return $this->mode_payment;
    }

    /**
     * Set the value of mode_payment
     *
     * @return  self
     */ 
    public function setMode_payment($mode_payment)
    {
        $this->mode_payment = $mode_payment;

        return $this;
    }

    /**
     * Get the value of ref_transaction
     */ 
    public function getRef_transaction()
    {
        return $this->ref_transaction;
    }

    /**
     * Set the value of ref_transaction
     *
     * @return  self
     */ 
    public function setRef_transaction($ref_transaction)
    {
        $this->ref_transaction = $ref_transaction;

        return $this;
    }

    /**
     * Get the value of id_client
     */ 
    public function getId_client()
    {
        return $this->id_client;
    }

    /**
     * Set the value of id_client
     *
     * @return  self
     */ 
    public function setId_client($id_client)
    {
        $this->id_client = $id_client;

        return $this;
    }

    /**
     * Get the value of type_client
     */ 
    public function getType_client()
    {
        return $this->type_client;
    }

    /**
     * Set the value of type_client
     *
     * @return  self
     */ 
    public function setType_client($type_client)
    {
        $this->type_client = $type_client;

        return $this;
    }

    /**
     * Get the value of statut_client
     */ 
    public function getStatut_client()
    {
        return $this->statut_client;
    }

    /**
     * Set the value of statut_client
     *
     * @return  self
     */ 
    public function setStatut_client($statut_client)
    {
        $this->statut_client = $statut_client;

        return $this;
    }

}    

?>