        <?php
           //   Nouvelles Bases de données MySQL

            $servername = '127.0.0.1';
            $username = 'root';
            $password = '';
            $basedonnee = 'boutiqueiris';
            $tabledonne01 = 'livresrevues'; 
            $tabledonne02 = 'auteurs'; 
            $tabledonne03 = 'articles'; 
            $tabledonne04 = 'plans'; 
            $tabledonne05 = 'commandes'; 
            $tabledonne06 = 'paniers'; 
            $tabledonne07 = 'compteur'; 
            $tabledonne08 = 'monnaies'; 
            $tabledonne09 = 'editeurs'; 
            $tabledonne10 = 'mots_cles'; 
            $tabledonne11 = 'abonnements'; 
            $tabledonne12 = 'client_invites'; 
            $tabledonne13 = 'client_membres'; 
            $tabledonne14 = 'priorites'; 
            $tabledonne15 = 'specif_revues'; 
            $tabledonne16 = 'themes'; 
            $tabledonne20 = 'utilisateurs'; 
            
            $connect2 = mysqli_connect($servername, $username, $password);
            $sql = "CREATE DATABASE IF NOT EXISTS $basedonnee";

            if (mysqli_query($connect2, $sql) === TRUE) {
                //echo "Database created successfully with the name newDB<br><br>";
            } else {
                echo "Error creating database: <br><br>" . $connect2->error;
            }

            mysqli_select_db($connect2, $basedonnee);

               
                    /*
                    Précisions pour $tabledonne01 "livresrevues" :
                        "auteur_princ" peut être "directeur_publication"
                        "auteur_princ" est ramené de la Table "auteurs"
                        "description" peut être "resume"
                        "morceau_choisi" peut être quelques lignes prises du livre
                        "reference" peut être suite alphanum uppercase commençant par une lettre
                        "monnaie" peut être ramenée de la Table "monnaies"
                        "image?" peut être ramenée du dossier "assets/images"
                        "redac_chef" est ramené de la Table "auteurs"
                        "editeur?" peut être de la Table "editeurs"
                        "fichier_ebook" peut être ramenée du dossier "assets/filesebook"
                        "format_livre" peut être de la forme xxXxx (en cmXcm)
                        "type_support" peut être "papier, ebook"
                        "isbn" peut être reste à définir
                        "doi" peut être reste à définir
                        "periodicite" peut être le nombre de parutions par an
                        "issn" peut être reste à définir
                        "issn_online" peut être reste à définir
                    */


                    $sql1 = "CREATE TABLE IF NOT EXISTS $tabledonne01".
                    "(  id_livre INT(11) NOT NULL AUTO_INCREMENT,
                        reference VARCHAR(30) NOT NULL,
                        nom_specif_revue VARCHAR(100) NOT NULL,
                        titre VARCHAR(100) NOT NULL,     
                        prix FLOAT(11) NOT NULL, 
                        monnaie VARCHAR(30) NOT NULL,
                        image1 VARCHAR(100),
                        image2 VARCHAR(100),
                        image3 VARCHAR(100),
                        image4 VARCHAR(100),
                        image5 VARCHAR(100),
                        auteur_princ1_nom VARCHAR(50) NOT NULL,     
                        auteur_princ1_prenom VARCHAR(50) NOT NULL,
                        auteur_princ1_institution VARCHAR(50) NOT NULL,
                        auteur_princ2_nom VARCHAR(50) NOT NULL,     
                        auteur_princ2_prenom VARCHAR(50) NOT NULL,
                        auteur_princ2_institution VARCHAR(50) NOT NULL,
                        redac_chef_nom VARCHAR(50) NOT NULL,     
                        redac_chef_prenom VARCHAR(50) NOT NULL,
                        redac_chef_institution VARCHAR(50) NOT NULL,
                        nbre_pages INT(4) NOT NULL,
                        description TEXT,
                        morceau_choisi TEXT,
                        editeur1 VARCHAR(50) NOT NULL,
                        editeur2 VARCHAR(50) NOT NULL,
                        date_parution DATE NOT NULL,
                        fichier_ebook VARCHAR(100),
                        format_livre VARCHAR(30),
                        type_support VARCHAR(50),
                        stock_iris INT(11),
                        stock_editeur INT(11),
                        isbn VARCHAR(30),
                        doi VARCHAR(30),
                        critique TEXT,
                        consign_auteur TEXT,
                        periodicite INT(2),
                        issn VARCHAR(30),
                        issn_online VARCHAR(30),". 
                    "PRIMARY KEY (id_livre),
                    CONSTRAINT livre_unique UNIQUE (id_livre, reference, titre, image1, prix),
                    CONSTRAINT reference_unique UNIQUE (reference),
                    FOREIGN KEY (auteur_princ1_nom, auteur_princ1_prenom, auteur_princ1_institution) REFERENCES auteurs (nom, prenom, institution) ON UPDATE CASCADE,
                    FOREIGN KEY (auteur_princ2_nom, auteur_princ2_prenom, auteur_princ2_institution) REFERENCES auteurs (nom, prenom, institution) ON UPDATE CASCADE,
                    FOREIGN KEY (redac_chef_nom, redac_chef_prenom, redac_chef_institution) REFERENCES auteurs (nom, prenom, institution) ON UPDATE CASCADE,
                    FOREIGN KEY (nom_specif_revue) REFERENCES specif_revues (nom_specif_revue) ON UPDATE CASCADE,
                    FOREIGN KEY (monnaie) REFERENCES monnaies (nom_monnaie) ON UPDATE CASCADE, 
                    FOREIGN KEY (editeur1) REFERENCES editeurs (nom_editeur) ON UPDATE CASCADE,
                    FOREIGN KEY (editeur2) REFERENCES editeurs (nom_editeur) ON UPDATE CASCADE )
                      ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";

                   
                    /*
                    Précisions pour $tabledonne02 "auteurs" :
                        "" peut être ""
                        "photo" peut être ramenée du dossier "assets/photosPers"
                    */
                    $sql2 = "CREATE TABLE IF NOT EXISTS $tabledonne02".
                    "(  id_auteur INT(11) NOT NULL AUTO_INCREMENT, 
                        nom VARCHAR(50) NOT NULL, 
                        prenom VARCHAR(50) NOT NULL, 
                        institution VARCHAR(50) NOT NULL,
                        fonction VARCHAR(100),
                        photo VARCHAR(100),". 
                    "PRIMARY KEY (id_auteur),
                    CONSTRAINT auteur_unique UNIQUE (nom, prenom, institution) )  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";

                    /*
                    Précisions pour $tabledonne03 "articles" :
                        "theme_article" peut être l'élément principal en discussion
                        "ref_livre" peut être ramené de la Table "livres (reference)"
                        "id_livre" peut être ramené de la Table "livres (id_livre)"
                        "emplacement" peut être "position de l'article dans le livre"
                        "id_auteur1 & id_auteur2" peuvent être ramenés de la Table "auteurs (id_auteur)"
                        "nbre_mots_pert" peut être "nombre de mots pertinents de l'article qui servira au calcul du prix/pxlivre"
                        "type_support" peut être "HTML, PDF, IMAGE, VIDEO, PAPIER_NORMAL, AUDIO, PAPIER_POCHE"
                    */

                    $sql3 = "CREATE TABLE IF NOT EXISTS $tabledonne03".
                    "(  id_article INT(11) NOT NULL AUTO_INCREMENT,
                        titre VARCHAR(100) NOT NULL,
                        theme_article VARCHAR(100) NOT NULL,
                        id_livre INT(11) NOT NULL,
                        ref_livre VARCHAR(30) NOT NULL,
                        emplacement int(4) NOT NULL,
                        fichier_earticle VARCHAR(100),
                        auteur1_nom VARCHAR(50) NOT NULL,     
                        auteur1_prenom VARCHAR(50) NOT NULL,
                        auteur1_institution VARCHAR(50) NOT NULL,
                        auteur2_nom VARCHAR(50) NOT NULL,     
                        auteur2_prenom VARCHAR(50) NOT NULL,
                        auteur2_institution VARCHAR(50) NOT NULL,
                        nbre_pages INT(4) NOT NULL,
                        nbre_mots_pert INT(6) NOT NULL,     
                        prix FLOAT(11), 
                        monnaie VARCHAR(30) NOT NULL,
                        type_support VARCHAR(30) NOT NULL,". 
                    "PRIMARY KEY (id_article), 
                    CONSTRAINT article_place_unique UNIQUE (titre, ref_livre, emplacement),
                    CONSTRAINT article_unique UNIQUE (id_article, titre, prix),
                    FOREIGN KEY (theme_article) REFERENCES themes (nom_theme) ON UPDATE CASCADE,
                    FOREIGN KEY (id_livre) REFERENCES livresrevues (id_livre) ON UPDATE CASCADE,
                    FOREIGN KEY (ref_livre) REFERENCES livresrevues (reference) ON UPDATE CASCADE,
                    FOREIGN KEY (auteur1_nom, auteur1_prenom, auteur1_institution) REFERENCES auteurs (nom, prenom, institution) ON UPDATE CASCADE,
                    FOREIGN KEY (auteur2_nom, auteur2_prenom, auteur2_institution) REFERENCES auteurs (nom, prenom, institution) ON UPDATE CASCADE,
                    FOREIGN KEY (monnaie) REFERENCES monnaies (nom_monnaie) ON UPDATE CASCADE )
                      ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
                     

                    /*
                    Précisions pour $tabledonne04 "plans" :
                        "id_artcl" peut être ramené de la Table "articles (id_article)"
                        "titre1" peut être "un titre en gras de l'article"
                        "ss_ttr11" peut être "un sous-titre en normal de l'article"
                    */
                    $sql4 = "CREATE TABLE IF NOT EXISTS $tabledonne04".
                    "(  id_plan INT(11) NOT NULL AUTO_INCREMENT,
                        id_artcl INT(11) NOT NULL,
                        titre1 VARCHAR(100) NOT NULL,
                        ss_ttr11 VARCHAR(100),
                        ss_ttr12 VARCHAR(100),
                        ss_ttr13 VARCHAR(100),
                        titre2 VARCHAR(100),
                        ss_ttr21 VARCHAR(100),
                        ss_ttr22 VARCHAR(100),
                        ss_ttr23 VARCHAR(100),
                        titre3 VARCHAR(100),
                        ss_ttr31 VARCHAR(100),
                        ss_ttr32 VARCHAR(100),
                        ss_ttr33 VARCHAR(100),". 
                    "PRIMARY KEY (id_plan),
                    CONSTRAINT article_unique UNIQUE (id_artcl),
                    FOREIGN KEY (id_artcl) REFERENCES articles(id_article) ON UPDATE CASCADE ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
       
                    /*
                    Précisions pour $tabledonne05 "commandes" :
                        "ref_commande" peut être calculé auto et peut faire référence à plusieures id_commande XXXXX
                        "type_client" peut être "numéro de tables par nature de client : 0(anonyme), 1(inscrit)"
                        "id_client" peut être ramené des Tables "clientsN° (id_client) OU chiffre id_compteur(>=10) de la table compteur"
                        "id_livre" peut être ramené de la Table "livres (id_livre)"
                        "ref_livre" peut être ramené de la Table "livres (reference)"
                        "ttr_livre" peut être ramené de la Table "livres (titre)"
                        "id_artcl" peut être ramené de la Table "articles (id_article)"
                        "frs_livrais" peut être ramené de la Table "sys_livraison" par calcul  XXXXXX
                        "mode_payment" peut être "1:carte ou 2:chèque"
                        "mode_livrais" peut être "retrait magasin, livraison domicile, poste, livraison express"
                    */
                    $sql5 = "CREATE TABLE IF NOT EXISTS $tabledonne05".
                    "(  id_commande INT(11) NOT NULL AUTO_INCREMENT,
                        ref_commande VARCHAR(30) NOT NULL,
                        type_client INT(1) NOT NULL,
                        id_client INT(11) NOT NULL,
                        id_livre INT(11) NOT NULL,
                        ref_livre VARCHAR(30) NOT NULL,
                        ttr_livre VARCHAR(100) NOT NULL,    
                        id_artcl INT(11),
                        ttr_artcl VARCHAR(100),    
                        nombre INT(11) NOT NULL,
                        px_ht FLOAT(11) NOT NULL,
                        tva_percent FLOAT(11) NOT NULL,
                        px_ttc FLOAT(11) NOT NULL,
                        frs_livrais FLOAT(11) NOT NULL,
                        monnaie VARCHAR(30) NOT NULL,
                        mode_payment INT(2) NOT NULL,
                        ref_transaction VARCHAR(100) NOT NULL,
                        date_ordre_livrais DATE NOT NULL,
                        nom_destin VARCHAR(30) NOT NULL, 
                        prenom_destin VARCHAR(30),
                        institution_destin VARCHAR(50),
                        adr_livrais VARCHAR(100) NOT NULL,
                        code_postal VARCHAR(10) NOT NULL,
                        ville VARCHAR(50) NOT NULL,
                        pays VARCHAR(50) NOT NULL,
                        telephone VARCHAR(15) NOT NULL,
                        mode_livrais VARCHAR(50) NOT NULL,
                        msg_livrais VARCHAR(1000),
                        date_livrais_prev DATE NOT NULL,
                        date_livrais_real DATE NOT NULL,". 
                    "PRIMARY KEY (id_commande),
                    FOREIGN KEY (monnaie) REFERENCES monnaies (nom_monnaie) ON UPDATE CASCADE )
                       ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
       

                    /*
                    Précisions pour $tabledonne06 "paniers" :
                        "type_client" peut être "numéro de tables par nature(role) de client : 0 (anonyme), 1(inscrit)"
                        "id_client" peut être ramené des Tables "clientsN° (id_client) OU chiffre id_compteur(>=10) de la table compteur"
                        "id_livre?" peut être ramené de la Table "livres (id_livre)"
                    */
                    $sql6 = "CREATE TABLE IF NOT EXISTS $tabledonne06".
                    "(  id_panier INT(11) NOT NULL AUTO_INCREMENT,
                        date_created  DATE NOT NULL,
                        type_client INT(11) NOT NULL,
                        id_client INT(11) NOT NULL,
                        monnaie VARCHAR(30) NOT NULL,
                        id_livre1 INT(11) NOT NULL,
                        ref_livre1 VARCHAR(30) NOT NULL,
                        ttr_livre1 VARCHAR(100) NOT NULL,
                        img_livre1 VARCHAR(100),
                        pxttc_livre1 FLOAT(11) NOT NULL,    
                        id_artcl1 INT(11),
                        ttr_artcl1 VARCHAR(100),
                        pxttc_artcl1 FLOAT(11) NOT NULL,
                        nbre1 INT(11) NOT NULL,
                        frs_livrais1 FLOAT(11) NOT NULL,
                        id_livre2 INT(11),
                        ref_livre2 VARCHAR(30),
                        ttr_livre2 VARCHAR(100),
                        img_livre2 VARCHAR(100),
                        pxttc_livre2 FLOAT(11) NOT NULL,    
                        id_artcl2 INT(11),
                        ttr_artcl2 VARCHAR(100),
                        pxttc_artcl2 FLOAT(11) NOT NULL,
                        nbre2 INT(11) NOT NULL,
                        frs_livrais2 FLOAT(11) NOT NULL,
                        id_livre3 INT(11),
                        ref_livre3 VARCHAR(30),
                        ttr_livre3 VARCHAR(100),
                        img_livre3 VARCHAR(100),
                        pxttc_livre3 FLOAT(11) NOT NULL,    
                        id_artcl3 INT(11),
                        ttr_artcl3 VARCHAR(100),
                        pxttc_artcl3 FLOAT(11) NOT NULL,
                        nbre3 INT(11) NOT NULL,
                        frs_livrais3 FLOAT(11) NOT NULL,". 
                    "PRIMARY KEY (id_panier),
                    FOREIGN KEY (monnaie) REFERENCES monnaies (nom_monnaie) ON UPDATE CASCADE,
                    FOREIGN KEY (id_livre1, ref_livre1, ttr_livre1, img_livre1, pxttc_livre1) REFERENCES livresrevues (id_livre, reference, titre, image1, prix) ON UPDATE CASCADE,
                    FOREIGN KEY (id_livre2, ref_livre2, ttr_livre2, img_livre2, pxttc_livre2) REFERENCES livresrevues (id_livre, reference, titre, image1, prix) ON UPDATE CASCADE,
                    FOREIGN KEY (id_livre3, ref_livre3, ttr_livre3, img_livre3, pxttc_livre3) REFERENCES livresrevues (id_livre, reference, titre, image1, prix) ON UPDATE CASCADE,
                    FOREIGN KEY (id_artcl1, ttr_artcl1, pxttc_artcl1) REFERENCES articles (id_article, titre, prix) ON UPDATE CASCADE,
                    FOREIGN KEY (id_artcl2, ttr_artcl2, pxttc_artcl2) REFERENCES articles (id_article, titre, prix) ON UPDATE CASCADE,
                    FOREIGN KEY (id_artcl3, ttr_artcl3, pxttc_artcl3) REFERENCES articles (id_article, titre, prix) ON UPDATE CASCADE )
                      ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
       

                    /*
                    Précisions pour $tabledonne07 "compteur" :
                        "id_compteur >=10, pour les clients anonymes"
                    */
                    $sql7 = "CREATE TABLE IF NOT EXISTS $tabledonne07".
                    "(  id_compteur INT(11) NOT NULL AUTO_INCREMENT,". 
                    "PRIMARY KEY (id_compteur) )  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";


                    /*
                    Précisions pour $tabledonne08 "monnaies" :
                    */
                    $sql8 = "CREATE TABLE IF NOT EXISTS $tabledonne08".
                    "(  id_monnaie INT(11) NOT NULL AUTO_INCREMENT,
                        nom_monnaie VARCHAR(30) NOT NULL,
                        symb_monnaie VARCHAR(5) NOT NULL,
                        val_commerc_en_euro FLOAT(11) NOT NULL,". 
                    "PRIMARY KEY (id_monnaie), 
                    CONSTRAINT monnaie_unique UNIQUE (nom_monnaie) )  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";


                    /*
                    Précisions pour $tabledonne09 "editeurs" :
                    */
                    $sql9 = "CREATE TABLE IF NOT EXISTS $tabledonne09".
                    "(  id_editeur INT(11) NOT NULL AUTO_INCREMENT,
                        code_editeur VARCHAR(30) NOT NULL,
                        nom_editeur VARCHAR(50) NOT NULL,
                        localite_editeur VARCHAR(30) NOT NULL,
                        pays_editeur VARCHAR(30) NOT NULL,
                        frs_livrais FLOAT(11) NOT NULL,". 
                    "PRIMARY KEY (id_editeur), 
                    CONSTRAINT editeur_unique UNIQUE (nom_editeur) )
                      ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";

                    /*
                    Précisions pour $tabledonne10 "mots_cles" :
                        "id_livre" peut être ramené de la Table "livres (id_livre)"
                        "id_artcl" peut être ramené de la Table "articles (id_artcl)"
                    */
                    $sql10 = "CREATE TABLE IF NOT EXISTS $tabledonne10".
                    "(  id_motcle INT(11) NOT NULL AUTO_INCREMENT,
                        nom_motcle VARCHAR(30) NOT NULL,
                        id_livre INT(11) NOT NULL,
                        id_artcl INT(11) NOT NULL,". 
                    "PRIMARY KEY (id_motcle),
                    CONSTRAINT mot_cle_unique UNIQUE (nom_motcle, id_livre, id_artcl)
                    )  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
                    

                    /*
                    Précisions pour $tabledonne11 "abonnements" :
                        "nom_specif_revue" peut être ramené de la Table "nom_specif_revue (nom_specif_revue)"
                        "periodicite" peut être le nombre de parutions par an
                        "type_client" peut être "numéro de tables par nature de client : 1, 2, 3, 4"
                        "id_client" peut être ramené des Tables "clientsN° (id_client)"
                    */
                    $sql11 = "CREATE TABLE IF NOT EXISTS $tabledonne11".
                    "(  id_abonnement INT(11) NOT NULL AUTO_INCREMENT,
                        nom_specif_revue VARCHAR(100) NOT NULL,
                        periodicite INT(2) NOT NULL,
                        type_papier_epapier VARCHAR(30) NOT NULL,
                        date_debut DATE NOT NULL,
                        date_fin DATE NOT NULL,
                        prix INT(11) NOT NULL,
                        monnaie VARCHAR(30) NOT NULL,
                        mode_payment INT(2) NOT NULL,
                        ref_transaction VARCHAR(100) NOT NULL,
                        type_client INT(11) NOT NULL,
                        id_client INT(11) NOT NULL,
                        statut_client INT(11) NOT NULL,". 
                    "PRIMARY KEY (id_abonnement),
                    FOREIGN KEY (nom_specif_revue) REFERENCES specif_revues (nom_specif_revue) ON UPDATE CASCADE,
                    FOREIGN KEY (monnaie) REFERENCES monnaies (nom_monnaie) ON UPDATE CASCADE )
                      ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";

                    /*
                    Précisions pour $tabledonne12 "client_invites" :
                        "num_compteur" peut être ramené de la Table "compteur (id_compteur)"
                        Si "institution" alors "fonction" obligatoire
                    */
                    $sql12 = "CREATE TABLE IF NOT EXISTS $tabledonne12".
                    "(  id_clt_anonym INT(11) NOT NULL AUTO_INCREMENT,
                        num_compteur INT(11) NOT NULL,
                        civilite VARCHAR(20) NOT NULL,
                        nom_destin VARCHAR(30) NOT NULL, 
                        prenom_destin VARCHAR(30), 
                        institution VARCHAR(50),
                        fonction VARCHAR(50),
                        email VARCHAR(100) NOT NULL,
                        offre_partenaire INT(1) NOT NULL,
                        offre_newsletter INT(1) NOT NULL,
                        adr_livrais VARCHAR(100) NOT NULL,
                        code_postal VARCHAR(10) NOT NULL,
                        ville VARCHAR(50) NOT NULL,
                        pays VARCHAR(50) NOT NULL,
                        zip_etat VARCHAR(5) NOT NULL,
                        indicatif VARCHAR(5) NOT NULL,
                        telephone VARCHAR(15) NOT NULL,". 
                    "PRIMARY KEY (id_clt_anonym) )  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";

                    /*
                    Précisions pour $tabledonne13 "client_membres" :
                        "type_membre" peut être "membre, adhérent, chercheur"
                        Si "institution" alors "fonction" obligatoire
                        "statut_client" peut être "individuel, étudiant, sans emploi, institution"
                        "prix_membre" calculé sur une année
                        "date_debut" permet un rappel avant un mois pour un renouvellement
                    */
                    $sql13 = "CREATE TABLE IF NOT EXISTS $tabledonne13".
                    "(  id_clt_membre INT(11) NOT NULL AUTO_INCREMENT,
                        type_membre INT(11) NOT NULL,
                        civilite VARCHAR(20) NOT NULL,
                        nom_membre VARCHAR(30) NOT NULL, 
                        prenom_membre VARCHAR(30), 
                        institution VARCHAR(50),
                        fonction VARCHAR(50),
                        statut_client INT(11) NOT NULL,
                        justif_etud_ou_ssemploi_instit VARCHAR(100) NOT NULL,
                        email VARCHAR(100) NOT NULL,
                        mdp VARCHAR(100) NOT NULL,
                        offre_partenaire INT(1) NOT NULL,
                        offre_newsletter INT(1) NOT NULL,
                        prix_membre FLOAT(11) NOT NULL, 
                        monnaie VARCHAR(30) NOT NULL,
                        date_debut DATE NOT NULL,
                        adr_livrais VARCHAR(100) NOT NULL,
                        code_postal VARCHAR(10) NOT NULL,
                        ville VARCHAR(50) NOT NULL,
                        pays VARCHAR(50) NOT NULL,
                        zip_etat VARCHAR(5) NOT NULL,
                        indicatif VARCHAR(5) NOT NULL,
                        telephone VARCHAR(15) NOT NULL,". 
                    "PRIMARY KEY (id_clt_membre),
                    CONSTRAINT email_unique UNIQUE (email),
                    FOREIGN KEY (monnaie) REFERENCES monnaies (nom_monnaie) ON UPDATE CASCADE )
                      ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
 
                      /*
                    Précisions pour $tabledonne14 "priorites" :
                        Si "institution" alors "fonction" obligatoire
                        "prix_chercheur" calculé sur une année
                        "date_debut" permet un rappel avant un mois pour un renouvellement
                    */
                    $sql14 = "CREATE TABLE IF NOT EXISTS $tabledonne14".
                    "(  pr1_acces_X1 INT(1) NOT NULL,
                        pr1_acces_X2 INT(1) NOT NULL,
                        pr1_acces_X3 INT(1) NOT NULL,
                        pr1_acces_X4 INT(1) NOT NULL,
                        pr2_acces_X1 INT(1) NOT NULL,
                        pr2_acces_X2 INT(1) NOT NULL,
                        pr2_acces_X3 INT(1) NOT NULL,
                        pr2_acces_X4 INT(1) NOT NULL,
                        pr3_acces_X1 INT(1) NOT NULL,
                        pr3_acces_X2 INT(1) NOT NULL,
                        pr3_acces_X3 INT(1) NOT NULL,
                        pr3_acces_X4 INT(1) NOT NULL,
                        pr4_acces_X1 INT(1) NOT NULL,
                        pr4_acces_X2 INT(1) NOT NULL,
                        pr4_acces_X3 INT(1) NOT NULL,
                        pr4_acces_X4 INT(1) NOT NULL )". 
                    " ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";

                  /*
                    Précisions pour $tabledonne15 "specif_revues" :
                        "periodicite" peut être le nombre de parutions par an
                    */
                    $sql15 = "CREATE TABLE IF NOT EXISTS $tabledonne15".
                    "(  id_specif_revue INT(11) NOT NULL AUTO_INCREMENT,
                        nom_specif_revue VARCHAR(100) NOT NULL,
                        periodicite INT(2) NOT NULL,
                        hors_serie INT(1) NOT NULL,
                        prix INT(11) NOT NULL,
                        monnaie VARCHAR(30) NOT NULL,". 
                    "PRIMARY KEY (id_specif_revue),
                    CONSTRAINT specif_revue_unique UNIQUE (nom_specif_revue),
                    FOREIGN KEY (monnaie) REFERENCES monnaies (nom_monnaie) ON UPDATE CASCADE )
                      ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";

                    /*
                    Précisions pour $tabledonne16 "themes" :
                    */
                    $sql16 = "CREATE TABLE IF NOT EXISTS $tabledonne16".
                    "(  id_theme INT(11) NOT NULL AUTO_INCREMENT,
                        nom_theme VARCHAR(30) NOT NULL,". 
                    "PRIMARY KEY (id_theme), 
                    CONSTRAINT theme_unique UNIQUE (nom_theme) )
                      ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
                    
                    /*
                    Précisions pour $tabledonne20 "utilisateurs" :
                    */
                    $sql20 = "CREATE TABLE IF NOT EXISTS $tabledonne20".
                    "( id_util INT(11) NOT NULL AUTO_INCREMENT,". 
                    "nom VARCHAR(30) NOT NULL, 
                     prenom VARCHAR(30) NOT NULL, 
                     pseudo VARCHAR(30) NOT NULL,
                     email VARCHAR(30) NOT NULL, 
                     pass VARCHAR(100), 
                     role INT(1),
                     operationnel INT(1),
                     date_created  DATE NOT NULL,". 
                    "PRIMARY KEY (id_util) )  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";


/****************************  Exécution des requêtes  ***********************************/

                    /* monnaies */
                    if (mysqli_query($connect2, $sql8) === TRUE) {
                        //echo "Table monnaies created successfully";
                        $sqldonn="INSERT INTO `monnaies` (`nom_monnaie`, `symb_monnaie`, `val_commerc_en_euro`) 
                            VALUES ('Euro', 'Є', '1')";
                        $res22 = mysqli_query($connect2, $sqldonn);

                        $sqldonn="INSERT INTO `monnaies` (`nom_monnaie`, `symb_monnaie`, `val_commerc_en_euro`) 
                            VALUES ('Dollar', '$', '0.94')";
                        $res22 = mysqli_query($connect2, $sqldonn);

                        $sqldonn="INSERT INTO `monnaies` (`nom_monnaie`, `symb_monnaie`, `val_commerc_en_euro`) 
                            VALUES ('Livre Sterling', '£', '1.15')";
                        $res22 = mysqli_query($connect2, $sqldonn);

                    } else {
                        echo "Error creating table: ".mysqli_error($connect2);
                    }
      
                    /* editeurs */
                    if (mysqli_query($connect2, $sql9) === TRUE) {
                        //echo "Table editeurs created successfully";
                        $sqlx="SELECT * FROM $tabledonne09";
                        $resx= mysqli_query($connect2, $sqlx);
                        if ($row = mysqli_fetch_assoc($resx)) {
                              //echo "";
                        } else {
                                //echo "SALUTTTTT15";

                            $sqldonn="INSERT INTO `editeurs` (`code_editeur`, `nom_editeur`, `localite_editeur`, `pays_editeur`, `frs_livrais`) 
                            VALUES ('', '', '', '', '0'),
                            ('E5677', 'ERT', 'Zere ZEE', 'Gsddsd', '10'), 
                            ('Y5687', 'ZOO', 'Ztttt ZTT', 'Hfdsss', '20'),
                            ('Z4566', 'TAA', 'Uyyyy OOO', 'Kjhhggf', '5')";
                            mysqli_query($connect2, $sqldonn);
                        }
                    } else {
                        echo "Error creating table: ".mysqli_error($connect2);
                    }
                    
                    /* auteurs */
                    if (mysqli_query($connect2, $sql2) === TRUE) {
                        //echo "Table auteurs created successfully";
                        $sqlx="SELECT * FROM $tabledonne02";
                        $resx= mysqli_query($connect2, $sqlx);
                        if ($row = mysqli_fetch_assoc($resx)) {
                                //echo "";
                            } else {
                                //echo "SALUTTTTT15";

                            $sqldonn="INSERT INTO `auteurs` (`nom`, `prenom`, `institution`, `fonction`, `photo`) 
                            VALUES ('', '', '', '', ''),
                            ('ZEE', 'Zee', 'Zere ZEE', 'Gsddsd', '20080519-alex-art-1.jpg'), 
                            ('ZRR', 'Zrr', 'Ztttt ZTT', 'Hfdsss', '20050801-alex-art-1.jpg'),
                            ('TTT', 'Ttt', 'Uyyyy OOO', 'Kjhhggf', '4DPIb2EvcxOW_1824x0_wmhqkGbg.jpg')";
                            mysqli_query($connect2, $sqldonn);
                        }
                    } else {
                        echo "Error creating table: ".mysqli_error($connect2);
                    }

                    /* specif_revues */
                    if (mysqli_query($connect2, $sql15) === TRUE) {
                        //echo "Table specif_revues created successfully";

                        $sqlx="SELECT * FROM $tabledonne15";
                        $resx= mysqli_query($connect2, $sqlx);
                        if ($row = mysqli_fetch_assoc($resx)) {
                              //echo "";
                        } else {
                            //echo "SALUTTTTT15";

                        $sqldonn="INSERT INTO `specif_revues` (`nom_specif_revue`, `periodicite`,
                         `hors_serie`, `prix`, `monnaie`) 
                         VALUES ('', '0', '0', '0','Euro'),('Yui', '4', '1', '20','Euro')";
                        mysqli_query($connect2, $sqldonn);
                        }
                    } else {
                        echo "Error creating table: ".mysqli_error($connect2);
                    }


                    /* livresrevues */
                    if (mysqli_query($connect2, $sql1) === TRUE) {
                        
                        //echo "Table livresrevues created successfully";
    
                        $sqlx="SELECT * FROM $tabledonne01";
                        $resx= mysqli_query($connect2, $sqlx);
                        if ($row = mysqli_fetch_assoc($resx)) {
                              echo "";
                        } else {
                            //echo "SALUTTTTT";
                        $sqldonn="INSERT INTO `livresrevues` (`reference`, `nom_specif_revue`, `titre`,
                         `prix`, `monnaie`, `image1`, `image2`, `image3`, `image4`, `image5`,
                         `auteur_princ1_nom`, `auteur_princ1_prenom`, `auteur_princ1_institution`,
                         `auteur_princ2_nom`, `auteur_princ2_prenom`, `auteur_princ2_institution`,
                         `redac_chef_nom`, `redac_chef_prenom`, `redac_chef_institution`,
                         `nbre_pages`, `description`, `morceau_choisi`,
                         `editeur1`, `editeur2`, `date_parution`, `fichier_ebook`,
                         `format_livre`, `type_support`, `stock_iris`, `stock_editeur`,
                         `isbn`, `doi`, `critique`, `consign_auteur`, `periodicite`, `issn`,
                         `issn_online`) 
                         VALUES ('', '', '', '0','Euro', '', '', '', '', '', '', '', '', '', '', '','',
                         '','', '0','','', '','','2100-01-01','', '','','0','0','','','', '','0','',''),
                         ('Z234556', '', 'Zere ZEE', '1234','Euro', '20080519-alex-art-1.jpg', 
                         '20080519-alex-art-1.jpg', '20080519-alex-art-1.jpg', '20080519-alex-art-1.jpg'
                         , '20080519-alex-art-1.jpg', 'ZEE', 'Zee', 'Zere ZEE',
                         'ZRR', 'Zrr', 'Ztttt ZTT','','','', '340',
                         'sdcsdcsd v sdvq vqdvq ','dvtt bynyn  uu, ,yu','ERT','','2020-02-01','a.pdf',
                         'Poche','Papier','44','50','54534536','6336363','bsb sf s fbs fb sfb s',
                         'yjt yjty jt jty j','0','2342424','656546456'),     
                         ('A5678', 'Yui', 'Iuy', '654','Euro', '4DPIb2EvcxOW_1824x0_wmhqkGbg.jpg', 
                         '4DPIb2EvcxOW_1824x0_wmhqkGbg.jpg', '4DPIb2EvcxOW_1824x0_wmhqkGbg.jpg',
                         '4DPIb2EvcxOW_1824x0_wmhqkGbg.jpg',
                         '4DPIb2EvcxOW_1824x0_wmhqkGbg.jpg', 'ZRR', 'Zrr', 'Ztttt ZTT','','','',
                         'TTT', 'Ttt', 'Uyyyy OOO', '280',
                         'jgjks sg gigsdcsdcsd v sdvq vqdvq ','ggsgs g g dvtt bynyn  uu, ,yu','TAA','',
                         '2001-05-03','b.pdf',
                         'Normal','Papier','0','0','43434','23467','vdvdvdv bsb sf s fbs fb sfb s',
                         'tntynty  yjt yjty jt jty j','4','226565777','98876'),     
                         ('I999999', '', 'Iuy', '54','Dollar', '4DPIb2EvcxOW_1824x0_wmhqkGbg.jpg', 
                         '4DPIb2EvcxOW_1824x0_wmhqkGbg.jpg', '4DPIb2EvcxOW_1824x0_wmhqkGbg.jpg',
                         '4DPIb2EvcxOW_1824x0_wmhqkGbg.jpg',
                         '4DPIb2EvcxOW_1824x0_wmhqkGbg.jpg', 'ZRR', 'Zrr', 'Ztttt ZTT','','','',
                         'TTT', 'Ttt', 'Uyyyy OOO', '280',
                         'fvdf jgjks sg gigsdcsdcsd v sdvq vqdvq ',
                         'kjkjk ggsgs g g dvtt bynyn  uu, ,yu','ZOO','','2008-06-07','b.pdf',
                         'Normal','Ebook','0','0','43434','23467','vdvdvdv bsb sf s fbs fb sfb s',
                         'tntynty  yjt yjty jt jty j','4','65433','98765')";
                        mysqli_query($connect2, $sqldonn);
                        }
                    } else {
                        echo "Error creating table: ".mysqli_error($connect2);
                    }

                    /* themes */

                    if (mysqli_query($connect2, $sql16) === TRUE) {
                        //echo "Table themes created successfully";
                        $sqlx="SELECT * FROM $tabledonne16";
                        $resx= mysqli_query($connect2, $sqlx);
                        if ($row = mysqli_fetch_assoc($resx)) {
                            //echo "";
                        } else {
                            //echo "SALUTTTTT";
                        $sqldonn="INSERT INTO `themes` (`nom_theme`) 
                        VALUES (''),('theme1'),('theme2'),('theme3'),('theme4'),('theme5'),('theme6')";
                        mysqli_query($connect2, $sqldonn);
                        }
                    } else {
                        echo "Error creating table: ".mysqli_error($connect2);
                    }

 
                    /* articles */

                    if (mysqli_query($connect2, $sql3) === TRUE) {
                        //echo "Table articles created successfully";
                        $sqlx="SELECT * FROM $tabledonne03";
                        $resx= mysqli_query($connect2, $sqlx);
                        if ($row = mysqli_fetch_assoc($resx)) {
                            //echo "";
                        } else {
                            //echo "SALUTTTTT";

                           

                        $sqldonn="INSERT INTO `articles` (`titre`, `theme_article`, `id_livre`,
                         `ref_livre`, `emplacement`, `fichier_earticle`, `auteur1_nom`, `auteur1_prenom`,
                         `auteur1_institution`, `auteur2_nom`, `auteur2_prenom`, `auteur2_institution`, `nbre_pages`,
                        `nbre_mots_pert`, `prix`, `monnaie`, `type_support`) 
                        VALUES ('', '', '1','', '0','', '', '', '',
                         '', '', '', '0', '0', '0', 'Euro', 'Ebook'),     
                         ('Préface', 'theme1', '2','Z234556', '0','zeer0.pdf', 'ZEE', 'Zee', 'Zere ZEE',
                         'ZRR', 'Zrr', 'Ztttt ZTT', '2', '3', '1', 'Euro', 'Ebook'),     
                         ('Préambule', 'theme2', '2','Z234556', '1','zeer1.pdf', 'ZEE', 'Zee', 'Zere ZEE',
                        'ZRR', 'Zrr', 'Ztttt ZTT', '1', '2', '1', 'Euro', 'Ebook'),
                        ('Hkj jf unfu djdjd', 'theme3', '1','', '0','jhff.pdf', 'ZEE', 'Zee', 'Zere ZEE',
                        '', '', '', '3', '6', '2', 'Euro', 'Ebook')";
                        mysqli_query($connect2, $sqldonn);
                        }
                    } else {
                        echo "Error creating table: ".mysqli_error($connect2);
                    }


                   /* mots_cles */
                   if (mysqli_query($connect2, $sql10) === TRUE) {
                    //echo "Table mots_cles created successfully";
                    
                        $sqlx="SELECT * FROM $tabledonne10";
                        $resx= mysqli_query($connect2, $sqlx);
                        if ($row = mysqli_fetch_assoc($resx)) {
                            //echo "";
                        } else {
                            //echo "SALUTTTTT";
                        $sqldonn="INSERT INTO `mots_cles` (`nom_motcle`, `id_livre`, `id_artcl`) 
                        VALUES ('Gfkfkf', '2', '2'),('Gfkfkf', '2', '3'),('Hktkt', '2', '2'),('Kjhhg', '2', '2'),
                        ('Kjhhg', '2', '3'),('Dezz','1','4'),('Gfkfkf', '1', '4'),('Hktkt', '1', '4'),
                        ('Srer','1','4'),('Yteezz', '1', '4'),('Iuyytt', '1', '4')";
                        mysqli_query($connect2, $sqldonn);
                        }
                    } else {
                        echo "Error creating table: ".mysqli_error($connect2);
                    }

                    /* plans */
                    if (mysqli_query($connect2, $sql4) === TRUE) {
                        //echo "Table plans created successfully";
                    } else {
                        echo "Error creating table: ".mysqli_error($connect2);
                    }

                    /* compteur */
                    if (mysqli_query($connect2, $sql7) === TRUE) {
                        //echo "Table compteur created successfully";
                        $sqlx="SELECT * FROM $tabledonne07";
                        $resx= mysqli_query($connect2, $sqlx);
                        if ($row = mysqli_fetch_assoc($resx)) {
                              //echo "";
                        } else {
                            $sqldonn07="INSERT INTO `compteur` () VALUES ()";
                            $res22 = mysqli_query($connect2, $sqldonn07);
                        }
                    } else {
                        echo "Error creating table: ".mysqli_error($connect2);
                    }

                    /* paniers */

                    if (mysqli_query($connect2, $sql6) === TRUE) {
                        //echo "Table paniers created successfully";

                        $sqlx="SELECT * FROM $tabledonne06";
                        $resx= mysqli_query($connect2, $sqlx);
                        if ($row = mysqli_fetch_assoc($resx)) {
                              //echo "";
                        } else {
                            $sqldonn06="INSERT INTO `paniers` (`date_created`, `type_client`, `id_client`, `monnaie`, 
                            `id_livre1`, `ref_livre1`, `ttr_livre1`,
                            `img_livre1`, `pxttc_livre1`, `id_artcl1`, `ttr_artcl1`, `pxttc_artcl1`,
                            `nbre1`, `frs_livrais1`, `id_livre2`, `ref_livre2`, `ttr_livre2`,
                            `img_livre2`, `pxttc_livre2`, `id_artcl2`, `ttr_artcl2`, `pxttc_artcl2`,
                            `nbre2`, `frs_livrais2`, `id_livre3`, `ref_livre3`, `ttr_livre3`,
                            `img_livre3`, `pxttc_livre3`, `id_artcl3`, `ttr_artcl3`, `pxttc_artcl3`,
                            `nbre3`, `frs_livrais3`) 
                            VALUES ('2020-04-04', '1', '15','Euro', 
                            '2','Z234556', 'Zere ZEE', '20080519-alex-art-1.jpg',
                             '1234','30','Préface','1', '1','0',
                            '3','A5678', 'Iuy', '4DPIb2EvcxOW_1824x0_wmhqkGbg.jpg',
                             '654','1','','0', '1','5',
                             '1','', '', '',
                             '0','1','','0', '0','0'),
                             ('2020-04-04', '0', '2', 'Euro',
                             '4','I999999', 'Iuy', '4DPIb2EvcxOW_1824x0_wmhqkGbg.jpg',
                             '54','1','','0', '1','20',
                            '1','', '', '',
                             '0','1','','0', '0','0',
                             '1','', '', '',
                             '0','1','','0', '0','0'),
                             ('2020-04-05', '0', '41', 'Euro',
                             '4','I999999', 'Iuy', '4DPIb2EvcxOW_1824x0_wmhqkGbg.jpg',
                             '54','1','','0', '1','20',
                            '1','', '', '',
                             '0','1','','0', '0','0',
                             '1','', '', '',
                             '0','1','','0', '0','0')";
                             mysqli_query($connect2, $sqldonn06);
                        }
                    } else {
                        echo "Error creating table: ".mysqli_error($connect2);
                    }
                    
                     
                    /* abonnements */

                    if (mysqli_query($connect2, $sql11) === TRUE) {
                        //echo "Table abonnements created successfully";
                        $sqlx="SELECT * FROM $tabledonne11";
                        $resx= mysqli_query($connect2, $sqlx);
                        if ($row = mysqli_fetch_assoc($resx)) {
                              //echo "";
                        } else {
                            $sqldonn11="INSERT INTO `abonnements` (`nom_specif_revue`, `periodicite`,
                            `type_papier_epapier`, `date_debut`, `date_fin`, `prix`, `monnaie`, 
                            `mode_payment`, `ref_transaction`, 
                            `type_client`, `id_client`, `statut_client`) 
                            VALUES ('Yui', '4', 'Papier',
                             '2020-06-04', '2021-06-03', '50', 'Euro',
                             '1', '76585488339700',
                             '4', '14', '1')";
                            $res22 = mysqli_query($connect2, $sqldonn11);
                        }
                    } else {
                        echo "Error creating table: ".mysqli_error($connect2);
                    }


                    /*client_invites*/

                    if (mysqli_query($connect2, $sql12) === TRUE) {
                        //echo "Table client_invites created successfully";
                        $sqlx="SELECT * FROM $tabledonne12";
                        $resx= mysqli_query($connect2, $sqlx);
                        if ($row = mysqli_fetch_assoc($resx)) {
                              //echo "";
                        } else {
                            $sqldonn12="INSERT INTO `client_invites` (`num_compteur`, `civilite`,`nom_destin`,
                             `prenom_destin`,`institution`, `fonction`,`email`, `offre_partenaire`,
                            `offre_newsletter`, `adr_livrais`,`code_postal`, `ville`,`pays`, `zip_etat`,
                            `indicatif`, `telephone`) 
                            VALUES ('2', '1','DFSFVS', 'DVQ VSDV','DVD', 'FVFFV','DQD@NJVNF.GT', '0','1',
                             'XGSSB FJWF   H FH HF ','98766', 'SDQG ','France', 'FR','+33', '4565766543'),
                             ('3', '2','RRGR', 'RER','', '','DSDS@JHGG.HT', '0','0',
                             'FJVJF JDFV FVDF','76543', 'LHKHJG','Suisse', 'CH','+33', '78786985')";
                            $res22 = mysqli_query($connect2, $sqldonn12);

                        }
                    } else {
                        echo "Error creating table: ".mysqli_error($connect2);
                    }
                    
                    /*client_membres*/
                    if (mysqli_query($connect2, $sql13) === TRUE) {
                        //echo "Table client_membres created successfully";
                        $sqlx="SELECT * FROM $tabledonne13";
                        $resx= mysqli_query($connect2, $sqlx);
                        if ($row = mysqli_fetch_assoc($resx)) {
                              //echo "";
                        } else {
                            $sqldonn13="INSERT INTO `client_membres` (`type_membre`, `civilite`,`nom_membre`,
                            `prenom_membre`,`institution`, `fonction`, `statut_client`,
                             `justif_etud_ou_ssemploi_instit`, `email`, `mdp`, `offre_partenaire`,
                           `offre_newsletter`, `prix_membre`, `monnaie`, `date_debut`,                   
                           `adr_livrais`,`code_postal`, `ville`,`pays`, `zip_etat`,
                           `indicatif`, `telephone`) 
                            VALUES ('4', '1','DFSFVS', 'DVQ VSDV','DVD', 'FVFFV','1', '', 
                            'DQD@NJVNF.GT', MD5('a'), '0','1', '70', 'Euro', '2020-04-03',
                             'XGSSB FJWF dcsd  H FH HF ','98766', 'SDQG ','France', 'FR',
                             '+33', '4578766543'),
                              ('3', '2','RRGR', 'RER','FFFSSF', 'BFBFBFB','4', 'CVDahmaniApec2019.pdf',
                               'DSDS@JHGG.HT', MD5('b'), '0','0', '265', 'Dollar', '2020-04-24',
                             'FJVJF JDFV vsdvdv FVDF','76543', 'LHKHJG','Suisse', 'CH','+33', '78789085')";
                            $res22 = mysqli_query($connect2, $sqldonn13);
                        }
                    } else {
                        echo "Error creating table: ".mysqli_error($connect2);
                    }

                    /*priorites*/
                    if (mysqli_query($connect2, $sql14) === TRUE) {
                        //echo "Table priorites created successfully";
                    } else {
                        echo "Error creating table: ".mysqli_error($connect2);
                    }

               
                    /*commandes*/
                    if (mysqli_query($connect2, $sql5) === TRUE) {
                        //echo "Table commandes created successfully";
                        $sqlx="SELECT * FROM $tabledonne05";
                        $resx= mysqli_query($connect2, $sqlx);
                        if ($row = mysqli_fetch_assoc($resx)) {
                              //echo "";
                        } else {
                            $sqldonn05="INSERT INTO `commandes` (`ref_commande`,`type_client`, `id_client`,
                             `id_livre`, `ref_livre`, `ttr_livre`,
                            `id_artcl`, `ttr_artcl`, `nombre`, `px_ht`, `tva_percent`, `px_ttc`,
                            `frs_livrais`,`monnaie`, `mode_payment`, `ref_transaction`,
                            `date_ordre_livrais`,  `nom_destin`,  `prenom_destin`,  `institution_destin`,
                            `adr_livrais`, `code_postal`, `ville`, `pays`, `telephone`, 
                            `mode_livrais`, `msg_livrais`, `date_livrais_prev`, `date_livrais_real`)
                            VALUES ('15', '1','15','2','Z234556', 'Zere ZEE',
                            '30', 'Préface','1', '0.95','0.05', '1',
                            '0', 'Euro','1', '76584848339393',
                            '2020-06-05','RRGR','RER', 'FFFSSF',
                            'FJVJF JDFV vsdvdv FVDF','76543','LHKHJG', 'Suisse','+3378789085',
                            'Email','sij sni j isj sij', '2020-06-05', '2100-01-01'),
                            ('15', '1','15','3','A5678', 'Iuy',
                            '1', '','1', '622.85','0.05', '654',
                            '10', 'Euro','1', '76584848339393',
                            '2020-06-05','RRGR','RER', 'FFFSSF',
                            'FJVJF JDFV vsdvdv FVDF','76543','LHKHJG', 'Suisse','+3378789085',
                            'Domicile','sij sni j isj sij', '2020-06-25', '2100-01-01'),
                            ('16', '0','2','4','I999999', 'Iuy',
                            '1', '','1', '51.42','0.05', '54',
                            '20', 'Euro','1', '54584848789091',
                            '2020-06-07','DFSFVS','DVQ VSDV', 'DVD',
                            'XGSSB FJWF   H FH HF ','98766','SDQG 	', 'France','+334565766543',
                            'Domicile','jfj, fffk kffkdd.', '2020-06-26', '2100-01-01'),                           
                            ('17', '1','15',
                            '3','A5678', 'Yui',
                            '1', '','1', '0','0.05', '0',
                            '0', 'Euro','1', '76585488339700',
                            '2020-06-04','RRGR','RER', 'FFFSSF',
                            'FJVJF JDFV vsdvdv FVDF','76543','LHKHJG', 'Suisse','+3378789085',
                            'Poste','', '2020-06-21', '2100-01-01'),
                            ('17', '1','15',
                            '3','A5678', 'Yui',
                            '1', '','1', '0','0.05', '0',
                            '0', 'Euro','1', '76585488339700',
                            '2020-06-04','RRGR','RER', 'FFFSSF',
                            'FJVJF JDFV vsdvdv FVDF','76543','LHKHJG', 'Suisse','+3378789085',
                            'Poste','', '2020-09-21', '2100-01-01'),
                            ('17', '1','15',
                            '3','A5678', 'Yui',
                            '1', '','1', '0','0.05', '0',
                            '0', 'Euro','1', '76585488339700',
                            '2020-06-04','RRGR','RER', 'FFFSSF',
                            'FJVJF JDFV vsdvdv FVDF','76543','LHKHJG', 'Suisse','+3378789085',
                            'Poste','', '2020-12-21', '2100-01-01'),
                            ('17', '1','15',
                            '3','A5678', 'Yui',
                            '1', '','1', '0','0.05', '0',
                            '0', 'Euro','1', '76585488339700',
                            '2020-06-04','RRGR','RER', 'FFFSSF',
                            'FJVJF JDFV vsdvdv FVDF','76543','LHKHJG', 'Suisse','+3378789085',
                            'Poste','', '2021-03-21', '2100-01-01'),
                            ('17', '1','15',
                            '3','A5678', 'Yui',
                            '1', '','1', '0','0.05', '0',
                            '0', 'Euro','1', '76585488339700',
                            '2020-06-04','RRGR','RER', 'FFFSSF',
                            'FJVJF JDFV vsdvdv FVDF','76543','LHKHJG', 'Suisse','+3378789085',
                            'Email','', '2020-06-21', '2100-01-01'),
                            ('17', '1','15',
                            '3','A5678', 'Yui',
                            '1', '','1', '0','0.05', '0',
                            '0', 'Euro','1', '76585488339700',
                            '2020-06-04','RRGR','RER', 'FFFSSF',
                            'FJVJF JDFV vsdvdv FVDF','76543','LHKHJG', 'Suisse','+3378789085',
                            'Email','', '2020-09-21', '2100-01-01'),
                            ('17', '1','15',
                            '3','A5678', 'Yui',
                            '1', '','1', '0','0.05', '0',
                            '0', 'Euro','1', '76585488339700',
                            '2020-06-04','RRGR','RER', 'FFFSSF',
                            'FJVJF JDFV vsdvdv FVDF','76543','LHKHJG', 'Suisse','+3378789085',
                            'Email','', '2020-12-21', '2100-01-01'),
                            ('17', '1','15',
                            '3','A5678', 'Yui',
                            '1', '','1', '0','0.05', '0',
                            '0', 'Euro','1', '76585488339700',
                            '2020-06-04','RRGR','RER', 'FFFSSF',
                            'FJVJF JDFV vsdvdv FVDF','76543','LHKHJG', 'Suisse','+3378789085',
                            'Email','', '2021-03-21', '2100-01-01')";
                             mysqli_query($connect2, $sqldonn05);
                        }
                    } else {
                        echo "Error creating table: ".mysqli_error($connect2);
                    }


                    /*utilisateurs*/
                    if (mysqli_query($connect2, $sql20) === TRUE) {
                        $sqlx="SELECT * FROM $tabledonne20";
                        $resx= mysqli_query($connect2, $sqlx);
                        if ($row = mysqli_fetch_assoc($resx)) {
                              //echo "";
                        } else {
                            $sqldonn20="INSERT INTO `utilisateurs` (`nom`, `prenom`, `pseudo`, `email`, `pass`, `role`, `operationnel`, `date_created`) 
                            VALUES ('', '', '', '', MD5('HJHjfvjnfnjlskxkok1928203948!!**$$'), '0', '1', '1970-01-01')";
                            $res22 = mysqli_query($connect2, $sqldonn20);

                            $sqldonn20="INSERT INTO `utilisateurs` (`nom`, `prenom`, `pseudo`, `email`, `pass`, `role`, `operationnel`, `date_created`) 
                            VALUES ('DAHMANI', 'Azz', 'DAHMANI', 'afpadahmani@orange.fr', MD5('xyz'), '1', '1', '2020-05-05')";
                            $res22 = mysqli_query($connect2, $sqldonn20);

                            $sqldonn20="INSERT INTO `utilisateurs` (`nom`, `prenom`, `pseudo`, `email`, `pass`, `role`, `operationnel`, `date_created`) 
                            VALUES ('TOSTAO', 'Enrique', 'TOSTAO', 'afpatostao@free.fr', MD5('abc'), '2', '1', '2020-05-05')";
                            $res22 = mysqli_query($connect2, $sqldonn20);

                        }
                    } else {
                        echo "Error creating table: ".mysqli_error($connect2);
                    }
                                    
                    
            mysqli_close($connect2);
            

?>

