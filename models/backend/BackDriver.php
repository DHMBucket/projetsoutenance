<?php

require_once('./models/Driver.php');
require_once('./models/User.php');
require_once('./models/ClientMembre.php');
require_once('./models/ClientInvite.php');
require_once('./models/MargeBrute.php');


class BackDriver extends Driver {

    public function __construct($base) {
        Parent::__construct($base);
    }
/****************************************************************************************/

public function calculMB($dataCmd, $dataMbr, $dataMon, $debut, $fin) 
{
    $margeBrute= new Margebrute(); 
    $dtedeb= strtotime($debut);
    $dtefin= strtotime($fin);
    if ($dtefin < $dtedeb) {
        $dtefin= $dtedeb;
    }

    $mbCmd= 0; $mbfrslivr= 0;
    if (count($dataCmd) > 0) {
        foreach ($dataCmd as $cmd) {
            $dtecmd= strtotime($cmd->getDate_ordre_livrais());
            if ($dtecmd >= $dtedeb && $dtecmd <= $dtefin) {
                foreach ($dataMon as $mon) {
                    if ($cmd->getMonnaie() == $mon->getNom_monnaie()) {
                        $mbCmd += $cmd->getPx_ht()*$cmd->getNombre()*$mon->getVal_commerc_en_euro();
                        $mbfrslivr += $cmd->getFrs_livrais()*.8*$mon->getVal_commerc_en_euro();
                        break;
                    }
                }
            }
        }
    }

    $mbAdh= 0;
    if (count($dataMbr) > 0) {
        foreach ($dataMbr as $mbr) {
            $dtembr= strtotime($mbr->getDate_debut());
            if ($dtembr >= $dtedeb && $dtembr <= $dtefin) {
                foreach ($dataMon as $mon) {
                    if ($mbr->getMonnaie() == $mon->getNom_monnaie()) {
                        $mbAdh += $mbr->getPrix_membre()*.8*$mon->getVal_commerc_en_euro();
                        break;
                    }
                }
            }
        }
    }



    $margeBrute->setMbCmd($mbCmd); $margeBrute->setMbfrslivr($mbfrslivr); $margeBrute->setMbAdh($mbAdh);
    return $margeBrute;
}


/****************************************************************************************/

public function suppressRevue($idrev) 
{
   
    $m00= $idrev;
    $sql= "DELETE FROM `specif_revues` WHERE `id_specif_revue` = ?";
    $res= $this->base->prepare($sql);
    $res->execute([$m00]); 
    if ($res) { $res->closeCursor(); return true;
    } else { $res->closeCursor(); return false;}

}


public function suppressArticle($idart) 
{
   
    $m00= $idart;
    $sql= "DELETE FROM `articles` WHERE `id_article` = ?";
    $res= $this->base->prepare($sql);
    $res->execute([$m00]); 
    if ($res) { $res->closeCursor(); return true;
    } else { $res->closeCursor(); return false;}

}


public function suppressTheme($idthm) 
{
   
    $m00= $idthm;
    $sql= "DELETE FROM `themes` WHERE `id_theme` = ?";
    $res= $this->base->prepare($sql);
    $res->execute([$m00]); 
    if ($res) { $res->closeCursor(); return true;
    } else { $res->closeCursor(); return false;}

}

public function suppressClientMembre($idmbr) 
{
   
    $m00= $idmbr;
    $sql= "DELETE FROM `client_membres` WHERE `id_clt_membre` = ?";
    $res= $this->base->prepare($sql);
    $res->execute([$m00]); 
    if ($res) { $res->closeCursor(); return true;
    } else { $res->closeCursor(); return false;}

}


public function suppressClientInvite($idcti) 
{
   
    $m00= $idcti;
    $sql= "DELETE FROM `client_invites` WHERE `id_clt_anonym` = ?";
    $res= $this->base->prepare($sql);
    $res->execute([$m00]); 
    if ($res) { $res->closeCursor(); return true;
    } else { $res->closeCursor(); return false;}

}


public function suppressPanier($idpan) 
{
    
    $m00= $idpan;
    $sql= "DELETE FROM `paniers` WHERE `id_panier` = ?";
    $res= $this->base->prepare($sql);
    $res->execute([$m00]); 
    if ($_SESSION['dataA'][0]->getType_client() == $_SESSION['Auth']['role'] && 
        $_SESSION['dataA'][0]->getId_client() == $_SESSION['Auth']['numero']) {
            $_SESSION['panier'] = 0;
        }
    

    if ($res) { $res->closeCursor(); return true;
    } else { $res->closeCursor(); return false;}

}


public function suppressCommande($idcmd) 
{

    $m00= $idcmd;
    $sql= "DELETE FROM `commandes` WHERE `id_commande` = ?";
    $res= $this->base->prepare($sql);
    $res->execute([$m00]); 

    if ($res) { return true;} else { return false;}

}



public function suppressAbonnement($idabn) 
{

    $m00= $idabn;
    $sql= "DELETE FROM `abonnements` WHERE `id_abonnement` = ?";
    $res= $this->base->prepare($sql);
    $res->execute([$m00]); 

    if ($res) { return true;} else { return false;}

}


public function suppressEditeur($idedt) 
{

    $m00= $idedt;
    $sql= "DELETE FROM `editeurs` WHERE `id_editeur` = ?";
    $res= $this->base->prepare($sql);
    $res->execute([$m00]); 

    if ($res) { return true;} else { return false;}

}


public function suppressAuteur($idaut) 
{

    $m00= $idaut;
    $sql= "DELETE FROM `auteurs` WHERE `id_auteur` = ?";
    $res= $this->base->prepare($sql);
    $res->execute([$m00]); 

    if ($res) { return true;} else { return false;}

}


public function suppressLivreRevue($idlvr) 
{

    $m00= $idlvr;
    $sql= "DELETE FROM `livresrevues` WHERE `id_livre` = ?";
    $res= $this->base->prepare($sql);
    $res->execute([$m00]); 

    if ($res) { return true;} else { return false;}

}

public function suppressMotcle($idmcl) 
{

    $m00= $idmcl;
    $sql= "DELETE FROM `mots_cles` WHERE `id_motcle` = ?";
    $res= $this->base->prepare($sql);
    $res->execute([$m00]); 

    if ($res) { return true;} else { return false;}

}

public function suppressUser($idusr) 
{

    $m00= $idusr;
    $sql= "DELETE FROM `utilisateurs` WHERE `id_util` = ?";
    $res= $this->base->prepare($sql);
    $res->execute([$m00]); 

    if ($res) { return true;} else { return false;}

}

public function changerOperUser($idusr, $oper) 
{
    $m00= $idusr;
    $m01= $oper;

    $sql= "UPDATE `utilisateurs` SET `operationnel` = ? WHERE `id_util` = ?";
     $res= $this->base->prepare($sql);
     $res->execute([$m01, $m00]); 

    if ($res) { return true;} else { return false;}

}



/***************************************Fin Suppression Enregistrement *********/


    public function modifierUser() 
    {

        $nom= $_POST['nom'];
        $prenom= $_POST['prenom'];
        $pseudo= $_POST['pseudo'];
        $email= $_POST['email'];
        if (isset($_POST['rolsearch'])) {$role= (int)$_POST['rolsearch'];} else {$role= $_SESSION['dataA'][0]->getRole();} 
        $date_created= $_POST['datecreat'];
        $m00= $_POST['idl'];

        $sql= "UPDATE `utilisateurs` SET `nom` = ?, `prenom` = ?, `pseudo` = ?, `email` = ?, `role` = ?, `date_created` = ? 
                WHERE `id_util` = ?";
         $res= $this->base->prepare($sql);
         $res->execute([$nom, $prenom, $pseudo, $email, $role, $date_created, $m00]); 

        if ($res) { return true;} else { return false;}

    }



    public function modifierArticle() 
    {

        //Récupération d'un fichier .pdf ou .docx ou .jpg

        if (isset($_FILES['ficjustif']['name']) && (!empty($_FILES['ficjustif']['name']))) { 
            $destination = './assets/fichiers/';
            if (isset($_FILES['ficjustif']['name'])) { $m00= $_FILES['ficjustif']['name']; }
            move_uploaded_file($_FILES['ficjustif']['tmp_name'], $destination.$_FILES['ficjustif']['name']);    
            $fichier= $_FILES['ficjustif']['name'];
        } else {
            if ($_SESSION['dataA'][0]->getFichier_earticle() != NULL) {$m00=$_SESSION['dataA'][0]->getFichier_earticle();}
            else {$m00="";}
        }
       
  
        $m01= $_POST['titre'];
        $m02= $_POST['theme'];   
        $m03= $m00;   

        $str= explode("//", $_POST['nauteur1']);
        if (count($str)>0) {$m04= $str[0];} else {$m04="";}
        if (count($str)>1) {$m05= $str[1];} else {$m05="";}   
        if (count($str)>2) {$m06= $str[2];} else {$m06="";}   
        
        $str= explode("//", $_POST['nauteur2']);
        if (count($str)>0) {$m07= $str[0];} else {$m07="";}
        if (count($str)>1) {$m08= $str[1];} else {$m08="";}   
        if (count($str)>2) {$m09= $str[2];} else {$m09="";}
        
        $m10= (int)$_POST['nbpage'];  
        $m11= (int)$_POST['prix'];   
        $m12= (int)$_POST['ida'];   

        $sql= "UPDATE `articles` SET `titre` = ?, `theme_article` = ?, `fichier_earticle` = ?, 
        `auteur1_nom` = ?, `auteur1_prenom` = ?, `auteur1_institution` = ?, 
        `auteur2_nom` = ?, `auteur2_prenom` = ?, `auteur2_institution` = ?, 
        `nbre_pages` = ?, `prix` = ? WHERE `id_article` = ?";

         $res= $this->base->prepare($sql);
         $res->execute([$m01,$m02,$m03,$m04,$m05,$m06,$m07,$m08,$m09,$m10,$m11,$m12]); 


        if ($res) {
            return true;
        } else {
            return false;
        }
    }


    public function modifierTheme() 
    {

        $m01= $_POST['idl'];
        $m02= $_POST['nom'];

        $sql= "UPDATE `themes` SET `nom_theme` = ? WHERE id_theme = ?";
        $res= $this->base->prepare($sql);
        $res->execute([$m02,$m01]); 

        if ($res) { return true;} else { return false;}
    }


    public function modifierRevue() 
    {

        $m01= $_POST['idl'];
        $m02= $_POST['nom'];
        $m03= (int)$_POST['period'];
        if (isset($_POST['horsserie'])) {$m04=1;} else {$m04=0;}
        $m05= (int)$_POST['prix'];

        $sql= "UPDATE `specif_revues` SET `nom_specif_revue` = ?, `periodicite` = ?,
        `hors_serie` = ?, `prix` = ? WHERE id_specif_revue = ?";
        $res= $this->base->prepare($sql);
        $res->execute([$m02,$m03,$m04,$m05,$m01]); 

        if ($res) { return true;} else { return false;}
    }

    public function modifierMembre() 
    {

        if (isset($_POST['offrpartner'])) {$m01=1;} else {$m01=0;}
        if (isset($_POST['offrnwlett'])) {$m02=1;} else {$m02=0;}
        if ($_POST['inputAddress2'] <> "") {$m03= $_POST['inputAddress']."//".$_POST['inputAddress2'];} 
            else {$m03= $_POST['inputAddress'];}

        $m04= $_POST['inputZip'];
        $m05= $_POST['inputCity'];
        $m06= $_POST['inputEtat'];
        $m07= $_POST['inputZipEtat'];
        $m08= $_POST['indicatif'];        
        $m09= $_POST['telClt'];
        $m10= $_POST['idl'];
      

        $sql= "UPDATE client_membres SET offre_partenaire = ?, offre_newsletter = ?, adr_livrais = ?, 
        code_postal = ?, ville = ?, pays = ?, zip_etat = ?, indicatif = ?, telephone = ? 
        WHERE id_clt_membre = ?";

        $res= $this->base->prepare($sql);
        $res->execute([$m01,$m02,$m03,$m04,$m05,$m06,$m07,$m08,$m09,$m10]); 

        if ($res) { return true;} else { return false;}
    }


    public function modifierMonnaie()
    {
                $m01=  $_POST['prix'];
                $m02=  $_POST['idm'];
                
                $sql= "UPDATE `monnaies` SET `val_commerc_en_euro` = ? WHERE `id_monnaie` = ?";
                $res= $this->base->prepare($sql);
                $res->execute([$m01,$m02]); 

            if ($res) { return true;} else { return false;}
 
    }


    public function ajouterClientInvite() 
    {
       
        $mbr1= $_SESSION['mbr1'];

        $m01=    $mbr1->getNum_compteur();
        $m02=    $mbr1->getCivilite();
        $m03=    $mbr1->getNom_destin();
        $m04=    $mbr1->getPrenom_destin();
        $m05=    $mbr1->getInstitution();
        $m06=    $mbr1->getFonction();    
        $m07=    $mbr1->getEmail();
        $m08=    $mbr1->getOffre_partenaire();
        $m09=    $mbr1->getOffre_newsletter();
        $m10=    $mbr1->getAdr_livrais(); 
        $m11=    $mbr1->getCode_postal();
        $m12=    $mbr1->getVille();
        $m13=    $mbr1->getPays();
        $m14=    $mbr1->getZip_etat();
        $m15=    $mbr1->getIndicatif();
        $m16=    $mbr1->getTelephone();

         $sql= "INSERT INTO client_invites (num_compteur, civilite, nom_destin, prenom_destin, 
         institution, fonction, email, offre_partenaire, offre_newsletter, adr_livrais, 
         code_postal, ville, pays, zip_etat, indicatif, telephone) 
        VALUES ('$m01','$m02','$m03','$m04','$m05','$m06','$m07','$m08','$m09','$m10',
        '$m11','$m12','$m13','$m14','$m15','$m16')";
        $res= $this->base->query($sql);

        if ($res) { return true;} else { return false;}
 
    }



    public function enregistrerAbonnement($mode_payment, $ref_transaction, $mode_livrais, $msg_livrais)
    {

        $annee_apres=date('Y-m-d', strtotime('+1 year', strtotime(date("Y-01-01"))));
        $i1= date("Y-03-21"); 
        $i2= date("Y-06-21");
        $i3= date("Y-09-21"); 
        $i4= date("Y-12-21");
        
        $i5= strtotime($annee_apres)+strtotime(date("1970-03-21")); $i5= date("Y-m-d", $i5);
        $i6= strtotime($annee_apres)+strtotime(date("1970-06-21")); $i6= date("Y-m-d", $i6);
        $i7= strtotime($annee_apres)+strtotime(date("1970-09-21")); $i7= date("Y-m-d", $i7);
        $i8= strtotime($annee_apres)+strtotime(date("1970-12-21")); $i8= date("Y-m-d", $i8);

        $cmd1= new LivreRevue();
        $cmd1= $_SESSION['dataA'];
        
        $mbr1= new ClientMembre();
        
        $mbr1= $_SESSION['mbr1'];

                $m01=    $cmd1->getId_livre();
                $m02=    $mbr1->getType_membre();
                $m03=    $mbr1->getId_clt_membre();

                $m04=    $cmd1->getId_livre();
                $m05=    $cmd1->getReference();
                $m06=    $cmd1->getNom_specif_revue()." // ".$cmd1->getTitre();
                $m07=    1;
                $m08=    "";
                $m09=    1;
                $m10= (int)$_SESSION['pxtotal']*0.95/(int)$cmd1->getPeriodicite();
                $m11= 0.05;
                $m12= (int)$_SESSION['pxtotal']/(int)$cmd1->getPeriodicite();
                $m13= 0;
                $m14= $cmd1->getMonnaie();
                $m15= $mode_payment;
                $m16= $ref_transaction;
                $m17= date("Y-m-d");
                
                $m18= $mbr1->getNom_membre();
                $m19= $mbr1->getPrenom_membre();

                $m20= $mbr1->getInstitution();
                $m21= $mbr1->getAdr_livrais(); 
                $m22= $mbr1->getCode_postal();
                $m23= $mbr1->getVille();
                $m24= $mbr1->getPays();
                $m25= "(".$mbr1->getIndicatif().") ".$mbr1->getTelephone();
                $m26= $mode_livrais;
                $m27= $msg_livrais;

                if (strtotime(date("Y-m-d")) < strtotime(date("Y-03-12"))) {
                    $m281= $i1; $m282= $i2; $m283= $i3; $m284= $i4; 
                } else {
                    if (strtotime(date("Y-m-d")) < strtotime(date("Y-06-12"))) {
                        $m281= $i2; $m282= $i3; $m283= $i4; $m284= $i5; 
                    } else {
                        if (strtotime(date("Y-m-d")) < strtotime(date("Y-09-12"))) {
                            $m281= $i3; $m282= $i4; $m283= $i5; $m284= $i6; 
                        } else {
                            if (strtotime(date("Y-m-d")) < strtotime(date("Y-12-12"))) {
                                $m281= $i4; $m282= $i5; $m283= $i6; $m284= $i7; 
                            } else {
                                $m281= $i5; $m282= $i6; $m283= $i7; $m284= $i8;
                            }
                        }
                    }
                }
                 
                $m29= "2100-01-01";

                $sql= "INSERT INTO `commandes` (`ref_commande`,`type_client`, `id_client`,
                `id_livre`, `ref_livre`, `ttr_livre`,
                `id_artcl`, `ttr_artcl`, `nombre`, `px_ht`, `tva_percent`, `px_ttc`,
                `frs_livrais`,`monnaie`, `mode_payment`, `ref_transaction`,
                `date_ordre_livrais`,  `nom_destin`,  `prenom_destin`,  `institution_destin`,
                `adr_livrais`, `code_postal`, `ville`, `pays`, `telephone`, 
                `mode_livrais`, `msg_livrais`, `date_livrais_prev`, `date_livrais_real`)
                VALUES ('$m01','$m02','$m03','$m04','$m05','$m06','$m07','$m08','$m09','$m10',
                '$m11','$m12','$m13','$m14','$m15','$m16','$m17','$m18','$m19','$m20','$m21',
                '$m22','$m23','$m24','$m25','$m26','$m27','$m281','$m29')";
                $res= $this->base->query($sql);

                $sql= "INSERT INTO `commandes` (`ref_commande`,`type_client`, `id_client`,
                `id_livre`, `ref_livre`, `ttr_livre`,
                `id_artcl`, `ttr_artcl`, `nombre`, `px_ht`, `tva_percent`, `px_ttc`,
                `frs_livrais`,`monnaie`, `mode_payment`, `ref_transaction`,
                `date_ordre_livrais`,  `nom_destin`,  `prenom_destin`,  `institution_destin`,
                `adr_livrais`, `code_postal`, `ville`, `pays`, `telephone`, 
                `mode_livrais`, `msg_livrais`, `date_livrais_prev`, `date_livrais_real`)
                VALUES ('$m01','$m02','$m03','$m04','$m05','$m06','$m07','$m08','$m09','$m10',
                '$m11','$m12','$m13','$m14','$m15','$m16','$m17','$m18','$m19','$m20','$m21',
                '$m22','$m23','$m24','$m25','$m26','$m27','$m282','$m29')";
                $res= $this->base->query($sql);
            
                $sql= "INSERT INTO `commandes` (`ref_commande`,`type_client`, `id_client`,
                `id_livre`, `ref_livre`, `ttr_livre`,
                `id_artcl`, `ttr_artcl`, `nombre`, `px_ht`, `tva_percent`, `px_ttc`,
                `frs_livrais`,`monnaie`, `mode_payment`, `ref_transaction`,
                `date_ordre_livrais`,  `nom_destin`,  `prenom_destin`,  `institution_destin`,
                `adr_livrais`, `code_postal`, `ville`, `pays`, `telephone`, 
                `mode_livrais`, `msg_livrais`, `date_livrais_prev`, `date_livrais_real`)
                VALUES ('$m01','$m02','$m03','$m04','$m05','$m06','$m07','$m08','$m09','$m10',
                '$m11','$m12','$m13','$m14','$m15','$m16','$m17','$m18','$m19','$m20','$m21',
                '$m22','$m23','$m24','$m25','$m26','$m27','$m283','$m29')";
                $res= $this->base->query($sql);

                $sql= "INSERT INTO `commandes` (`ref_commande`,`type_client`, `id_client`,
                `id_livre`, `ref_livre`, `ttr_livre`,
                `id_artcl`, `ttr_artcl`, `nombre`, `px_ht`, `tva_percent`, `px_ttc`,
                `frs_livrais`,`monnaie`, `mode_payment`, `ref_transaction`,
                `date_ordre_livrais`,  `nom_destin`,  `prenom_destin`,  `institution_destin`,
                `adr_livrais`, `code_postal`, `ville`, `pays`, `telephone`, 
                `mode_livrais`, `msg_livrais`, `date_livrais_prev`, `date_livrais_real`)
                VALUES ('$m01','$m02','$m03','$m04','$m05','$m06','$m07','$m08','$m09','$m10',
                '$m11','$m12','$m13','$m14','$m15','$m16','$m17','$m18','$m19','$m20','$m21',
                '$m22','$m23','$m24','$m25','$m26','$m27','$m284','$m29')";
                $res= $this->base->query($sql);

                $m30=    $cmd1->getNom_specif_revue();
                $m31= (int)$cmd1->getPeriodicite();
                $m32=    $cmd1->getType_support(); 
                $m33= date('Y-m-d');
                $m34=date('Y-m-d', strtotime('+1 year', strtotime(date("Y-m-d"))));
                $m35=(int)$_SESSION['pxtotal'];
                $m36= $cmd1->getMonnaie();
                $m37= $mode_payment;
                $m38= $ref_transaction;
                $m39=    $mbr1->getType_membre();
                $m40=    $mbr1->getId_clt_membre();
                
                if ($mbr1->getStatut_client() === NULL)  {$m41= 0;} 
                    else {$m41= $mbr1->getStatut_client();} 

                $sql= "INSERT INTO `abonnements` (`nom_specif_revue`, `periodicite`,
                `type_papier_epapier`, `date_debut`, `date_fin`, `prix`, `monnaie`, 
                `mode_payment`, `ref_transaction`, 
                `type_client`, `id_client`, `statut_client`)
                VALUES ('$m30','$m31','$m32','$m33','$m34','$m35','$m36','$m37','$m38','$m39',
                '$m40','$m41')";
                $res= $this->base->query($sql);



            if ($res) { 

                return true;} else { return false;}
 
    }


    public function modifierCommande()
    {

                $m01=  $_POST['dtelvr'];
                $m02=  $_POST['idcmd'];
                
                $sql= "UPDATE `commandes` SET `date_livrais_real` = ? WHERE `id_commande` = ?";
                $res= $this->base->prepare($sql);
                $res->execute([$m01,$m02]); 

            if ($res) { return true;} else { return false;}
 
    }


    public function enregistrerCommande($mode_payment, $ref_transaction, $mode_livrais, $msg_livrais)
    {

        $cmd1= new Panier();
        $cmd1= $_SESSION['dataA'][0];
        if ($cmd1->getType_client()=="0") {
            $mbr1= new ClientInvite();
        } else {
            $mbr1= new ClientMembre();
        }
        $mbr1= $_SESSION['mbr1'];

            if ((int)$cmd1->getId_livre1()>1 || (int)$cmd1->getId_artcl1()>1) {

                $m01=    $cmd1->getId_panier();
                if ($cmd1->getType_client()=="0") {
                    $m02=    $cmd1->getType_client();
                } else {
                    $m02=    $mbr1->getType_membre();
                }
                $m03=    $cmd1->getId_client();
                $m04=    $cmd1->getId_livre1();
                $m05=    $cmd1->getRef_livre1();
                $m06=    $cmd1->getTtr_livre1();
                $m07=    $cmd1->getId_artcl1();
                $m08=    $cmd1->getTtr_artcl1();
                $m09=    $cmd1->getNbre1();   
                if ($m04>1)  {
                    if ($m07>1) {
                        $m10= $cmd1->getPxttc_artcl1()*0.95;
                        $m11= 0.05;
                        $m12= $cmd1->getPxttc_artcl1();
                    } else {
                        $m10= $cmd1->getPxttc_livre1()*0.95;
                        $m11= 0.05;
                        $m12= $cmd1->getPxttc_livre1();
                    }
                } else {
                    $m10= $cmd1->getPxttc_artcl1()*0.95;
                    $m11= 0.05;
                    $m12= $cmd1->getPxttc_artcl1();
                }
                $m13= $cmd1->getFrs_livrais1();
                $m14= $cmd1->getMonnaie();
                $m15= $mode_payment;
                $m16= $ref_transaction;
                $m17= date("Y-m-d");
                
                if ($cmd1->getType_client()=="0") {
                    $m18= $mbr1->getNom_destin();
                    $m19= $mbr1->getPrenom_destin();
                } else {
                    $m18= $mbr1->getNom_membre();
                    $m19= $mbr1->getPrenom_membre();
                }

                $m20= $mbr1->getInstitution();
                $m21= $mbr1->getAdr_livrais(); 
                $m22= $mbr1->getCode_postal();
                $m23= $mbr1->getVille();
                $m24= $mbr1->getPays();
                $m25= "(".$mbr1->getIndicatif().") ".$mbr1->getTelephone();
                $m26= $mode_livrais;
                $m27= $msg_livrais;

                $start = strtotime('2009-02-01');$end = strtotime('2009-02-11');
                $m28= date("Y-m-d", strtotime($m17)+$end-$start);
                 
                $m29= "2100-01-01";

                $sql= "INSERT INTO `commandes` (`ref_commande`,`type_client`, `id_client`,
                `id_livre`, `ref_livre`, `ttr_livre`,
                `id_artcl`, `ttr_artcl`, `nombre`, `px_ht`, `tva_percent`, `px_ttc`,
                `frs_livrais`,`monnaie`, `mode_payment`, `ref_transaction`,
                `date_ordre_livrais`,  `nom_destin`,  `prenom_destin`,  `institution_destin`,
                `adr_livrais`, `code_postal`, `ville`, `pays`, `telephone`, 
                `mode_livrais`, `msg_livrais`, `date_livrais_prev`, `date_livrais_real`)
                VALUES ('$m01','$m02','$m03','$m04','$m05','$m06','$m07','$m08','$m09','$m10',
                '$m11','$m12','$m13','$m14','$m15','$m16','$m17','$m18','$m19','$m20','$m21',
                '$m22','$m23','$m24','$m25','$m26','$m27','$m28','$m29')";
                $res= $this->base->query($sql);

                $donnLivre= $this->listeLivreRevue($m04, "", "", "", 0, 0, "toutes");

                if ( count($donnLivre) > 0 && $m07 < 2 ) {
                    if ($donnLivre[0]->getstock_iris()>0) {
                        $a= max((int)$donnLivre[0]->getstock_iris()-(int)$m09, -1);
                        if ($a>=0) {$donnLivre[0]->setstock_iris((int)$donnLivre[0]->getstock_iris()-(int)$m09);}
                            else {
                                $m09-=(int)$donnLivre[0]->getstock_iris();
                                $donnLivre[0]->setstock_iris(0);
                                $donnLivre[0]->setstock_editeur((int)$donnLivre[0]->getstock_editeur()-$m09);
                        }
                    } else {
                        $donnLivre[0]->setstock_editeur((int)$donnLivre[0]->getstock_editeur()-$m09);
                    }
                }
                if ($m07 < 2 ) {
                    $stk1=(int)$donnLivre[0]->getstock_iris(); 
                    $stk2=(int)$donnLivre[0]->getstock_editeur();
                    $sql= "UPDATE `livresrevues` SET `stock_iris` = ?, `stock_editeur` = ? WHERE id_livre = ?";
                    $res= $this->base->prepare($sql);
                    $res->execute([$stk1, $stk2, $m04]);
                }
            }

            if ((int)$cmd1->getId_livre2()>1 || (int)$cmd1->getId_artcl2()>1) {

                $m01=    $cmd1->getId_panier();
                if ($cmd1->getType_client()=="0") {
                    $m02=    $cmd1->getType_client();
                } else {
                    $m02=    $mbr1->getType_membre();
                }
                $m03=    $cmd1->getId_client();
                $m04=    $cmd1->getId_livre2();
                $m05=    $cmd1->getRef_livre2();
                $m06=    $cmd1->getTtr_livre2();
                $m07=    $cmd1->getId_artcl2();
                $m08=    $cmd1->getTtr_artcl2();
                $m09=    $cmd1->getNbre2();
                if ($m04>1)  {
                    if ($m07>1) {
                        $m10= $cmd1->getPxttc_artcl2()*0.95;
                        $m11= 0.05;
                        $m12= $cmd1->getPxttc_artcl2();
                    } else {
                        $m10= $cmd1->getPxttc_livre2()*0.95;
                        $m11= 0.05;
                        $m12= $cmd1->getPxttc_livre2();
                    }
                } else {
                    $m10= $cmd1->getPxttc_artcl2()*0.95;
                    $m11= 0.05;
                    $m12= $cmd1->getPxttc_artcl2();
                }
                $m13= $cmd1->getFrs_livrais2();
                $m14= $cmd1->getMonnaie();
                $m15= $mode_payment;
                $m16= $ref_transaction;
                $m17= date("Y-m-d");      

                if ($cmd1->getType_client()=="0") {
                    $m18= $mbr1->getNom_destin();
                    $m19= $mbr1->getPrenom_destin();
                } else {
                    $m18= $mbr1->getNom_membre();
                    $m19= $mbr1->getPrenom_membre();
                }

                $m20= $mbr1->getInstitution();
                $m21= $mbr1->getAdr_livrais(); 
                $m22= $mbr1->getCode_postal();
                $m23= $mbr1->getVille();
                $m24= $mbr1->getPays();
                $m25= "(".$mbr1->getIndicatif().") ".$mbr1->getTelephone();
                $m26= $mode_livrais;
                $m27= $msg_livrais;

                $start = strtotime('2009-02-01');$end = strtotime('2009-02-11');
                $m28= date("Y-m-d", strtotime($m17)+$end-$start);

                $m29= "2100-01-01";

                $sql= "INSERT INTO `commandes` (`ref_commande`,`type_client`, `id_client`,
                `id_livre`, `ref_livre`, `ttr_livre`,
                `id_artcl`, `ttr_artcl`, `nombre`, `px_ht`, `tva_percent`, `px_ttc`,
                `frs_livrais`,`monnaie`, `mode_payment`, `ref_transaction`,
                `date_ordre_livrais`,  `nom_destin`,  `prenom_destin`,  `institution_destin`,
                `adr_livrais`, `code_postal`, `ville`, `pays`, `telephone`, 
                `mode_livrais`, `msg_livrais`, `date_livrais_prev`, `date_livrais_real`)
                VALUES ('$m01','$m02','$m03','$m04','$m05','$m06','$m07','$m08','$m09','$m10',
                '$m11','$m12','$m13','$m14','$m15','$m16','$m17','$m18','$m19','$m20','$m21',
                '$m22','$m23','$m24','$m25','$m26','$m27','$m28','$m29')";
                $res= $this->base->query($sql);

                $donnLivre= $this->listeLivreRevue($m04, "", "", "", 0, 0, "toutes");
                if ( count($donnLivre) > 0 && $m07 < 2 ) {
                    if ($donnLivre[0]->getstock_iris()>0) {
                        $a= max((int)$donnLivre[0]->getstock_iris()-(int)$m09, -1);
                        if ($a>=0) {$donnLivre[0]->setstock_iris((int)$donnLivre[0]->getstock_iris()-(int)$m09);}
                            else {
                                $m09-=(int)$donnLivre[0]->getstock_iris();
                                $donnLivre[0]->setstock_iris(0);
                                $donnLivre[0]->setstock_editeur((int)$donnLivre[0]->getstock_editeur()-$m09);
                        }
                    } else {
                        $donnLivre[0]->setstock_editeur((int)$donnLivre[0]->getstock_editeur()-$m09);
                    }
                }
                if ($m07 < 2 ) {
                    $stk1=(int)$donnLivre[0]->getstock_iris(); 
                    $stk2=(int)$donnLivre[0]->getstock_editeur();
                    $sql= "UPDATE `livresrevues` SET `stock_iris` = ?, `stock_editeur` = ? WHERE id_livre = ?";
                    $res= $this->base->prepare($sql);
                    $res->execute([$stk1, $stk2, $m04]);
                }
            }

            if ((int)$cmd1->getId_livre3()>1 || (int)$cmd1->getId_artcl3()>1) {
               
                $m01=    $cmd1->getId_panier();
                if ($cmd1->getType_client()=="0") {
                    $m02=    $cmd1->getType_client();
                } else {
                    $m02=    $mbr1->getType_membre();
                }
                $m03= $cmd1->getId_client();
                $m04= $cmd1->getId_livre3();
                $m05= $cmd1->getRef_livre3();
                $m06= $cmd1->getTtr_livre3();
                $m07= $cmd1->getId_artcl3();
                $m08= $cmd1->getTtr_artcl3();
                $m09= $cmd1->getNbre3();
                if ($m04>1)  {
                    if ($m07>1) {
                        $m10= $cmd1->getPxttc_artcl3()*0.95;
                        $m11= 0.05;
                        $m12= $cmd1->getPxttc_artcl3();
                    } else {
                        $m10= $cmd1->getPxttc_livre3()*0.95;
                        $m11= 0.05;
                        $m12= $cmd1->getPxttc_livre3();
                    }
                } else {
                    $m10= $cmd1->getPxttc_artcl3()*0.95;
                    $m11= 0.05;
                    $m12= $cmd1->getPxttc_artcl3();
                }
                $m13= $cmd1->getFrs_livrais3();
                $m14= $cmd1->getMonnaie();
                $m15= $mode_payment;
                $m16= $ref_transaction;
                $m17= date("Y-m-d");      

                if ($cmd1->getType_client()=="0") {
                    $m18= $mbr1->getNom_destin();
                    $m19= $mbr1->getPrenom_destin();
                } else {
                    $m18= $mbr1->getNom_membre();
                    $m19= $mbr1->getPrenom_membre();
                }

                $m20= $mbr1->getInstitution();
                $m21= $mbr1->getAdr_livrais(); 
                $m22= $mbr1->getCode_postal();
                $m23= $mbr1->getVille();
                $m24= $mbr1->getPays();
                $m25= "(".$mbr1->getIndicatif().") ".$mbr1->getTelephone();
                $m26= $mode_livrais;
                $m27= $msg_livrais;

                $start = strtotime('2009-02-01');$end = strtotime('2009-02-11');
                $m28= date("Y-m-d", strtotime($m17)+$end-$start);

                $m29= "2100-01-01";
                /*echo "$m01'.'$m02'.'$m03'.'$m04'.'$m05'.'$m06'.'$m07'.'$m08'.'$m09'.'$m10'.
                '$m11'.'$m12'.'$m13'.'$m14'.'$m15'.'$m16'.'$m17'.'$m18'.'$m19'.'$m20'.'$m21'.
                '$m22'.'$m23'.'$m24'.'$m25'.'$m26'.'$m27'.'$m28'.'$m29";*/

                $sql= "INSERT INTO `commandes` (`ref_commande`,`type_client`, `id_client`,
                `id_livre`, `ref_livre`, `ttr_livre`,
                `id_artcl`, `ttr_artcl`, `nombre`, `px_ht`, `tva_percent`, `px_ttc`,
                `frs_livrais`,`monnaie`, `mode_payment`, `ref_transaction`,
                `date_ordre_livrais`,  `nom_destin`,  `prenom_destin`,  `institution_destin`,
                `adr_livrais`, `code_postal`, `ville`, `pays`, `telephone`, 
                `mode_livrais`, `msg_livrais`, `date_livrais_prev`, `date_livrais_real`)
                VALUES ('$m01','$m02','$m03','$m04','$m05','$m06','$m07','$m08','$m09','$m10',
                '$m11','$m12','$m13','$m14','$m15','$m16','$m17','$m18','$m19','$m20','$m21',
                '$m22','$m23','$m24','$m25','$m26','$m27','$m28','$m29')";
                $res= $this->base->query($sql);

                $donnLivre= $this->listeLivreRevue($m04, "", "", "", 0, 0, "toutes");
                if ( count($donnLivre) > 0 && $m07 < 2 ) {
                    if ($donnLivre[0]->getstock_iris()>0) {
                        $a= max((int)$donnLivre[0]->getstock_iris()-(int)$m09, -1);
                        if ($a>=0) {$donnLivre[0]->setstock_iris((int)$donnLivre[0]->getstock_iris()-(int)$m09);}
                            else {
                                $m09-=(int)$donnLivre[0]->getstock_iris();
                                $donnLivre[0]->setstock_iris(0);
                                $donnLivre[0]->setstock_editeur((int)$donnLivre[0]->getstock_editeur()-$m09);
                        }
                    } else {
                        $donnLivre[0]->setstock_editeur((int)$donnLivre[0]->getstock_editeur()-$m09);
                    }
                }
                if ($m07 < 2 ) {
                    $stk1=(int)$donnLivre[0]->getstock_iris(); 
                    $stk2=(int)$donnLivre[0]->getstock_editeur();
                    $sql= "UPDATE `livresrevues` SET `stock_iris` = ?, `stock_editeur` = ? WHERE id_livre = ?";
                    $res= $this->base->prepare($sql);
                    $res->execute([$stk1, $stk2, $m04]);
                }
            }
            if ($res) { 

                return true;} else { return false;}
 
    }

    public function ajouterTheme(Theme $theme) 
    {

        $theme= new Theme();            
        
        $theme->setNom_theme($_POST["nom"]);  
     
        $m01= $theme->getNom_theme();
     

        $sql= "INSERT INTO `themes` (`nom_theme`) 
        VALUES ('$m01')";
        $res= $this->base->query($sql);

        if ($res) {
            return true;
        } else {
            return false;
        }
    }


    public function ajouterMotcle(Motcle $motcle) 
    {
       
        $motcle= new MotCle();            

        $nomrev1= explode("//",$_POST['idartcl']); 
        $idArt= (int)$nomrev1[0];
        $idLvR= (int)$nomrev1[1];
        $str = str_replace("$", " ", $nomrev1[2], $count);$nomrev1[2]=$str;
        $ttrArt= $nomrev1[2];

        $motcle->setNom_motcle($_POST["nom"]);  
        $motcle->setId_livre($idLvR);  
        $motcle->setId_artcl($idArt);  
      
        $m01= $motcle->getNom_motcle();
        $m02= $motcle->getId_livre();   
        $m03= $motcle->getId_artcl();   
     

        $sql= "INSERT INTO `mots_cles` (`nom_motcle`, `id_livre`, `id_artcl`)
        VALUES ('$m01','$m02','$m03')";
        $res= $this->base->query($sql);

        if ($res) {
            return true;
        } else {
            return false;
        }
    }


    public function ajouterEditeur(Editeur $editeur, $ajt1mdf2) 
    {

        $editeur= new Editeur();            
        $editeur->setCode_editeur($_POST["code"]);  
        $editeur->setNom_editeur($_POST["nom"]);  
        $editeur->setLocalite_editeur($_POST["localite"]);  
        $editeur->setPays_editeur($_POST["pays"]);
        $editeur->setFrs_livrais($_POST["frslivr"]);  
      
        if ($ajt1mdf2 == 2) {$m00= (int)$_POST['idl'];}
        $m01= $editeur->getCode_editeur();
        $m02= $editeur->getNom_editeur();   
        $m03= $editeur->getLocalite_editeur();   
        $m04= $editeur->getPays_editeur();
        $m05= $editeur->getFrs_livrais();

        if ($ajt1mdf2 == 1) {
            $sql= "INSERT INTO `editeurs` (`code_editeur`, `nom_editeur`, `localite_editeur`, `pays_editeur`, `frs_livrais`)
            VALUES ('$m01','$m02','$m03','$m04','$m05')";
            $res= $this->base->query($sql);
    }
        else {

            $sql= "UPDATE `editeurs` SET `code_editeur` = ?, `nom_editeur` = ?, `localite_editeur` = ?,
                         `pays_editeur` = ?, `frs_livrais` = ? WHERE `id_editeur` = ?";

            $res= $this->base->prepare($sql);
            $res->execute([$m01,$m02,$m03,$m04,$m05,$m00]); 
    }

        if ($res) {
            return true;
        } else {
            return false;
        }
    }


    public function ajouterAuteur(Auteur $auteur, $ajt1mdf2) 
    {

        $auteur= new Auteur();            
        $auteur->setNom($_POST["nom"]);  
        $auteur->setPrenom($_POST["prenom"]);  
        $auteur->setInstitution($_POST["institution"]);  
        $auteur->setFonction($_POST["fonction"]);  
        //Récupération d'une image .jpg ou .bmp ou .png

        if (isset($_FILES['image1']['name']) && (!empty($_FILES['image1']['name']))) { 
            $destination = './assets/images/';
            if (isset($_FILES['image1']['name'])) { $auteur->setPhoto($_FILES['image1']['name']); }
            move_uploaded_file($_FILES['image1']['tmp_name'], $destination.$_FILES['image1']['name']);    
            $fichier= $_FILES['image1']['name'];
        } else {
            if ($ajt1mdf2 == 1) {$auteur->setPhoto("");} 
                else {$auteur->setPhoto($_SESSION["dataA"][0]->getPhoto());}
        }

        if ($ajt1mdf2 == 2) {$m00= (int)$_POST['idl'];} 
        
        $m01= $auteur->getNom();
        $m02= $auteur->getPrenom();   
        $m03= $auteur->getInstitution();   
        $m04= $auteur->getFonction();
        $m05= $auteur->getPhoto();

        if ($ajt1mdf2 == 1) {
                $sql= "INSERT INTO `auteurs` (`nom`, `prenom`, `institution`, `fonction`, `photo`)
                VALUES ('$m01','$m02','$m03','$m04','$m05')";
                $res= $this->base->query($sql);
        }
            else {

                $sql= "UPDATE `auteurs` SET `nom` = ?, `prenom` = ?, `institution` = ?, 
                        `fonction` = ?, `photo` = ? WHERE `id_auteur` = ?";
    
                $res= $this->base->prepare($sql);
                $res->execute([$m01,$m02,$m03,$m04,$m05, $m00]); 
        }

        if ($res) {
            return true;
        } else {
            return false;
        }
    }


    public function ajouterArticle(Article $article) 
    {

        //Récupération d'un fichier .pdf ou .docx ou .jpg

        if (isset($_FILES['ficjustif']['name']) && (!empty($_FILES['ficjustif']['name']))) { 
            $destination = './assets/fichiers/';
            if (isset($_FILES['ficjustif']['name'])) { $article->setFichier_earticle($_FILES['ficjustif']['name']); }
            move_uploaded_file($_FILES['ficjustif']['tmp_name'], $destination.$_FILES['ficjustif']['name']);    
            $fichier= $_FILES['ficjustif']['name'];
        }

        $m01= $article->getTitre();
        $m02= $article->getTheme_article();   
        $m03= $article->getId_livre();   
        $m04= $article->getRef_livre();
        $m05= $article->getEmplacement();   
        $m06= $article->getFichier_earticle();   
        $m07= $article->getAuteur1_nom();  
        $m08= $article->getAuteur1_prenom();   
        $m09= $article->getAuteur1_institution();   
        $m10= $article->getAuteur2_nom();   
        $m11= $article->getAuteur2_prenom();   
        $m12= $article->getAuteur2_institution();   
        $m13= $article->getNbre_pages();   
        $m14= $article->getNbre_mots_pert();   
        $m15= $article->getPrix();   
        $m16= $article->getMonnaie();   
        $m17= $article->getType_support();   

        $sql= "INSERT INTO `articles` (`titre`, `theme_article`, `id_livre`,
        `ref_livre`, `emplacement`, `fichier_earticle`, `auteur1_nom`, `auteur1_prenom`,
        `auteur1_institution`, `auteur2_nom`, `auteur2_prenom`, `auteur2_institution`, `nbre_pages`,
       `nbre_mots_pert`, `prix`, `monnaie`, `type_support`) 
        VALUES ('$m01','$m02','$m03','$m04','$m05', '$m06','$m07','$m08','$m09','$m10',
        '$m11','$m12','$m13','$m14','$m15','$m16','$m17')";
        $res= $this->base->query($sql);

        if ($res) {
            return true;
        } else {
            return false;
        }
    }


    public function ajouterLivre(LivreRevue $livre, $ajt1mdf2) 
    {

        $str = str_replace("$", " ", $_POST["nomrev"], $count);$_POST["nomrev"]=$str;
        $nomrev= explode("//", $_POST['nomrev']);

        $livre= new LivreRevue();            
        $livre->setReference($_POST["ref"]);   
        if ($_POST["type"] == "2") {$livre->setNom_specif_revue($nomrev[0]); $livre->setPeriodicite((int)$nomrev[1]);}
            else {$livre->setNom_specif_revue(""); $livre->setPeriodicite(0);}

        $livre->setTitre($_POST["titre"]);
        $livre->setPrix(floatval($_POST["prix"]));
        $livre->setMonnaie("Euro");

        //Récupération d'une image .jpg ou .bmp ou .png

        if (isset($_FILES['image1']['name']) && (!empty($_FILES['image1']['name']))) { 
            $destination = './assets/images/';
            if (isset($_FILES['image1']['name'])) { $livre->setImage1($_FILES['image1']['name']); }
            move_uploaded_file($_FILES['image1']['tmp_name'], $destination.$_FILES['image1']['name']);    
            $fichier= $_FILES['image1']['name'];
        } else {
            if ($ajt1mdf2 == 1) {$livre->setImage1("");} 
                else {$livre->setImage1($_SESSION["dataA"][0]->getImage1());}
        }

        if (isset($_FILES['image2']['name']) && (!empty($_FILES['image2']['name']))) { 
            $destination = './assets/images/';
            if (isset($_FILES['image2']['name'])) { $livre->setImage2($_FILES['image2']['name']); }
            move_uploaded_file($_FILES['image2']['tmp_name'], $destination.$_FILES['image2']['name']);    
            $fichier= $_FILES['image2']['name'];
        } else {
            if ($ajt1mdf2 == 1) {$livre->setImage2("");} 
                else {$livre->setImage2($_SESSION["dataA"][0]->getImage2());}
        }

        if (isset($_FILES['image3']['name']) && (!empty($_FILES['image3']['name']))) { 
            $destination = './assets/images/';
            if (isset($_FILES['image3']['name'])) { $livre->setImage3($_FILES['image3']['name']); }
            move_uploaded_file($_FILES['image3']['tmp_name'], $destination.$_FILES['image3']['name']);    
            $fichier= $_FILES['image3']['name'];
        } else {
            if ($ajt1mdf2 == 1) {$livre->setImage3("");} 
                else {$livre->setImage3($_SESSION["dataA"][0]->getImage3());}
        }

        if (isset($_FILES['image4']['name']) && (!empty($_FILES['image4']['name']))) { 
            $destination = './assets/images/';
            if (isset($_FILES['image4']['name'])) { $livre->setImage4($_FILES['image4']['name']); }
            move_uploaded_file($_FILES['image4']['tmp_name'], $destination.$_FILES['image4']['name']);    
            $fichier= $_FILES['image4']['name'];
        } else {
            if ($ajt1mdf2 == 1) {$livre->setImage4("");} 
                else {$livre->setImage4($_SESSION["dataA"][0]->getImage4());}
        }

        if (isset($_FILES['image5']['name']) && (!empty($_FILES['image5']['name']))) { 
            $destination = './assets/images/';
            if (isset($_FILES['image5']['name'])) { $livre->setImage5($_FILES['image5']['name']); }
            move_uploaded_file($_FILES['image5']['tmp_name'], $destination.$_FILES['image5']['name']);    
            $fichier= $_FILES['image5']['name'];
        } else {
            if ($ajt1mdf2 == 1) {$livre->setImage5("");} 
                else {$livre->setImage5($_SESSION["dataA"][0]->getImage5());}
        }

        //Récupération d'un fichier .pdf ou .docx ou .jpg

        if (isset($_FILES['ficjustif']['name']) && (!empty($_FILES['ficjustif']['name']))) { 
            $destination = './assets/fichiers/';
            if (isset($_FILES['ficjustif']['name'])) { $livre->setFichier_ebook($_FILES['ficjustif']['name']); }
            move_uploaded_file($_FILES['ficjustif']['tmp_name'], $destination.$_FILES['ficjustif']['name']);    
            $fichier= $_FILES['ficjustif']['name'];
        } else {
            if ($ajt1mdf2 == 1) {$livre->setFichier_ebook("");} 
                else {$livre->setFichier_ebook($_SESSION["dataA"][0]->getFichier_ebook());}
        }


        $str = str_replace("$", " ", $_POST["nauteur1"], $count);$_POST["nauteur1"]=$str;

        $nomrev= explode("//",$_POST["nauteur1"]);

        if(isset($nomrev[0])) {$livre->setAuteur_princ1_nom($nomrev[0]);}
        if(isset($nomrev[1])) {$livre->setAuteur_princ1_prenom($nomrev[1]);}
        if(isset($nomrev[2])) {$livre->setAuteur_princ1_institution($nomrev[2]);}

        $str = str_replace("$", " ", $_POST["nauteur2"], $count);$_POST["nauteur2"]=$str;

        $nomrev= explode("//",$_POST["nauteur2"]);

        if(isset($nomrev[0])) {$livre->setAuteur_princ2_nom($nomrev[0]);}
        if(isset($nomrev[1])) {$livre->setAuteur_princ2_prenom($nomrev[1]);}
        if(isset($nomrev[2])) {$livre->setAuteur_princ2_institution($nomrev[2]);}

        $str = str_replace("$", " ", $_POST["nredac"], $count);$_POST["nredac"]=$str;

        $nomrev= explode("//",$_POST["nredac"]);

        if(isset($nomrev[0])) {$livre->setRedac_chef_nom($nomrev[0]);}
        if(isset($nomrev[1])) {$livre->setRedac_chef_prenom($nomrev[1]);}
        if(isset($nomrev[2])) {$livre->setRedac_chef_institution($nomrev[2]);}

        if ($_POST["nbpage"] <> "0") {$livre->setNbre_pages((int)$_POST["nbpage"]);}
            else {$livre->setNbre_pages(1);}

        $livre->setDescription($_POST["descript"]);

        $livre->setMorceau_choisi($_POST["mchoisi"]);
        $str = str_replace("$", " ", $_POST["edit1"], $count);$_POST["edit1"]=$str;
        $livre->setEditeur1($_POST["edit1"]);
        $str = str_replace("$", " ", $_POST["edit2"], $count);$_POST["edit2"]=$str;
        $livre->setEditeur2($_POST["edit2"]);
        $livre->setDate_parution($_POST["dateparut"]);

        $livre->setFormat_livre($_POST["format"] );
        $livre->setType_support($_POST["support"]);
        $livre->setStock_iris((int)$_POST["stkirs"]);
        $livre->setStock_editeur((int)$_POST["stkedt"]);
        $livre->setIsbn($_POST["isbn"]);
        $livre->setDoi($_POST["doi"]);
        $livre->setCritique($_POST["critique"]);
        $livre->setConsign_auteur($_POST["csgnaut"]);

        $livre->setIssn($_POST["issn"]);
        $livre->setIssn_online($_POST["issn_online"]);

        if ($ajt1mdf2 == 2) {
            $m01= (int)$_POST['idl'];
        }
        $m02= $livre->getReference();   
        $m03= $livre->getNom_specif_revue();   
        $m04= $livre->getTitre();
        $m05= $livre->getPrix();   
        $m06= $livre->getMonnaie();   
        $m07= $livre->getImage1();   
        $m08= $livre->getImage2();   
        $m09= $livre->getImage3();   
        $m10= $livre->getImage4();   
        $m11= $livre->getImage5();   
        $m12= $livre->getAuteur_princ1_nom();   
        $m13= $livre->getAuteur_princ1_prenom();   
        $m14= $livre->getAuteur_princ1_institution();   
        $m15= $livre->getAuteur_princ2_nom();   
        $m16= $livre->getAuteur_princ2_prenom();   
        $m17= $livre->getAuteur_princ2_institution();   
        $m18= $livre->getRedac_chef_nom();  
        $m19= $livre->getRedac_chef_prenom();   
        $m20= $livre->getRedac_chef_institution();   
        $m21= $livre->getNbre_pages();  
        $m22= $livre->getDescription();   
        $m23= $livre->getMorceau_choisi();   
        $m24= $livre->getEditeur1();  
        $m25= $livre->getEditeur2();   
        $m26= $livre->getDate_parution();   
        $m27= $livre->getFichier_ebook();   
        $m28= $livre->getFormat_livre(); 
        $m29= $livre->getType_support();   
        $m30= $livre->getStock_iris();   
        $m31= $livre->getStock_editeur();   
        $m32= $livre->getIsbn();  
        $m33= $livre->getDoi();   
        $m34= $livre->getCritique();   
        $m35= $livre->getConsign_auteur();   
        $m36= $livre->getPeriodicite();   
        $m37= $livre->getIssn();   
        $m38= $livre->getIssn_online();   

        if ($ajt1mdf2 == 1) {
            $sql= "INSERT INTO `livresrevues` (`reference`, `nom_specif_revue`, `titre`,
            `prix`, `monnaie`, `image1`, `image2`, `image3`, `image4`, `image5`,
            `auteur_princ1_nom`, `auteur_princ1_prenom`, `auteur_princ1_institution`,
            `auteur_princ2_nom`, `auteur_princ2_prenom`, `auteur_princ2_institution`,
            `redac_chef_nom`, `redac_chef_prenom`, `redac_chef_institution`,
            `nbre_pages`, `description`, `morceau_choisi`,
            `editeur1`, `editeur2`, `date_parution`, `fichier_ebook`,
            `format_livre`, `type_support`, `stock_iris`, `stock_editeur`,
            `isbn`, `doi`, `critique`, `consign_auteur`, `periodicite`, `issn`,
            `issn_online`) 
            VALUES ('$m02','$m03','$m04','$m05', '$m06','$m07','$m08','$m09','$m10',
            '$m11','$m12','$m13','$m14','$m15','$m16','$m17','$m18','$m19','$m20','$m21','$m22',
            '$m23','$m24','$m25','$m26','$m27','$m28','$m29','$m30','$m31','$m32','$m33','$m34',
            '$m35','$m36','$m37','$m38')";
            $res= $this->base->query($sql);
        } else {

            $sql= "UPDATE `livresrevues` SET `prix` = ?, `monnaie` = ?, `image1` = ?, `image2` = ?, `image3` = ?, `image4` = ?, `image5` = ?,
            `auteur_princ1_nom` = ?, `auteur_princ1_prenom` = ?, `auteur_princ1_institution` = ?,
            `auteur_princ2_nom` = ?, `auteur_princ2_prenom` = ?, `auteur_princ2_institution` = ?,
            `redac_chef_nom` = ?, `redac_chef_prenom` = ?, `redac_chef_institution` = ?,
            `nbre_pages` = ?, `description` = ?, `morceau_choisi` = ?,
            `editeur1` = ?, `editeur2` = ?, `date_parution` = ?, `fichier_ebook` = ?,
            `format_livre` = ?, `type_support` = ?, `stock_iris` = ?, `stock_editeur` = ?,
            `isbn` = ?, `doi` = ?, `critique` = ?, `consign_auteur` = ?, `issn` = ?,
            `issn_online` = ? WHERE `id_livre` = ?";

            $res= $this->base->prepare($sql);
            $res->execute([$m05, $m06,$m07,$m08,$m09,$m10,
            $m11,$m12,$m13,$m14,$m15,$m16,$m17,$m18,$m19,$m20,$m21,$m22,
            $m23,$m24,$m25,$m26,$m27,$m28,$m29,$m30,$m31,$m32,$m33,$m34,
            $m35,$m37,$m38,$m01]); 
            
        }

        if ($res) {
            return true;
        } else {
            return false;
        }
    }


    public function ajouterRevue(Specif_revue $mbr1) 
    {

        

        if (isset($_POST['nom'])) { $mbr1->setNom_specif_revue($_POST['nom']); }
        if (isset($_POST['period'])) { $mbr1->setPeriodicite((int)htmlentities(trim($_POST['period']))); }
        if (isset($_POST['horsserie'])) { $mbr1->setHors_serie(1);} 
          else { $mbr1->setHors_serie(0);}
        if (isset($_POST['prix'])) { $mbr1->setPrix((int)htmlentities(trim($_POST['prix']))); }
        $mbr1->setMonnaie("Euro");

        $m01=    $mbr1->getNom_specif_revue();
        $m02=    $mbr1->getPeriodicite();
        $m03=    $mbr1->getHors_serie();
        $m04=    $mbr1->getPrix();
        $m05=    $mbr1->getMonnaie();

        $sql= "INSERT INTO `specif_revues` (`nom_specif_revue`, `periodicite`,
        `hors_serie`, `prix`, `monnaie`) 
        VALUES ('$m01','$m02','$m03','$m04','$m05')";
        $res= $this->base->query($sql);

        if ($res) {
            return true;
        } else {
            return false;
        }
    }


    public function ajouterMembre(ClientMembre $mbr1) 
    {
        $m01=    $mbr1->getType_membre();
        $m02=    $mbr1->getCivilite();
        $m03=    $mbr1->getNom_membre();
        $m04=    $mbr1->getPrenom_membre();
        $m05=    $mbr1->getInstitution();
        $m06=    $mbr1->getFonction();
        $m07=    $mbr1->getStatut_client();
        $m08=    $mbr1->getJustif_etud_ou_ssemploi_instit();        
        $m09=    $mbr1->getEmail();
        $m10=    $mbr1->getMdp(); 
        $m11=    $mbr1->getOffre_partenaire();
        $m12=    $mbr1->getOffre_newsletter();
        $m13=    $mbr1->getPrix_membre();
        $m14=    $mbr1->getMonnaie(); 
        $m15=    $mbr1->getDate_debut(); 
        $m16=    $mbr1->getAdr_livrais(); 
        $m17=    $mbr1->getCode_postal();
        $m18=    $mbr1->getVille();
        $m19=    $mbr1->getPays();
        $m20=    $mbr1->getZip_etat();
        $m21=    $mbr1->getIndicatif();
        $m22=    $mbr1->getTelephone();

        $sql= "SELECT nom_monnaie FROM monnaies WHERE symb_monnaie = '".$m14."'";
        $res= $this->base->query($sql);
        $rows= $res->fetchAll(PDO::FETCH_OBJ);
        $res->closeCursor();
        $m14= $rows[0]->nom_monnaie;

        $sql= "INSERT INTO client_membres(type_membre, civilite, nom_membre, prenom_membre, institution, fonction,
        statut_client, justif_etud_ou_ssemploi_instit, email, mdp, offre_partenaire,
        offre_newsletter, prix_membre, monnaie, date_debut, adr_livrais, code_postal,
        ville, pays, zip_etat, indicatif, telephone) 
        VALUES ('$m01','$m02','$m03','$m04','$m05','$m06','$m07','$m08','$m09','$m10','$m11','$m12','$m13','$m14','$m15','$m16','$m17','$m18','$m19','$m20','$m21','$m22')";
        $res= $this->base->query($sql);

        if ($res) {
            return true;
        } else {
            return false;
        }
    }


    public function changerIdClientPanier($idAuthAncien, $idAuth, $typclt) 
    {
        $sqldonn= "UPDATE `paniers` SET
        `type_client` = ?, `id_client` = ? WHERE `id_client` = '".$idAuthAncien."' AND `type_client` = 0";

        $res= $this->base->prepare($sqldonn);
        $res->execute([$typclt, $idAuth]);
        if ($res) {return true;} else {return false;}
        $res->closeCursor();

    }



    public function modifierPanier($data ,$idPan, $nbre, $nbPan, $pos) 
                                                   
    {     
                if ($nbre > 0) {
                    
                    if ($pos == 1) {$sqldonn= "UPDATE `paniers` SET  nbre1 = '".$nbre."' WHERE `id_panier` = '".$idPan."'";}
                    if ($pos == 2) {$sqldonn= "UPDATE `paniers` SET  nbre2 = '".$nbre."' WHERE `id_panier` = '".$idPan."'";}
                    if ($pos == 3) {$sqldonn= "UPDATE `paniers` SET  nbre3 = '".$nbre."' WHERE `id_panier` = '".$idPan."'";}
                    $res= $this->base->query($sqldonn);

                } else {
                    if ($pos==3) {
                        $sqldonn="UPDATE `paniers` SET  
                        `id_livre3` = ? , `ref_livre3` = ? , `ttr_livre3` = ? , `img_livre3` = ? ,
                         `pxttc_livre3` = ? , `id_artcl3` = ? , `ttr_artcl3` = ? , `pxttc_artcl3` = ? ,
                        `nbre3` = ? , `frs_livrais3` = ? WHERE `id_panier` = '".$idPan."'";
                        
                        $res= $this->base->prepare($sqldonn);
                        $res->execute(['1','','','','0','1','','0','0','0']);
                            
                    }

                    if ($pos==2) {
                        $sqldonn="UPDATE `paniers` SET  
                        `id_livre2` = ? , `ref_livre2` = ? , `ttr_livre2` = ? , `img_livre2` = ? ,
                         `pxttc_livre2` = ? , `id_artcl2` = ? , `ttr_artcl2` = ? , `pxttc_artcl2` = ? ,
                        `nbre2` = ? , `frs_livrais2` = ?,
                        `id_livre3` = ? , `ref_livre3` = ? , `ttr_livre3` = ? , `img_livre3` = ? ,
                         `pxttc_livre3` = ? , `id_artcl3` = ? , `ttr_artcl3` = ? , `pxttc_artcl3` = ? ,
                        `nbre3` = ? , `frs_livrais3` = ? WHERE `id_panier` = '".$idPan."'";
                        
                        $res= $this->base->prepare($sqldonn);
                        $res->execute([$data->getId_livre3(), $data->getRef_livre3(), $data->getTtr_livre3(),
                        $data->getImg_livre3(), $data->getPxttc_livre3(), $data->getId_artcl3(),
                        $data->getTtr_artcl3(), $data->getPxttc_artcl3(), $data->getNbre3(), $data->getFrs_livrais3(),
                        '1','','','','0','1','','0','0','0']);
                    }

                    if ($pos==1) {
                        if ($nbPan > 0) {
                            $sqldonn="UPDATE `paniers` SET  
                            `id_livre1` = ? , `ref_livre1` = ? , `ttr_livre1` = ? , `img_livre1` = ? ,
                            `pxttc_livre1` = ? , `id_artcl1` = ? , `ttr_artcl1` = ? , `pxttc_artcl1` = ? ,
                            `nbre1` = ? , `frs_livrais1` = ?,
                            `id_livre2` = ? , `ref_livre2` = ? , `ttr_livre2` = ? , `img_livre2` = ? ,
                            `pxttc_livre2` = ? , `id_artcl2` = ? , `ttr_artcl2` = ? , `pxttc_artcl2` = ? ,
                            `nbre2` = ? , `frs_livrais2` = ?,
                            `id_livre3` = ? , `ref_livre3` = ? , `ttr_livre3` = ? , `img_livre3` = ? ,
                            `pxttc_livre3` = ? , `id_artcl3` = ? , `ttr_artcl3` = ? , `pxttc_artcl3` = ? ,
                            `nbre3` = ? , `frs_livrais3` = ? WHERE `id_panier` = '".$idPan."'";
                            
                            $res= $this->base->prepare($sqldonn);
                            $res->execute([$data->getId_livre2(), $data->getRef_livre2(), $data->getTtr_livre2(),
                            $data->getImg_livre2(), $data->getPxttc_livre2(), $data->getId_artcl2(),
                            $data->getTtr_artcl2(), $data->getPxttc_artcl2(), $data->getNbre2(), $data->getFrs_livrais2(),
                            $data->getId_livre3(), $data->getRef_livre3(), $data->getTtr_livre3(),
                            $data->getImg_livre3(), $data->getPxttc_livre3(), $data->getId_artcl3(),
                            $data->getTtr_artcl3(), $data->getPxttc_artcl3(), $data->getNbre3(), $data->getFrs_livrais3(),
                            '1','','','','0','1','','0','0','0']);
                        } else {
                            $sqldonn="DELETE FROM `paniers` WHERE `paniers`.`id_panier` = '".$idPan."'";
                            $res= $this->base->query($sqldonn);
                        }
                    }
                }
                    if ($res) { $_SESSION["panier"]= $nbPan; $res->closeCursor(); return true;} 
                    else {$res->closeCursor(); return false;}
    }


    public function ajouterPanier($dataL, $dataAuth, $typclt, $nouv, $pos, $ajout, $lvr0art1) 
                                                    // $nouv = 1 nouveau, 0 existe
                                                    // $pos = 1, 2, 3 dans la ligne panier
                                                    // $ajout = 1 on incrémente Nbre++, 0 Nbre -> 1
    {

            if ($nouv == 1) {
                    $mbr1= new Panier();           // créer à chaque each un nouvel objet 
                    
                    $mbr1->setDate_created(date("Y-m-d"));
                    $mbr1->setType_client($typclt);
                    $mbr1->setId_client((int)$dataAuth["numero"]);


                if ($lvr0art1 == 0) {

                    $mbr1->setId_livre1((int)$dataL->getId_livre());
                    $mbr1->setRef_livre1($dataL->getReference());
                    $mbr1->setTtr_livre1($dataL->getTitre());
                    $mbr1->setImg_livre1($dataL->getImage1());
                    $mbr1->setPxttc_livre1((int)$dataL->getPrix());
                    $mbr1->setId_artcl1(1);
                    $mbr1->setTtr_artcl1("");
                    $mbr1->setPxttc_artcl1(0);
                    $mbr1->setNbre1(1);

                    $frais=0;
                    if ($dataL->getEditeur1() <> "") {
                        $edit1= $this->listeEditeur(0, 0, $dataL->getEditeur1(), "", 0);
                        
                        $frais= (int)$edit1[0]->getFrs_livrais();
                    }
                    if ($dataL->getEditeur2() <> "") {
                        $edit2= $this->listeEditeur(0, 0, $dataL->getEditeur2(), "", 0);
                        if ($frais > (int)$edit2[0]->getFrs_livrais()) {
                            $frais= (int)$edit2[0]->getFrs_livrais();
                        }
                    }
                    $mbr1->setFrs_livrais1($frais);
                    $mbr1->setMonnaie($dataL->getMonnaie());
                    
                } else {
                    if ((int)$dataL->getId_livre() > 1) {
                        $donnLivre= $this->listeLivreRevue((int)$dataL->getId_livre(), "", "", "", 0, 0, "toutes");

                        $mbr1->setTtr_livre1($donnLivre[0]->getTitre());
                        $mbr1->setImg_livre1($donnLivre[0]->getImage1());
                        $mbr1->setPxttc_livre1($donnLivre[0]->getPrix());
                    } else {
                        $mbr1->setTtr_livre1("");
                        $mbr1->setImg_livre1("");
                        $mbr1->setPxttc_livre1(0);
                    }
                    $mbr1->setId_livre1((int)$dataL->getId_livre());
                    $mbr1->setRef_livre1($dataL->getRef_livre());
                    $mbr1->setId_artcl1($dataL->getId_article());
                    $mbr1->setTtr_artcl1($dataL->getTitre());
                    $mbr1->setPxttc_artcl1($dataL->getPrix());
                    $mbr1->setNbre1(1);
                    $mbr1->setFrs_livrais1(0);
                    $mbr1->setMonnaie($dataL->getMonnaie());

                }

                $mbr1->setId_livre2(1);
                $mbr1->setRef_livre2("");
                $mbr1->setTtr_livre2("");
                $mbr1->setImg_livre2("");
                $mbr1->setPxttc_livre2(0);
                $mbr1->setId_artcl2(1);
                $mbr1->setTtr_artcl2("");
                $mbr1->setPxttc_artcl2(0);
                $mbr1->setNbre2(0);
                $mbr1->setFrs_livrais2(0);
                $mbr1->setId_livre3(1);
                $mbr1->setRef_livre3("");
                $mbr1->setTtr_livre3("");
                $mbr1->setImg_livre3("");
                $mbr1->setPxttc_livre3(0);
                $mbr1->setId_artcl3(1);
                $mbr1->setTtr_artcl3("");
                $mbr1->setPxttc_artcl3(0);
                $mbr1->setNbre3(0);
                $mbr1->setFrs_livrais3(0);

                    $sqldonn="INSERT INTO `paniers` (`date_created`, `type_client`, `id_client`, `monnaie`, 
                    `id_livre1`, `ref_livre1`, `ttr_livre1`,
                    `img_livre1`, `pxttc_livre1`, `id_artcl1`, `ttr_artcl1`, `pxttc_artcl1`,
                    `nbre1`, `frs_livrais1`, `id_livre2`, `ref_livre2`, `ttr_livre2`,
                    `img_livre2`, `pxttc_livre2`, `id_artcl2`, `ttr_artcl2`, `pxttc_artcl2`,
                    `nbre2`, `frs_livrais2`, `id_livre3`, `ref_livre3`, `ttr_livre3`,
                    `img_livre3`, `pxttc_livre3`, `id_artcl3`, `ttr_artcl3`, `pxttc_artcl3`,
                    `nbre3`, `frs_livrais3`) 
                    VALUES ('".$mbr1->getDate_created()."', '".$mbr1->getType_client()."', '".$mbr1->getId_client()."','".$mbr1->getMonnaie()."', 
                    '".$mbr1->getId_livre1()."','".$mbr1->getRef_livre1()."', '".$mbr1->getTtr_livre1()."', 
                    '".$mbr1->getImg_livre1()."',
                     '".$mbr1->getPxttc_livre1()."','".$mbr1->getId_artcl1()."','".$mbr1->getTtr_artcl1()."',
                     '".$mbr1->getPxttc_artcl1()."', '".$mbr1->getNbre1()."','".$mbr1->getFrs_livrais1()."',
                    '".$mbr1->getId_livre2()."','".$mbr1->getRef_livre2()."', '".$mbr1->getTtr_livre2()."', 
                    '".$mbr1->getImg_livre2()."',
                     '".$mbr1->getPxttc_livre2()."','".$mbr1->getId_artcl2()."','".$mbr1->getTtr_artcl2()."',
                     '".$mbr1->getPxttc_artcl2()."', '".$mbr1->getNbre2()."','".$mbr1->getFrs_livrais2()."',
                     '".$mbr1->getId_livre3()."','".$mbr1->getRef_livre3()."', '".$mbr1->getTtr_livre3()."', 
                    '".$mbr1->getImg_livre3()."',
                     '".$mbr1->getPxttc_livre3()."', '".$mbr1->getId_artcl3()."',
                     '".$mbr1->getTtr_artcl3()."', '".$mbr1->getPxttc_artcl3()."', '".$mbr1->getNbre3()."','".$mbr1->getFrs_livrais3()."')";

                    $res= $this->base->query($sqldonn);
                    if ($res) {$_SESSION["panier"]= 1; 
                        return true;} 
                    else {return false;}
                    $res->closeCursor();

            } else {

                if ($ajout == 1) {

                    $ii= (int)$dataAuth["numero"];
                    if ($pos == 1) {$sqldonn= "UPDATE `paniers` SET  nbre1 = nbre1 + 1 WHERE `id_client` = '".$ii."'";}
                    if ($pos == 2) {$sqldonn= "UPDATE `paniers` SET  nbre2 = nbre2 + 1 WHERE `id_client` = '".$ii."'";}
                    if ($pos == 3) {$sqldonn= "UPDATE `paniers` SET  nbre3 = nbre3 + 1 WHERE `id_client` = '".$ii."'";}
                    $res= $this->base->query($sqldonn);
                    if ($res) {
                        if (!isset($_SESSION["panier"])) {$_SESSION["panier"]= 1;}
                        else {$_SESSION["panier"]++;}
                        return true;} 
                    else {return false;}
                    $res->closeCursor();

                } else {
                    $mbr1= new Panier();           // créer à chaque each un nouvel objet 
                    
                    $mbr1->setType_client($typclt);
                    $mbr1->setId_client($dataAuth["numero"]);

                    if ($lvr0art1 == 0) {

                        $mbr1->setId_livre1((int)$dataL->getId_livre());
                        $mbr1->setRef_livre1($dataL->getReference());
                        $mbr1->setTtr_livre1($dataL->getTitre());
                        $mbr1->setImg_livre1($dataL->getImage1());
                        $mbr1->setPxttc_livre1((int)$dataL->getPrix());
                        $mbr1->setId_artcl1(1);
                        $mbr1->setTtr_artcl1("");
                        $mbr1->setPxttc_artcl1(0);
                        $mbr1->setNbre1(1);

                        $frais=0;
                        if ($dataL->getEditeur1() <> "") {
                            $edit1= $this->listeEditeur(0, 0, $dataL->getEditeur1(), "", 0);
                            
                            $frais= (int)$edit1[0]->getFrs_livrais();
                        }
                        if ($dataL->getEditeur2() <> "") {
                            $edit2= $this->listeEditeur(0, 0, $dataL->getEditeur2(), "", 0);
                            if ($frais > (int)$edit2[0]->getFrs_livrais()) {
                                $frais= (int)$edit2[0]->getFrs_livrais();
                            }
                        }
                        $mbr1->setFrs_livrais1($frais);

                    } else {

                        if ((int)$dataL->getId_livre() > 1) {
                            $donnLivre= $this->listeLivreRevue((int)$dataL->getId_livre(), "", "", "", 0, 0, "toutes");
                            $mbr1->setTtr_livre1($donnLivre[0]->getTitre());
                            $mbr1->setImg_livre1($donnLivre[0]->getImage1());
                            $mbr1->setPxttc_livre1($donnLivre[0]->getPrix());
                        } else {
                            $mbr1->setTtr_livre1("");
                            $mbr1->setImg_livre1("");
                            $mbr1->setPxttc_livre1(0);
                        }   
                        $mbr1->setId_livre1((int)$dataL->getId_livre());
                        $mbr1->setRef_livre1($dataL->getRef_livre());
                        $mbr1->setId_artcl1($dataL->getId_article());
                        $mbr1->setTtr_artcl1($dataL->getTitre());
                        $mbr1->setPxttc_artcl1($dataL->getPrix());
                        $mbr1->setNbre1(1);
                        $mbr1->setFrs_livrais1(0);
                        $mbr1->setMonnaie($dataL->getMonnaie());

                    } 

                $ii= (int)$dataAuth["numero"];
                if ($pos == 1) {
                    $sqldonn= "UPDATE `paniers` SET  
                    `id_livre1` = ? , `ref_livre1` = ? , `ttr_livre1` = ? , `img_livre1` = ? ,
                     `pxttc_livre1` = ? , `id_artcl1` = ? , `ttr_artcl1` = ? , `pxttc_artcl1` = ? ,
                    `nbre1` = ? , `frs_livrais1` = ? WHERE `id_client` = '".$ii."'";
                }
                if ($pos == 2) {
                    $sqldonn= "UPDATE `paniers` SET  
                    `id_livre2` = ? , `ref_livre2` = ? , `ttr_livre2` = ? , `img_livre2` = ? ,
                     `pxttc_livre2` = ? , `id_artcl2` = ? , `ttr_artcl2` = ? , `pxttc_artcl2` = ? ,
                    `nbre2` = ? , `frs_livrais2` = ? WHERE `id_client` = '".$ii."'";
                }
                if ($pos == 3) {
                    $sqldonn= "UPDATE `paniers` SET  
                    `id_livre3` = ? , `ref_livre3` = ? , `ttr_livre3` = ? , `img_livre3` = ? ,
                     `pxttc_livre3` = ? , `id_artcl3` = ? , `ttr_artcl3` = ? , `pxttc_artcl3` = ? ,
                    `nbre3` = ? , `frs_livrais3` = ? WHERE `id_client` = '".$ii."'";
                }

                $res= $this->base->prepare($sqldonn);
                $res->execute([$mbr1->getId_livre1(), $mbr1->getRef_livre1(), $mbr1->getTtr_livre1(),
                               $mbr1->getImg_livre1(), $mbr1->getPxttc_livre1(), $mbr1->getId_artcl1(),
                               $mbr1->getTtr_artcl1(), $mbr1->getPxttc_artcl1(), 1, $mbr1->getFrs_livrais1()]);

                    if ($res) {
                        if (!isset($_SESSION["panier"])) {$_SESSION["panier"]= 1;}
                        else {$_SESSION["panier"]++;}
                        return true;} 
                    else {return false;}
                    $res->closeCursor();

                }

            }

    }

/******************* Autres fonctions ***************************/

    public function getUsers(User $user){
        
        $sql = "SELECT * FROM utilisateurs 
        WHERE (nom = :nom OR pseudo = :pseudo OR email = :email)
         AND (pass = :pass)";
         $res = $this->base->prepare($sql);
         $res->execute(array('nom'=>$user->getNom(), 'pseudo'=>$user->getPseudo(), 'email'=>$user->getEmail(), 'pass'=>$user->getPass()));
         if($res){
            $row = $res->fetch();
            $newUser = new User();
            $newUser->setId_util(0);
            if ($row) {
                $newUser->setId_util($row['id_util']);
                $newUser->setNom($row['nom']);
                $newUser->setPrenom($row['prenom']);
                $newUser->setPseudo($row['pseudo']);
                $newUser->setEmail($row['email']);
                $newUser->setPass($row['pass']);
                $newUser->setRole($row['role']);
                $newUser->setOperationnel($row['operationnel']);
                $newUser->setDate_created($row['date_created']);
            }

            return $newUser;
         }
    }

    public function getClients(User $user){
        
        $sql = "SELECT * FROM client_membres 
        WHERE (email = :email)
         AND (mdp = :pass)";
         $res = $this->base->prepare($sql);
         $res->execute(array('email'=>$user->getPseudo(), 'pass'=>$user->getPass()));
         if($res){
            $row = $res->fetch();
            $newUser = new User();
            $newUser->setId_util(0);
            if ($row) {
                $newUser->setId_util($row['id_clt_membre']);
                $newUser->setPseudo($row['email']);
                $newUser->setPass($row['mdp']);
                $newUser->setRole($row['type_membre']);
            }

            return $newUser;
         }
    }

    public function ajoutUtilisateur(User $donnUsr) 
    {
        
        $nom= $donnUsr->getNom();
        $prenom= $donnUsr->getPrenom();
        $pseudo= $donnUsr->getPseudo();
        $email= $donnUsr->getEmail();
        $pass= $donnUsr->getPass();
        $role= $donnUsr->getRole();
        $operationnel= 1;
        $date_created= $donnUsr->getDate_created();

        $sql= "INSERT INTO `utilisateurs` (`nom`, `prenom`, `pseudo`, `email`, `pass`, `role`, `operationnel`, `date_created`) 
        VALUES ('$nom','$prenom','$pseudo','$email','$pass','$role','$operationnel','$date_created')";
        $res= $this->base->query($sql);
        if ($res) {
            return true;
        } else {
            return false;
        }

    }


}


?>