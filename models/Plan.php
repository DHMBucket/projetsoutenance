<?php

class Plan
{
    private $id_plan;
    private $id_artcl;
    private $titre1; 
    private $ss_ttr11; 
    private $ss_ttr12;
    private $ss_ttr13;
    private $titre2; 
    private $ss_ttr21; 
    private $ss_ttr22;
    private $ss_ttr23;
    private $titre3; 
    private $ss_ttr31; 
    private $ss_ttr32;
    private $ss_ttr33;



    /**
     * Get the value of id_plan
     */ 
    public function getId_plan()
    {
        return $this->id_plan;
    }

    /**
     * Set the value of id_plan
     *
     * @return  self
     */ 
    public function setId_plan($id_plan)
    {
        $this->id_plan = $id_plan;

        return $this;
    }

    /**
     * Get the value of id_artcl
     */ 
    public function getId_artcl()
    {
        return $this->id_artcl;
    }

    /**
     * Set the value of id_artcl
     *
     * @return  self
     */ 
    public function setId_artcl($id_artcl)
    {
        $this->id_artcl = $id_artcl;

        return $this;
    }

    /**
     * Get the value of titre1
     */ 
    public function getTitre1()
    {
        return $this->titre1;
    }

    /**
     * Set the value of titre1
     *
     * @return  self
     */ 
    public function setTitre1($titre1)
    {
        $this->titre1 = $titre1;

        return $this;
    }

    /**
     * Get the value of ss_ttr11
     */ 
    public function getSs_ttr11()
    {
        return $this->ss_ttr11;
    }

    /**
     * Set the value of ss_ttr11
     *
     * @return  self
     */ 
    public function setSs_ttr11($ss_ttr11)
    {
        $this->ss_ttr11 = $ss_ttr11;

        return $this;
    }

    /**
     * Get the value of ss_ttr12
     */ 
    public function getSs_ttr12()
    {
        return $this->ss_ttr12;
    }

    /**
     * Set the value of ss_ttr12
     *
     * @return  self
     */ 
    public function setSs_ttr12($ss_ttr12)
    {
        $this->ss_ttr12 = $ss_ttr12;

        return $this;
    }

    /**
     * Get the value of ss_ttr13
     */ 
    public function getSs_ttr13()
    {
        return $this->ss_ttr13;
    }

    /**
     * Set the value of ss_ttr13
     *
     * @return  self
     */ 
    public function setSs_ttr13($ss_ttr13)
    {
        $this->ss_ttr13 = $ss_ttr13;

        return $this;
    }

    /**
     * Get the value of titre2
     */ 
    public function getTitre2()
    {
        return $this->titre2;
    }

    /**
     * Set the value of titre2
     *
     * @return  self
     */ 
    public function setTitre2($titre2)
    {
        $this->titre2 = $titre2;

        return $this;
    }

    /**
     * Get the value of ss_ttr21
     */ 
    public function getSs_ttr21()
    {
        return $this->ss_ttr21;
    }

    /**
     * Set the value of ss_ttr21
     *
     * @return  self
     */ 
    public function setSs_ttr21($ss_ttr21)
    {
        $this->ss_ttr21 = $ss_ttr21;

        return $this;
    }

    /**
     * Get the value of ss_ttr22
     */ 
    public function getSs_ttr22()
    {
        return $this->ss_ttr22;
    }

    /**
     * Set the value of ss_ttr22
     *
     * @return  self
     */ 
    public function setSs_ttr22($ss_ttr22)
    {
        $this->ss_ttr22 = $ss_ttr22;

        return $this;
    }

    /**
     * Get the value of ss_ttr23
     */ 
    public function getSs_ttr23()
    {
        return $this->ss_ttr23;
    }

    /**
     * Set the value of ss_ttr23
     *
     * @return  self
     */ 
    public function setSs_ttr23($ss_ttr23)
    {
        $this->ss_ttr23 = $ss_ttr23;

        return $this;
    }

    /**
     * Get the value of titre3
     */ 
    public function getTitre3()
    {
        return $this->titre3;
    }

    /**
     * Set the value of titre3
     *
     * @return  self
     */ 
    public function setTitre3($titre3)
    {
        $this->titre3 = $titre3;

        return $this;
    }

    /**
     * Get the value of ss_ttr31
     */ 
    public function getSs_ttr31()
    {
        return $this->ss_ttr31;
    }

    /**
     * Set the value of ss_ttr31
     *
     * @return  self
     */ 
    public function setSs_ttr31($ss_ttr31)
    {
        $this->ss_ttr31 = $ss_ttr31;

        return $this;
    }

    /**
     * Get the value of ss_ttr32
     */ 
    public function getSs_ttr32()
    {
        return $this->ss_ttr32;
    }

    /**
     * Set the value of ss_ttr32
     *
     * @return  self
     */ 
    public function setSs_ttr32($ss_ttr32)
    {
        $this->ss_ttr32 = $ss_ttr32;

        return $this;
    }

    /**
     * Get the value of ss_ttr33
     */ 
    public function getSs_ttr33()
    {
        return $this->ss_ttr33;
    }

    /**
     * Set the value of ss_ttr33
     *
     * @return  self
     */ 
    public function setSs_ttr33($ss_ttr33)
    {
        $this->ss_ttr33 = $ss_ttr33;

        return $this;
    }
}    

?>