<?php

class Panier
{
    private $id_panier;
    private $date_created;
    private $type_client;
    private $id_client;
    private $id_livre1;
    private $ref_livre1;
    private $ttr_livre1;
    private $img_livre1;
    private $pxttc_livre1;
    private $id_artcl1;
    private $ttr_artcl1;
    private $pxttc_artcl1;
    private $nbre1;
    private $frs_livrais1;
    private $monnaie;
    private $id_livre2;
    private $ref_livre2;
    private $ttr_livre2;
    private $img_livre2;
    private $pxttc_livre2;
    private $id_artcl2;
    private $ttr_artcl2;
    private $pxttc_artcl2;
    private $nbre2;
    private $frs_livrais2;
    private $id_livre3;
    private $ref_livre3;
    private $ttr_livre3;
    private $img_livre3;
    private $pxttc_livre3;
    private $id_artcl3;
    private $ttr_artcl3;
    private $pxttc_artcl3;
    private $nbre3;
    private $frs_livrais3;


        /**
     * Get the value of date_created
     */ 
    public function getDate_created()
    {
        return $this->date_created;
    }

    /**
     * Set the value of date_created
     *
     * @return  self
     */ 
    public function setDate_created($date_created)
    {
        $this->date_created = $date_created;

        return $this;
    }

    /**
     * Get the value of id_panier
     */ 
    public function getId_panier()
    {
        return $this->id_panier;
    }

    /**
     * Set the value of id_panier
     *
     * @return  self
     */ 
    public function setId_panier($id_panier)
    {
        $this->id_panier = $id_panier;

        return $this;
    }

    /**
     * Get the value of type_client
     */ 
    public function getType_client()
    {
        return $this->type_client;
    }

    /**
     * Set the value of type_client
     *
     * @return  self
     */ 
    public function setType_client($type_client)
    {
        $this->type_client = $type_client;

        return $this;
    }

    /**
     * Get the value of id_client
     */ 
    public function getId_client()
    {
        return $this->id_client;
    }

    /**
     * Set the value of id_client
     *
     * @return  self
     */ 
    public function setId_client($id_client)
    {
        $this->id_client = $id_client;

        return $this;
    }

    /**
     * Get the value of id_livre1
     */ 
    public function getId_livre1()
    {
        return $this->id_livre1;
    }

    /**
     * Set the value of id_livre1
     *
     * @return  self
     */ 
    public function setId_livre1($id_livre1)
    {
        $this->id_livre1 = $id_livre1;

        return $this;
    }

    /**
     * Get the value of ref_livre1
     */ 
    public function getRef_livre1()
    {
        return $this->ref_livre1;
    }

    /**
     * Set the value of ref_livre1
     *
     * @return  self
     */ 
    public function setRef_livre1($ref_livre1)
    {
        $this->ref_livre1 = $ref_livre1;

        return $this;
    }

    /**
     * Get the value of ttr_livre1
     */ 
    public function getTtr_livre1()
    {
        return $this->ttr_livre1;
    }

    /**
     * Set the value of ttr_livre1
     *
     * @return  self
     */ 
    public function setTtr_livre1($ttr_livre1)
    {
        $this->ttr_livre1 = $ttr_livre1;

        return $this;
    }

    /**
     * Get the value of img_livre1
     */ 
    public function getImg_livre1()
    {
        return $this->img_livre1;
    }

    /**
     * Set the value of img_livre1
     *
     * @return  self
     */ 
    public function setImg_livre1($img_livre1)
    {
        $this->img_livre1 = $img_livre1;

        return $this;
    }

    /**
     * Get the value of pxttc_livre1
     */ 
    public function getPxttc_livre1()
    {
        return $this->pxttc_livre1;
    }

    /**
     * Set the value of pxttc_livre1
     *
     * @return  self
     */ 
    public function setPxttc_livre1($pxttc_livre1)
    {
        $this->pxttc_livre1 = $pxttc_livre1;

        return $this;
    }

    /**
     * Get the value of id_artcl1
     */ 
    public function getId_artcl1()
    {
        return $this->id_artcl1;
    }

    /**
     * Set the value of id_artcl1
     *
     * @return  self
     */ 
    public function setId_artcl1($id_artcl1)
    {
        $this->id_artcl1 = $id_artcl1;

        return $this;
    }

    /**
     * Get the value of ttr_artcl1
     */ 
    public function getTtr_artcl1()
    {
        return $this->ttr_artcl1;
    }

    /**
     * Set the value of ttr_artcl1
     *
     * @return  self
     */ 
    public function setTtr_artcl1($ttr_artcl1)
    {
        $this->ttr_artcl1 = $ttr_artcl1;

        return $this;
    }

    /**
     * Get the value of pxttc_artcl1
     */ 
    public function getPxttc_artcl1()
    {
        return $this->pxttc_artcl1;
    }

    /**
     * Set the value of pxttc_artcl1
     *
     * @return  self
     */ 
    public function setPxttc_artcl1($pxttc_artcl1)
    {
        $this->pxttc_artcl1 = $pxttc_artcl1;

        return $this;
    }

    /**
     * Get the value of nbre1
     */ 
    public function getNbre1()
    {
        return $this->nbre1;
    }

    /**
     * Set the value of nbre1
     *
     * @return  self
     */ 
    public function setNbre1($nbre1)
    {
        $this->nbre1 = $nbre1;

        return $this;
    }

    /**
     * Get the value of frs_livrais1
     */ 
    public function getFrs_livrais1()
    {
        return $this->frs_livrais1;
    }

    /**
     * Set the value of frs_livrais1
     *
     * @return  self
     */ 
    public function setFrs_livrais1($frs_livrais1)
    {
        $this->frs_livrais1 = $frs_livrais1;

        return $this;
    }

    /**
     * Get the value of monnaie
     */ 
    public function getMonnaie()
    {
        return $this->monnaie;
    }

    /**
     * Set the value of monnaie
     *
     * @return  self
     */ 
    public function setMonnaie($monnaie)
    {
        $this->monnaie = $monnaie;

        return $this;
    }

    /**
     * Get the value of id_livre2
     */ 
    public function getId_livre2()
    {
        return $this->id_livre2;
    }

    /**
     * Set the value of id_livre2
     *
     * @return  self
     */ 
    public function setId_livre2($id_livre2)
    {
        $this->id_livre2 = $id_livre2;

        return $this;
    }

    /**
     * Get the value of ref_livre2
     */ 
    public function getRef_livre2()
    {
        return $this->ref_livre2;
    }

    /**
     * Set the value of ref_livre2
     *
     * @return  self
     */ 
    public function setRef_livre2($ref_livre2)
    {
        $this->ref_livre2 = $ref_livre2;

        return $this;
    }

    /**
     * Get the value of ttr_livre2
     */ 
    public function getTtr_livre2()
    {
        return $this->ttr_livre2;
    }

    /**
     * Set the value of ttr_livre2
     *
     * @return  self
     */ 
    public function setTtr_livre2($ttr_livre2)
    {
        $this->ttr_livre2 = $ttr_livre2;

        return $this;
    }

    /**
     * Get the value of img_livre2
     */ 
    public function getImg_livre2()
    {
        return $this->img_livre2;
    }

    /**
     * Set the value of img_livre2
     *
     * @return  self
     */ 
    public function setImg_livre2($img_livre2)
    {
        $this->img_livre2 = $img_livre2;

        return $this;
    }

    /**
     * Get the value of pxttc_livre2
     */ 
    public function getPxttc_livre2()
    {
        return $this->pxttc_livre2;
    }

    /**
     * Set the value of pxttc_livre2
     *
     * @return  self
     */ 
    public function setPxttc_livre2($pxttc_livre2)
    {
        $this->pxttc_livre2 = $pxttc_livre2;

        return $this;
    }

    /**
     * Get the value of id_artcl2
     */ 
    public function getId_artcl2()
    {
        return $this->id_artcl2;
    }

    /**
     * Set the value of id_artcl2
     *
     * @return  self
     */ 
    public function setId_artcl2($id_artcl2)
    {
        $this->id_artcl2 = $id_artcl2;

        return $this;
    }

    /**
     * Get the value of ttr_artcl2
     */ 
    public function getTtr_artcl2()
    {
        return $this->ttr_artcl2;
    }

    /**
     * Set the value of ttr_artcl2
     *
     * @return  self
     */ 
    public function setTtr_artcl2($ttr_artcl2)
    {
        $this->ttr_artcl2 = $ttr_artcl2;

        return $this;
    }

    /**
     * Get the value of pxttc_artcl2
     */ 
    public function getPxttc_artcl2()
    {
        return $this->pxttc_artcl2;
    }

    /**
     * Set the value of pxttc_artcl2
     *
     * @return  self
     */ 
    public function setPxttc_artcl2($pxttc_artcl2)
    {
        $this->pxttc_artcl2 = $pxttc_artcl2;

        return $this;
    }

    /**
     * Get the value of nbre2
     */ 
    public function getNbre2()
    {
        return $this->nbre2;
    }

    /**
     * Set the value of nbre2
     *
     * @return  self
     */ 
    public function setNbre2($nbre2)
    {
        $this->nbre2 = $nbre2;

        return $this;
    }

    /**
     * Get the value of frs_livrais2
     */ 
    public function getFrs_livrais2()
    {
        return $this->frs_livrais2;
    }

    /**
     * Set the value of frs_livrais2
     *
     * @return  self
     */ 
    public function setFrs_livrais2($frs_livrais2)
    {
        $this->frs_livrais2 = $frs_livrais2;

        return $this;
    }

    /**
     * Get the value of id_livre3
     */ 
    public function getId_livre3()
    {
        return $this->id_livre3;
    }

    /**
     * Set the value of id_livre3
     *
     * @return  self
     */ 
    public function setId_livre3($id_livre3)
    {
        $this->id_livre3 = $id_livre3;

        return $this;
    }

    /**
     * Get the value of ref_livre3
     */ 
    public function getRef_livre3()
    {
        return $this->ref_livre3;
    }

    /**
     * Set the value of ref_livre3
     *
     * @return  self
     */ 
    public function setRef_livre3($ref_livre3)
    {
        $this->ref_livre3 = $ref_livre3;

        return $this;
    }

    /**
     * Get the value of ttr_livre3
     */ 
    public function getTtr_livre3()
    {
        return $this->ttr_livre3;
    }

    /**
     * Set the value of ttr_livre3
     *
     * @return  self
     */ 
    public function setTtr_livre3($ttr_livre3)
    {
        $this->ttr_livre3 = $ttr_livre3;

        return $this;
    }

    /**
     * Get the value of img_livre3
     */ 
    public function getImg_livre3()
    {
        return $this->img_livre3;
    }

    /**
     * Set the value of img_livre3
     *
     * @return  self
     */ 
    public function setImg_livre3($img_livre3)
    {
        $this->img_livre3 = $img_livre3;

        return $this;
    }

    /**
     * Get the value of pxttc_livre3
     */ 
    public function getPxttc_livre3()
    {
        return $this->pxttc_livre3;
    }

    /**
     * Set the value of pxttc_livre3
     *
     * @return  self
     */ 
    public function setPxttc_livre3($pxttc_livre3)
    {
        $this->pxttc_livre3 = $pxttc_livre3;

        return $this;
    }

    /**
     * Get the value of id_artcl3
     */ 
    public function getId_artcl3()
    {
        return $this->id_artcl3;
    }

    /**
     * Set the value of id_artcl3
     *
     * @return  self
     */ 
    public function setId_artcl3($id_artcl3)
    {
        $this->id_artcl3 = $id_artcl3;

        return $this;
    }

    /**
     * Get the value of ttr_artcl3
     */ 
    public function getTtr_artcl3()
    {
        return $this->ttr_artcl3;
    }

    /**
     * Set the value of ttr_artcl3
     *
     * @return  self
     */ 
    public function setTtr_artcl3($ttr_artcl3)
    {
        $this->ttr_artcl3 = $ttr_artcl3;

        return $this;
    }

    /**
     * Get the value of pxttc_artcl3
     */ 
    public function getPxttc_artcl3()
    {
        return $this->pxttc_artcl3;
    }

    /**
     * Set the value of pxttc_artcl3
     *
     * @return  self
     */ 
    public function setPxttc_artcl3($pxttc_artcl3)
    {
        $this->pxttc_artcl3 = $pxttc_artcl3;

        return $this;
    }

    /**
     * Get the value of nbre3
     */ 
    public function getNbre3()
    {
        return $this->nbre3;
    }

    /**
     * Set the value of nbre3
     *
     * @return  self
     */ 
    public function setNbre3($nbre3)
    {
        $this->nbre3 = $nbre3;

        return $this;
    }

    /**
     * Get the value of frs_livrais3
     */ 
    public function getFrs_livrais3()
    {
        return $this->frs_livrais3;
    }

    /**
     * Set the value of frs_livrais3
     *
     * @return  self
     */ 
    public function setFrs_livrais3($frs_livrais3)
    {
        $this->frs_livrais3 = $frs_livrais3;

        return $this;
    }

}    

?>