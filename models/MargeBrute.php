<?php

class Margebrute
{
    private $id_margebrute;
    private $mbCmd;
    private $mbfrslivr;
    private $mbAdh;
    private $monnaie;



    /**
     * Get the value of id_margebrute
     */ 
    public function getId_margebrute()
    {
        return $this->id_margebrute;
    }

    /**
     * Set the value of id_margebrute
     *
     * @return  self
     */ 
    public function setId_margebrute($id_margebrute)
    {
        $this->id_margebrute = $id_margebrute;

        return $this;
    }

    /**
     * Get the value of mbCmd
     */ 
    public function getMbCmd()
    {
        return $this->mbCmd;
    }

    /**
     * Set the value of mbCmd
     *
     * @return  self
     */ 
    public function setMbCmd($mbCmd)
    {
        $this->mbCmd = $mbCmd;

        return $this;
    }

    /**
     * Get the value of mbfrslivr
     */ 
    public function getMbfrslivr()
    {
        return $this->mbfrslivr;
    }

    /**
     * Set the value of mbfrslivr
     *
     * @return  self
     */ 
    public function setMbfrslivr($mbfrslivr)
    {
        $this->mbfrslivr = $mbfrslivr;

        return $this;
    }

    /**
     * Get the value of mbAdh
     */ 
    public function getMbAdh()
    {
        return $this->mbAdh;
    }

    /**
     * Set the value of mbAdh
     *
     * @return  self
     */ 
    public function setMbAdh($mbAdh)
    {
        $this->mbAdh = $mbAdh;

        return $this;
    }

    /**
     * Get the value of monnaie
     */ 
    public function getMonnaie()
    {
        return $this->monnaie;
    }

    /**
     * Set the value of monnaie
     *
     * @return  self
     */ 
    public function setMonnaie($monnaie)
    {
        $this->monnaie = $monnaie;

        return $this;
    }
}    

?>