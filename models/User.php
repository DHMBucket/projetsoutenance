<?php

class User
{
    private $id_util;
    private $nom;
    private $prenom;
    private $pseudo;
    private $email;
    private $pass;
    private $role;
    private $operationnel;
    private $date_created;

    // Getteurs

    public function getId_util()
    {
        return $this->id_util;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function getPrenom()
    {
        return $this->prenom;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPass()
    {
        return $this->pass;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function getDate_created()
    {
        return $this->date_created;
    }


    // Setteurs

    public function setId_util($id_util)
    {
        $this->id_util = $id_util;

        return $this;
    }

    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function setPass($pass)
    {
        $this->pass = $pass;

        return $this;
    }

    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    public function setDate_created($date_created)
    {
        $this->date_created = $date_created;

        return $this;
    }
    

    /**
     * Get the value of pseudo
     */ 
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     * Set the value of pseudo
     *
     * @return  self
     */ 
    public function setPseudo($pseudo)
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    /**
     * Get the value of operationnel
     */ 
    public function getOperationnel()
    {
        return $this->operationnel;
    }

    /**
     * Set the value of operationnel
     *
     * @return  self
     */ 
    public function setOperationnel($operationnel)
    {
        $this->operationnel = $operationnel;

        return $this;
    }
}


?>