<?php

class Priorite
{
    private $pr1_acces_X1;
    private $pr1_acces_X2;
    private $pr1_acces_X3;
    private $pr1_acces_X4;
    private $pr2_acces_X1;
    private $pr2_acces_X2;
    private $pr2_acces_X3;
    private $pr2_acces_X4;
    private $pr3_acces_X1;
    private $pr3_acces_X2;
    private $pr3_acces_X3;
    private $pr3_acces_X4;
    private $pr4_acces_X1;
    private $pr4_acces_X2;
    private $pr4_acces_X3;
    private $pr4_acces_X4;


    /**
     * Get the value of pr1_acces_X1
     */ 
    public function getPr1_acces_X1()
    {
        return $this->pr1_acces_X1;
    }

    /**
     * Set the value of pr1_acces_X1
     *
     * @return  self
     */ 
    public function setPr1_acces_X1($pr1_acces_X1)
    {
        $this->pr1_acces_X1 = $pr1_acces_X1;

        return $this;
    }

    /**
     * Get the value of pr1_acces_X2
     */ 
    public function getPr1_acces_X2()
    {
        return $this->pr1_acces_X2;
    }

    /**
     * Set the value of pr1_acces_X2
     *
     * @return  self
     */ 
    public function setPr1_acces_X2($pr1_acces_X2)
    {
        $this->pr1_acces_X2 = $pr1_acces_X2;

        return $this;
    }

    /**
     * Get the value of pr1_acces_X3
     */ 
    public function getPr1_acces_X3()
    {
        return $this->pr1_acces_X3;
    }

    /**
     * Set the value of pr1_acces_X3
     *
     * @return  self
     */ 
    public function setPr1_acces_X3($pr1_acces_X3)
    {
        $this->pr1_acces_X3 = $pr1_acces_X3;

        return $this;
    }

    /**
     * Get the value of pr1_acces_X4
     */ 
    public function getPr1_acces_X4()
    {
        return $this->pr1_acces_X4;
    }

    /**
     * Set the value of pr1_acces_X4
     *
     * @return  self
     */ 
    public function setPr1_acces_X4($pr1_acces_X4)
    {
        $this->pr1_acces_X4 = $pr1_acces_X4;

        return $this;
    }

    /**
     * Get the value of pr2_acces_X1
     */ 
    public function getPr2_acces_X1()
    {
        return $this->pr2_acces_X1;
    }

    /**
     * Set the value of pr2_acces_X1
     *
     * @return  self
     */ 
    public function setPr2_acces_X1($pr2_acces_X1)
    {
        $this->pr2_acces_X1 = $pr2_acces_X1;

        return $this;
    }

    /**
     * Get the value of pr2_acces_X2
     */ 
    public function getPr2_acces_X2()
    {
        return $this->pr2_acces_X2;
    }

    /**
     * Set the value of pr2_acces_X2
     *
     * @return  self
     */ 
    public function setPr2_acces_X2($pr2_acces_X2)
    {
        $this->pr2_acces_X2 = $pr2_acces_X2;

        return $this;
    }

    /**
     * Get the value of pr2_acces_X3
     */ 
    public function getPr2_acces_X3()
    {
        return $this->pr2_acces_X3;
    }

    /**
     * Set the value of pr2_acces_X3
     *
     * @return  self
     */ 
    public function setPr2_acces_X3($pr2_acces_X3)
    {
        $this->pr2_acces_X3 = $pr2_acces_X3;

        return $this;
    }

    /**
     * Get the value of pr2_acces_X4
     */ 
    public function getPr2_acces_X4()
    {
        return $this->pr2_acces_X4;
    }

    /**
     * Set the value of pr2_acces_X4
     *
     * @return  self
     */ 
    public function setPr2_acces_X4($pr2_acces_X4)
    {
        $this->pr2_acces_X4 = $pr2_acces_X4;

        return $this;
    }

    /**
     * Get the value of pr3_acces_X1
     */ 
    public function getPr3_acces_X1()
    {
        return $this->pr3_acces_X1;
    }

    /**
     * Set the value of pr3_acces_X1
     *
     * @return  self
     */ 
    public function setPr3_acces_X1($pr3_acces_X1)
    {
        $this->pr3_acces_X1 = $pr3_acces_X1;

        return $this;
    }

    /**
     * Get the value of pr3_acces_X2
     */ 
    public function getPr3_acces_X2()
    {
        return $this->pr3_acces_X2;
    }

    /**
     * Set the value of pr3_acces_X2
     *
     * @return  self
     */ 
    public function setPr3_acces_X2($pr3_acces_X2)
    {
        $this->pr3_acces_X2 = $pr3_acces_X2;

        return $this;
    }

    /**
     * Get the value of pr3_acces_X3
     */ 
    public function getPr3_acces_X3()
    {
        return $this->pr3_acces_X3;
    }

    /**
     * Set the value of pr3_acces_X3
     *
     * @return  self
     */ 
    public function setPr3_acces_X3($pr3_acces_X3)
    {
        $this->pr3_acces_X3 = $pr3_acces_X3;

        return $this;
    }

    /**
     * Get the value of pr3_acces_X4
     */ 
    public function getPr3_acces_X4()
    {
        return $this->pr3_acces_X4;
    }

    /**
     * Set the value of pr3_acces_X4
     *
     * @return  self
     */ 
    public function setPr3_acces_X4($pr3_acces_X4)
    {
        $this->pr3_acces_X4 = $pr3_acces_X4;

        return $this;
    }

    /**
     * Get the value of pr4_acces_X1
     */ 
    public function getPr4_acces_X1()
    {
        return $this->pr4_acces_X1;
    }

    /**
     * Set the value of pr4_acces_X1
     *
     * @return  self
     */ 
    public function setPr4_acces_X1($pr4_acces_X1)
    {
        $this->pr4_acces_X1 = $pr4_acces_X1;

        return $this;
    }

    /**
     * Get the value of pr4_acces_X2
     */ 
    public function getPr4_acces_X2()
    {
        return $this->pr4_acces_X2;
    }

    /**
     * Set the value of pr4_acces_X2
     *
     * @return  self
     */ 
    public function setPr4_acces_X2($pr4_acces_X2)
    {
        $this->pr4_acces_X2 = $pr4_acces_X2;

        return $this;
    }

    /**
     * Get the value of pr4_acces_X3
     */ 
    public function getPr4_acces_X3()
    {
        return $this->pr4_acces_X3;
    }

    /**
     * Set the value of pr4_acces_X3
     *
     * @return  self
     */ 
    public function setPr4_acces_X3($pr4_acces_X3)
    {
        $this->pr4_acces_X3 = $pr4_acces_X3;

        return $this;
    }

    /**
     * Get the value of pr4_acces_X4
     */ 
    public function getPr4_acces_X4()
    {
        return $this->pr4_acces_X4;
    }

    /**
     * Set the value of pr4_acces_X4
     *
     * @return  self
     */ 
    public function setPr4_acces_X4($pr4_acces_X4)
    {
        $this->pr4_acces_X4 = $pr4_acces_X4;

        return $this;
    }
}    

?>