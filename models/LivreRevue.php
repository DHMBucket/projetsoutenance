<?php

class LivreRevue
{
    private $id_livre;
    private $reference;
    private $nom_specif_revue; 
    private $titre;     
    private $prix; 
    private $monnaie; 
    private $image1; 
    private $image2; 
    private $image3; 
    private $image4; 
    private $image5; 
    private $auteur_princ1_nom;     
    private $auteur_princ1_prenom; 
    private $auteur_princ1_institution; 
    private $auteur_princ2_nom;
    private $auteur_princ2_prenom; 
    private $auteur_princ2_institution; 
    private $redac_chef_nom;
    private $redac_chef_prenom; 
    private $redac_chef_institution; 
    private $nbre_pages; 
    private $description; 
    private $morceau_choisi; 
    private $editeur1; 
    private $editeur2; 
    private $date_parution; 
    private $fichier_ebook; 
    private $format_livre; 
    private $type_support; 
    private $stock_iris; 
    private $stock_editeur; 
    private $isbn; 
    private $doi; 
    private $critique; 
    private $consign_auteur; 
    private $periodicite; 
    private $issn; 
    private $issn_online; 
 
    

    /**
     * Get the value of id_livre
     */ 
    public function getId_livre()
    {
        return $this->id_livre;
    }

    /**
     * Set the value of id_livre
     *
     * @return  self
     */ 
    public function setId_livre($id_livre)
    {
        $this->id_livre = $id_livre;

        return $this;
    }

    /**
     * Get the value of reference
     */ 
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set the value of reference
     *
     * @return  self
     */ 
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get the value of nom_specif_revue
     */ 
    public function getNom_specif_revue()
    {
        return $this->nom_specif_revue;
    }

    /**
     * Set the value of nom_specif_revue
     *
     * @return  self
     */ 
    public function setNom_specif_revue($nom_specif_revue)
    {
        $this->nom_specif_revue = $nom_specif_revue;

        return $this;
    }

    /**
     * Get the value of titre
     */ 
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set the value of titre
     *
     * @return  self
     */ 
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get the value of prix
     */ 
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set the value of prix
     *
     * @return  self
     */ 
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get the value of monnaie
     */ 
    public function getMonnaie()
    {
        return $this->monnaie;
    }

    /**
     * Set the value of monnaie
     *
     * @return  self
     */ 
    public function setMonnaie($monnaie)
    {
        $this->monnaie = $monnaie;

        return $this;
    }

    /**
     * Get the value of image1
     */ 
    public function getImage1()
    {
        return $this->image1;
    }

    /**
     * Set the value of image1
     *
     * @return  self
     */ 
    public function setImage1($image1)
    {
        $this->image1 = $image1;

        return $this;
    }

    /**
     * Get the value of image2
     */ 
    public function getImage2()
    {
        return $this->image2;
    }

    /**
     * Set the value of image2
     *
     * @return  self
     */ 
    public function setImage2($image2)
    {
        $this->image2 = $image2;

        return $this;
    }

    /**
     * Get the value of image3
     */ 
    public function getImage3()
    {
        return $this->image3;
    }

    /**
     * Set the value of image3
     *
     * @return  self
     */ 
    public function setImage3($image3)
    {
        $this->image3 = $image3;

        return $this;
    }

    /**
     * Get the value of image4
     */ 
    public function getImage4()
    {
        return $this->image4;
    }

    /**
     * Set the value of image4
     *
     * @return  self
     */ 
    public function setImage4($image4)
    {
        $this->image4 = $image4;

        return $this;
    }

    /**
     * Get the value of image5
     */ 
    public function getImage5()
    {
        return $this->image5;
    }

    /**
     * Set the value of image5
     *
     * @return  self
     */ 
    public function setImage5($image5)
    {
        $this->image5 = $image5;

        return $this;
    }

    /**
     * Get the value of auteur_princ1_nom
     */ 
    public function getAuteur_princ1_nom()
    {
        return $this->auteur_princ1_nom;
    }

    /**
     * Set the value of auteur_princ1_nom
     *
     * @return  self
     */ 
    public function setAuteur_princ1_nom($auteur_princ1_nom)
    {
        $this->auteur_princ1_nom = $auteur_princ1_nom;

        return $this;
    }

    /**
     * Get the value of auteur_princ1_prenom
     */ 
    public function getAuteur_princ1_prenom()
    {
        return $this->auteur_princ1_prenom;
    }

    /**
     * Set the value of auteur_princ1_prenom
     *
     * @return  self
     */ 
    public function setAuteur_princ1_prenom($auteur_princ1_prenom)
    {
        $this->auteur_princ1_prenom = $auteur_princ1_prenom;

        return $this;
    }

    /**
     * Get the value of auteur_princ1_institution
     */ 
    public function getAuteur_princ1_institution()
    {
        return $this->auteur_princ1_institution;
    }

    /**
     * Set the value of auteur_princ1_institution
     *
     * @return  self
     */ 
    public function setAuteur_princ1_institution($auteur_princ1_institution)
    {
        $this->auteur_princ1_institution = $auteur_princ1_institution;

        return $this;
    }

    /**
     * Get the value of auteur_princ2_nom
     */ 
    public function getAuteur_princ2_nom()
    {
        return $this->auteur_princ2_nom;
    }

    /**
     * Set the value of auteur_princ2_nom
     *
     * @return  self
     */ 
    public function setAuteur_princ2_nom($auteur_princ2_nom)
    {
        $this->auteur_princ2_nom = $auteur_princ2_nom;

        return $this;
    }

    /**
     * Get the value of auteur_princ2_prenom
     */ 
    public function getAuteur_princ2_prenom()
    {
        return $this->auteur_princ2_prenom;
    }

    /**
     * Set the value of auteur_princ2_prenom
     *
     * @return  self
     */ 
    public function setAuteur_princ2_prenom($auteur_princ2_prenom)
    {
        $this->auteur_princ2_prenom = $auteur_princ2_prenom;

        return $this;
    }

    /**
     * Get the value of auteur_princ2_institution
     */ 
    public function getAuteur_princ2_institution()
    {
        return $this->auteur_princ2_institution;
    }

    /**
     * Set the value of auteur_princ2_institution
     *
     * @return  self
     */ 
    public function setAuteur_princ2_institution($auteur_princ2_institution)
    {
        $this->auteur_princ2_institution = $auteur_princ2_institution;

        return $this;
    }

    /**
     * Get the value of redac_chef_nom
     */ 
    public function getRedac_chef_nom()
    {
        return $this->redac_chef_nom;
    }

    /**
     * Set the value of redac_chef_nom
     *
     * @return  self
     */ 
    public function setRedac_chef_nom($redac_chef_nom)
    {
        $this->redac_chef_nom = $redac_chef_nom;

        return $this;
    }

    /**
     * Get the value of redac_chef_prenom
     */ 
    public function getRedac_chef_prenom()
    {
        return $this->redac_chef_prenom;
    }

    /**
     * Set the value of redac_chef_prenom
     *
     * @return  self
     */ 
    public function setRedac_chef_prenom($redac_chef_prenom)
    {
        $this->redac_chef_prenom = $redac_chef_prenom;

        return $this;
    }

    /**
     * Get the value of redac_chef_institution
     */ 
    public function getRedac_chef_institution()
    {
        return $this->redac_chef_institution;
    }

    /**
     * Set the value of redac_chef_institution
     *
     * @return  self
     */ 
    public function setRedac_chef_institution($redac_chef_institution)
    {
        $this->redac_chef_institution = $redac_chef_institution;

        return $this;
    }

    /**
     * Get the value of nbre_pages
     */ 
    public function getNbre_pages()
    {
        return $this->nbre_pages;
    }

    /**
     * Set the value of nbre_pages
     *
     * @return  self
     */ 
    public function setNbre_pages($nbre_pages)
    {
        $this->nbre_pages = $nbre_pages;

        return $this;
    }

    /**
     * Get the value of description
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of morceau_choisi
     */ 
    public function getMorceau_choisi()
    {
        return $this->morceau_choisi;
    }

    /**
     * Set the value of morceau_choisi
     *
     * @return  self
     */ 
    public function setMorceau_choisi($morceau_choisi)
    {
        $this->morceau_choisi = $morceau_choisi;

        return $this;
    }

    /**
     * Get the value of editeur1
     */ 
    public function getEditeur1()
    {
        return $this->editeur1;
    }

    /**
     * Set the value of editeur1
     *
     * @return  self
     */ 
    public function setEditeur1($editeur1)
    {
        $this->editeur1 = $editeur1;

        return $this;
    }

    /**
     * Get the value of editeur2
     */ 
    public function getEditeur2()
    {
        return $this->editeur2;
    }

    /**
     * Set the value of editeur2
     *
     * @return  self
     */ 
    public function setEditeur2($editeur2)
    {
        $this->editeur2 = $editeur2;

        return $this;
    }

    /**
     * Get the value of date_parution
     */ 
    public function getDate_parution()
    {
        return $this->date_parution;
    }

    /**
     * Set the value of date_parution
     *
     * @return  self
     */ 
    public function setDate_parution($date_parution)
    {
        $this->date_parution = $date_parution;

        return $this;
    }

    /**
     * Get the value of fichier_ebook
     */ 
    public function getFichier_ebook()
    {
        return $this->fichier_ebook;
    }

    /**
     * Set the value of fichier_ebook
     *
     * @return  self
     */ 
    public function setFichier_ebook($fichier_ebook)
    {
        $this->fichier_ebook = $fichier_ebook;

        return $this;
    }

    /**
     * Get the value of format_livre
     */ 
    public function getFormat_livre()
    {
        return $this->format_livre;
    }

    /**
     * Set the value of format_livre
     *
     * @return  self
     */ 
    public function setFormat_livre($format_livre)
    {
        $this->format_livre = $format_livre;

        return $this;
    }

    /**
     * Get the value of type_support
     */ 
    public function getType_support()
    {
        return $this->type_support;
    }

    /**
     * Set the value of type_support
     *
     * @return  self
     */ 
    public function setType_support($type_support)
    {
        $this->type_support = $type_support;

        return $this;
    }

    /**
     * Get the value of stock_iris
     */ 
    public function getStock_iris()
    {
        return $this->stock_iris;
    }

    /**
     * Set the value of stock_iris
     *
     * @return  self
     */ 
    public function setStock_iris($stock_iris)
    {
        $this->stock_iris = $stock_iris;

        return $this;
    }

    /**
     * Get the value of stock_editeur
     */ 
    public function getStock_editeur()
    {
        return $this->stock_editeur;
    }

    /**
     * Set the value of stock_editeur
     *
     * @return  self
     */ 
    public function setStock_editeur($stock_editeur)
    {
        $this->stock_editeur = $stock_editeur;

        return $this;
    }

    /**
     * Get the value of isbn
     */ 
    public function getIsbn()
    {
        return $this->isbn;
    }

    /**
     * Set the value of isbn
     *
     * @return  self
     */ 
    public function setIsbn($isbn)
    {
        $this->isbn = $isbn;

        return $this;
    }

    /**
     * Get the value of doi
     */ 
    public function getDoi()
    {
        return $this->doi;
    }

    /**
     * Set the value of doi
     *
     * @return  self
     */ 
    public function setDoi($doi)
    {
        $this->doi = $doi;

        return $this;
    }

    /**
     * Get the value of critique
     */ 
    public function getCritique()
    {
        return $this->critique;
    }

    /**
     * Set the value of critique
     *
     * @return  self
     */ 
    public function setCritique($critique)
    {
        $this->critique = $critique;

        return $this;
    }

    /**
     * Get the value of consign_auteur
     */ 
    public function getConsign_auteur()
    {
        return $this->consign_auteur;
    }

    /**
     * Set the value of consign_auteur
     *
     * @return  self
     */ 
    public function setConsign_auteur($consign_auteur)
    {
        $this->consign_auteur = $consign_auteur;

        return $this;
    }

    /**
     * Get the value of periodicite
     */ 
    public function getPeriodicite()
    {
        return $this->periodicite;
    }

    /**
     * Set the value of periodicite
     *
     * @return  self
     */ 
    public function setPeriodicite($periodicite)
    {
        $this->periodicite = $periodicite;

        return $this;
    }

    /**
     * Get the value of issn
     */ 
    public function getIssn()
    {
        return $this->issn;
    }

    /**
     * Set the value of issn
     *
     * @return  self
     */ 
    public function setIssn($issn)
    {
        $this->issn = $issn;

        return $this;
    }

    /**
     * Get the value of issn_online
     */ 
    public function getIssn_online()
    {
        return $this->issn_online;
    }

    /**
     * Set the value of issn_online
     *
     * @return  self
     */ 
    public function setIssn_online($issn_online)
    {
        $this->issn_online = $issn_online;

        return $this;
    }
}    

?>