<?php

class MotCle
{
    private $id_motcle;
    private $nom_motcle; 
    private $id_livre; 
    private $id_artcl;


    /**
     * Get the value of id_motcle
     */ 
    public function getId_motcle()
    {
        return $this->id_motcle;
    }

    /**
     * Set the value of id_motcle
     *
     * @return  self
     */ 
    public function setId_motcle($id_motcle)
    {
        $this->id_motcle = $id_motcle;

        return $this;
    }

    /**
     * Get the value of nom_motcle
     */ 
    public function getNom_motcle()
    {
        return $this->nom_motcle;
    }

    /**
     * Set the value of nom_motcle
     *
     * @return  self
     */ 
    public function setNom_motcle($nom_motcle)
    {
        $this->nom_motcle = $nom_motcle;

        return $this;
    }

    /**
     * Get the value of id_livre
     */ 
    public function getId_livre()
    {
        return $this->id_livre;
    }

    /**
     * Set the value of id_livre
     *
     * @return  self
     */ 
    public function setId_livre($id_livre)
    {
        $this->id_livre = $id_livre;

        return $this;
    }

    /**
     * Get the value of id_artcl
     */ 
    public function getId_artcl()
    {
        return $this->id_artcl;
    }

    /**
     * Set the value of id_artcl
     *
     * @return  self
     */ 
    public function setId_artcl($id_artcl)
    {
        $this->id_artcl = $id_artcl;

        return $this;
    }
}    

?>