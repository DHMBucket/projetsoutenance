<?php

require_once('./models/Auteur.php');
require_once('./models/LivreRevue.php');
require_once('./models/Monnaie.php');
require_once('./models/ClientMembre.php');
require_once('./models/ClientInvite.php');
require_once('./models/MotCle.php');
require_once('./models/Plan.php');
require_once('./models/Article.php');
require_once('./models/Commande.php');
require_once('./models/Panier.php');
require_once('./models/Editeur.php');
require_once('./models/Theme.php');
require_once('./models/Specif_revue.php');
require_once('./models/Abonnement.php');
require_once('./models/Priorite.php');
require_once('./models/User.php');


abstract class Driver
{
    protected $base;

    public function __construct($base1)
    {
        $this->base= $base1;
        // utiliser une variable $base pour toute connexion
    }

    public function getBase() {
        return $this->base;
    }


/******************** fonctions de gestion **************************/

    public function nouvNumCompteur() 
        {
            $sql= "DELETE FROM `compteur`";
            $res= $this->base->query($sql);
            $sql= "INSERT INTO compteur() VALUES ()";
            $res= $this->base->query($sql);
            $sql= "SELECT * FROM compteur";
            $res= $this->base->query($sql);
            $rows= $res->fetchAll(PDO::FETCH_OBJ);
            $res->closeCursor();
            $donnees= [$rows[0]];

            return $donnees;
    }

    public function detailAuteur($id_auteur, $nom, $prenom, $institution) 
    {
        if ($id_auteur>0) {
            $sql= "SELECT * FROM auteurs WHERE `id_auteur` = ".$id_auteur;
        } else {
            $sql= "SELECT * FROM auteurs WHERE `nom` = ".$nom." AND `prenom` LIKE %".$prenom."%"." AND `institution` LIKE %".$institution."%";
        }
        $res= $this->base->query($sql);
        $rows= $res->fetchAll(PDO::FETCH_OBJ);
        $res->closeCursor();

        $donnees= [];
        foreach ($rows as $row) {
            
            $auteur= new Auteur();           // créer à chaque each un nouvel objet 
            $auteur->setId_auteur($row->id_auteur);   // pour avoir une adresse différente 
            $auteur->setNom($row->nom);   // de l'objet créé précédemment.
            $auteur->setPrenom($row->prenom);   
            $auteur->setInstitution($row->institution);
            $auteur->setFonction($row->fonction);
            $auteur->setPhoto($row->photo);
            array_push($donnees, $auteur);
            
        break;
        }
      
        return $donnees;
    }


    public function listeAuteur($id_auteur, $nom, $prenom, $institution, $egNUL)    
    // $egNUL=0, ne pas affichier enregistrement nul
    // 1 pour afficher l'enregistrement nul s'il existe
    {                                                                               

        if ($id_auteur > 0) {
            $sql= "SELECT * FROM auteurs WHERE id_auteur = ?";
            $res= $this->base->prepare($sql);
            $res->execute([$id_auteur]);
        } else {
                if ($nom <> "") {
                    if ($prenom <> "") {
                        if ($institution <> "") {
                            $sql= "SELECT * FROM auteurs WHERE nom LIKE ? AND prenom LIKE ? AND institution LIKE ?";
                            $res= $this->base->prepare($sql);
                            $res->execute(["%$nom%", "%$prenom%", "%$institution%"]);
                        } else {
                            $sql= "SELECT * FROM auteurs WHERE nom LIKE ? AND prenom LIKE ?";
                            $res= $this->base->prepare($sql);
                            $res->execute(["%$nom%", "%$prenom%"]);
                        }
                    } else {
                        if ($institution <> "") {
                            $sql= "SELECT * FROM auteurs WHERE nom LIKE ? AND institution LIKE ?";
                            $res= $this->base->prepare($sql);
                            $res->execute(["%$nom%", "%$institution%"]);
                        } else {
                            $sql= "SELECT * FROM auteurs WHERE nom LIKE ?";
                            $res= $this->base->prepare($sql);
                            $res->execute(["%$nom%"]);
                        }
                    }
                } else {
                    if ($prenom <> "") {
                        if ($institution <> "") {
                            $sql= "SELECT * FROM auteurs WHERE prenom LIKE ? AND institution LIKE ?";
                            $res= $this->base->prepare($sql);
                            $res->execute(["%$prenom%", "%$institution%"]);
                        } else {
                            $sql= "SELECT * FROM auteurs WHERE prenom LIKE ?";
                            $res= $this->base->prepare($sql);
                            $res->execute(["%$prenom%"]);
                        }
                    } else {
                        if ($institution <> "") {
                            $sql= "SELECT * FROM auteurs WHERE institution LIKE ?";
                            $res= $this->base->prepare($sql);
                            $res->execute(["%$institution%"]);
                        } else {
                            $sql= "SELECT * FROM auteurs";
                            $res= $this->base->prepare($sql);
                            $res->execute();
                        }
                    }
                }
        }

        $rows= $res->fetchAll(PDO::FETCH_OBJ);
        $res->closeCursor();
        $donnees= [];$i=0;
        
        foreach ($rows as $row) {

            if ($egNUL==1 || ($row->nom <>"" || $row->institution <>"")) {    
                $auteur= new Auteur();           // créer à chaque each un nouvel objet 
                $auteur->setId_auteur($row->id_auteur);   // pour avoir une adresse différente 
                $auteur->setNom($row->nom);   // de l'objet créé précédemment.
                $auteur->setPrenom($row->prenom);   
                $auteur->setInstitution($row->institution);
                $auteur->setFonction($row->fonction);
                $auteur->setPhoto($row->photo);
                array_push($donnees, $auteur);
                $i++;
            }
        }
        
        if (!isset($_COOKIE['total'])) {
            setcookie('total',count($donnees));
        }
        $_COOKIE['total']=count($donnees);

        return $donnees;
    }


    public function listeLivreRevue($id_livre, $reference, $nomOUtitre, $autOUredacOUedit, $stock, $egNUL, $strannee) 
    {

        if ($id_livre > 1 || $reference <> "") {
            if ($id_livre > 1) {
                if ($reference <> "") {
                    $sql= "SELECT * FROM livresrevues WHERE id_livre = ? AND reference = ?";
                    $res= $this->base->prepare($sql);
                    $res->execute([$id_livre,$reference]);
                } else {
                    $sql= "SELECT * FROM livresrevues WHERE id_livre = ?";
                    $res= $this->base->prepare($sql);
                    $res->execute([$id_livre]);
                }
            } else {
                if ($reference <> "") {
                    $sql= "SELECT * FROM livresrevues WHERE reference = ?";
                    $res= $this->base->prepare($sql);
                    $res->execute([$reference]);
                } else {
                    $sql= "SELECT * FROM livresrevues";
                    $res= $this->base->prepare($sql);
                    $res->execute();
                }
            }
        } else {
            if ($nomOUtitre <> "") {
                if ($autOUredacOUedit <> "") {
                    if ($stock > 0) {
                        $sql= "SELECT * FROM livresrevues WHERE ( nom_specif_revue LIKE ? OR titre LIKE ? ) AND ( auteur_princ1_nom LIKE ? OR auteur_princ1_prenom LIKE ? OR auteur_princ1_institution LIKE ? OR auteur_princ2_nom LIKE ? OR auteur_princ2_prenom LIKE ? OR auteur_princ2_institution LIKE ? OR redac_chef_nom LIKE ? OR redac_chef_prenom LIKE ? OR redac_chef_institution LIKE ? OR editeur1 LIKE ? OR editeur2 LIKE ? ) AND ( stock_iris + stock_editeur ) >= ?";
                        $res= $this->base->prepare($sql);
                        $res->execute(["%$nomOUtitre%","%$nomOUtitre%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", $stock]);
                    } else {
                        if ($stock == 0) {
                            $sql= "SELECT * FROM livresrevues WHERE ( nom_specif_revue LIKE ? OR titre LIKE ? ) AND ( auteur_princ1_nom LIKE ? OR auteur_princ1_prenom LIKE ? OR auteur_princ1_institution LIKE ? OR auteur_princ2_nom LIKE ? OR auteur_princ2_prenom LIKE ? OR auteur_princ2_institution LIKE ? OR redac_chef_nom LIKE ? OR redac_chef_prenom LIKE ? OR redac_chef_institution LIKE ? OR editeur1 LIKE ? OR editeur2 LIKE ? ) AND ( stock_iris + stock_editeur ) = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute(["%$nomOUtitre%","%$nomOUtitre%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", $stock]);
                        } else {
                            $sql= "SELECT * FROM livresrevues WHERE ( nom_specif_revue LIKE ? OR titre LIKE ? ) AND ( auteur_princ1_nom LIKE ? OR auteur_princ1_prenom LIKE ? OR auteur_princ1_institution LIKE ? OR auteur_princ2_nom LIKE ? OR auteur_princ2_prenom LIKE ? OR auteur_princ2_institution LIKE ? OR redac_chef_nom LIKE ? OR redac_chef_prenom LIKE ? OR redac_chef_institution LIKE ? OR editeur1 LIKE ? OR editeur2 LIKE ? )";
                            $res= $this->base->prepare($sql);
                            $res->execute(["%$nomOUtitre%","%$nomOUtitre%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%"]);
                        }
                    }
                } else {
                    if ($stock > 0) {
                        $sql= "SELECT * FROM livresrevues WHERE ( nom_specif_revue LIKE ? OR titre LIKE ? ) AND ( stock_iris + stock_editeur ) >= ?";
                        $res= $this->base->prepare($sql);
                        $res->execute(["%$nomOUtitre%","%$nomOUtitre%", $stock]);
                    } else {
                        if ($stock == 0) {
                            $sql= "SELECT * FROM livresrevues WHERE ( nom_specif_revue LIKE ? OR titre LIKE ? ) AND ( stock_iris + stock_editeur ) = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute(["%$nomOUtitre%","%$nomOUtitre%", $stock]);
                        } else {
                            $sql= "SELECT * FROM livresrevues WHERE ( nom_specif_revue LIKE ? OR titre LIKE ? )";
                            $res= $this->base->prepare($sql);
                            $res->execute(["%$nomOUtitre%","%$nomOUtitre%"]);
                        }
                    }
                }
            } else {
                if ($autOUredacOUedit <> "") {
                    if ($stock > 0) {
                        $sql= "SELECT * FROM livresrevues WHERE ( auteur_princ1_nom LIKE ? OR auteur_princ1_prenom LIKE ? OR auteur_princ1_institution LIKE ? OR auteur_princ2_nom LIKE ? OR auteur_princ2_prenom LIKE ? OR auteur_princ2_institution LIKE ? OR redac_chef_nom LIKE ? OR redac_chef_prenom LIKE ? OR redac_chef_institution LIKE ? OR editeur1 LIKE ? OR editeur2 LIKE ? ) AND ( stock_iris + stock_editeur ) >= ?";
                        $res= $this->base->prepare($sql);
                        $res->execute(["%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", $stock]);
                    } else {
                        if ($stock == 0) {
                            $sql= "SELECT * FROM livresrevues WHERE ( auteur_princ1_nom LIKE ? OR auteur_princ1_prenom LIKE ? OR auteur_princ1_institution LIKE ? OR auteur_princ2_nom LIKE ? OR auteur_princ2_prenom LIKE ? OR auteur_princ2_institution LIKE ? OR redac_chef_nom LIKE ? OR redac_chef_prenom LIKE ? OR redac_chef_institution LIKE ? OR editeur1 LIKE ? OR editeur2 LIKE ? ) AND ( stock_iris + stock_editeur ) = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute(["%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", $stock]);
                        } else {
                            $sql= "SELECT * FROM livresrevues WHERE ( auteur_princ1_nom LIKE ? OR auteur_princ1_prenom LIKE ? OR auteur_princ1_institution LIKE ? OR auteur_princ2_nom LIKE ? OR auteur_princ2_prenom LIKE ? OR auteur_princ2_institution LIKE ? OR redac_chef_nom LIKE ? OR redac_chef_prenom LIKE ? OR redac_chef_institution LIKE ? OR editeur1 LIKE ? OR editeur2 LIKE ? )";
                            $res= $this->base->prepare($sql);
                            $res->execute(["%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%", "%$autOUredacOUedit%"]);
                        }
                    }
                } else {
                    if ($stock > 0) {
                        $sql= "SELECT * FROM livresrevues WHERE ( stock_iris + stock_editeur ) >= ?";
                        $res= $this->base->prepare($sql);
                        $res->execute([$stock]);
                    } else {
                        if ($stock == 0) {
                            $sql= "SELECT * FROM livresrevues WHERE ( stock_iris + stock_editeur ) = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$stock]);
                        } else {
                            $sql= "SELECT * FROM livresrevues";
                            $res= $this->base->prepare($sql);
                            $res->execute();
                        }
                    }
                }
            }
        }

        $rows= $res->fetchAll(PDO::FETCH_OBJ);
        $res->closeCursor();
        $donnees= [];$i=0;
       
        
        if (count($rows)>0) {
        foreach ($rows as $row) {

            if ($egNUL==1 || ($row->reference <>"" || $row->nom_specif_revue <>"" || $row->titre <>"")) {  
                
                if (($strannee <> "toutes" && date("Y", strtotime($row->date_parution)) == $strannee) || ($strannee == "toutes")) {

                    $livre= new LivreRevue();           // créer à chaque each un nouvel objet 
                    $livre->setId_livre($row->id_livre);   // pour avoir une adresse différente 
                    $livre->setReference($row->reference);   // de l'objet créé précédemment.
                    $livre->setNom_specif_revue($row->nom_specif_revue);
                    $livre->setTitre($row->titre);
                    $livre->setPrix($row->prix);
                    $livre->setMonnaie($row->monnaie);
                    $livre->setImage1($row->image1);
                    $livre->setImage2($row->image2);
                    $livre->setImage3($row->image3);
                    $livre->setImage4($row->image4);
                    $livre->setImage5($row->image5);
                    $livre->setAuteur_princ1_nom($row->auteur_princ1_nom);
                    $livre->setAuteur_princ1_prenom($row->auteur_princ1_prenom);
                    $livre->setAuteur_princ1_institution($row->auteur_princ1_institution);
                    $livre->setAuteur_princ2_nom($row->auteur_princ2_nom);
                    $livre->setAuteur_princ2_prenom($row->auteur_princ2_prenom);
                    $livre->setAuteur_princ2_institution($row->auteur_princ2_institution);
                    $livre->setRedac_chef_nom($row->redac_chef_nom);
                    $livre->setRedac_chef_prenom($row->redac_chef_prenom);
                    $livre->setRedac_chef_institution($row->redac_chef_institution);
                    $livre->setNbre_pages($row->nbre_pages);
                    $livre->setDescription($row->description);
                    $livre->setMorceau_choisi($row->morceau_choisi);
                    $livre->setEditeur1($row->editeur1);
                    $livre->setEditeur2($row->editeur2);
                    $livre->setDate_parution($row->date_parution);
                    $livre->setFichier_ebook($row->fichier_ebook);
                    $livre->setFormat_livre($row->format_livre);
                    $livre->setType_support($row->type_support);
                    $livre->setStock_iris($row->stock_iris);
                    $livre->setStock_editeur($row->stock_editeur);
                    $livre->setIsbn($row->isbn);
                    $livre->setDoi($row->doi);
                    $livre->setCritique($row->critique);
                    $livre->setConsign_auteur($row->consign_auteur);
                    $livre->setPeriodicite((int)$row->periodicite);
                    $livre->setIssn($row->issn);
                    $livre->setIssn_online($row->issn_online);

                    array_push($donnees, $livre);
                    $i++;
                }
            }
        }
    }
        
        if (!isset($_COOKIE['total'])) {
            setcookie('total',count($donnees));
        }
        $_COOKIE['total']=count($donnees);

        
        return $donnees;
    }


    public function listeMonnaie($id_monnaie, $nom_monnaie) 
    {

        if ($id_monnaie > 0) {
            $sql= "SELECT * FROM monnaies WHERE id_monnaie = ?";
            $res= $this->base->prepare($sql);
            $res->execute([$id_monnaie]);
        } else {

            if ($nom_monnaie <> "") {
                $sql= "SELECT * FROM monnaies WHERE nom_monnaie LIKE ?";
                $res= $this->base->prepare($sql);
                $res->execute(["%$nom_monnaie%"]);
            } else {
                $sql= "SELECT * FROM monnaies";
                $res= $this->base->prepare($sql);
                $res->execute();
            }
        }
 
        $rows= $res->fetchAll(PDO::FETCH_OBJ);
        $res->closeCursor();
        $donnees= [];$i=0;

        foreach ($rows as $row) {
            
            $monnaie= new Monnaie();           // créer à chaque each un nouvel objet 
            $monnaie->setId_monnaie($row->id_monnaie);   // pour avoir une adresse différente 
            $monnaie->setNom_monnaie($row->nom_monnaie);   // de l'objet créé précédemment.
            $monnaie->setSymb_monnaie($row->symb_monnaie);  
            $monnaie->setVal_commerc_en_euro($row->val_commerc_en_euro);
            array_push($donnees, $monnaie);
            $i++;
        }

        if (!isset($_COOKIE['total'])) {
            setcookie('total',count($donnees));
        }
        $_COOKIE['total']=count($donnees);


        return $donnees;
    }


    public function listeClientInvite($idInvite, $numcompteur, $nomOuInstit, $prenom, $pays, $egNUL) 
    {
        if ($numcompteur > 0 || $idInvite > 0) {
            if ($numcompteur > 0) {
                if ($idInvite > 0) {
                    $sql= "SELECT * FROM client_invites WHERE num_compteur = ? AND id_clt_anonym = ?";
                    $res= $this->base->prepare($sql);
                    $res->execute([$numcompteur, $idInvite]);
                } else {
                    $sql= "SELECT * FROM client_invites WHERE num_compteur = ?";
                    $res= $this->base->prepare($sql);
                    $res->execute([$numcompteur]);
                }
            } else {
                if ($idInvite > 0) {
                    $sql= "SELECT * FROM client_invites WHERE id_clt_anonym = ?";
                    $res= $this->base->prepare($sql);
                    $res->execute([$idInvite]);
                } else {
                    $sql= "SELECT * FROM client_invites";
                    $res= $this->base->prepare($sql);
                    $res->execute();
                }
            }
        } else {
                if ($nomOuInstit <> "") {
                    if ($prenom <> "") {
                        if ($pays <> "") {
                            $sql= "SELECT * FROM client_invites WHERE (nom_destin LIKE ? OR institution LIKE ?) AND prenom_destin = ? AND pays = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute(["%$nomOuInstit%", "%$nomOuInstit%", $prenom, $pays]);
                        } else {
                            $sql= "SELECT * FROM client_invites WHERE (nom_destin LIKE ? OR institution LIKE ?) AND prenom_destin = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute(["%$nomOuInstit%", "%$nomOuInstit%", $prenom]);
                        }
                    } else {
                        if ($pays <> "") {
                            $sql= "SELECT * FROM client_invites WHERE (nom_destin LIKE ? OR institution LIKE ?) AND pays = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute(["%$nomOuInstit%", "%$nomOuInstit%", $pays]);
                        } else {
                            $sql= "SELECT * FROM client_invites WHERE nom_destin LIKE ? OR institution LIKE ?";
                            $res= $this->base->prepare($sql);
                            $res->execute(["%$nomOuInstit%", "%$nomOuInstit%"]);
                        }
                    }
                } else {
                    if ($prenom <> "") {
                        if ($pays <> "") {
                            $sql= "SELECT * FROM client_invites WHERE prenom_destin = ? AND pays = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$prenom, $pays]);
                        } else {
                            $sql= "SELECT * FROM client_invites WHERE prenom_destin = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$prenom]);
                        }
                    } else {
                        if ($pays <> "") {
                            $sql= "SELECT * FROM client_invites WHERE pays = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$pays]);
                        } else {
                            $sql= "SELECT * FROM client_invites";
                            $res= $this->base->prepare($sql);
                            $res->execute();
                        }
                    }
                }
        }
 
        $rows= $res->fetchAll(PDO::FETCH_OBJ);
        $res->closeCursor();
        $donnees= [];$i=0;

        foreach ($rows as $row) {
            
            if ($egNUL==1 || ($row->nom_destin <>"" || $row->institution <>"")) {      
    
                $mbr1= new ClientInvite();           // créer à chaque each un nouvel objet 

                $mbr1->setId_clt_anonym($row->id_clt_anonym);
                $mbr1->setNum_compteur($row->num_compteur);
                $mbr1->setCivilite($row->civilite);
                $mbr1->setNom_destin($row->nom_destin);
                $mbr1->setPrenom_destin($row->prenom_destin);
                $mbr1->setInstitution($row->institution);
                $mbr1->setFonction($row->fonction);
                $mbr1->setEmail($row->email);
                $mbr1->setOffre_partenaire($row->offre_partenaire);
                $mbr1->setOffre_newsletter($row->offre_newsletter); 
                $mbr1->setAdr_livrais($row->adr_livrais); 
                $mbr1->setCode_postal($row->code_postal);
                $mbr1->setPays($row->pays);
                $mbr1->setVille($row->ville);
                $mbr1->setZip_etat($row->zip_etat);
                $mbr1->setIndicatif($row->indicatif);
                $mbr1->setTelephone($row->telephone);
        
                array_push($donnees, $mbr1);
                $i++;
            }
        }

        if (!isset($_COOKIE['total'])) {
            setcookie('total',count($donnees));
        }
        $_COOKIE['total']=count($donnees);

        return $donnees;
    }



    public function listeClientMembre($idmembre, $nomOuInstit, $prenom, $pays, $type_membre, $statut_client, $email, $datefinMmoinsUn, $egNUL) 
    {
        if ($idmembre > 0) {
            $sql= "SELECT * FROM client_membres WHERE id_clt_membre = ?";
            $res= $this->base->prepare($sql);
            $res->execute([$idmembre]);
        } else {
            if ($email <> "") {
                if ($nomOuInstit <> "") {
                    if ($type_membre > 0) {
                            if ($pays <> "") {
                                $sql= "SELECT * FROM client_membres WHERE email = ? AND (nom_membre LIKE ? OR institution LIKE ?) AND type_membre = ? AND (pays = ? OR ville = ?)";
                                $res= $this->base->prepare($sql);
                                $res->execute([$email, "%$nomOuInstit%", "%$nomOuInstit%", $type_membre, $pays, $pays]);
                            } else {
                                $sql= "SELECT * FROM client_membres WHERE email = ? AND (nom_membre LIKE ? OR institution LIKE ?) AND type_membre = ?";
                                $res= $this->base->prepare($sql);
                                $res->execute([$email, "%$nomOuInstit%", "%$nomOuInstit%", $type_membre]);
                            }
                        } else {
                            if ($pays <> "") {
                                $sql= "SELECT * FROM client_membres WHERE email = ? AND (nom_membre LIKE ? OR institution LIKE ?) AND (pays = ? OR ville = ?)";
                                $res= $this->base->prepare($sql);
                                $res->execute([$email, "%$nomOuInstit%", "%$nomOuInstit%", $pays, $pays]);
                            } else {
                                $sql= "SELECT * FROM client_membres WHERE email = ? AND (nom_membre LIKE ? OR institution LIKE ?)";
                                $res= $this->base->prepare($sql);
                                $res->execute([$email, "%$nomOuInstit%", "%$nomOuInstit%"]);
                            }
                        }
                    } else {
                        if ($type_membre > 0) {
                            if ($pays <> "") {
                                $sql= "SELECT * FROM client_membres WHERE email = ? AND type_membre = ? AND (pays = ? OR ville = ?)";
                                $res= $this->base->prepare($sql);
                                $res->execute([$email, $type_membre, $pays, $pays]);
                            } else {
                                $sql= "SELECT * FROM client_membres WHERE email = ? AND type_membre = ?";
                                $res= $this->base->prepare($sql);
                                $res->execute([$email, $type_membre]);
                            }
                        } else {
                            if ($pays <> "") {
                                $sql= "SELECT * FROM client_membres WHERE email = ? AND (pays = ? OR ville = ?)";
                                $res= $this->base->prepare($sql);
                                $res->execute([$email, $pays, $pays]);
                            } else {
                                $sql= "SELECT * FROM client_membres WHERE  email = ?";
                                $res= $this->base->prepare($sql);
                                $res->execute([$email]);
                            }
                        }
                    }
            } else {
                if ($nomOuInstit <> "") {
                    if ($type_membre > 0) {
                            if ($pays <> "") {
                                $sql= "SELECT * FROM client_membres WHERE (nom_membre LIKE ? OR institution LIKE ?) AND type_membre = ? AND (pays = ? OR ville = ?)";
                                $res= $this->base->prepare($sql);
                                $res->execute(["%$nomOuInstit%", "%$nomOuInstit%", $type_membre, $pays, $pays]);
                            } else {
                                $sql= "SELECT * FROM client_membres WHERE (nom_membre LIKE ? OR institution LIKE ?) AND type_membre = ?";
                                $res= $this->base->prepare($sql);
                                $res->execute(["%$nomOuInstit%", "%$nomOuInstit%", $type_membre]);
                            }
                        } else {
                            if ($pays <> "") {
                                $sql= "SELECT * FROM client_membres WHERE (nom_membre LIKE ? OR institution LIKE ?) AND (pays = ? OR ville = ?)";
                                $res= $this->base->prepare($sql);
                                $res->execute(["%$nomOuInstit%", "%$nomOuInstit%", $pays, $pays]);
                            } else {
                                $sql= "SELECT * FROM client_membres WHERE nom_membre LIKE ? OR institution LIKE ?";
                                $res= $this->base->prepare($sql);
                                $res->execute(["%$nomOuInstit%", "%$nomOuInstit%"]);
                            }
                        }
                    } else {
                        if ($type_membre > 0) {
                            if ($pays <> "") {
                                $sql= "SELECT * FROM client_membres WHERE type_membre = ? AND (pays = ? OR ville = ?)";
                                $res= $this->base->prepare($sql);
                                $res->execute([$type_membre, $pays, $pays]);
                            } else {
                                $sql= "SELECT * FROM client_membres WHERE type_membre = ?";
                                $res= $this->base->prepare($sql);
                                $res->execute([$type_membre]);
                            }
                        } else {
                            if ($pays <> "") {
                                $sql= "SELECT * FROM client_membres WHERE (pays = ? OR ville = ?)";
                                $res= $this->base->prepare($sql);
                                $res->execute([$pays, $pays]);
                            } else {
                                $sql= "SELECT * FROM client_membres";
                                $res= $this->base->prepare($sql);
                                $res->execute();
                            }
                        }
                    }
    
            }
        }

            
        $rows= $res->fetchAll(PDO::FETCH_OBJ);
        $res->closeCursor();
        $donnees= [];$i=0;

        foreach ($rows as $row) {

            if ($egNUL==1 || ($row->nom_membre <>"" || $row->institution <>"")) {                       
                if (($prenom <> "" && $row->prenom_membre == $prenom) || ($prenom == "")) {
                    if (($statut_client > 0 && $row->statut_client == $statut_client) || ($statut_client == 0)) {

                        $mbr1= new ClientMembre();           // créer à chaque each un nouvel objet 

                        $mbr1->setId_clt_membre($row->id_clt_membre);
                        $mbr1->setType_membre($row->type_membre);
                        $mbr1->setCivilite($row->civilite);
                        $mbr1->setNom_membre($row->nom_membre);
                        $mbr1->setPrenom_membre($row->prenom_membre);
                        $mbr1->setInstitution($row->institution);
                        $mbr1->setFonction($row->fonction);
                        $mbr1->setStatut_client($row->statut_client);
                        $mbr1->setJustif_etud_ou_ssemploi_instit($row->justif_etud_ou_ssemploi_instit);
                        $mbr1->setEmail($row->email);
                        $mbr1->setOffre_partenaire($row->offre_partenaire);
                        $mbr1->setOffre_newsletter($row->offre_newsletter);
                        $mbr1->setPrix_membre($row->prix_membre);
                        $mbr1->setMonnaie($row->monnaie); 
                        $mbr1->setMdp($row->mdp); 
                        $mbr1->setDate_debut($row->date_debut); 
                        $mbr1->setAdr_livrais($row->adr_livrais); 
                        $mbr1->setCode_postal($row->code_postal);
                        $mbr1->setPays($row->pays);
                        $mbr1->setVille($row->ville);
                        $mbr1->setZip_etat($row->zip_etat);
                        $mbr1->setIndicatif($row->indicatif);
                        $mbr1->setTelephone($row->telephone);
                
                        array_push($donnees, $mbr1);
                        $i++;
                    }
                }
            }
                
        }

        if (!isset($_COOKIE['total'])) {
            setcookie('total',count($donnees));
        }
        $_COOKIE['total']=count($donnees);

        return $donnees;
    }


    public function listeMotsCles($id_motcle, $nom, $id_livre, $id_artcl, $egNUL) 
    {
        if ($id_motcle > 0) {
            if ($nom <> "") {
                if ($id_livre > 1) {
                        if ($id_artcl > 1) {
                            $sql= "SELECT * FROM mots_cles WHERE id_motcle = ? AND nom_motcle LIKE ? AND id_livre = ? AND id_artcl = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_motcle, "%$nom%", $id_livre, $id_artcl]);
                        } else {
                            $sql= "SELECT * FROM mots_cles WHERE id_motcle = ? AND nom_motcle LIKE ? AND id_livre = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_motcle, "%$nom%", $id_livre]);
                        }
                    } else {
                        if ($id_artcl > 1) {
                            $sql= "SELECT * FROM mots_cles WHERE id_motcle = ? AND nom_motcle LIKE ? AND id_artcl = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_motcle, "%$nom%", $id_artcl]);
                        } else {
                            $sql= "SELECT * FROM mots_cles WHERE id_motcle = ? AND nom_motcle LIKE ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_motcle, "%$nom%"]);
                        }
                    }
                } else {
                    if ($id_livre > 1) {
                        if ($id_artcl > 1) {
                            $sql= "SELECT * FROM mots_cles WHERE id_motcle = ? AND id_livre = ? AND id_artcl = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_motcle, $id_livre, $id_artcl]);
                        } else {
                            $sql= "SELECT * FROM mots_cles WHERE id_motcle = ? AND id_livre = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_motcle, $id_livre]);
                        }
                    } else {
                        if ($id_artcl > 1) {
                            $sql= "SELECT * FROM mots_cles WHERE id_motcle = ? AND id_artcl = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_motcle, $id_artcl]);
                        } else {
                            $sql= "SELECT * FROM mots_cles WHERE  id_motcle = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_motcle]);
                        }
                    }
                }
        } else {
            if ($nom <> "") {
                if ($id_livre > 1) {
                        if ($id_artcl > 1) {
                            $sql= "SELECT * FROM mots_cles WHERE nom_motcle LIKE ? AND id_livre = ? AND id_artcl = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute(["%$nom%", $id_livre, $id_artcl]);
                        } else {
                            $sql= "SELECT * FROM mots_cles WHERE nom_motcle LIKE ? AND id_livre = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute(["%$nom%", $id_livre]);
                        }
                    } else {
                        if ($id_artcl > 1) {
                            $sql= "SELECT * FROM mots_cles WHERE nom_motcle LIKE ? AND id_artcl = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute(["%$nom%", $id_artcl]);
                        } else {
                            $sql= "SELECT * FROM mots_cles WHERE nom_motcle LIKE ?";
                            $res= $this->base->prepare($sql);
                            $res->execute(["%$nom%"]);
                        }
                    }
                } else {
                    if ($id_livre > 1) {
                        if ($id_artcl > 1) {
                            $sql= "SELECT * FROM mots_cles WHERE id_livre = ? AND id_artcl = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_livre, $id_artcl]);
                        } else {
                            $sql= "SELECT * FROM mots_cles WHERE id_livre = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_livre]);
                        }
                    } else {
                        if ($id_artcl > 1) {
                            $sql= "SELECT * FROM mots_cles WHERE id_artcl = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_artcl]);
                        } else {
                            $sql= "SELECT * FROM mots_cles";
                            $res= $this->base->prepare($sql);
                            $res->execute();
                        }
                    }
                }

        }


        $rows= $res->fetchAll(PDO::FETCH_OBJ);
        $res->closeCursor();
        $donnees= [];$i=0;

        foreach ($rows as $row) {

            if ($egNUL==1 || ($row->nom_motcle <>"")) { 
                $auteur= new MotCle();           // créer à chaque each un nouvel objet 
                $auteur->setId_motcle($row->id_motcle);   // pour avoir une adresse différente 
                $auteur->setNom_motcle($row->nom_motcle);   // de l'objet créé précédemment.
                $auteur->setId_livre($row->id_livre);   
                $auteur->setId_artcl($row->id_artcl);

                array_push($donnees, $auteur);
                $i++;
            }
        }

        if (!isset($_COOKIE['total'])) {
            setcookie('total',count($donnees));
        }
        $_COOKIE['total']=count($donnees);

        return $donnees;
    }


    public function listePlan($idPlan, $idArtcl, $egNUL) 
    {
        if ($idPlan > 0) {
            $sql= "SELECT * FROM plans WHERE id_plan = ?";
            $res= $this->base->prepare($sql);
            $res->execute([$idPlan]);
        } else {
            if ($idArtcl > 0) {
                $sql= "SELECT * FROM plans WHERE id_artcl = ?";
                $res= $this->base->prepare($sql);
                $res->execute([$idArtcl]);
            } else {
                $sql= "SELECT * FROM plans";
                $res= $this->base->prepare($sql);
                $res->execute();
            }
        }
                
        
 
        $rows= $res->fetchAll(PDO::FETCH_OBJ);
        $res->closeCursor();
        $donnees= [];$i=0;

        foreach ($rows as $row) {

            if ($egNUL==1 || ($row->titre1 <>"" || $row->titre2 <>"" || $row->titre3 <>"")) { 
                $mbr1= new Plan();           // créer à chaque each un nouvel objet 
                $mbr1->setId_plan($row->id_plan);
                $mbr1->setId_artcl($row->id_artcl);
                $mbr1->setTitre1($row->titre1);
                $mbr1->setSs_ttr11($row->ss_ttr11);
                $mbr1->setSs_ttr12($row->ss_ttr12);
                $mbr1->setSs_ttr13($row->ss_ttr13);
                $mbr1->setTitre2($row->titre2);
                $mbr1->setSs_ttr21($row->ss_ttr21);
                $mbr1->setSs_ttr22($row->ss_ttr22);
                $mbr1->setSs_ttr23($row->ss_ttr23);
                $mbr1->setTitre3($row->titre3);
                $mbr1->setSs_ttr31($row->ss_ttr31);
                $mbr1->setSs_ttr32($row->ss_ttr32);
                $mbr1->setSs_ttr33($row->ss_ttr33);
        
                array_push($donnees, $mbr1);
                $i++;
            }
        }

        if (!isset($_COOKIE['total'])) {
            setcookie('total',count($donnees));
        }
        $_COOKIE['total']=count($donnees);


        return $donnees;
    }


    public function listeArticle($id_article, $titre, $theme, $nomOuInstit, $ref_livre, $id_livre, $emplacement, $prenom, $mclsearch, $egNUL) 
    {
        if ($id_article > 1) {
            $sql= "SELECT * FROM articles WHERE id_article = ?";
            $res= $this->base->prepare($sql);
            $res->execute([$id_article]);
        } else {
            if ($theme <> "") {
                if ($nomOuInstit <> "") {
                    if ($id_livre > 1) {
                            if ($emplacement > -1) {
                                $sql= "SELECT * FROM articles WHERE theme_article = ? AND (auteur1_nom LIKE ? OR auteur1_institution LIKE ? OR auteur2_nom LIKE ? OR auteur2_institution LIKE ?) AND id_livre = ? AND emplacement = ?";
                                $res= $this->base->prepare($sql);
                                $res->execute([$theme, "%$nomOuInstit%", "%$nomOuInstit%", "%$nomOuInstit%", "%$nomOuInstit%", $id_livre, $emplacement]);
                            } else {
                                $sql= "SELECT * FROM articles WHERE theme_article = ? AND (auteur1_nom LIKE ? OR auteur1_institution LIKE ? OR auteur2_nom LIKE ? OR auteur2_institution LIKE ?) AND id_livre = ?";
                                $res= $this->base->prepare($sql);
                                $res->execute([$theme, "%$nomOuInstit%", "%$nomOuInstit%", "%$nomOuInstit%", "%$nomOuInstit%", $id_livre]);
                            }
                        } else {
                            if ($emplacement > -1) {
                                $sql= "SELECT * FROM articles WHERE theme_article = ? AND (auteur1_nom LIKE ? OR auteur1_institution LIKE ? OR auteur2_nom LIKE ? OR auteur2_institution LIKE ?) AND emplacement = ?";
                                $res= $this->base->prepare($sql);
                                $res->execute([$theme, "%$nomOuInstit%", "%$nomOuInstit%", "%$nomOuInstit%", "%$nomOuInstit%", $emplacement]);
                            } else {
                                $sql= "SELECT * FROM articles WHERE theme_article = ? AND (auteur1_nom LIKE ? OR auteur1_institution LIKE ? OR auteur2_nom LIKE ? OR auteur2_institution LIKE ?)";
                                $res= $this->base->prepare($sql);
                                $res->execute([$theme, "%$nomOuInstit%", "%$nomOuInstit%", "%$nomOuInstit%", "%$nomOuInstit%"]);
                            }
                        }
                    } else {
                        if ($id_livre > 1) {
                            if ($emplacement > -1) {
                                $sql= "SELECT * FROM articles WHERE theme_article = ? AND id_livre = ? AND emplacement = ?";
                                $res= $this->base->prepare($sql);
                                $res->execute([$theme, $id_livre, $emplacement]);
                            } else {
                                $sql= "SELECT * FROM articles WHERE theme_article = ? AND id_livre = ?";
                                $res= $this->base->prepare($sql);
                                $res->execute([$theme, $id_livre]);
                            }
                        } else {
                            if ($emplacement > -1) {
                                $sql= "SELECT * FROM articles WHERE theme_article = ? AND emplacement = ?";
                                $res= $this->base->prepare($sql);
                                $res->execute([$theme, $emplacement]);
                            } else {
                                $sql= "SELECT * FROM articles WHERE  theme_article = ?";
                                $res= $this->base->prepare($sql);
                                $res->execute([$theme]);
                            }
                        }
                    }
            } else {
                if ($nomOuInstit <> "") {
                    if ($id_livre > 1) {
                            if ($emplacement > -1) {
                                $sql= "SELECT * FROM articles WHERE (auteur1_nom LIKE ? OR auteur1_institution LIKE ? OR auteur2_nom LIKE ? OR auteur2_institution LIKE ?) AND id_livre = ? AND emplacement = ?";
                                $res= $this->base->prepare($sql);
                                $res->execute(["%$nomOuInstit%", "%$nomOuInstit%", "%$nomOuInstit%", "%$nomOuInstit%", $id_livre, $emplacement]);
                            } else {
                                $sql= "SELECT * FROM articles WHERE (auteur1_nom LIKE ? OR auteur1_institution LIKE ? OR auteur2_nom LIKE ? OR auteur2_institution LIKE ?) AND id_livre = ?";
                                $res= $this->base->prepare($sql);
                                $res->execute(["%$nomOuInstit%", "%$nomOuInstit%", "%$nomOuInstit%", "%$nomOuInstit%", $id_livre]);
                            }
                        } else {
                            if ($emplacement > -1) {
                                $sql= "SELECT * FROM articles WHERE (auteur1_nom LIKE ? OR auteur1_institution LIKE ? OR auteur2_nom LIKE ? OR auteur2_institution LIKE ?) AND emplacement = ?";
                                $res= $this->base->prepare($sql);
                                $res->execute(["%$nomOuInstit%", "%$nomOuInstit%", "%$nomOuInstit%", "%$nomOuInstit%", $emplacement]);
                            } else {
                                $sql= "SELECT * FROM articles WHERE auteur1_nom LIKE ? OR auteur1_institution LIKE ? OR auteur2_nom LIKE ? OR auteur2_institution LIKE ?";
                                $res= $this->base->prepare($sql);
                                $res->execute(["%$nomOuInstit%", "%$nomOuInstit%", "%$nomOuInstit%", "%$nomOuInstit%"]);
                            }
                        }
                    } else {
                        if ($id_livre > 1) {
                            if ($emplacement > -1) {
                                $sql= "SELECT * FROM articles WHERE id_livre = ? AND emplacement = ?";
                                $res= $this->base->prepare($sql);
                                $res->execute([$id_livre, $emplacement]);
                            } else {
                                $sql= "SELECT * FROM articles WHERE id_livre = ?";
                                $res= $this->base->prepare($sql);
                                $res->execute([$id_livre]);
                            }
                        } else {
                            if ($emplacement > -1) {
                                $sql= "SELECT * FROM articles WHERE emplacement = ?";
                                $res= $this->base->prepare($sql);
                                $res->execute([$emplacement]);
                            } else {
                                $sql= "SELECT * FROM articles";
                                $res= $this->base->prepare($sql);
                                $res->execute();
                            }
                        }
                    }
    
            }
        }

            
        $rows= $res->fetchAll(PDO::FETCH_OBJ);
        $res->closeCursor();
        $donnees= [];$i=0;

        foreach ($rows as $row) {

            if ($egNUL==1 || ($row->titre <>"")) { 
         
                if (($prenom <> "" && ($row->auteur1_prenom == $prenom || $row->auteur2_prenom == $prenom)) || ($prenom == "")) {
                
                    if (($titre <> "" && (stristr($row->titre, $titre))) || ($titre == "")) {

                        if (($ref_livre <> "" && $row->ref_livre == $ref_livre) || ($ref_livre == "")) {

                            $mbr1= new Article();           // créer à chaque each un nouvel objet 
                            $mbr1->setId_article($row->id_article);
                            $mbr1->setTitre($row->titre);
                            $mbr1->setTheme_article($row->theme_article);
                            $mbr1->setId_livre($row->id_livre);
                            $mbr1->setRef_livre($row->ref_livre);
                            $mbr1->setEmplacement($row->emplacement);
                            $mbr1->setFichier_earticle($row->fichier_earticle);
                            $mbr1->setAuteur1_nom($row->auteur1_nom);
                            $mbr1->setAuteur1_prenom($row->auteur1_prenom);
                            $mbr1->setAuteur1_institution($row->auteur1_institution);
                            $mbr1->setAuteur2_nom($row->auteur2_nom);
                            $mbr1->setAuteur2_prenom($row->auteur2_prenom);
                            $mbr1->setAuteur2_institution($row->auteur2_institution);
                            $mbr1->setNbre_pages($row->nbre_pages);
                            $mbr1->setNbre_mots_pert($row->nbre_mots_pert);
                            $mbr1->setPrix($row->prix);
                            $mbr1->setMonnaie($row->monnaie);
                            $mbr1->setType_support($row->type_support);
                    
                            array_push($donnees, $mbr1);
                            $i++;
                        }
                    }
                }
            }
            
        }

         if ($mclsearch <> "") {
            if (count($donnees)>0) {
                for ($i=0; $i<count($donnees); $i++) {

                    $mcl1= $this->listeMotsCles(0,$mclsearch,$donnees[$i]->getId_livre(), $donnees[$i]->getId_article(), 0);
                    if (count($mcl1)==0) {array_splice($donnees, $i, 1);$i--;} 

                }
            }
        }



        

        if (!isset($_COOKIE['total'])) {
            setcookie('total',count($donnees));
        }
        $_COOKIE['total']=count($donnees);

        return $donnees;
    }


    public function listeCommande($id_commande, $ref_commande, $ref_transaction, $type_client, $id_client, $id_livre, $ref_livre, $id_artcl, $mode_payment, $pays, $date_ordre_livrais, $date_livrais_prev, $date_livrais_real, $titre, $egNUL) 
    {
        if ($id_commande > 0 || $ref_commande <> "" || $ref_transaction <> "") {
            $sql= "SELECT * FROM commandes WHERE id_commande = ? OR ref_commande = ? OR ref_transaction = ?";
            $res= $this->base->prepare($sql);
            $res->execute([$id_commande, $ref_commande, $ref_transaction]);

            } else {
                if ($mode_payment > 0) {
                    if ($type_client > -1) {   //$nomOuInstit <> ""
                        if ($id_livre > 1 || $ref_livre <> "-1" || $id_artcl > 1) {
                                if ($pays <> "") {
                                    $sql= "SELECT * FROM commandes WHERE mode_payment = ? AND type_client = ? AND (ref_livre = ? OR id_livre = ? OR id_artcl =?) AND pays = ?";
                                    $res= $this->base->prepare($sql);
                                    $res->execute([$mode_payment, $type_client, $ref_livre, $id_livre, $id_artcl, $pays]);
                                } else {
                                    $sql= "SELECT * FROM commandes WHERE mode_payment = ? AND type_client = ? AND (ref_livre = ? OR id_livre = ? OR id_artcl =?)";
                                    $res= $this->base->prepare($sql);
                                    $res->execute([$mode_payment, $type_client, $ref_livre, $id_livre, $id_artcl]);
                                }
                            } else {
                                if ($pays <> "") {
                                    $sql= "SELECT * FROM commandes WHERE mode_payment = ? AND type_client = ? AND pays = ?";
                                    $res= $this->base->prepare($sql);
                                    $res->execute([$mode_payment, $type_client, $pays]);
                                } else {
                                    $sql= "SELECT * FROM commandes WHERE mode_payment = ? AND type_client = ?";
                                    $res= $this->base->prepare($sql);
                                    $res->execute([$mode_payment, $type_client]);
                                }
                            }
                        } else {
                            if ($id_livre > 1 || $ref_livre <> "-1" || $id_artcl > 1) {
                                if ($pays <> "") {
                                    $sql= "SELECT * FROM commandes WHERE mode_payment = ? AND (ref_livre = ? OR id_livre = ? OR id_artcl =?) AND pays = ?";
                                    $res= $this->base->prepare($sql);
                                    $res->execute([$mode_payment, $ref_livre, $id_livre, $id_artcl, $pays]);
                                } else {
                                    $sql= "SELECT * FROM commandes WHERE mode_payment = ? AND (ref_livre = ? OR id_livre = ? OR id_artcl =?)";
                                    $res= $this->base->prepare($sql);
                                    $res->execute([$mode_payment, $ref_livre, $id_livre, $id_artcl]);
                                }
                            } else {
                                if ($pays <> "") {
                                    $sql= "SELECT * FROM commandes WHERE mode_payment = ? AND pays = ?";
                                    $res= $this->base->prepare($sql);
                                    $res->execute([$mode_payment, $pays]);
                                } else {
                                    $sql= "SELECT * FROM commandes WHERE  mode_payment = ?";
                                    $res= $this->base->prepare($sql);
                                    $res->execute([$mode_payment]);
                                }
                            }
                        }
                } else {
                    if ($type_client > -1) {
                        if ($id_livre > 1 || $ref_livre <> "-1" || $id_artcl > 1) {
                                if ($pays <> "") {
                                    $sql= "SELECT * FROM commandes WHERE type_client = ? AND (ref_livre = ? OR id_livre = ? OR id_artcl =?) AND pays = ?";
                                    $res= $this->base->prepare($sql);
                                    $res->execute([$type_client, $ref_livre, $id_livre, $id_artcl, $pays]);
                                } else {
                                    $sql= "SELECT * FROM commandes WHERE type_client = ? AND (ref_livre = ? OR id_livre = ? OR id_artcl =?)";
                                    $res= $this->base->prepare($sql);
                                    $res->execute([$type_client, $ref_livre, $id_livre, $id_artcl]);
                                }
                            } else {
                                if ($pays <> "") {
                                    $sql= "SELECT * FROM commandes WHERE type_client = ? AND pays = ?";
                                    $res= $this->base->prepare($sql);
                                    $res->execute([$type_client, $pays]);
                                } else {
                                    $sql= "SELECT * FROM commandes WHERE type_client = ?";
                                    $res= $this->base->prepare($sql);
                                    $res->execute([$type_client]);
                                }
                            }
                        } else {
                            if ($id_livre > 1 || $ref_livre <> "-1" || $id_artcl > 1) {
                                if ($pays <> "") {
                                    $sql= "SELECT * FROM commandes WHERE (ref_livre = ? OR id_livre = ? OR id_artcl =?) AND pays = ?";
                                    $res= $this->base->prepare($sql);
                                    $res->execute([$ref_livre, $id_livre, $id_artcl, $pays]);
                                } else {
                                    $sql= "SELECT * FROM commandes WHERE (ref_livre = ? OR id_livre = ? OR id_artcl =?)";
                                    $res= $this->base->prepare($sql);
                                    $res->execute([$ref_livre, $id_livre, $id_artcl]);
                                }
                            } else {
                                if ($pays <> "") {
                                    $sql= "SELECT * FROM commandes WHERE pays = ?";
                                    $res= $this->base->prepare($sql);
                                    $res->execute([$pays]);
                                } else {
                                    $sql= "SELECT * FROM commandes";
                                    $res= $this->base->prepare($sql);
                                    $res->execute();
                                }
                            }
                        }
        
                }
            }
    
            
        $rows= $res->fetchAll(PDO::FETCH_OBJ);
        $res->closeCursor();
        $donnees= [];$i=0;

        foreach ($rows as $row) {

            if ($egNUL==1 || ($row->ref_commande <>"" || $row->ttr_livre <>"" || $row->ttr_artcl <>"")) { 
                if (($titre <> "" && (stristr($row->ttr_livre, $titre) || stristr($row->ttr_artcl, $titre))) || ($titre == "")) {
                    if (($id_client > 0 && $row->id_client == $id_client) || $id_client == 0) {

                        $mbr1= new Commande();           // créer à chaque each un nouvel objet 
                        $mbr1->setId_commande($row->id_commande);
                        $mbr1->setRef_commande($row->ref_commande);
                        $mbr1->setType_client($row->type_client);
                        $mbr1->setId_client($row->id_client);
                        $mbr1->setId_livre($row->id_livre);
                        $mbr1->setRef_livre($row->ref_livre);
                        $mbr1->setTtr_livre($row->ttr_livre);
                        $mbr1->setId_artcl($row->id_artcl);
                        $mbr1->setTtr_artcl($row->ttr_artcl);
                        $mbr1->setNombre($row->nombre);
                        $mbr1->setPx_ht($row->px_ht);
                        $mbr1->setTva_percent($row->tva_percent);
                        $mbr1->setPx_ttc($row->px_ttc);
                        $mbr1->setFrs_livrais($row->frs_livrais);
                        $mbr1->setMonnaie($row->monnaie);
                        $mbr1->setMode_payment($row->mode_payment);
                        $mbr1->setRef_transaction($row->ref_transaction);
                        $mbr1->setDate_ordre_livrais($row->date_ordre_livrais);
                        $mbr1->setNom_destin($row->nom_destin);
                        $mbr1->setPrenom_destin($row->prenom_destin);
                        $mbr1->setInstitution_destin($row->institution_destin);
                        $mbr1->setAdr_livrais($row->adr_livrais);
                        $mbr1->setCode_postal($row->code_postal);
                        $mbr1->setVille($row->ville);
                        $mbr1->setPays($row->pays);
                        $mbr1->setTelephone($row->telephone);
                        $mbr1->setMode_livrais($row->mode_livrais);
                        $mbr1->setMsg_livrais($row->msg_livrais);
                        $mbr1->setDate_livrais_prev($row->date_livrais_prev);
                        $mbr1->setDate_livrais_real($row->date_livrais_real);
                
                        array_push($donnees, $mbr1);
                        $i++;
                    }
                }
            }
           
        }


        if (!isset($_COOKIE['total'])) {
            setcookie('total',count($donnees));
        }
        $_COOKIE['total']=count($donnees);

        return $donnees;
    }


    public function listePanierArt($id_panier, $id_client, $type_client, $id_livre1, $ref_livre1, $id_artcl1, $id_livre2, $ref_livre2, $id_artcl2, $id_livre3, $ref_livre3, $id_artcl3, $titre, $egNUL) 
    {


        if ($id_panier > 0) {
            $sql= "SELECT * FROM paniers WHERE id_panier = ?";
            $res= $this->base->prepare($sql);
            $res->execute([$id_panier]);
        } else {
            if ($id_client > 0) {
                if ($type_client > -1) {
                    if ($id_livre1 > 1 || $ref_livre1 <> "-1" || $id_livre2 > 1 || $ref_livre2 <> "-1" || $id_livre3 > 1 || $ref_livre3 <> "-1") {
                            $sql= "SELECT * FROM paniers WHERE id_client = ? AND type_client = ? AND (id_livre1 = ? OR ref_livre1 = ? OR id_livre2 = ? OR ref_livre2 = ? OR id_livre3 = ? OR ref_livre3 = ?)";
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_client, $type_client, $id_livre1, $ref_livre1, $id_livre2, $ref_livre2, $id_livre3, $ref_livre3]);
                        } else {
                            $sql= "SELECT * FROM paniers WHERE id_client = ? AND type_client = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_client, $type_client]);
                        }
                } else { 
                    if ($id_livre1 > 1 || $ref_livre1 <> "-1" || $id_artcl1 > 0 || $id_livre2 > 1 || $ref_livre2 <> "-1" || $id_artcl2 > 0 || $id_livre3 > 1 || $ref_livre3 <> "-1" || $id_artcl3 > 0) {
                        $sql= "SELECT * FROM paniers WHERE id_client = ? AND id_livre1 = ? OR ref_livre1 = ? OR id_livre2 = ? OR ref_livre2 = ? OR id_livre3 = ? OR ref_livre3 = ?";
                        $res= $this->base->prepare($sql);
                        $res->execute([$id_client, $id_livre1, $ref_livre1, $id_livre2, $ref_livre2, $id_livre3, $ref_livre3]);
                    } else {
                            $sql= "SELECT * FROM paniers WHERE id_client = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_client]);
                    }
                }
            } else {
                if ($type_client > -1) {
                    if ($id_livre1 > 1 || $ref_livre1 <> "-1" || $id_livre2 > 1 || $ref_livre2 <> "-1" || $id_livre3 > 1 || $ref_livre3 <> "-1") {
                            $sql= "SELECT * FROM paniers WHERE type_client = ? AND (id_livre1 = ? OR ref_livre1 = ? OR id_livre2 = ? OR ref_livre2 = ? OR id_livre3 = ? OR ref_livre3 = ?)";
                            $res= $this->base->prepare($sql);
                            $res->execute([$type_client, $id_livre1, $ref_livre1, $id_livre2, $ref_livre2, $id_livre3, $ref_livre3]);
                        } else {
                                $sql= "SELECT * FROM paniers WHERE type_client = ?";
                                $res= $this->base->prepare($sql);
                                $res->execute([$type_client]);
                        }
                } else { 
                    if ($id_livre1 > 1 || $ref_livre1 <> "-1" || $id_livre2 > 1 || $ref_livre2 <> "-1" || $id_livre3 > 1 || $ref_livre3 <> "-1") {
                        $sql= "SELECT * FROM paniers WHERE id_livre1 = ? OR ref_livre1 = ? OR id_livre2 = ? OR ref_livre2 = ? OR id_livre3 = ? OR ref_livre3 = ?";
                        $res= $this->base->prepare($sql);
                        $res->execute([$id_livre1, $ref_livre1, $id_livre2, $ref_livre2, $id_livre3, $ref_livre3]);
                    } else {
                            $sql= "SELECT * FROM paniers";
                            $res= $this->base->prepare($sql);
                            $res->execute();
                    }
                }
            }
        }
    
            
        $rows= $res->fetchAll(PDO::FETCH_OBJ);
        $res->closeCursor();
        $donnees= [];$i=0;

        foreach ($rows as $row) {
            
            if ($egNUL==1 || ($row->ref_livre1 <>"" || $row->ttr_livre1 <>"" || $row->ttr_artcl1 <>"")) { 
                if (($titre <> "" && (stristr($row->ttr_livre1, $titre) || stristr($row->ttr_artcl1, $titre) || 
                                      stristr($row->ttr_livre2, $titre) || stristr($row->ttr_artcl2, $titre) ||
                                      stristr($row->ttr_livre3, $titre) || stristr($row->ttr_artcl3, $titre))) ||
                                       ($titre == "")) {

                    if (($id_artcl1 > 0 && $row->id_artcl1 == $id_artcl1) || ($id_artcl2 > 0 && $row->id_artcl2 == $id_artcl2) || ($id_artcl3 > 0 && $row->id_artcl3 == $id_artcl3) || ($id_artcl1 < 1 && $id_artcl2 < 1 && $id_artcl3 < 1)) { 

                        $mbr1= new Panier();           // créer à chaque each un nouvel objet 
                        $mbr1->setId_panier($row->id_panier);
                        $mbr1->setDate_created($row->date_created);
                        $mbr1->setType_client($row->type_client);
                        $mbr1->setId_client($row->id_client);
                        $mbr1->setId_livre1($row->id_livre1);
                        $mbr1->setRef_livre1($row->ref_livre1);
                        $mbr1->setTtr_livre1($row->ttr_livre1);
                        $mbr1->setImg_livre1($row->img_livre1);
                        $mbr1->setPxttc_livre1($row->pxttc_livre1);
                        $mbr1->setId_artcl1($row->id_artcl1);
                        $mbr1->setTtr_artcl1($row->ttr_artcl1);
                        $mbr1->setPxttc_artcl1($row->pxttc_artcl1);
                        $mbr1->setNbre1($row->nbre1);
                        $mbr1->setFrs_livrais1($row->frs_livrais1);
                        $mbr1->setMonnaie($row->monnaie);
                        $mbr1->setId_livre2($row->id_livre2);
                        $mbr1->setRef_livre2($row->ref_livre2);
                        $mbr1->setTtr_livre2($row->ttr_livre2);
                        $mbr1->setImg_livre2($row->img_livre2);
                        $mbr1->setPxttc_livre2($row->pxttc_livre2);
                        $mbr1->setId_artcl2($row->id_artcl2);
                        $mbr1->setTtr_artcl2($row->ttr_artcl2);
                        $mbr1->setPxttc_artcl2($row->pxttc_artcl2);
                        $mbr1->setNbre2($row->nbre2);
                        $mbr1->setFrs_livrais2($row->frs_livrais2);
                        $mbr1->setId_livre3($row->id_livre3);
                        $mbr1->setRef_livre3($row->ref_livre3);
                        $mbr1->setTtr_livre3($row->ttr_livre3);
                        $mbr1->setImg_livre3($row->img_livre3);
                        $mbr1->setPxttc_livre3($row->pxttc_livre3);
                        $mbr1->setId_artcl3($row->id_artcl3);
                        $mbr1->setTtr_artcl3($row->ttr_artcl3);
                        $mbr1->setPxttc_artcl3($row->pxttc_artcl3);
                        $mbr1->setNbre3($row->nbre3);
                        $mbr1->setFrs_livrais3($row->frs_livrais3);
                
                        array_push($donnees, $mbr1);
                        $i++;
                    }
                }
            }
            
        }

        if (!isset($_COOKIE['total'])) {
            setcookie('total',count($donnees));
        }
        $_COOKIE['total']=count($donnees);

        return $donnees;
    }



    public function listePanier($id_panier, $id_client, $type_client, $id_livre1, $ref_livre1, $id_artcl1, $id_livre2, $ref_livre2, $id_artcl2, $id_livre3, $ref_livre3, $id_artcl3, $titre, $egNUL) 
    {
        if ($id_panier > 0) {
            $sql= "SELECT * FROM paniers WHERE id_panier = ?";
            $res= $this->base->prepare($sql);
            $res->execute([$id_panier]);
        } else {
            if ($id_client > 0) {
                if ($type_client > -1) {
                    if ($id_livre1 > 1 || $ref_livre1 <> "-1" || $id_livre2 > 1 || $ref_livre2 <> "-1" || $id_livre3 > 1 || $ref_livre3 <> "-1") {
                            $sql= "SELECT * FROM paniers WHERE id_client = ? AND type_client = ? AND (id_livre1 = ? OR ref_livre1 = ? OR id_livre2 = ? OR ref_livre2 = ? OR id_livre3 = ? OR ref_livre3 = ?)";
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_client, $type_client, $id_livre1, $ref_livre1, $id_livre2, $ref_livre2, $id_livre3, $ref_livre3]);
                        } else {
                            $sql= "SELECT * FROM paniers WHERE id_client = ? AND type_client = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_client, $type_client]);
                        }
                } else { 
                    if ($id_livre1 > 1 || $ref_livre1 <> "-1" || $id_artcl1 > 0 || $id_livre2 > 1 || $ref_livre2 <> "-1" || $id_artcl2 > 0 || $id_livre3 > 1 || $ref_livre3 <> "-1" || $id_artcl3 > 0) {
                        $sql= "SELECT * FROM paniers WHERE id_client = ? AND id_livre1 = ? OR ref_livre1 = ? OR id_livre2 = ? OR ref_livre2 = ? OR id_livre3 = ? OR ref_livre3 = ?";
                        $res= $this->base->prepare($sql);
                        $res->execute([$id_client, $id_livre1, $ref_livre1, $id_livre2, $ref_livre2, $id_livre3, $ref_livre3]);
                    } else {
                            $sql= "SELECT * FROM paniers WHERE id_client = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_client]);
                    }
                }
            } else {
                if ($type_client > -1) {
                    if ($id_livre1 > 1 || $ref_livre1 <> "-1" || $id_livre2 > 1 || $ref_livre2 <> "-1" || $id_livre3 > 1 || $ref_livre3 <> "-1") {
                            $sql= "SELECT * FROM paniers WHERE type_client = ? AND (id_livre1 = ? OR ref_livre1 = ? OR id_livre2 = ? OR ref_livre2 = ? OR id_livre3 = ? OR ref_livre3 = ?)";
                            $res= $this->base->prepare($sql);
                            $res->execute([$type_client, $id_livre1, $ref_livre1, $id_livre2, $ref_livre2, $id_livre3, $ref_livre3]);
                        } else {
                                $sql= "SELECT * FROM paniers WHERE type_client = ?";
                                $res= $this->base->prepare($sql);
                                $res->execute([$type_client]);
                        }
                } else { 
                    if ($id_livre1 > 1 || $ref_livre1 <> "-1" || $id_livre2 > 1 || $ref_livre2 <> "-1" || $id_livre3 > 1 || $ref_livre3 <> "-1") {
                        $sql= "SELECT * FROM paniers WHERE id_livre1 = ? OR ref_livre1 = ? OR id_livre2 = ? OR ref_livre2 = ? OR id_livre3 = ? OR ref_livre3 = ?";
                        $res= $this->base->prepare($sql);
                        $res->execute([$id_livre1, $ref_livre1, $id_livre2, $ref_livre2, $id_livre3, $ref_livre3]);
                    } else {
                            $sql= "SELECT * FROM paniers";
                            $res= $this->base->prepare($sql);
                            $res->execute();
                    }
                }
            }
        }
    
            
        $rows= $res->fetchAll(PDO::FETCH_OBJ);
        $res->closeCursor();
        $donnees= [];$i=0;

        foreach ($rows as $row) {
           
            if ($egNUL==1 || ($row->ref_livre1 <>"" || $row->ttr_livre1 <>"" || $row->ttr_artcl1 <>"")) { 
                if (($titre <> "" && (stristr($row->ttr_livre1, $titre) || stristr($row->ttr_artcl1, $titre) || 
                                      stristr($row->ttr_livre2, $titre) || stristr($row->ttr_artcl2, $titre) ||
                                      stristr($row->ttr_livre3, $titre) || stristr($row->ttr_artcl3, $titre))) ||
                                       ($titre == "")) {

                    if (($id_artcl1 > 0 && $row->id_artcl1 == $id_artcl1) || ($id_artcl2 > 0 && $row->id_artcl2 == $id_artcl2) || ($id_artcl3 > 0 && $row->id_artcl3 == $id_artcl3) || ($id_artcl1 < 1 && $id_artcl2 < 1 && $id_artcl3 < 1)) { 

                        $mbr1= new Panier();           // créer à chaque each un nouvel objet 
                        $mbr1->setId_panier($row->id_panier);
                        $mbr1->setDate_created($row->date_created);
                        $mbr1->setType_client($row->type_client);
                        $mbr1->setId_client($row->id_client);
                        $mbr1->setId_livre1($row->id_livre1);
                        $mbr1->setRef_livre1($row->ref_livre1);
                        $mbr1->setTtr_livre1($row->ttr_livre1);
                        $mbr1->setImg_livre1($row->img_livre1);
                        $mbr1->setPxttc_livre1($row->pxttc_livre1);
                        $mbr1->setId_artcl1($row->id_artcl1);
                        $mbr1->setTtr_artcl1($row->ttr_artcl1);
                        $mbr1->setPxttc_artcl1($row->pxttc_artcl1);
                        $mbr1->setNbre1($row->nbre1);
                        $mbr1->setFrs_livrais1($row->frs_livrais1);
                        $mbr1->setMonnaie($row->monnaie);
                        $mbr1->setId_livre2($row->id_livre2);
                        $mbr1->setRef_livre2($row->ref_livre2);
                        $mbr1->setTtr_livre2($row->ttr_livre2);
                        $mbr1->setImg_livre2($row->img_livre2);
                        $mbr1->setPxttc_livre2($row->pxttc_livre2);
                        $mbr1->setId_artcl2($row->id_artcl2);
                        $mbr1->setTtr_artcl2($row->ttr_artcl2);
                        $mbr1->setPxttc_artcl2($row->pxttc_artcl2);
                        $mbr1->setNbre2($row->nbre2);
                        $mbr1->setFrs_livrais2($row->frs_livrais2);
                        $mbr1->setId_livre3($row->id_livre3);
                        $mbr1->setRef_livre3($row->ref_livre3);
                        $mbr1->setTtr_livre3($row->ttr_livre3);
                        $mbr1->setImg_livre3($row->img_livre3);
                        $mbr1->setPxttc_livre3($row->pxttc_livre3);
                        $mbr1->setId_artcl3($row->id_artcl3);
                        $mbr1->setTtr_artcl3($row->ttr_artcl3);
                        $mbr1->setPxttc_artcl3($row->pxttc_artcl3);
                        $mbr1->setNbre3($row->nbre3);
                        $mbr1->setFrs_livrais3($row->frs_livrais3);
                
                        array_push($donnees, $mbr1);
                        $i++;
                    }
                }
            }
            
        }

        if (!isset($_COOKIE['total'])) {
            setcookie('total',count($donnees));
        }
        $_COOKIE['total']=count($donnees);

        return $donnees;
    }


    public function listePanierLvr($id_panier, $id_client, $type_client, $id_livre, $id_artcl, $pos) 
    {
        if ($id_panier > 0) {
            $sql= "SELECT * FROM paniers WHERE id_panier = ?";
            $res= $this->base->prepare($sql);
            $res->execute([$id_panier]);
        } else {
            if ($id_client > 0) {
                if ($type_client > -1) {
                    if ($id_livre > 0) {
                        if ($id_artcl > 0) {
                            if ($pos == 1) {$sql= "SELECT * FROM paniers WHERE id_client = ? AND type_client = ? AND id_livre1 = ? AND id_artcl1 = ?";}
                            if ($pos == 2) {$sql= "SELECT * FROM paniers WHERE id_client = ? AND type_client = ? AND id_livre2 = ? AND id_artcl2 = ?";}
                            if ($pos == 3) {$sql= "SELECT * FROM paniers WHERE id_client = ? AND type_client = ? AND id_livre3 = ? AND id_artcl3 = ?";}
                            
                            
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_client, $type_client, $id_livre, $id_artcl]);
                        } else {
                            if ($pos == 1) {$sql= "SELECT * FROM paniers WHERE id_client = ? AND type_client = ? AND id_livre1 = ?";}
                            if ($pos == 2) {$sql= "SELECT * FROM paniers WHERE id_client = ? AND type_client = ? AND id_livre2 = ?";}
                            if ($pos == 3) {$sql= "SELECT * FROM paniers WHERE id_client = ? AND type_client = ? AND id_livre3 = ?";}
                            
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_client, $type_client, $id_livre]);
                        }
                    } else { 
                        if ($id_artcl > 0) {
                            if ($pos == 1) {$sql= "SELECT * FROM paniers WHERE id_client = ? AND type_client = ? AND id_artcl1 = ?";}
                            if ($pos == 2) {$sql= "SELECT * FROM paniers WHERE id_client = ? AND type_client = ? AND id_artcl2 = ?";}
                            if ($pos == 3) {$sql= "SELECT * FROM paniers WHERE id_client = ? AND type_client = ? AND id_artcl3 = ?";}
                            
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_client, $type_client, $id_artcl]);
                        } else {
                            $sql= "SELECT * FROM paniers WHERE id_client = ? AND type_client = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_client, $type_client]);
                        }
                    } 
                
                
                } else {
                        if ($id_livre > 0) {
                            if ($id_artcl > 0) {
                                if ($pos == 1) {$sql= "SELECT * FROM paniers WHERE id_client = ? AND id_livre1 = ? AND id_artcl1 = ?";}
                                if ($pos == 2) {$sql= "SELECT * FROM paniers WHERE id_client = ? AND id_livre2 = ? AND id_artcl2 = ?";}
                                if ($pos == 3) {$sql= "SELECT * FROM paniers WHERE id_client = ? AND id_livre3 = ? AND id_artcl3 = ?";}
                                
                                $res= $this->base->prepare($sql);
                                $res->execute([$id_client, $id_livre, $id_artcl]);
                            } else {
                                if ($pos == 1) {$sql= "SELECT * FROM paniers WHERE id_client = ? AND id_livre1 = ?";}
                                if ($pos == 2) {$sql= "SELECT * FROM paniers WHERE id_client = ? AND id_livre2 = ?";}
                                if ($pos == 3) {$sql= "SELECT * FROM paniers WHERE id_client = ? AND id_livre3 = ?";}
                                
                                $res= $this->base->prepare($sql);
                                $res->execute([$id_client, $id_livre]);
                            }
                    } else { 
                        if ($id_artcl > 0) {
                            if ($pos == 1) {$sql= "SELECT * FROM paniers WHERE id_client = ? AND id_artcl1 = ?";}
                            if ($pos == 2) {$sql= "SELECT * FROM paniers WHERE id_client = ? AND id_artcl2 = ?";}
                            if ($pos == 3) {$sql= "SELECT * FROM paniers WHERE id_client = ? AND id_artcl3 = ?";}
                            
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_client, $id_artcl]);
                        } else {
                            $sql= "SELECT * FROM paniers WHERE id_client = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_client]);
                        }
                    }

                }


            } else {
                if ($type_client > -1) {
                    if ($id_livre > 0) {
                        if ($id_artcl > 0) {
                            if ($pos == 1) {$sql= "SELECT * FROM paniers WHERE type_client = ? AND id_livre1 = ? AND id_artcl1 = ?";}
                            if ($pos == 2) {$sql= "SELECT * FROM paniers WHERE type_client = ? AND id_livre2 = ? AND id_artcl2 = ?";}
                            if ($pos == 3) {$sql= "SELECT * FROM paniers WHERE type_client = ? AND id_livre3 = ? AND id_artcl3 = ?";}
                            
                            $res= $this->base->prepare($sql);
                            $res->execute([$type_client, $id_livre, $id_artcl]);
                        } else {
                            if ($pos == 1) {$sql= "SELECT * FROM paniers WHERE type_client = ? AND id_livre1 = ?";}
                            if ($pos == 2) {$sql= "SELECT * FROM paniers WHERE type_client = ? AND id_livre2 = ?";}
                            if ($pos == 3) {$sql= "SELECT * FROM paniers WHERE type_client = ? AND id_livre3 = ?";}
                            
                            $res= $this->base->prepare($sql);
                            $res->execute([$type_client, $id_livre]);
                        }
                    } else { 
                        if ($id_artcl > 0) {
                            if ($pos == 1) {$sql= "SELECT * FROM paniers WHERE type_client = ? AND id_artcl1 = ?";}
                            if ($pos == 2) {$sql= "SELECT * FROM paniers WHERE type_client = ? AND id_artcl2 = ?";}
                            if ($pos == 3) {$sql= "SELECT * FROM paniers WHERE type_client = ? AND id_artcl3 = ?";}
                            
                            $res= $this->base->prepare($sql);
                            $res->execute([$type_client, $id_artcl]);
                        } else {
                            $sql= "SELECT * FROM paniers WHERE type_client = ?";
                            $res= $this->base->prepare($sql);
                            $res->execute([$type_client]);
                        }
                    } 
                
                
                } else {
                        if ($id_livre > 0) {
                            if ($id_artcl > 0) {
                                if ($pos == 1) {$sql= "SELECT * FROM paniers WHERE id_livre1 = ? AND id_artcl1 = ?";}
                                if ($pos == 2) {$sql= "SELECT * FROM paniers WHERE id_livre2 = ? AND id_artcl2 = ?";}
                                if ($pos == 3) {$sql= "SELECT * FROM paniers WHERE id_livre3 = ? AND id_artcl3 = ?";}
                                
                                $res= $this->base->prepare($sql);
                                $res->execute([$id_livre, $id_artcl]);
                            } else {
                                if ($pos == 1) {$sql= "SELECT * FROM paniers WHERE id_livre1 = ?";}
                                if ($pos == 2) {$sql= "SELECT * FROM paniers WHERE id_livre2 = ?";}
                                if ($pos == 3) {$sql= "SELECT * FROM paniers WHERE id_livre3 = ?";}
                                
                                $res= $this->base->prepare($sql);
                                $res->execute([$id_livre]);
                            }
                    } else { 
                        if ($id_artcl > 0) {
                            if ($pos == 1) {$sql= "SELECT * FROM paniers WHERE id_artcl1 = ?";}
                            if ($pos == 2) {$sql= "SELECT * FROM paniers WHERE id_artcl2 = ?";}
                            if ($pos == 3) {$sql= "SELECT * FROM paniers WHERE id_artcl3 = ?";}
                            
                            $res= $this->base->prepare($sql);
                            $res->execute([$id_artcl]);
                        } else {
                            $sql= "SELECT * FROM paniers";
                            $res= $this->base->prepare($sql);
                            $res->execute();
                        }
                    }

                }
            }
        }
                            
        $rows= $res->fetchAll(PDO::FETCH_OBJ);
        $res->closeCursor();
        $donnees= [];$i=0;

        foreach ($rows as $row) {

                        $mbr1= new Panier();           // créer à chaque each un nouvel objet 
                        $mbr1->setId_panier($row->id_panier);
                        $mbr1->setDate_created($row->date_created);
                        $mbr1->setType_client($row->type_client);
                        $mbr1->setId_client($row->id_client);
                        $mbr1->setId_livre1($row->id_livre1);
                        $mbr1->setRef_livre1($row->ref_livre1);
                        $mbr1->setTtr_livre1($row->ttr_livre1);
                        $mbr1->setImg_livre1($row->img_livre1);
                        $mbr1->setPxttc_livre1($row->pxttc_livre1);
                        $mbr1->setId_artcl1($row->id_artcl1);
                        $mbr1->setTtr_artcl1($row->ttr_artcl1);
                        $mbr1->setPxttc_artcl1($row->pxttc_artcl1);
                        $mbr1->setNbre1($row->nbre1);
                        $mbr1->setFrs_livrais1($row->frs_livrais1);
                        $mbr1->setMonnaie($row->monnaie);
                        $mbr1->setId_livre2($row->id_livre2);
                        $mbr1->setRef_livre2($row->ref_livre2);
                        $mbr1->setTtr_livre2($row->ttr_livre2);
                        $mbr1->setImg_livre2($row->img_livre2);
                        $mbr1->setPxttc_livre2($row->pxttc_livre2);
                        $mbr1->setId_artcl2($row->id_artcl2);
                        $mbr1->setTtr_artcl2($row->ttr_artcl2);
                        $mbr1->setPxttc_artcl2($row->pxttc_artcl2);
                        $mbr1->setNbre2($row->nbre2);
                        $mbr1->setFrs_livrais2($row->frs_livrais2);
                        $mbr1->setId_livre3($row->id_livre3);
                        $mbr1->setRef_livre3($row->ref_livre3);
                        $mbr1->setTtr_livre3($row->ttr_livre3);
                        $mbr1->setImg_livre3($row->img_livre3);
                        $mbr1->setPxttc_livre3($row->pxttc_livre3);
                        $mbr1->setId_artcl3($row->id_artcl3);
                        $mbr1->setTtr_artcl3($row->ttr_artcl3);
                        $mbr1->setPxttc_artcl3($row->pxttc_artcl3);
                        $mbr1->setNbre3($row->nbre3);
                        $mbr1->setFrs_livrais3($row->frs_livrais3);
                
                        array_push($donnees, $mbr1);
                        $i++;
            
        }
     
        if (!isset($_COOKIE['total'])) {
            setcookie('total',count($donnees));
        }
        $_COOKIE['total']=count($donnees);

        return $donnees;
    }



    public function listeEditeur($id_editeur, $code_editeur, $nom_editeur, $pays_localite_editeur, $egNUL) 
    {
        if ($id_editeur > 0 || $code_editeur <> "") {
            if ($pays_localite_editeur <> "") {
                if ($nom_editeur == "") {
                    $sql= "SELECT * FROM editeurs WHERE (id_editeur = ? OR code_editeur = ?) AND (pays_editeur LIKE ? OR localite_editeur LIKE ?)";
                    $res= $this->base->prepare($sql);
                    $res->execute([$id_editeur, $code_editeur, "%$pays_localite_editeur%", "%$pays_localite_editeur%"]);
                } else {
                    $sql= "SELECT * FROM editeurs WHERE (id_editeur = ? OR code_editeur = ?) AND nom_editeur LIKE ? AND (pays_editeur LIKE ? OR localite_editeur LIKE ?)";
                    $res= $this->base->prepare($sql);
                    $res->execute([$id_editeur, $code_editeur, "%$nom_editeur%", "%$pays_localite_editeur%", "%$pays_localite_editeur%"]);
                }
            } else {
                if ($nom_editeur == "") {
                    $sql= "SELECT * FROM editeurs WHERE id_editeur = ? OR code_editeur = ?";
                    $res= $this->base->prepare($sql);
                    $res->execute([$id_editeur, $code_editeur]);
                } else {
                    $sql= "SELECT * FROM editeurs WHERE id_editeur = ? OR code_editeur = ? AND nom_editeur LIKE ?";
                    $res= $this->base->prepare($sql);
                    $res->execute([$id_editeur, $code_editeur, "%$nom_editeur%"]);
                }    
            }
        } else {
            if ($pays_localite_editeur <> "") {
                if ($nom_editeur == "") {
                    $sql= "SELECT * FROM editeurs WHERE (pays_editeur LIKE ? OR localite_editeur LIKE ?)";
                    $res= $this->base->prepare($sql);
                    $res->execute(["%$pays_localite_editeur%", "%$pays_localite_editeur%"]);
                } else {
                    $sql= "SELECT * FROM editeurs WHERE (nom_editeur LIKE ?) AND (pays_editeur LIKE ? OR localite_editeur LIKE ?)";
                    $res= $this->base->prepare($sql);
                    $res->execute(["%$nom_editeur%", "%$pays_localite_editeur%", "%$pays_localite_editeur%"]);
                }
            } else {
                if ($nom_editeur == "") {
                    $sql= "SELECT * FROM editeurs";
                    $res= $this->base->prepare($sql);
                    $res->execute();
                } else {
                    $sql= "SELECT * FROM editeurs WHERE nom_editeur LIKE ?";
                    $res= $this->base->prepare($sql);
                    $res->execute(["%$nom_editeur%"]);
                }    
            }
        }
    
            
        $rows= $res->fetchAll(PDO::FETCH_OBJ);
        $res->closeCursor();
        $donnees= [];$i=0;

        foreach ($rows as $row) {


            if ($egNUL==1 || ($row->code_editeur <>"" || $row->nom_editeur <>"")) { 

                $mbr1= new Editeur();           // créer à chaque each un nouvel objet 
            
                $mbr1->setId_editeur($row->id_editeur);
                $mbr1->setCode_editeur($row->code_editeur);
                $mbr1->setNom_editeur($row->nom_editeur);
                $mbr1->setLocalite_editeur($row->localite_editeur);
                $mbr1->setPays_editeur($row->pays_editeur);
                $mbr1->setFrs_livrais($row->frs_livrais);
        
                array_push($donnees, $mbr1);
                $i++;
            }
            
        }


        if (!isset($_COOKIE['total'])) {
            setcookie('total',count($donnees));
        }
        $_COOKIE['total']=count($donnees);

        return $donnees;
    }



    public function listeTheme($id_theme, $nom_theme, $egNUL) 
    {
        if ($id_theme > 0) {
            if ($nom_theme <> "") {
                $sql= "SELECT * FROM themes WHERE id_theme = ? AND nom_theme LIKE ?";
                $res= $this->base->prepare($sql);
                $res->execute([$id_theme, "%$nom_theme%"]);
            } else {
                $sql= "SELECT * FROM themes WHERE  id_theme = ?";
                $res= $this->base->prepare($sql);
                $res->execute([$id_theme]);
            }
        } else {
            if ($nom_theme <> "") {
                $sql= "SELECT * FROM themes WHERE nom_theme LIKE ?";
                $res= $this->base->prepare($sql);
                $res->execute(["%$nom_theme%"]);
            } else {
                $sql= "SELECT * FROM themes";
                $res= $this->base->prepare($sql);
                $res->execute();
            }
        }

            
        $rows= $res->fetchAll(PDO::FETCH_OBJ);
        $res->closeCursor();
        $donnees= [];$i=0;

        foreach ($rows as $row) {

            if ($egNUL==1 || ($row->nom_theme <>"")) { 
            
                $mbr1= new Theme();           // créer à chaque each un nouvel objet 
                $mbr1->setId_theme($row->id_theme);
                $mbr1->setNom_theme($row->nom_theme);
            
                array_push($donnees, $mbr1);
                $i++;
            }
            
        }

        if (!isset($_COOKIE['total'])) {
            setcookie('total',count($donnees));
        }
        $_COOKIE['total']=count($donnees);

        return $donnees;
    }



    public function listeSpecifRevue($id_specif_revue, $nom_specif_revue, $periodicite, $egNUL) 
    {
        if ($id_specif_revue > 0 || $nom_specif_revue <> "") {
            if ($id_specif_revue > 0) {
                if ($nom_specif_revue <> "") {
                    $sql= "SELECT * FROM specif_revues WHERE id_specif_revue = ? AND nom_specif_revue LIKE ?";
                    $res= $this->base->prepare($sql);
                    $res->execute([$id_specif_revue, "%$nom_specif_revue%"]);
                } else {
                    $sql= "SELECT * FROM specif_revues WHERE  id_specif_revue = ?";
                    $res= $this->base->prepare($sql);
                    $res->execute([$id_specif_revue]);
                }
            } else {
                if ($nom_specif_revue <> "") {
                    $sql= "SELECT * FROM specif_revues WHERE nom_specif_revue LIKE ?";
                    $res= $this->base->prepare($sql);
                    $res->execute(["%$nom_specif_revue%"]);
                } else {
                    $sql= "SELECT * FROM specif_revues";
                    $res= $this->base->prepare($sql);
                    $res->execute();
                }
            }    
        } else {
            if ($periodicite > 0) {
                $sql= "SELECT * FROM specif_revues WHERE periodicite = ?";
                $res= $this->base->prepare($sql);
                $res->execute([$periodicite]);
            } else {
                $sql= "SELECT * FROM specif_revues";
                $res= $this->base->prepare($sql);
                $res->execute();
            }
        }
    
            
        $rows= $res->fetchAll(PDO::FETCH_OBJ);
        $res->closeCursor();
        $donnees= [];$i=0;

        foreach ($rows as $row) {

            if ($egNUL==1 || ($row->nom_specif_revue <>"")) { 
           
                $mbr1= new Specif_revue();           // créer à chaque each un nouvel objet 
                $mbr1->setId_specif_revue($row->id_specif_revue);
                $mbr1->setNom_specif_revue($row->nom_specif_revue);
                $mbr1->setPeriodicite($row->periodicite);
                $mbr1->setHors_serie($row->hors_serie);
                $mbr1->setPrix($row->prix);
                $mbr1->setMonnaie($row->monnaie);
            
                array_push($donnees, $mbr1);
                $i++;
            }
            
        }

        if (!isset($_COOKIE['total'])) {
            setcookie('total',count($donnees));
        }
        $_COOKIE['total']=count($donnees);

        return $donnees;
    }


    public function listeAbonnement($id_abonnement, $nom_specif_revue, $id_client, $type_client, $statut_client, $egNUL) 
    {
        if ($id_abonnement > 0) {
            $sql= "SELECT * FROM abonnements WHERE id_abonnement = ?";
            $res= $this->base->prepare($sql);
            $res->execute([$id_abonnement]);
        } else {
            if ($nom_specif_revue <> "") {
                if ($id_client > 0) {
                        $sql= "SELECT * FROM abonnements WHERE nom_specif_revue = ? AND id_client = ?";
                        $res= $this->base->prepare($sql);
                        $res->execute([$nom_specif_revue, $id_client]);
                } else {
                        $sql= "SELECT * FROM abonnements WHERE nom_specif_revue = ?";
                        $res= $this->base->prepare($sql);
                        $res->execute([$nom_specif_revue]);
                }
            } else {
                if ($id_client > 0) {
                        $sql= "SELECT * FROM abonnements WHERE id_client = ?";
                        $res= $this->base->prepare($sql);
                        $res->execute([$id_client]);
                } else {
                        $sql= "SELECT * FROM abonnements";
                        $res= $this->base->prepare($sql);
                        $res->execute();
                }
            }
        }
            
        $rows= $res->fetchAll(PDO::FETCH_OBJ);
        $res->closeCursor();
        $donnees= [];$i=0;

        foreach ($rows as $row) {

            if ($egNUL==1 || ($row->nom_specif_revue <>"")) { 

                if (($type_client > 0 && $row->type_client == $type_client) || $type_client == -1 || $type_client == 0) {
                    if (($statut_client > 0 && $row->statut_client == $statut_client) || $statut_client == 0) {
            
                        $mbr1= new Abonnement();           // créer à chaque each un nouvel objet 

                        $mbr1->setId_abonnement($row->id_abonnement);
                        $mbr1->setNom_specif_revue($row->nom_specif_revue);
                        $mbr1->setPeriodicite($row->periodicite);
                        $mbr1->setType_papier_epapier($row->type_papier_epapier);
                        $mbr1->setDate_debut($row->date_debut);
                        $mbr1->setDate_fin($row->date_fin);
                        $mbr1->setPrix($row->prix);
                        $mbr1->setMonnaie($row->monnaie);
                        $mbr1->setMode_payment($row->mode_payment);
                        $mbr1->setRef_transaction($row->ref_transaction);
                        $mbr1->setId_client($row->id_client);
                        $mbr1->setType_client($row->type_client);
                        $mbr1->setStatut_client($row->statut_client);
                
                        array_push($donnees, $mbr1);
                        $i++;
                    }
                }
            }
        }

        if (!isset($_COOKIE['total'])) {
            setcookie('total',count($donnees));
        }
        $_COOKIE['total']=count($donnees);

        return $donnees;
    }


    public function listePriorite($role) 
    {
        if ($role > 0) {
            if ($role == 1 || $role == 2 || $role == 3) {
                $sql= "SELECT (pr1_acces_X1, pr1_acces_X2, pr1_acces_X3, pr1_acces_X4) FROM priorites";
                $res= $this->base->prepare($sql);
                $res->execute();
            } else {
                if ($role == 4) {
                    $sql= "SELECT (pr2_acces_X1, pr2_acces_X2, pr2_acces_X3, pr2_acces_X4) FROM priorites";
                    $res= $this->base->prepare($sql);
                    $res->execute();
                } else {
                    if ($role == 5) {
                        $sql= "SELECT (pr3_acces_X1, pr3_acces_X2, pr3_acces_X3, pr3_acces_X4) FROM priorites";
                        $res= $this->base->prepare($sql);
                        $res->execute();
                    } else {
                        $sql= "SELECT (pr4_acces_X1, pr4_acces_X2, pr4_acces_X3, pr4_acces_X4) FROM priorites";
                        $res= $this->base->prepare($sql);
                        $res->execute();
                    }
                }
            } 
        } else {
            $sql= "SELECT * FROM priorites";
            $res= $this->base->prepare($sql);
            $res->execute();
        }
 
        $rows= $res->fetchAll(PDO::FETCH_OBJ);
        $res->closeCursor();
        $donnees= [];$i=0;

        foreach ($rows as $row) {
            
            $prior= new priorite();           // créer à chaque each un nouvel objet         
          
            $prior->setpr1_acces_X1(-1);    
            $prior->setpr1_acces_X2(-1);    
            $prior->setpr1_acces_X3(-1);    
            $prior->setpr1_acces_X4(-1);    
            $prior->setpr2_acces_X1(-1);    
            $prior->setpr2_acces_X2(-1);    
            $prior->setpr2_acces_X3(-1);    
            $prior->setpr2_acces_X4(-1);    
            $prior->setpr3_acces_X1(-1);    
            $prior->setpr3_acces_X2(-1);    
            $prior->setpr3_acces_X3(-1);    
            $prior->setpr3_acces_X4(-1);    
            $prior->setpr4_acces_X1(-1);    
            $prior->setpr4_acces_X2(-1);    
            $prior->setpr4_acces_X3(-1);    
            $prior->setpr4_acces_X4(-1);  

            if ($role == 1 || $role == 2 || $role == 3) {
                $prior->setpr1_acces_X1($row->pr1_acces_X1);    
                $prior->setpr1_acces_X2($row->pr1_acces_X2);    
                $prior->setpr1_acces_X3($row->pr1_acces_X3);    
                $prior->setpr1_acces_X4($row->pr1_acces_X4);    
            }

            if ($role == 4) {
                $prior->setpr2_acces_X1($row->pr2_acces_X1);    
                $prior->setpr2_acces_X2($row->pr2_acces_X2);    
                $prior->setpr2_acces_X3($row->pr2_acces_X3);    
                $prior->setpr2_acces_X4($row->pr2_acces_X4);    
            }

            if ($role == 5) {
                $prior->setpr3_acces_X1($row->pr3_acces_X1);    
                $prior->setpr3_acces_X2($row->pr3_acces_X2);    
                $prior->setpr3_acces_X3($row->pr3_acces_X3);    
                $prior->setpr3_acces_X4($row->pr3_acces_X4);    
            }

            if ($role > 5) {
                $prior->setpr4_acces_X1($row->pr4_acces_X1);    
                $prior->setpr4_acces_X2($row->pr4_acces_X2);    
                $prior->setpr4_acces_X3($row->pr4_acces_X3);    
                $prior->setpr4_acces_X4($row->pr4_acces_X4);    
            }

            array_push($donnees, $prior);
            $i++;
        }

        if (!isset($_COOKIE['total'])) {
            setcookie('total',count($donnees));
        }
        $_COOKIE['total']=count($donnees);

        return $donnees;
    }


    public function listeUser($id_util, $nomOuPseudo, $email, $role, $dateCr, $egNUL) 
    {

        if ($id_util > 0) {
            $sql= "SELECT * FROM utilisateurs WHERE id_util = ?";
            $res= $this->base->prepare($sql);
            $res->execute([$id_util]);
        } else {
            if ($email <> "") {
                if ($nomOuPseudo <> "") {
                    if ($role > 0) {
                                $sql= "SELECT * FROM utilisateurs WHERE email LIKE ? AND (nom LIKE ? OR pseudo LIKE ? OR prenom LIKE ?) AND (role = ?)";
                                $res= $this->base->prepare($sql);
                                $res->execute(["%$email%", "%$nomOuPseudo%", "%$nomOuPseudo%", "%$nomOuPseudo%", $role]);
                        } else {
                                $sql= "SELECT * FROM utilisateurs WHERE email LIKE ? AND (nom LIKE ? OR pseudo LIKE ? OR prenom LIKE ?)";
                                $res= $this->base->prepare($sql);
                                $res->execute(["%$email%", "%$nomOuPseudo%", "%$nomOuPseudo%", "%$nomOuPseudo%"]);
                        }
                    } else {
                        if ($role > 0) {
                                $sql= "SELECT * FROM utilisateurs WHERE email LIKE ? AND (role = ?)";
                                $res= $this->base->prepare($sql);
                                $res->execute(["%$email%", $role]);
                        } else {
                                $sql= "SELECT * FROM utilisateurs WHERE  email LIKE ?";
                                $res= $this->base->prepare($sql);
                                $res->execute(["%$email%"]);
                        }
                    }
            } else {
                if ($nomOuPseudo <> "") {
                    if ($role > 0) {
                                $sql= "SELECT * FROM utilisateurs WHERE (nom LIKE ? OR pseudo LIKE ? OR prenom LIKE ?) AND (role = ?)";
                                $res= $this->base->prepare($sql);
                                $res->execute(["%$nomOuPseudo%", "%$nomOuPseudo%", "%$nomOuPseudo%", $role]);
                        } else {
                                $sql= "SELECT * FROM utilisateurs WHERE nom LIKE ? OR pseudo LIKE ? OR prenom LIKE ?";
                                $res= $this->base->prepare($sql);
                                $res->execute(["%$nomOuPseudo%", "%$nomOuPseudo%", "%$nomOuPseudo%"]);
                        }
                    } else {
                        if ($role > 0) {
                                $sql= "SELECT * FROM utilisateurs WHERE (role = ?)";
                                $res= $this->base->prepare($sql);
                                $res->execute([$role]);
                        } else {
                                $sql= "SELECT * FROM utilisateurs";
                                $res= $this->base->prepare($sql);
                                $res->execute();
                        }
                    }
    
            }
        }


        $rows= $res->fetchAll(PDO::FETCH_OBJ);
        $res->closeCursor();

        $donnees= [];$i=0;

        foreach ($rows as $row) {

            if ($egNUL==1 || ($row->nom <>"")) { 

                $usr= new User();           // créer à chaque each un nouvel objet 
                $usr->setId_util($row->id_util);   // pour avoir une adresse différente 
                $usr->setNom($row->nom);   // de l'objet créé précédemment.
                $usr->setPrenom($row->prenom);
                $usr->setPseudo($row->pseudo);
                $usr->setEmail($row->email);
                $usr->setPass($row->pass);
                $usr->setRole($row->role);
                $usr->setDate_created($row->date_created);
                
                array_push($donnees, $usr);
                $i++;
            }
        }

        if (!isset($_COOKIE['total'])) {
            setcookie('total',count($donnees));
        }
        $_COOKIE['total']=count($donnees);

        return $donnees;
    }


    /******************** Autres fonctions ******************************/

    
    public function changerRole(User $usr) {
        $sql= "UPDATE utilisateurs SET role=? WHERE nom=".$usr->getNom()." AND prenom=".$usr->getPrenom();
        $res= $this->base->prepare($sql);
        
        if ($usr->getRole == 1) {
            $res->execute([2]);
        } else {
            $res->execute([1]);
        }
        
    }



}


?>