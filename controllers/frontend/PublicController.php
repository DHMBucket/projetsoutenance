<?php

require_once('./communs/connect.php');
require_once('./models/frontend/FrontDriver.php');
require_once('./models/Auteur.php');
require_once('./models/LivreRevue.php');
require_once('./controllers/frontend/PresentationtraitementController.php');
require_once('./assets/librairies/stripe/vendor/autoload.php');        // paiement


class PublicController {

  private $driver;
  private $presTrait;

  public function __construct($base)
  {
    $this->driver= new FrontDriver($base);
    $this->presTrait= new PresentationTraitementController($base);

  }


  public function accueilEditeur($dataA) {
    
    $numDim= count($dataA); 

    $this->presTrait->defilerEntite($numDim);              // exec 4

  
    $idData=[];$cdeData=[];$nomData=[];$locData=[];$frlData=[];
        
            $jN=0;
            for ($j=0; $j<count($dataA); $j++) {
              $idData[$j]=$dataA[$j]->getId_editeur();
              $cdeData[$j]=$dataA[$j]->getCode_editeur();
              $nomData[$j]=$dataA[$j]->getNom_editeur();
        
              if ($dataA[$j]->getPays_editeur() <> "") {
                $locData[$jN]=$dataA[$j]->getPays_editeur();$jN++;
              }
              if ($dataA[$j]->getLocalite_editeur() <> "") {
                $locData[$jN]=$dataA[$j]->getLocalite_editeur();$jN++;
              }
               
              $frlData[$j]=$dataA[$j]->getFrs_livrais();
            }
        
            natcasesort($cdeData);
            $nomData = array_unique($nomData);
            natcasesort($nomData);
            $locData = array_unique($locData);
            natcasesort($locData);
            $frlData = array_unique($frlData);
            natcasesort($frlData);


    require_once('./views/frontend/accueilEditeur.php');    // exec 9

  }


  public function accueilAuteur($dataA) {
    
    $numDim= count($dataA); 

    $this->presTrait->defilerEntite($numDim);              // exec 4
 
  $idData=[];$nomData=[];$insData=[];$prnData=[];
        
            
  for ($j=0; $j<count($dataA); $j++) {
    $idData[$j]=$dataA[$j]->getId_auteur();
    $nomData[$j]=$dataA[$j]->getNom();

    $insData[$j]=$dataA[$j]->getInstitution();
      
    $prnData[$j]=$dataA[$j]->getPrenom();

    }

  $nomData = array_unique($nomData);
  natcasesort($nomData);
  $insData = array_unique($insData);
  natcasesort($insData);
  $prnData = array_unique($prnData);
  natcasesort($prnData);


    require_once('./views/frontend/accueilAuteur.php');    // exec 9

  }


  public function accueilLivreRevue($dataA) {
    
    $numDim= count($dataA);

    $this->presTrait->defilerEntite($numDim);              // exec 4

     
    $idData=[];$refData=[];$nomData=[];$autData=[];$anneeData=[];
        
        
    $jN=0;$jA=0;
    for ($j=0; $j<count($dataA); $j++) {
      $idData[$j]=$dataA[$j]->getId_livre();
      $refData[$j]=$dataA[$j]->getReference();
      if ($dataA[$j]->getTitre() <> "") {
        $nomData[$jN]=$dataA[$j]->getTitre();$jN++;
      }
      if ($dataA[$j]->getNom_specif_revue() <> "") {
        $nomData[$jN]=$dataA[$j]->getNom_specif_revue();$jN++;
      }
      if ($dataA[$j]->getAuteur_princ1_nom() <> "") {
        $autData[$jA]=$dataA[$j]->getAuteur_princ1_nom();$jA++;
      }
      if ($dataA[$j]->getAuteur_princ1_institution() <> "") {
        $autData[$jA]=$dataA[$j]->getAuteur_princ1_institution();$jA++;
      }
      if ($dataA[$j]->getAuteur_princ2_nom() <> "") {
        $autData[$jA]=$dataA[$j]->getAuteur_princ2_nom();$jA++;
      }
      if ($dataA[$j]->getAuteur_princ2_institution() <> "") {
        $autData[$jA]=$dataA[$j]->getAuteur_princ2_institution();$jA++;
      }
      if ($dataA[$j]->getRedac_chef_nom() <> "") {
        $autData[$jA]=$dataA[$j]->getRedac_chef_nom();$jA++;
      }
      if ($dataA[$j]->getRedac_chef_institution() <> "") {
        $autData[$jA]=$dataA[$j]->getRedac_chef_institution();$jA++;
      }
      if ($dataA[$j]->getEditeur1() <> "") {
        $autData[$jA]=$dataA[$j]->getEditeur1();$jA++;
      }
      if ($dataA[$j]->getEditeur2() <> "") {
        $autData[$jA]=$dataA[$j]->getEditeur2();$jA++;
      }
      $anneeData[$j]=date("Y", strtotime($dataA[$j]->getDate_parution()));

    }

    natcasesort($refData);
    $nomData = array_unique($nomData);
    natcasesort($nomData);
    $autData = array_unique($autData);
    natcasesort($autData);
    $anneeData = array_unique($anneeData);
    natcasesort($anneeData);

    require_once('./views/frontend/accueilLivreRevue.php');    // exec 9
  }

  public function genererNumCompteur() {
    $dataA= $this->driver->nouvNumCompteur();
    return $dataA;
  }

  public function checkout() {

    //require_once('./views/frontend/checkout.php');

  }


  public function paiement($monn, $titre) {

    if ($monn=="Є") { $m='eur';
    } else { 
      if ($monn=="$") { $m='usd';
      } else {$m='gbp';} 
    }

    if (isset($_POST['stripeToken']) && !empty($_POST['stripeToken'])) {
      $token= $_POST['stripeToken'];
      \Stripe\Stripe::setApiKey("sk_test_bFDsuQg9OKcGB5nCeDOWy6Bs007IvoaEq1");
      $prix= floatval($_POST['px']*100);

      $charge= \Stripe\Charge::create(['amount' => $prix,
                                       'currency'  => $m,
                                       'description' => $titre,
                                       'source' => $token
                                       ]);
    }
 
  } 


}
