<?php 
    require_once('./controllers/backend/AuthController.php');
    require_once('./communs/connect.php');
    require_once('./models/Abonnement.php');
    require_once('./models/backend/BackDriver.php');

    class AbonnementController 
    {

        private $driver;

        public function __construct($base)
        {
          $this->driver= new BackDriver($base);

        }

        /******************************************************************************/

        public function supprimerAbonnement($idabn){
        
          $this->driver->suppressAbonnement($idabn);
        }

      

        public function detailAbonnement($donnAbn){
                  
          require_once('./views/backend/detailAbn.php');
  
        }
      

        public function modifierAbonnement($donnAbn) {

          if (isset($_GET['id'])) {

              require_once('./views/backend/modifDetailAbn.php');
        
          }
      }


/************************************************************************* */


        public function tableauRecapAbonnement($dataA) {
            $numDim= count($dataA);        
 
            $idData=[];$nomData=[];$idcData=[];$tymbData=[];$sttData=[];
           
            for ($j=0; $j<count($dataA); $j++) {
              $idData[$j]=$dataA[$j]->getId_abonnement();
              $nomData[$j]=$dataA[$j]->getNom_specif_revue();
              $idcData[$j]=$dataA[$j]->getId_client();
              $tymbData[$j]=$dataA[$j]->getType_client();
              $sttData[$j]=$dataA[$j]->getStatut_client();
            }
        
            $nomData = array_unique($nomData);
            natcasesort($nomData);
            $idcData = array_unique($idcData);
            natcasesort($idcData);
            $tymbData = array_unique($tymbData);
            natcasesort($tymbData);
            $sttData = array_unique($sttData);
            natcasesort($sttData);
        

            require_once('./views/backend/afficheTableauAbonnement.php');    // exec 9
          }

}

?>



    

