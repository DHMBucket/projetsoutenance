<?php 
    require_once('./controllers/backend/AuthController.php');
    require_once('./communs/connect.php');
    require_once('./models/Auteur.php');
    require_once('./models/backend/BackDriver.php');


    class AuteurController 
    {

        private $driver;

        public function __construct($base)
        {
          $this->driver= new BackDriver($base);

        }

        /******************************************************************************/

        public function supprimerAuteur($idaut){
                
          $this->driver->suppressAuteur($idaut);
        }

        
        public function detailAuteur($donnAut)
        {
                  
          require_once('./views/backend/detailAut.php');
  
        }


        public function modifierAuteur($donnAut) {

          if (isset($_GET['id'])) {

              require_once('./views/backend/modifDetailAut.php');
        
          }
      }


      /********************************************************************** */

        
        public function verifierAuteur() {
     
          if (isset($_POST['ajoutaut'])) {
              $mbr1= new Auteur();
  
              $mbr1->setNom($_POST['nom']);
              $mbr1->setPrenom($_POST['prenom']);
              $mbr1->setInstitution($_POST['institution']);
              $mbr1->setFonction($_POST['fonction']);
              $listmbr=$this->driver->listeAuteur(0, $mbr1->getNom(), $mbr1->getPrenom(), $mbr1->getInstitution(), 0);

              if (count($listmbr) == 0) {
                  return $mbr1;
              } else { $mbr1->setNom("-1"); return $mbr1; }    
                  
          }
        }


        public function registerAut(){
            
          require_once('./views/backend/registerAut.php');
  
      }


        public function tableauRecapAuteur($dataA) {
    
            $numDim= count($dataA);        
 
            $idData=[];$nomData=[];$insData=[];$prnData=[];
        
        
            
            for ($j=0; $j<count($dataA); $j++) {
              $idData[$j]=$dataA[$j]->getId_auteur();
              $nomData[$j]=$dataA[$j]->getNom();
        
              $insData[$j]=$dataA[$j]->getInstitution();
                
              $prnData[$j]=$dataA[$j]->getPrenom();
        
              }
        
            $nomData = array_unique($nomData);
            natcasesort($nomData);
            $insData = array_unique($insData);
            natcasesort($insData);
            $prnData = array_unique($prnData);
            natcasesort($prnData);
        
            require_once('./views/backend/afficheTableauAuteur.php');    // exec 9
        
          }
        

}

?>



    

