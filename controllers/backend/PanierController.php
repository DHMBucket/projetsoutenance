<?php 
    require_once('./controllers/backend/AuthController.php');
    require_once('./communs/connect.php');
    require_once('./models/Panier.php');
    require_once('./models/backend/BackDriver.php');

    class PanierController 
    {

        private $driver;

        public function __construct($base)
        {
          $this->driver= new BackDriver($base);

        }

       /******************************************************************************/

       public function supprimerPanier($idpan){
        
        $this->driver->suppressPanier($idpan);
      }
    
       
        public function detailPanier($donnPan)
        {
                  
          require_once('./views/backend/detailPan.php');
  
        }

        public function modifierPanier($donnPan) {

          if (isset($_GET['id'])) {

              require_once('./views/backend/modifDetailPan.php');
        
          }
      }


      /********************************************************************** */

          
        public function tableauRecapPanier($dataA) {
        
    
          $numDim= count($dataA);
          

          $idData=[];$idcData=[];$typData=[];
          $idlData=[];$rflData=[];$idaData=[];    
          $ttrData=[];

          $jN=0;$jI=0;$jR=0;$jA=0;
          for ($j=0; $j<count($dataA); $j++) {

            $idData[$j]=$dataA[$j]->getId_panier();
            $idcData[$j]=$dataA[$j]->getId_client();
            $typData[$j]=$dataA[$j]->getType_client();

            if ($dataA[$j]->getId_artcl1() <> "") {
              $idaData[$jA]=$dataA[$j]->getId_artcl1();$jA++;
            }
            if ($dataA[$j]->getId_artcl2() <> "") {
              $idaData[$jA]=$dataA[$j]->getId_artcl2();$jA++;
            }
            if ($dataA[$j]->getId_artcl3() <> "") {
              $idaData[$jA]=$dataA[$j]->getId_artcl3();$jA++;
            }

            if ($dataA[$j]->getId_livre1() <> "") {
              $idlData[$jI]=$dataA[$j]->getId_livre1();$jI++;
            }
            if ($dataA[$j]->getId_livre2() <> "") {
              $idlData[$jI]=$dataA[$j]->getId_livre2();$jI++;
            }
            if ($dataA[$j]->getId_livre3() <> "") {
              $idlData[$jI]=$dataA[$j]->getId_livre3();$jI++;
            }           


            if ($dataA[$j]->getRef_livre1() <> "") {
              $rflData[$jR]=$dataA[$j]->getRef_livre1();$jR++;
            }
            if ($dataA[$j]->getRef_livre2() <> "") {
              $rflData[$jR]=$dataA[$j]->getRef_livre2();$jR++;
            }
            if ($dataA[$j]->getRef_livre3() <> "") {
              $rflData[$jR]=$dataA[$j]->getRef_livre3();$jR++;
            }


            if ($dataA[$j]->getTtr_livre1() <> "") {
              $ttrData[$jN]=$dataA[$j]->getTtr_livre1();$jN++;
            }
            if ($dataA[$j]->getTtr_artcl1() <> "") {
              $ttrData[$jN]=$dataA[$j]->getTtr_artcl1();$jN++;
            }
            if ($dataA[$j]->getTtr_livre2() <> "") {
              $ttrData[$jN]=$dataA[$j]->getTtr_livre2();$jN++;
            }
            if ($dataA[$j]->getTtr_artcl2() <> "") {
              $ttrData[$jN]=$dataA[$j]->getTtr_artcl2();$jN++;
            }
            if ($dataA[$j]->getTtr_livre3() <> "") {
              $ttrData[$jN]=$dataA[$j]->getTtr_livre3();$jN++;
            }
            if ($dataA[$j]->getTtr_artcl3() <> "") {
              $ttrData[$jN]=$dataA[$j]->getTtr_artcl3();$jN++;
            }

          }
      
          natcasesort($idData);
          $idcData = array_unique($idcData);
          natcasesort($idcData);
          $typData = array_unique($typData);
          natcasesort($typData);

          $idlData = array_unique($idlData);
          natcasesort($idlData);
          $rflData = array_unique($rflData);
          natcasesort($rflData);
          $idaData = array_unique($idaData);
          natcasesort($idaData);

          $ttrData = array_unique($ttrData);
          natcasesort($ttrData);
      
          require_once('./views/backend/afficheTableauPanier.php');    // exec 9
        }


        public function enregistrLivreRevuePanier() {

          if (!isset($_SESSION['id'])) {
                  echo "Action impossible";
          }
         
            if (isset($_SESSION['AuthClt']) || isset($_SESSION['Auth'])) {
          
              if (isset($_SESSION['Auth']['pseudo'])) {
                $authClt= $_SESSION['Auth'];
                $typClt= $_SESSION['Auth']['role'];
              } else {
                $authClt= $_SESSION['AuthClt'];
                $typClt= 0;
              }
              
                $donnPanexist= $this->driver->listePanier(0, $authClt['numero'], $typClt, 0,
                "-1", 0, 0, "-1", 0, 0, "-1", 0, "", 0);

                $donnPan1= $this->driver->listePanierLvr(0, $authClt['numero'], $typClt, 
                $_SESSION["dataA"][$_GET['t']]->getId_livre(), 1, 1); 
                $donnPan2= $this->driver->listePanierLvr(0, $authClt['numero'], $typClt, 
                $_SESSION["dataA"][$_GET['t']]->getId_livre(), 1, 2); 
                $donnPan3= $this->driver->listePanierLvr(0, $authClt['numero'], $typClt, 
                $_SESSION["dataA"][$_GET['t']]->getId_livre(), 1, 3); 
               
              if (count($donnPanexist) == 0) {
                $this->driver->ajouterPanier($_SESSION["dataA"][$_GET['t']], $authClt, $typClt, 1, 1, 0, 0);
              } else {
                if (count($donnPan1) == 0 && count($donnPan2) == 0 && count($donnPan3) == 0) {
                  if ($donnPanexist[0]->getId_livre2() == 1 && $donnPanexist[0]->getId_artcl2() == 1) {
                    $this->driver->ajouterPanier($_SESSION["dataA"][$_GET['t']], $authClt, $typClt, 0, 2, 0, 0);
                  } else {
                    if ($donnPanexist[0]->getId_livre3() == 1 && $donnPanexist[0]->getId_artcl3() == 1) {
                      $this->driver->ajouterPanier($_SESSION["dataA"][$_GET['t']], $authClt, $typClt, 0, 3, 0, 0);
                    }
                  }
                } else {
                    if (count($donnPan1) > 0) {
                      $this->driver->ajouterPanier($_SESSION["dataA"][$_GET['t']], $authClt, $typClt, 0, 1, 1, 0);
                    } else {
                      if (count($donnPan2) > 0) {
                        $this->driver->ajouterPanier($_SESSION["dataA"][$_GET['t']], $authClt, $typClt, 0, 2, 1, 0);
                      } else {
                        $this->driver->ajouterPanier($_SESSION["dataA"][$_GET['t']], $authClt, $typClt, 0, 3, 1, 0);
                      }
                    }
                }
              }
            } else { echo "Erreur client";}
      }



      public function enregistrArticlePanier() {

        if (!isset($_SESSION['id'])) {
                echo "Action impossible";
        }
       
          if (isset($_SESSION['AuthClt']) || isset($_SESSION['Auth'])) {
        
            if (isset($_SESSION['Auth']['pseudo'])) {
              $authClt= $_SESSION['Auth'];
              $typClt= $_SESSION['Auth']['role'];
            } else {
              $authClt= $_SESSION['AuthClt'];
              $typClt= 0;
            }

              $donnPanexist= $this->driver->listePanier(0, $authClt['numero'], $typClt, 0,
              "-1", 0, 0, "-1", 0, 0, "-1", 0, "", 0);

            $donnPan1= $this->driver->listePanierArt(0, $authClt['numero'], $typClt, $_SESSION["dataA"][$_GET['t']]->getId_livre(),
            $_SESSION["dataA"][$_GET['t']]->getRef_livre(), $_SESSION["dataA"][$_GET['t']]->getId_article(), $_SESSION["dataA"][$_GET['t']]->getId_livre(),
            $_SESSION["dataA"][$_GET['t']]->getRef_livre(), $_SESSION["dataA"][$_GET['t']]->getId_article(), $_SESSION["dataA"][$_GET['t']]->getId_livre(),
            $_SESSION["dataA"][$_GET['t']]->getRef_livre(), $_SESSION["dataA"][$_GET['t']]->getId_article(), "", 0);

            if (count($donnPanexist) == 0) {
              $this->driver->ajouterPanier($_SESSION["dataA"][$_GET['t']], $authClt, $typClt, 1, 1, 0, 1);
            } else {
              if (count($donnPan1) == 0) {

                if ($donnPanexist[0]->getId_livre2() == 1 && $donnPanexist[0]->getId_artcl2() == 1) {
                  $this->driver->ajouterPanier($_SESSION["dataA"][$_GET['t']], $authClt, $typClt, 0, 2, 0, 1);
                } else {
                  if ($donnPanexist[0]->getId_livre3() == 1 && $donnPanexist[0]->getId_artcl3() == 1) {
                    $this->driver->ajouterPanier($_SESSION["dataA"][$_GET['t']], $authClt, $typClt, 0, 3, 0, 1);
                  }
                }
              } else {
                  if ($donnPanexist[0]->getId_artcl1() == $_SESSION["dataA"][$_GET['t']]->getId_article()) {
                    $this->driver->ajouterPanier($_SESSION["dataA"][$_GET['t']], $authClt, $typClt, 0, 1, 1, 1);
                  } else {
                    if ($donnPanexist[0]->getId_artcl2() == $_SESSION["dataA"][$_GET['t']]->getId_article()) {
                      $this->driver->ajouterPanier($_SESSION["dataA"][$_GET['t']], $authClt, $typClt, 0, 2, 1, 1);
                    } else {
                      $this->driver->ajouterPanier($_SESSION["dataA"][$_GET['t']], $authClt, $typClt, 0, 3, 1, 1);
                    }
                  }
              }
            }
          } else {
            echo "Erreur client";
          }
    }


}

?>



    

