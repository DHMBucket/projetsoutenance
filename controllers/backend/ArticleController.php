<?php 
    require_once('./controllers/backend/AuthController.php');
    require_once('./communs/connect.php');
    require_once('./models/Article.php');
    require_once('./models/backend/BackDriver.php');

    class ArticleController 
    {

        private $driver;

        public function __construct($base)
        {
          $this->driver= new BackDriver($base);

        }

        /******************************************************************************/

        public function supprimerArticle($idart){

            $this->driver->suppressArticle($idart);
        }
        


        public function detailArticle($donnArt)
        {
                  
          require_once('./views/backend/detailArt.php');
  
        }


        public function modifierArticle($donnArt, $donnAut1, $donnAut2, $donnThm) {

            if (isset($_GET['id'])) {

                require_once('./views/backend/modifDetailArt.php');
          
            }
        }

        /********************************************************************** */

        public function verifierArticle() {
 
            if (isset($_POST['ajoutart'])) {
                $mbr1= new Article();
                $mbr1->setEmplacement((int)$_POST['rang']);
                $mbr1->setNbre_pages((int)$_POST['nbpage']);
                $mbr1->setNbre_mots_pert((int)$_POST['mpert']);
                $mbr1->setPrix((int)$_POST['prix']);
                $mbr1->setMonnaie("Euro");
                $mbr1->setType_support("Ebook");
                             
                $str = str_replace("$", " ", $_POST['theme'], $count);$theme=$str;
                $mbr1->setTheme_article($theme);

                $str = str_replace("$", " ", $_POST["livre"], $count);$_POST["livre"]=$str;
                $livre= explode("//", $_POST['livre']);
                $mbr1->setId_livre((int)$livre[0]);
                $mbr1->setRef_livre($livre[1]);
                $mbr1->setTitre($_POST['titre']);

                $str = str_replace("$", " ", $_POST["nauteur1"], $count);$_POST["nauteur1"]=$str;
                $nauteur1= explode("//", $_POST['nauteur1']);

                $mbr1->setAuteur1_nom($nauteur1[0]);
                $mbr1->setAuteur1_prenom($nauteur1[1]);
                $mbr1->setAuteur1_institution($nauteur1[2]);

                $str = str_replace("$", " ", $_POST["nauteur2"], $count);$_POST["nauteur2"]=$str;
                $nauteur2= explode("//", $_POST['nauteur2']);

                $mbr1->setAuteur2_nom($nauteur2[0]);
                $mbr1->setAuteur2_prenom($nauteur2[1]);
                $mbr1->setAuteur2_institution($nauteur2[2]);

                if ( $mbr1->getId_livre() > 1 ) {
                    $listlivre= $this->driver->listeLivreRevue($mbr1->getId_livre(), "", "", "", -1, 0, "toutes");

                    if (count($listlivre)>0) {
                        $mbr1->setAuteur1_nom($listlivre[0]->getAuteur_princ1_nom());
                        $mbr1->setAuteur1_prenom($listlivre[0]->getAuteur_princ1_prenom());
                        $mbr1->setAuteur1_institution($listlivre[0]->getAuteur_princ1_institution());
                        $mbr1->setAuteur2_nom($listlivre[0]->getAuteur_princ2_nom());
                        $mbr1->setAuteur2_prenom($listlivre[0]->getAuteur_princ2_prenom());
                        $mbr1->setAuteur2_institution($listlivre[0]->getAuteur_princ2_institution());
                    }
                    $listmbr= $this->driver->listeArticle(0, "", "", "", $mbr1->getRef_livre(), $mbr1->getId_livre(), $mbr1->getEmplacement(), "", "", 0);
                } else {
                    $listmbr= $this->driver->listeArticle(0, $mbr1->getTitre(), $mbr1->getTheme_article(), "", "", 0, $mbr1->getEmplacement(), "", "", 0);
                }


      
                if (count($listmbr) == 0) {

                return $mbr1;
                } else { 

                    $mbr1->setTitre("-1"); return $mbr1; }    
            }
          }
    
    
    

        public function registerArt($donnLvR, $donnAut1, $donnAut2, $donnThm){
            
            require_once('./views/backend/registerArt.php');
    
        }
  


        public function tableauRecapArticle($dataA, $dataSupp) {
        
    
            $numDim= count($dataA); 
        
            $idData=[]; $ttrData=[]; $thmData=[]; $idlData=[]; $rflData=[]; 
            
            $nomData=[]; $prnData=[]; $mclData=[];
        
            $jN=0;$jA=0;$jM=0;

            for ($j=0; $j<count($dataA); $j++) {

                for ($k=0; $k<count($dataSupp); $k++) {
                    if ($dataSupp[$k]->getId_artcl() == $dataA[$j]->getId_article()) { 
                        $mclData[$jM]=$dataSupp[$k]->getNom_motcle();$jM++;
                    } 
                }

                $idData[$j]=$dataA[$j]->getId_article();
                $ttrData[$j]=$dataA[$j]->getTitre();
                $thmData[$j]=$dataA[$j]->getTheme_article();               
                $idlData[$j]=$dataA[$j]->getId_livre();
                $rflData[$j]=$dataA[$j]->getRef_livre();

                if ($dataA[$j]->getAuteur1_nom() <> "") {
                    $nomData[$jN]=$dataA[$j]->getAuteur1_nom();$jN++;
                }
                if ($dataA[$j]->getAuteur2_nom() <> "") {
                    $nomData[$jN]=$dataA[$j]->getAuteur2_nom();$jN++;
                }
                if ($dataA[$j]->getAuteur1_institution() <> "") {
                    $nomData[$jN]=$dataA[$j]->getAuteur1_institution();$jN++;
                }
                if ($dataA[$j]->getAuteur2_institution() <> "") {
                    $nomData[$jN]=$dataA[$j]->getAuteur2_institution();$jN++;
                }


                if ($dataA[$j]->getAuteur1_prenom() <> "") {
                    $prnData[$jA]=$dataA[$j]->getAuteur1_prenom();$jA++;
                }
                if ($dataA[$j]->getAuteur2_prenom() <> "") {
                    $prnData[$jA]=$dataA[$j]->getAuteur2_prenom();$jA++;
                }

            }
        
        
            natcasesort($idData);
            $ttrData = array_unique($ttrData);
            natcasesort($ttrData);
            $thmData = array_unique($thmData);
            natcasesort($thmData);
            $idlData = array_unique($idlData);
            natcasesort($idlData);
            $rflData = array_unique($rflData);
            natcasesort($rflData);
            $nomData = array_unique($nomData);
            natcasesort($nomData);
            $prnData = array_unique($prnData);
            natcasesort($prnData);
            $mclData = array_unique($mclData);
            natcasesort($mclData);
        
            require_once('./views/backend/afficheTableauArticle.php');    // exec 9
          }
        

}

?>



    

