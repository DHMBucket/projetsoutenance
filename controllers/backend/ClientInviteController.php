<?php 
    require_once('./controllers/backend/AuthController.php');
    require_once('./communs/connect.php');
    require_once('./models/ClientInvite.php');
    require_once('./models/backend/BackDriver.php');


    class ClientInviteController 
    {

        private $driver;

        public function __construct($base)
        {
          $this->driver= new BackDriver($base);

        }

    
        /******************************************************************************/

       public function supprimerClientInvite($idcti){
        
        $this->driver->suppressClientInvite($idcti);
      }
    
    


        public function detailClientInvite($donnCti)
        {
                  
          require_once('./views/backend/detailCti.php');
  
        }


        public function modifierClientInvite($donnCti) {

            if (isset($_GET['id'])) {

                require_once('./views/backend/modifDetailCti.php');
          
            }
        }


        /********************************************************************** */


        public function coordClientInvite() {
     
                 $mbr1= new ClientInvite();
                 
                 if (isset($_SESSION["AuthClt"]["pseudo"])) { $mbr1->setNum_compteur((int)htmlentities(trim($_SESSION["AuthClt"]["pseudo"]))); }
                 if (isset($_POST['typecivil'])) { $mbr1->setCivilite((int)htmlentities(trim($_POST['typecivil']))); }
                 if (isset($_POST['nom'])) { $mbr1->setNom_destin($_POST['nom']); }
                 if (isset($_POST['prenom'])) { $mbr1->setPrenom_destin($_POST['prenom']); }
                 if (isset($_POST['institution'])) { $mbr1->setInstitution($_POST['institution']); }
                 $mbr1->setFonction("");           
                 if (isset($_POST['email'])) { $mbr1->setEmail($_POST['email']); }
                 $mbr1->setOffre_partenaire(0);
                 $mbr1->setOffre_newsletter(0);
                 if (isset($_POST['inputAddress'])) { 
                     if (isset($_POST['inputAddress2'])) { 
                         $mbr1->setAdr_livrais($_POST['inputAddress']."//".$_POST['inputAddress2']); 
                     } else {
                         $mbr1->setAdr_livrais($_POST['inputAddress']);
                     }
                 } else {
                     if (isset($_POST['inputAddress2'])) { 
                         $mbr1->setAdr_livrais($_POST['inputAddress2']); 
                     }
                 }
                 if (isset($_POST['inputZip'])) { $mbr1->setCode_postal($_POST['inputZip']); }
                 if (isset($_POST['nometat'])) { $mbr1->setPays($_POST['nometat']); }
                 if (isset($_POST['inputCity'])) { $mbr1->setVille($_POST['inputCity']); }
                 if (isset($_POST['inputState'])) { $mbr1->setZip_etat($_POST['inputState']); }
                 if (isset($_POST['indicatif'])) { $mbr1->setIndicatif($_POST['indicatif']); }
                 $tel= trim(htmlspecialchars(addslashes($_POST['telClt'])));
                 if (isset($_POST['telClt'])) { $mbr1->setTelephone($tel); }
                 
                 return $mbr1;
     
     }

     
        public function verifierClientInvite() {

            if (isset($_POST['ajoutclmb'])) {
                $mbr1= new ClientMembre();
                
                if (isset($_POST['typembre'])) { $mbr1->setType_membre((int)htmlentities(trim($_POST['typembre']))); }
                if (isset($_POST['typecivil'])) { $mbr1->setCivilite((int)htmlentities(trim($_POST['typecivil']))); }
                if (isset($_POST['nom'])) { $mbr1->setNom_membre($_POST['nom']); }
                if (isset($_POST['prenom'])) { $mbr1->setPrenom_membre($_POST['prenom']); }
                if (isset($_POST['institution'])) { $mbr1->setInstitution($_POST['institution']); }
                if (isset($_POST['fonction'])) { $mbr1->setFonction($_POST['fonction']); }
                if (isset($_POST['statutmbre'])) { $mbr1->setStatut_client((int)htmlentities(trim($_POST['statutmbre']))); }
                //Récupération d'un fichier .pdf ou .docx ou .jpg
                if (isset($_FILES['ficjustif']['name']) && (!empty($_FILES['ficjustif']['name']))) { 
                    $destination = './assets/fichiers/';
                    
                    if (isset($_FILES['ficjustif']['name'])) { $mbr1->setJustif_etud_ou_ssemploi_instit($_FILES['ficjustif']['name']); }

                    move_uploaded_file($_FILES['ficjustif']['tmp_name'], $destination.$_FILES['ficjustif']['name']);    
                    $fichier= $_FILES['ficjustif']['name'];
                }
                if (isset($_POST['email'])) { $mbr1->setEmail($_POST['email']); }
                if (isset($_POST['offrpartner'])) { $mbr1->setOffre_partenaire(1); }
                if (isset($_POST['offrnwlett'])) { $mbr1->setOffre_newsletter(1); }
                if (isset($_POST['tarif'])) { $mbr1->setPrix_membre(floatval(htmlentities(trim($_POST['tarif'])))); }
                if (isset($_POST['monnaie'])) { 
                    $monn= explode("-", $_POST['monnaie']);
                    $monn1= $monn[count($monn)-1];
                    $mbr1->setMonnaie($monn1); 
                }
                if (isset($_POST['mdp'])) { 
                    $pass = md5(trim(htmlspecialchars(addslashes($_POST['mdp']))));
                    $mbr1->setMdp($pass); 
                }
                $mbr1->setDate_debut(date("Y-m-d")); 
                
                if (isset($_POST['inputAddress'])) { 
                    if (isset($_POST['inputAddress2'])) { 
                        $mbr1->setAdr_livrais($_POST['inputAddress']."//".$_POST['inputAddress2']); 
                    } else {
                        $mbr1->setAdr_livrais($_POST['inputAddress']);
                    }
                } else {
                    if (isset($_POST['inputAddress2'])) { 
                        $mbr1->setAdr_livrais($_POST['inputAddress2']); 
                    }
                }
                if (isset($_POST['inputZip'])) { $mbr1->setCode_postal($_POST['inputZip']); }
                if (isset($_POST['nometat'])) { $mbr1->setPays($_POST['nometat']); }
                if (isset($_POST['inputCity'])) { $mbr1->setVille($_POST['inputCity']); }
                if (isset($_POST['inputState'])) { $mbr1->setZip_etat($_POST['inputState']); }
                if (isset($_POST['indicatif'])) { $mbr1->setIndicatif($_POST['indicatif']); }
                $tel= trim(htmlspecialchars(addslashes($_POST['telClt'])));
                if (isset($_POST['telClt'])) { $mbr1->setTelephone($tel); }
                
                $listmbr= $this->driver->listeClientMembre(0, $mbr1->getNom_membre(), $mbr1->getPrenom_membre(), $mbr1->getPays(), $mbr1->getType_membre(), $mbr1->getStatut_client(), "", "datefinMmoinsUn",1);
                $listmbrmail= $this->driver->listeClientMembre(0, "", "", "", "", "", $mbr1->getEmail(), "",1);
                // verifier l'existence de ce nouveau membre

            if (count($listmbr) == 0 && count($listmbrmail) == 0) {
                return $mbr1;
                
            } else {
                $mbr1->setType_membre(-1);
                return $mbr1;

            }

        }
      
    
    }
    
    
    public function tableauRecapClientInvite($dataA) {
        
    
        $numDim= count($dataA);
    
        $idData=[];$refData=[];$nomData=[];
        $emlData=[];$locData=[];
        
        $jN=0;$jA=0;
        for ($j=0; $j<count($dataA); $j++) {
            $idData[$j]=$dataA[$j]->getId_clt_anonym();
            $refData[$j]=$dataA[$j]->getNum_compteur();

          if ($dataA[$j]->getNom_destin() <> "") {
            $nomData[$jN]=$dataA[$j]->getNom_destin();$jN++;
          }
          if ($dataA[$j]->getInstitution() <> "") {
            $nomData[$jN]=$dataA[$j]->getInstitution();$jN++;
          }

          $emlData[$j]=$dataA[$j]->getEmail();

          if ($dataA[$j]->getVille() <> "") {
            $locData[$jA]=$dataA[$j]->getVille();$jA++;
          }
          if ($dataA[$j]->getPays() <> "") {
            $locData[$jA]=$dataA[$j]->getPays();$jA++;
          }


        }
    
        natcasesort($refData);
        $nomData = array_unique($nomData);
        natcasesort($nomData);
        $emlData = array_unique($emlData);
        natcasesort($emlData);
        $locData = array_unique($locData);
        natcasesort($locData);
    
        require_once('./views/backend/afficheTableauClientInvite.php');    // exec 9
      }
    
      public function genererNumCompteur() {
        $dataA= $this->driver->nouvNumCompteur();
        return $dataA;
      }
    

      
}

?>



    

