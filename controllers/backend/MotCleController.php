<?php 
    require_once('./controllers/backend/AuthController.php');
    require_once('./communs/connect.php');
    require_once('./models/MotCle.php');
    require_once('./models/backend/BackDriver.php');


    class MotCleController 
    {

        private $driver;

        public function __construct($base)
        {
          $this->driver= new BackDriver($base);

        }

        /******************************************************************************/
    
        public function supprimerMotcle($idmcl){
                        
          $this->driver->suppressMotcle($idmcl);
        }


        public function detailMotcle($donnMcl)
        {
                  
          require_once('./views/backend/detailMcl.php');
  
        }


      /********************************************************************** */


        public function verifierMotcle() 
        {
          if (isset($_POST['ajoutmcl'])) {
              $mbr1= new MotCle();
  
              $nomrev1= explode("//",$_POST['idartcl']); 
              $idArt= (int)$nomrev1[0];
              $idLvR= (int)$nomrev1[1];
      
              $str = str_replace("$", " ", $nomrev1[2], $count);$nomrev1[2]=$str;
              $ttrArt= $nomrev1[2];

              $mbr1->setNom_motcle($_POST['nom']);
              $mbr1->setId_livre($idLvR);
              $mbr1->setId_artcl($idArt);

              $listmbr= $this->driver->listeMotsCles(0, $mbr1->getNom_motcle(), $mbr1->getId_livre(), $mbr1->getId_artcl(), 0);
              
                if (count($listmbr) == 0) {
                  return $mbr1;
                } else { $mbr1->setNom_motcle("-1"); return $mbr1; }    
          }
        }
 

        public function registerMcl($donnArt){
            
          require_once('./views/backend/registerMcl.php');
  
      }


        public function tableauRecapMotCle($dataA) {
        
            $numDim= count($dataA);
            
            $idData=[]; $nomData=[]; $idlData=[]; $idaData=[];

            $jN=0;$jI=0;$jR=0;$jA=0;
            for ($j=0; $j<count($dataA); $j++) {

              $idData[$j]=$dataA[$j]->getId_motcle();
              $nomData[$j]=$dataA[$j]->getNom_motcle();
              $idlData[$j]=$dataA[$j]->getId_livre();
              $idaData[$j]=$dataA[$j]->getId_artcl();

            }
        
            natcasesort($idData);
            $nomData = array_unique($nomData);
            natcasesort($nomData);
            $idlData = array_unique($idlData);
            natcasesort($idlData);
            $idaData = array_unique($idaData);
            natcasesort($idaData);
        
            require_once('./views/backend/afficheTableauMotCle.php');    // exec 9
          }
         

}

?>



    

