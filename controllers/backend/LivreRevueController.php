<?php 
    require_once('./controllers/backend/AuthController.php');
    require_once('./communs/connect.php');
    require_once('./models/LivreRevue.php');
    require_once('./models/backend/BackDriver.php');


    class LivreRevueController 
    {

        private $driver;

        public function __construct($base)
        {
          $this->driver= new BackDriver($base);

        }

        /******************************************************************************/

        public function supprimerLivreRevue($idlvr){
                
          $this->driver->suppressLivreRevue($idlvr);
        }

        public function modifierLivreRevue($donnLvR, $donnRev, $donnAut1, $donnAut2, $donnEdt1, $donnEdt2) {

          if (isset($_GET['id'])) {

              require_once('./views/backend/modifDetailLvR.php');
        
          }
        }


/******************************************************************************/

      public function detailLivreRevue($donnLvR){
                  
        require_once('./views/backend/detailLvR.php');

      }
    
        public function registerLvR($donnRev, $donnAut1, $donnAut2, $donnEdt1, $donnEdt2){
            
          require_once('./views/backend/registerLvR.php');
  
      }

      public function verifierLivre() {
     
 
        if (isset($_POST['ajoutlvrv'])) {
            $mbr1= new LivreRevue();

            $nomrev1= explode("//",$_POST['nomrev']);       
            $str = str_replace("$", " ", $nomrev1[0], $count);$nomrev1[0]=$str;
            $str = str_replace("$", " ", $nomrev1[1], $count);$nomrev1[1]=$str;

            if (isset($_POST['type']) && $_POST['type'] == "2") { 
              $mbr1->setNom_specif_revue($nomrev1[0]);
              $mbr1->setPeriodicite((int)$nomrev1[1]); 
            } else { $mbr1->setNom_specif_revue(""); }
            
            if (isset($_POST['titre'])) { $mbr1->setTitre($_POST['titre']); }
              
            $listmbr1= $this->driver->listeLivreRevue("", "", $mbr1->getNom_specif_revue(), "", 0, 0, "toutes");
            $listmbr2= $this->driver->listeLivreRevue("", "", $mbr1->getTitre(), "", 0, 0, "toutes");
            
            if  (isset($_POST['type']) && $_POST['type'] == "2") {
                if (count($listmbr1) == 0 && count($listmbr2) == 0) { return $mbr1;
                } else { $mbr1->setNom_specif_revue("-1");$mbr1->setTitre("-1"); return $mbr1; }
            } else {
              if (count($listmbr2) == 0) {
                return $mbr1;
              } else { $mbr1->setNom_specif_revue("-1");$mbr1->setTitre("-1"); return $mbr1; }    
            }    
        }
      }


        public function tableauRecapLivreRevue($dataA) {
        
    
            $numDim= count($dataA);
        
            $idData=[];$refData=[];$nomData=[];$autData=[];$anneeData=[];
        
            $jN=0;$jA=0;
            for ($j=0; $j<count($dataA); $j++) {
              $idData[$j]=$dataA[$j]->getId_livre();
              $refData[$j]=$dataA[$j]->getReference();
              if ($dataA[$j]->getTitre() <> "") {
                $nomData[$jN]=$dataA[$j]->getTitre();$jN++;
              }
              if ($dataA[$j]->getNom_specif_revue() <> "") {
                $nomData[$jN]=$dataA[$j]->getNom_specif_revue();$jN++;
              }
              if ($dataA[$j]->getAuteur_princ1_nom() <> "") {
                $autData[$jA]=$dataA[$j]->getAuteur_princ1_nom();$jA++;
              }
              if ($dataA[$j]->getAuteur_princ1_institution() <> "") {
                $autData[$jA]=$dataA[$j]->getAuteur_princ1_institution();$jA++;
              }
              if ($dataA[$j]->getAuteur_princ2_nom() <> "") {
                $autData[$jA]=$dataA[$j]->getAuteur_princ2_nom();$jA++;
              }
              if ($dataA[$j]->getAuteur_princ2_institution() <> "") {
                $autData[$jA]=$dataA[$j]->getAuteur_princ2_institution();$jA++;
              }
              if ($dataA[$j]->getRedac_chef_nom() <> "") {
                $autData[$jA]=$dataA[$j]->getRedac_chef_nom();$jA++;
              }
              if ($dataA[$j]->getRedac_chef_institution() <> "") {
                $autData[$jA]=$dataA[$j]->getRedac_chef_institution();$jA++;
              }
              if ($dataA[$j]->getEditeur1() <> "") {
                $autData[$jA]=$dataA[$j]->getEditeur1();$jA++;
              }
              if ($dataA[$j]->getEditeur2() <> "") {
                $autData[$jA]=$dataA[$j]->getEditeur2();$jA++;
              }
              $anneeData[$j]=date("Y", strtotime($dataA[$j]->getDate_parution()));

            }
        
            natcasesort($refData);
            $nomData = array_unique($nomData);
            natcasesort($nomData);
            $autData = array_unique($autData);
            natcasesort($autData);
            $anneeData = array_unique($anneeData);
            natcasesort($anneeData);
        
            require_once('./views/backend/afficheTableauLivreRevue.php');    // exec 9
          }
        
          public function genererNumCompteur() {
            $dataA= $this->driver->nouvNumCompteur();
            return $dataA;
          }

          

}

?>



    

