<?php 
    require_once('./controllers/backend/AuthController.php');
    require_once('./communs/connect.php');
    require_once('./models/Specif_revue.php');
    require_once('./models/backend/BackDriver.php');

    class SpecifRevueController 
    {

        private $driver;

        public function __construct($base)
        {
          $this->driver= new BackDriver($base);

        }

        /******************************************************************************/


        public function supprimerRevue($idrev){

          $this->driver->suppressRevue($idrev);
        }

        
        public function detailSpecifRevue($donnRev)
        {
                  
          require_once('./views/backend/detailRev.php');
  
        }


        public function modifierSpecifRevue($donnRev) {

            if (isset($_GET['id'])) {

                require_once('./views/backend/modifDetailRev.php');
          
            }
        }


      /********************************************************************** */


        public function registerRev(){
            
          require_once('./views/backend/registerRev.php');
  
      }
  

        public function tableauRecapSpecifRevue($dataA) {
        
            $numDim= count($dataA);

            $idData=[]; $nomData=[]; $perData=[];

            for ($j=0; $j<count($dataA); $j++) {

              $idData[$j]=$dataA[$j]->getId_specif_revue();
              $nomData[$j]=$dataA[$j]->getNom_specif_revue();
              $perData[$j]=$dataA[$j]->getPeriodicite();

            }
        
            natcasesort($idData);
            $nomData = array_unique($nomData);
            natcasesort($nomData);
            $perData = array_unique($perData);
            natcasesort($perData);
        
            
            require_once('./views/backend/afficheTableauSpecifRevue.php');    // exec 9
          }
        
         
          public function verifierRevue() {
     
 
             if (isset($_POST['ajoutrev'])) {
                 $mbr1= new Specif_revue();
 
                if (isset($_POST['nom'])) { $mbr1->setNom_specif_revue($_POST['nom']); }
                if (isset($_POST['period'])) { $mbr1->setPeriodicite((int)htmlentities(trim($_POST['period']))); }
                if (isset($_POST['horsserie'])) { $mbr1->setHors_serie(1);} 
                  else { $mbr1->setHors_serie(0);}
                if (isset($_POST['prix'])) { $mbr1->setPrix((int)htmlentities(trim($_POST['prix']))); }
                $mbr1->setMonnaie("Euro");

                 
                 $listmbr= $this->driver->listeSpecifRevue(0, $mbr1->getNom_specif_revue(), 0, 1);
                 // verifier l'existence de ce nouveau membre
 
             if (count($listmbr) == 0) {
                 return $mbr1;
                 
             } else {
                 $mbr1->setNom_specif_revue("-1");
                 return $mbr1;

             }
 
      
         }
       
     
     }
     


}

?>



    

