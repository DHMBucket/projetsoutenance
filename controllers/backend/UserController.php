<?php

require_once('./communs/connect.php');
require_once('./models/backend/BackDriver.php');
require_once('./models/User.php');
require_once('./models/ClientMembre.php');

class UserController{

    private $driver;

    public function __construct($base){
    
        $this->driver = new BackDriver($base);
    }
/******************************************************************************/

public function tableauVoirMargeBrute($donnMB, $deb, $fn){
            
    require_once('./views/backend/afficherMargeBrute.php');

}



/********************************************************************************/
    
    public function supprimerUser($idusr){
                    
        $this->driver->suppressUser($idusr);
    }



    public function detailUser($donnUsr){
                  
        require_once('./views/backend/detailUsr.php');

      }
    
    public function modifierUser($donnUsr, $chg1nochg0) {

        if (isset($_GET['id'])) {

            require_once('./views/backend/modifDetailUsr.php');
      
        }
    }


    /********************************************************************** */

    public function login(){

        try {
        if(!empty($_POST['pseudo']) && strlen($_POST['pseudo'])>=4){

            $pseudo = trim(htmlspecialchars(addslashes($_POST['pseudo'])));
            $pass = md5(trim(htmlspecialchars(addslashes($_POST['pass']))));


            $user = new User();
            $user->setPseudo($pseudo);
            $user->setNom($pseudo);           // changement
            $user->setEmail($pseudo);   // changement
            $user->setPass($pass);

            $userObj = $this->driver->getUsers($user);
            if ($userObj->getId_util() != 0 && $userObj->getOperationnel() == 1) {
                $_SESSION['Auth'] = array('pseudo'=>$userObj->getPseudo(),'numero'=>$userObj->getId_util(),'pass'=>$userObj->getPass(),'role'=>$userObj->getRole(),'operationnel'=>$userObj->getOperationnel());
                if (isset($_SESSION['panier'])) {
                    $typClt= $_SESSION['Auth']['role'];
                    $idAuth= $_SESSION['Auth']['numero'];
                    $idAuthAncien= $_SESSION['AuthClt']['numero'];                                                        
                    $panier= $this->driver->listePanier(0, $_SESSION['Auth']['numero'], $typClt, 0,
                    "-1", 0, 0, "-1", 0, 0, "-1", 0, "", 0);
                    
                    if (count($panier)) {
                        for ($l=0; $l<3; $l++) {

                            $_SESSION['panier']=$panier[0]->getNbre1()+$panier[0]->getNbre2()+$panier[0]->getNbre3();

                        }
                    } else {
                        $this->driver->changerIdClientPanier($idAuthAncien, $idAuth, $typClt);
                    } 
                }               

                header('Location:./index.php?action=securite&securite');
            } else {                   
                $userObj = $this->driver->getClients($user);
                if ($userObj->getId_util() != 0) {
                    $_SESSION['Auth'] = array('pseudo'=>$userObj->getPseudo(),'numero'=>$userObj->getId_util(),'pass'=>$userObj->getPass(),'role'=>$userObj->getRole(),'operationnel'=> '0');
                    
                    if (isset($_SESSION['panier'])) {
                        $typClt= $_SESSION['Auth']['role'];
                        $idAuth= $_SESSION['Auth']['numero'];
                        $idAuthAncien= $_SESSION['AuthClt']['numero'];
                        $panier= $this->driver->listePanier(0, $_SESSION['Auth']['numero'], $typClt, 0,
                        "-1", 0, 0, "-1", 0, 0, "-1", 0, "", 0);
                        
                        if (count($panier)) {
                            for ($l=0; $l<3; $l++) {

                                $_SESSION['panier']=$panier[0]->getNbre1()+$panier[0]->getNbre2()+$panier[0]->getNbre3();

                            }
                        } else {
                            $this->driver->changerIdClientPanier($idAuthAncien, $idAuth, $typClt);
                        } 
                    }

                    header('Location:./index.php?action=securite&securite');
                } else {
                    $erreur = '<div class="alert alert-danger">
                                    <strong>
                                    Votre pseudo ou mot de passe ne correspondent pas!
                                    </strong> 
                                </div>';
                    echo $erreur;
                }
            }
        } else {
            
        }
        
        } catch(Exception $ex) {
            die("Erreur de traitement<br>".$ex->getMessage());
        } //finally { $base= null;}

        require_once('./views/backend/login.php');
    }

    public function logout(){
        if (isset($_SESSION['Auth'])) {
            session_destroy();
            session_start();
            session_unset();
            $_SESSION ["icompteur"]=0;
            $num= $this->driver->nouvNumCompteur();
            $_SESSION['AuthClt'] = array('pseudo'=>$num[0]->id_compteur,'numero'=>$num[0]->id_compteur,'pass'=>"",'role'=>6);
        } else {
            if (!isset($_SESSION)) {
                session_start();

                $_SESSION ["icompteur"]=0;
                if (!isset($_SESSION['AuthClt'])) {
                    $num= $this->driver->nouvNumCompteur();
                    $_SESSION['AuthClt'] = array('pseudo'=>$num[0]->id_compteur,'numero'=>$num[0]->id_compteur,'pass'=>"",'role'=>6);
                }
            }
        }
        
    }

    public function registerUsr(){
        
            try {
                
                if (isset($_POST["mdp"]) && isset($_POST["mdpbis"])) {

                    if (strcmp($_POST["mdp"],$_POST["mdpbis"]) == 0) {

                        $pseudo = trim(htmlspecialchars(addslashes($_POST['pseudo'])));
                        $nom = trim(htmlspecialchars(addslashes($_POST['nom'])));
                        $prenom = trim(htmlspecialchars(addslashes($_POST['prenom'])));
                        $email = trim(htmlspecialchars(addslashes($_POST['email'])));
                        $pass = md5(trim(htmlspecialchars(addslashes($_POST['mdp']))));
                        $role = (int)htmlentities(trim(htmlspecialchars(addslashes($_POST['role']))));

                        $user = new User();
                        $user->setPseudo($pseudo);
                        $user->setNom($nom);
                        $user->setPrenom($prenom);
                        $user->setEmail($email);
                        $user->setPass($pass);
                        $user->setRole($role);
                        $user->setDate_created(date("Y-m-d"));

                        $userObj = $this->driver->getUsers($user);
                        if($userObj->getId_util() != 0){
                            echo "Vous êtes déjà inscrit avec ces pseudo et mot de passe! Changez-les si vous le désirez";
                        } else {
                            //echo "Tout Ok";
                            $this->driver->ajoutUtilisateur($user);
                            header('Location:./index.php?action=tableau&tableau=tableauUsr');
                            
                        }
            
                    } else {
                        echo "Veuillez reconfirmer votre mot de passe !";
                    }
                } 
        
            } catch(Exception $ex) {
                die("Erreur d'ouverture de connexion à la BDD<br>".$ex->getMessage());
            } finally {
                $base= null;
            }
            
        require_once('./views/backend/registerUsr.php');

    }


    public function registerInv($donnMonn){
            
        require_once('./views/backend/registerInv.php');

    }


    public function registerAbonnemtMbr($donnMonn, $donnRevue, $dataM){
            
        require_once('./views/backend/registerAbonnemtMbr.php');

    }


    public function registerAbonnemtUsr($donnMonn, $donnRevue, $dataM){
            
        require_once('./views/backend/registerAbonnemtUsr.php');

    }

    public function registerCommandMbr($donnMonn, $dataM){
            
        require_once('./views/backend/registerCommandMbr.php');

    }


    public function registerCommandUsr($donnMonn, $dataM){
            
        require_once('./views/backend/registerCommandUsr.php');

    }


    public function registerClt($donnMonn){
            
        require_once('./views/backend/registerClt.php');

    }


    public function tableauRecapUser($dataA) {
        
        $numDim= count($dataA);
        
        $idData=[]; $nomData=[]; $mailData=[]; $rolData=[]; $dteData=[];

        $jN=0;
        for ($j=0; $j<count($dataA); $j++) {

          $idData[$j]=$dataA[$j]->getId_util();
          $mailData[$j]=$dataA[$j]->getEmail();
          $rolData[$j]=$dataA[$j]->getRole();
          $dteData[$j]=$dataA[$j]->getDate_created();

          if ($dataA[$j]->getNom() <> "") {
            $nomData[$jN]=$dataA[$j]->getNom();$jN++;
          }
          if ($dataA[$j]->getPrenom() <> "") {
            $nomData[$jN]=$dataA[$j]->getPrenom();$jN++;
          }
          if ($dataA[$j]->getPseudo() <> "") {
            $nomData[$jN]=$dataA[$j]->getPseudo();$jN++;
          }

        }
    
        natcasesort($idData);
        natcasesort($mailData);
        $nomData = array_unique($nomData);
        natcasesort($nomData);
        $rolData = array_unique($rolData);
        natcasesort($rolData);
        $dteData = array_unique($dteData);
        natcasesort($dteData);
    
        require_once('./views/backend/afficheTableauUser.php');    // exec 9
      }
    

      public function verifierAccesAction($action, $cible) {
        switch ($action) {
                    
            case "synthese" : {
                if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                        $_SESSION['Auth']['role'] == 1  && $_SESSION['Auth']['operationnel'] == 1)) 
                { header('location: ./index.php?action=login');}
                break;
            }

            case "supprimer" : {
                if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                        $_SESSION['Auth']['role'] == 1  && $_SESSION['Auth']['operationnel'] == 1)) 
                { header('location: ./index.php?action=login');}
                break;
            }

            case "detail" : {
                if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                        $_SESSION['Auth']['role'] == 1  && $_SESSION['Auth']['operationnel'] == 1)) 
                { header('location: ./index.php?action=login');}
                break;
            }

            case "modifierUsr" : {
                if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                        $_SESSION['Auth']['role'] == 1  && $_SESSION['Auth']['operationnel'] == 1)) 
                { header('location: ./index.php?action=login');}
                break;
            }
            case "modifierArt" : {
                if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                        $_SESSION['Auth']['role'] == 1  && $_SESSION['Auth']['operationnel'] == 1)) 
                { header('location: ./index.php?action=login');}
                break;
            }
            case "modifierThm" : {
                if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                        $_SESSION['Auth']['role'] == 1  && $_SESSION['Auth']['operationnel'] == 1)) 
                { header('location: ./index.php?action=login');}
                break;
            }
            case "modifierRev" : {
                if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                        $_SESSION['Auth']['role'] == 1  && $_SESSION['Auth']['operationnel'] == 1)) 
                { header('location: ./index.php?action=login');}
                break;
            }
            case "modifierMbr" : {
                if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                        $_SESSION['Auth']['role'] == 1  && $_SESSION['Auth']['operationnel'] == 1)) 
                { header('location: ./index.php?action=login');}
                break;
            }
            case "modifierMon" : {
                if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                        $_SESSION['Auth']['role'] == 1  && $_SESSION['Auth']['operationnel'] == 1)) 
                { header('location: ./index.php?action=login');}
                break;
            }
            case "modifierCmd" : {
                if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                        $_SESSION['Auth']['role'] == 1  && $_SESSION['Auth']['operationnel'] == 1)) 
                { header('location: ./index.php?action=login');}
                break;
            }
            case "modifierEdt" : {
                if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                        $_SESSION['Auth']['role'] == 1  && $_SESSION['Auth']['operationnel'] == 1)) 
                { header('location: ./index.php?action=login');}
                break;
            }
            case "modifierAut" : {
                if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                        $_SESSION['Auth']['role'] == 1  && $_SESSION['Auth']['operationnel'] == 1)) 
                { header('location: ./index.php?action=login');}
                break;
            }
            case "modifierLvR" : {
                if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                        $_SESSION['Auth']['role'] == 1  && $_SESSION['Auth']['operationnel'] == 1)) 
                { header('location: ./index.php?action=login');}
                break;
            }

            case "modifier" : {
                if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                        $_SESSION['Auth']['role'] == 1  && $_SESSION['Auth']['operationnel'] == 1)) 
                { header('location: ./index.php?action=login');}
                break;
            }

            case "abonnerMbr" : {
                if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                       $_SESSION['Auth']['role'] < 6)) 
                { header('location: ./index.php?action=login');}
                break;
            }

            case 'abonnement' : {
                if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                       $_SESSION['Auth']['role'] < 6)) 
                { header('location: ./index.php?action=login');}
                break;
            }


            case "paiementacceptAbonnemt" : {
                if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                       $_SESSION['Auth']['role'] < 6)) 
                { header('location: ./index.php?action=login');}
                break;
            }

            case 'passercommande' : {
                if ($cible == 'verifier') {
                    if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                            $_SESSION['Auth']['role'] < 6)) 
                    { header('location: ./index.php?action=login');}
                }
                break;
            }


            case "tableau" : {
            if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                    $_SESSION['Auth']['role'] < 5)) 
            { header('location: ./index.php?action=login');}
            break;
            } 

            case "searchCrit" : {
            if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                    $_SESSION['Auth']['role'] < 5)) 
            { header('location: ./index.php?action=login');}
            break;
            }


            case "search" : {
                if ($cible == 'searchTabLvR') {
                    if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                            $_SESSION['Auth']['role'] < 5)) 
                    { header('location: ./index.php?action=login');}
                }
                break;
            }


            case "ajouterArt" : {
            if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                    $_SESSION['Auth']['role'] < 3  && $_SESSION['Auth']['operationnel'] == 1)) 
            { header('location: ./index.php?action=login');}
            break;
            }
            case "ajouterEdt" : {
            if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                    $_SESSION['Auth']['role'] < 3  && $_SESSION['Auth']['operationnel'] == 1)) 
            { header('location: ./index.php?action=login');}
            break;
            }
            case "ajouterAut" : {
            if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                    $_SESSION['Auth']['role'] < 3  && $_SESSION['Auth']['operationnel'] == 1)) 
            { header('location: ./index.php?action=login');}
            break;
            }
            case "ajouterThm" : {
            if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                    $_SESSION['Auth']['role'] < 3  && $_SESSION['Auth']['operationnel'] == 1)) 
            { header('location: ./index.php?action=login');}
            break;
            }
            case "ajouterMcl" : {
            if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                    $_SESSION['Auth']['role'] < 3  && $_SESSION['Auth']['operationnel'] == 1)) 
            { header('location: ./index.php?action=login');}
            break;
            }
            case "ajouterLvR" : {
            if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                    $_SESSION['Auth']['role'] < 3  && $_SESSION['Auth']['operationnel'] == 1)) 
            { header('location: ./index.php?action=login');}
            break;
            }
            case "ajouterRev" : {
            if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                    $_SESSION['Auth']['role'] < 3  && $_SESSION['Auth']['operationnel'] == 1)) 
            { header('location: ./index.php?action=login');}
            break;
            }
            case "ajouterMbr" : {
            if ((AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                    $_SESSION['Auth']['role'] > 2 && $_SESSION['Auth']['operationnel'] < 1)) 
            { header('location: ./index.php?action=login');}
            break;
            }

            case 'login' : {
            if (AuthController::isLogged()) 
            { header('location: ./index.php');}
            break;
            }
            case 'logout' : {
            if (!(AuthController::isLogged())) 
            { header('location: ./index.php');}
            break;
            }
            
            case 'register' : {
                if ($cible <> 'client') {
                    if (!(AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                            $_SESSION['Auth']['role'] < 3  && $_SESSION['Auth']['operationnel'] == 1)) 
                    { header('location: ./index.php?action=login');}
                } else {
                    if (AuthController::isLogged() && isset($_SESSION['Auth']['role']) && 
                            $_SESSION['Auth']['role'] > 2  && $_SESSION['Auth']['role'] < 6) 
                    { header('location: ./index.php');}
                }
                break;
            }
      }

            /******************************************* */
            //case "commanderMbr"
            //case "paiementacceptAdherCherch" 
            /******************************************* */
            //case "securite"
            //case "ajouterInv"
            //case "paiementacceptCommand"
            //case "voirpanier"
            //case "changernbrepanier"
            //case "reservPanier"
            //case "initial"

    }


}


