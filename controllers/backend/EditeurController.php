<?php 
    require_once('./controllers/backend/AuthController.php');
    require_once('./communs/connect.php');
    require_once('./models/Editeur.php');
    require_once('./models/backend/BackDriver.php');


    class EditeurController 
    {

        private $driver;

        public function __construct($base)
        {
          $this->driver= new BackDriver($base);

        }

        /******************************************************************************/

        public function supprimerEditeur($idedt){
                
          $this->driver->suppressEditeur($idedt);
        }

               
        public function detailEditeur($donnEdt)
        {
                  
          require_once('./views/backend/detailEdt.php');
  
        }


        public function modifierEditeur($donnEdt) {

          if (isset($_GET['id'])) {

              require_once('./views/backend/modifDetailEdt.php');
        
          }
      }


      /********************************************************************** */


        public function verifierEditeur() {
     
          if (isset($_POST['ajoutedt'])) {
              $mbr1= new Editeur();
  
              $mbr1->setCode_editeur($_POST['code']);
              $mbr1->setNom_editeur($_POST['nom']);
              $mbr1->setLocalite_editeur($_POST['localite']);
              $mbr1->setPays_editeur($_POST['pays']);
              
              $listmbr1=$this->driver->listeEditeur(0, $mbr1->getCode_editeur(), $mbr1->getNom_editeur(), $mbr1->getLocalite_editeur(), 0);
              $listmbr2=$this->driver->listeEditeur(0, $mbr1->getCode_editeur(), $mbr1->getNom_editeur(), $mbr1->getPays_editeur(), 0);

              if (count($listmbr1) == 0 && count($listmbr2) == 0) {
                  return $mbr1;
              } else { $mbr1->setNom_editeur("-1"); return $mbr1; }    
                  
          }
        }


        public function registerEdt(){
            
          require_once('./views/backend/registerEdt.php');
  
      }


        public function tableauRecapEditeur($dataA) {
            $numDim= count($dataA);        
 
            $idData=[];$cdeData=[];$nomData=[];$locData=[];$frlData=[];
        
            $jN=0;
            for ($j=0; $j<count($dataA); $j++) {
              $idData[$j]=$dataA[$j]->getId_editeur();
              $cdeData[$j]=$dataA[$j]->getCode_editeur();
              $nomData[$j]=$dataA[$j]->getNom_editeur();
        
              if ($dataA[$j]->getPays_editeur() <> "") {
                $locData[$jN]=$dataA[$j]->getPays_editeur();$jN++;
              }
              if ($dataA[$j]->getLocalite_editeur() <> "") {
                $locData[$jN]=$dataA[$j]->getLocalite_editeur();$jN++;
              }
               
              $frlData[$j]=$dataA[$j]->getFrs_livrais();
            }
        
            natcasesort($cdeData);
            $nomData = array_unique($nomData);
            natcasesort($nomData);
            $locData = array_unique($locData);
            natcasesort($locData);
            $frlData = array_unique($frlData);
            natcasesort($frlData);
        
            require_once('./views/backend/afficheTableauEditeur.php');    // exec 9
        
          }
        

}

?>



    

