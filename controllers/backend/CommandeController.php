<?php 
    require_once('./controllers/backend/AuthController.php');
    require_once('./communs/connect.php');
    require_once('./models/Commande.php');
    require_once('./models/backend/BackDriver.php');


    class CommandeController 
    {

        private $driver;

        public function __construct($base)
        {
          $this->driver= new BackDriver($base);

        }


        /******************************************************************************/

        public function supprimerCommande($idcmd){
        
          $this->driver->suppressCommande($idcmd);
        }
      
              
                      
        public function detailCommande($donnCmd)
        {
                  
          require_once('./views/backend/detailCmd.php');
  
        }


        public function modifierCommande($donnCmd, $donnMonn) {

          if (isset($_GET['id'])) {

              require_once('./views/backend/modifDetailCmd.php');
        
          }
      }


      /********************************************************************** */


        public function tableauRecapCommande($dataA) {
        
    
            $numDim= count($dataA);

            $idData=[];$refData=[];$trscData=[];$typData=[];$idcData=[];
            $idlData=[];$rflData=[];$idaData=[];$modData=[];$locData=[];
            $dolData=[];$dlpData=[];$dlrData=[];$ttrData=[];

            $jN=0;
            for ($j=0; $j<count($dataA); $j++) {

              $idData[$j]=$dataA[$j]->getId_commande();
              $refData[$j]=$dataA[$j]->getRef_commande();
              $trscData[$j]=$dataA[$j]->getRef_transaction();
              $typData[$j]=$dataA[$j]->getType_client();
              $idcData[$j]=$dataA[$j]->getId_client();
              $idlData[$j]=$dataA[$j]->getId_livre();
              $rflData[$j]=$dataA[$j]->getRef_livre();
              $idaData[$j]=$dataA[$j]->getId_artcl();
              $modData[$j]=$dataA[$j]->getMode_payment();
              $locData[$j]=$dataA[$j]->getPays();
              $dolData[$j]=date("Y-m-d", strtotime($dataA[$j]->getDate_ordre_livrais()));
              $dlpData[$j]=date("Y-m-d", strtotime($dataA[$j]->getDate_livrais_prev()));
              $dlrData[$j]=date("Y-m-d", strtotime($dataA[$j]->getDate_livrais_real()));
              if ($dataA[$j]->getTtr_livre() <> "") {
                $ttrData[$jN]=$dataA[$j]->getTtr_livre();$jN++;
              }
              if ($dataA[$j]->getTtr_artcl() <> "") {
                $ttrData[$jN]=$dataA[$j]->getTtr_artcl();$jN++;
              }

            }
        
            natcasesort($idData);
            $refData = array_unique($refData);
            natcasesort($refData);
            $trscData = array_unique($trscData);
            natcasesort($trscData);
            $typData = array_unique($typData);
            natcasesort($typData);
            $idcData = array_unique($idcData);
            natcasesort($idcData);
            $idlData = array_unique($idlData);
            natcasesort($idlData);
            $rflData = array_unique($rflData);
            natcasesort($rflData);
            $idaData = array_unique($idaData);
            natcasesort($idaData);
            $modData = array_unique($modData);
            natcasesort($modData);
            $locData = array_unique($locData);
            natcasesort($locData);
            $dolData = array_unique($dolData);
            natcasesort($dolData);
            $dlpData = array_unique($dlpData);
            natcasesort($dlpData);
            $dlrData = array_unique($dlrData);
            natcasesort($dlrData);
            $ttrData = array_unique($ttrData);
            natcasesort($ttrData);
        
            require_once('./views/backend/afficheTableauCommande.php');    // exec 9
          }
         

          public function tableauVoirCommande($dataA) {
        
    
            $numDim= count($dataA);
          
            $idData=[];$refData=[];$trscData=[];$typData=[];$idcData=[];
            $idlData=[];$rflData=[];$idaData=[];$modData=[];$locData=[];
            $dolData=[];$dlpData=[];$dlrData=[];$ttrData=[];
          
            $jN=0;
            for ($j=0; $j<count($dataA); $j++) {
          
              $idData[$j]=$dataA[$j]->getId_commande();
              $refData[$j]=$dataA[$j]->getRef_commande();
              $trscData[$j]=$dataA[$j]->getRef_transaction();
              $typData[$j]=$dataA[$j]->getType_client();
              $idcData[$j]=$dataA[$j]->getId_client();
              $idlData[$j]=$dataA[$j]->getId_livre();
              $rflData[$j]=$dataA[$j]->getRef_livre();
              $idaData[$j]=$dataA[$j]->getId_artcl();
              $modData[$j]=$dataA[$j]->getMode_payment();
              $locData[$j]=$dataA[$j]->getPays();
              $dolData[$j]=date("Y-m-d", strtotime($dataA[$j]->getDate_ordre_livrais()));
              $dlpData[$j]=date("Y-m-d", strtotime($dataA[$j]->getDate_livrais_prev()));
              $dlrData[$j]=date("Y-m-d", strtotime($dataA[$j]->getDate_livrais_real()));
              if ($dataA[$j]->getTtr_livre() <> "") {
                $ttrData[$jN]=$dataA[$j]->getTtr_livre();$jN++;
              }
              if ($dataA[$j]->getTtr_artcl() <> "") {
                $ttrData[$jN]=$dataA[$j]->getTtr_artcl();$jN++;
              }
          
            }
          
            natcasesort($idData);
            $refData = array_unique($refData);
            natcasesort($refData);
            $trscData = array_unique($trscData);
            natcasesort($trscData);
            $typData = array_unique($typData);
            natcasesort($typData);
            $idcData = array_unique($idcData);
            natcasesort($idcData);
            $idlData = array_unique($idlData);
            natcasesort($idlData);
            $rflData = array_unique($rflData);
            natcasesort($rflData);
            $idaData = array_unique($idaData);
            natcasesort($idaData);
            $modData = array_unique($modData);
            natcasesort($modData);
            $locData = array_unique($locData);
            natcasesort($locData);
            $dolData = array_unique($dolData);
            natcasesort($dolData);
            $dlpData = array_unique($dlpData);
            natcasesort($dlpData);
            $dlrData = array_unique($dlrData);
            natcasesort($dlrData);
            $ttrData = array_unique($ttrData);
            natcasesort($ttrData);
          
            require_once('./views/backend/afficheTableauCommandeClient.php');    // exec 9
          }
          




}



?>



    

