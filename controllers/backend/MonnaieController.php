<?php 
    require_once('./controllers/backend/AuthController.php');
    require_once('./communs/connect.php');
    require_once('./models/Monnaie.php');
    require_once('./models/backend/BackDriver.php');


    class MonnaieController 
    {

        private $driver;

        public function __construct($base)
        {
          $this->driver= new BackDriver($base);

        }


        
        public function detailMonnaie($donnMon)
        {
                  
          require_once('./views/backend/detailMon.php');
  
        }

        public function modifierMonnaie($donnMon) {

          if (isset($_GET['id'])) {

              require_once('./views/backend/modifDetailMon.php');
        
          }
      }


      /********************************************************************** */



        public function tableauRecapMonnaie($dataA) {
    
            $numDim= count($dataA);        
 
            $idData=[];$nomData=[];$symbData=[];
        
            for ($j=0; $j<count($dataA); $j++) {
                $idData[$j]=$dataA[$j]->getId_monnaie();        
              
                $nomData[$j]=$dataA[$j]->getNom_monnaie();
                
                $fctData[$j]=$dataA[$j]->getSymb_monnaie();
        
              }
        
            $nomData = array_unique($nomData);
            natcasesort($nomData);
            $symbData = array_unique($symbData);
            natcasesort($symbData);
        
            require_once('./views/backend/afficheTableauMonnaie.php');    // exec 9
        
          }
        

}

?>



    

