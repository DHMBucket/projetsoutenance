    $(document).ready(function(){

        var lastWidth = $(window).width();
        var initialWidth = $(window).width();
        var divw1 = new Array('#fixsz1'); divw1 = divw1.join(',');
        var divph1 = new Array('#ph1'); divph1 = divph1.join(',');
        var divw2 = new Array('#fixsz2'); divw2 = divw2.join(',');
        var divph2 = new Array('#ph2'); divph2 = divph2.join(',');
        
        if ($( window ).width() < 560) {
            let maxl= 100;
            $(divw1).removeClass().addClass( "container col-8 offset-2 w-100" );
            $(divw2).removeClass().addClass( "container col-8 w-100" );
            $(divph1).css({"height": "70%", "width": "70%"});
            $(divph2).css({"height": "70%", "width": "70%"});
            $('#headId').css({"font-size": maxl + "%", "width": maxl + "%"});
            $('#spH').css({"font-size": maxl + "%", "width": maxl + "%"});
              
            $('#sequence').css({"font-size": maxl + "%", "width": "100%"});
            $('#seqrech').css({"font-size": maxl + "%", "width": "100%"});

            $('form').css({"font-size": maxl + "%"});
            $('select').css({"font-size": maxl + "%", "width": "100%"});
        }
        if ($( window ).width() < 1000 && $( window ).width() >= 560) {
            let maxl= $( window ).width()/initialWidth*100; if (maxl < 65) {maxl= 65;}; if (maxl > 100) {maxl= 100;}
            $(divw1).removeClass().addClass( "container col-8 offset-2 w-100" );
            $(divw2).removeClass().addClass( "container col-8 w-100" );
            $(divph1).css({"height": "85%", "width": "85%"});
            $(divph2).css({"height": "85%", "width": "85%"});
            $('#headId').css({"font-size": maxl + "%", "width": maxl + "%"});
            $('#spH').css({"font-size": maxl + "%", "width": maxl + "%"});
              
            $('#sequence').css({"font-size": "75%", "width": "auto"});
            $('#seqrech').css({"font-size": "75%", "width": "auto"});

            $('form').css({"font-size": maxl + "%"});
            $('select').css({"font-size": maxl + "%", "width": "auto"});

        }
        if ($( window ).width() >= 1000) {
            let maxl= 100;
            $(divw1).removeClass().addClass( "container col-5" );
            $(divw2).removeClass().addClass( "container col-5" );
            $(divph1).css({"height": "100%", "width": "100%"});
            $(divph2).css({"height": "100%", "width": "100%"});

            $('#headId').css({"font-size": maxl + "%", "width": maxl + "%"});
            $('#spH').css({"font-size": maxl + "%", "width": maxl + "%"});
              
            $('#sequence').css({"font-size": maxl + "%", "width": "100%"});
            $('#seqrech').css({"font-size": maxl + "%", "width": "100%"});

            $('form').css({"font-size": maxl + "%"});
            $('select').css({"font-size": maxl + "%", "width": "100%"});

        }    
        //document.location=\"$PHP_SELF?r=1&Largeur=\"+screen.width+\"&Hauteur=\"+screen.height;
        $( window ).resize(function() {
            if ($( window ).width() < 560) {
                console.log("window apres = ", $( window ).width());
                let maxl= 100;
                $(divw1).removeClass().addClass( "container col-8 offset-2 w-100" );
                $(divw2).removeClass().addClass( "container col-8 w-100" );
                $(divph1).css({"height": "70%", "width": "70%"});
                $(divph2).css({"height": "70%", "width": "70%"});
                $('#headId').css({"font-size": maxl + "%", "width": maxl + "%"});
                $('#spH').css({"font-size": maxl + "%", "width": maxl + "%"});
                  
                $('#sequence').css({"font-size": maxl + "%", "width": "100%"});
                $('#seqrech').css({"font-size": maxl + "%", "width": "100%"});

                $('form').css({"font-size": maxl + "%"});
                $('select').css({"font-size": maxl + "%", "width": "100%"});
                //box-sizing resize
                //$('ul').css({"font-size": "50%"});

            }
            if ($( window ).width() < 1000 && $( window ).width() >= 560) {
                //console.log("window avant = ", lastWidth);
                console.log("window apres = ", $( window ).width());    
                let maxl= $( window ).width()/initialWidth*100; if (maxl < 65) {maxl= 65;}; if (maxl > 100) {maxl= 100;}
                $(divw1).removeClass().addClass( "container col-8 offset-2 w-100" );
                $(divw2).removeClass().addClass( "container col-8 w-100" );
                $(divph1).css({"height": "85%", "width": "85%"});
                $(divph2).css({"height": "85%", "width": "85%"});
                $('#headId').css({"font-size": maxl + "%", "width": maxl + "%"});
                $('#spH').css({"font-size": maxl + "%", "width": maxl + "%"});
                  
                $('#sequence').css({"font-size": "75%", "width": "auto"});
                $('#seqrech').css({"font-size": "75%", "width": "auto"});

                $('form').css({"font-size": maxl + "%"});
                $('select').css({"font-size": maxl + "%", "width": "auto"});

                //$('ul').css({"font-size": "50%"});
                //$('li').removeClass("nav-item").addClass( "list-group-item" );
                //$('ul').removeClass("navbar-nav").addClass( "list-group" );

            }
            if ($( window ).width() >= 1000) {
                let maxl= 100;
                $(divw1).removeClass().addClass( "container col-5" );
                $(divw2).removeClass().addClass( "container col-5" );
                $(divph1).css({"height": "100%", "width": "100%"});
                $(divph2).css({"height": "100%", "width": "100%"});

                $('#headId').css({"font-size": maxl + "%", "width": maxl + "%"});
                $('#spH').css({"font-size": maxl + "%", "width": maxl + "%"});
                  
                $('#sequence').css({"font-size": maxl + "%", "width": "100%"});
                $('#seqrech').css({"font-size": maxl + "%", "width": "100%"});

                $('form').css({"font-size": maxl + "%"});
                $('select').css({"font-size": maxl + "%", "width": "100%"});

            }
        //if ($( window ).width() < 600) {
            /*
            console.log("window avant = ", lastWidth);
            console.log("window apres = ", $( window ).width());            
            var resize = new Array('html');
            resize = resize.join(',');
            var originalFontSize = $(resize).css('font-size');
            var originalFontNumber = parseFloat(originalFontSize, 10);
            var newFontSize = originalFontNumber*$( window ).width()/lastWidth;
            $(resize).css('font-size', newFontSize);
            $(divw).css('width', initialWidth*$( window ).width()/lastWidth);
            lastWidth = $(window).width();
            return false;*/
        //}
        //$( "body" ).prepend( "<div>" + $( window ).width() + "</div>" );
      });


        $("#descript1").hide();
        $("#descript2").hide();
       
        $('[type="search"]').focus(function() {
            //alert('ok');
            $(this).attr('class', 'form-control col text-center');
        });
 
 
        $('[type="search"]').blur(function() {
            //alert('ok');
            $(this).attr('placeholder', 'rechercher');  // no
            $(this).attr('value', 'rechercher');  // no
            $(this).attr('class', 'form-control col-4 text-center');
        });


        /*$('#recherche').click(function() {
            let search= $('#motcle').val();
            $.ajax({
                url: 'traitement.php',
                data: {mcle : search},
                type: 'POST',
                success: function(data) {
                    $('#display').html(data);
                }
            });
        });*/


        $('#precedent').click(function() {
            let chn1= './index.php?action=precedent&precedent='+$(this).attr('name');
            window.location.assign(chn1);
        });


        $('#suivant').click(function() {               
            let chn2= './index.php?action=suivant&suivant='+$(this).attr('name');
            window.location.assign(chn2);    
        });


        $("#details1").click(function(){          // c'est bon
            $("#descript1").toggle();
        });


        $("#details2").click(function(){         // c'est bon
            $("#descript2").toggle();
        });


        $("#reserv1").click(function() {
            alert("Value: " + $("#nbligne1").val() + ". Le Panier n'accepte pas la commande de plus de 3 livres/revues différents !");
            let compt= $("#nbligne1").val();
            
        });

        $("#reserv2").click(function() {
            let compt= parseInt($("#nbligne2").val())+1;
            alert("Value: " + compt + ". Le Panier n'accepte pas la commande de plus de 3 livres/revues différents !");
            
        });


    });

    


    $.ajax({
        //url:'admin/traitement.php',
        url:'presentationtraitementLivreRevue.php',
        type: 'GET',
        success: function() {
            $('#display').html();
        }
    });


    

