    $(document).ready(function(){

        function recupExtensionFic(name) {
            idx1= name.indexOf(".");
            if (idx1>-1) {
                var parts= name.split(".");
                var str= parts[parts.length-1];
                str= str.toLowerCase();
                
                if (str == "docx" || str == "doc" || str == "pdf" || str == "jpg") {
                    console.log("xParseBON = "+parts+"  str ="+parts[parts.length-1]);
                    return true;
                } else {
                    console.log("xParsePASBON = "+parts+"  str ="+parts[parts.length-1]);
                    return false;
                }
            } else {
                return false;
            }
           
        }

        function recupExtensionImg(name) {
            idx1= name.indexOf(".");
            if (idx1>-1) {
                var parts= name.split(".");
                var str= parts[parts.length-1];
                str= str.toLowerCase();
                
                if (str == "jpg" || str == "bmp" || str == "png") {
                    console.log("xParseBON = "+parts+"  str ="+parts[parts.length-1]);
                    return true;
                } else {
                    console.log("xParsePASBON = "+parts+"  str ="+parts[parts.length-1]);
                    return false;
                }
            } else {
                return false;
            }
           
        }




        $("#nbpage").click(function(){
           console.log("nbpage = ",$("#nbpage").val());
           if ($("#nbpage").val()<1) {$("#nbpage").val(1)};
        });

        $("#prix").click(function(){
           console.log("prix = ",$("#prix").val());
           if ($("#prix").val()<0) {$("#prix").val(0)};
        });
    
        $("#stkirs").click(function(){
           console.log("stkirs = ",$("#stkirs").val());
           if ($("#stkirs").val()<0) {$("#stkirs").val(0)};
        });
        
        $("#stkedt").click(function(){
           console.log("stkedt = ",$("#stkedt").val());
           if ($("#stkedt").val()<0) {$("#stkedt").val(0)};
        });
            
                                 
        if ($("#selectType").children("option:selected").val() == "1") {
            document.getElementById("nomrev").setAttribute("style", 'background-color: white');
            $("#nomrev").removeAttr('required');
            $("#nomrev").attr('hidden', 'true');
            $("#nrv").attr('hidden', 'true');
            //$("#nomrev").val("");
        } else {
            document.getElementById("nomrev").setAttribute("style", 'background-color: coral');
            $("#nomrev").removeAttr('hidden');
            $("#nrv").removeAttr('hidden');
            $("#nomrev").attr('required', 'true');
    
        }
      
        $("#selectType").change(function(){
           
            if ($("#selectType").children("option:selected").val() == "1") {
                document.getElementById("nomrev").setAttribute("style", 'background-color: white');
                $("#nomrev").removeAttr('required');
                $("#nomrev").attr('hidden', 'true');
                $("#nrv").attr('hidden', 'true');
                //$("#nomrev").val("");
            } else {
                document.getElementById("nomrev").setAttribute("style", 'background-color: coral');
                $("#nomrev").removeAttr('hidden');
                $("#nrv").removeAttr('hidden');
                $("#nomrev").attr('required', 'true');        
            }  
        });

        $("input[name='ficjustif']").change(function(){
            var name= $("input[name='ficjustif']").val();
            //var name= $("input[name='ficjustif']")[0].files[0];
            if (!recupExtensionFic(name)) {
                document.getElementById("fileHelp").setAttribute("style", 'background-color: red; size-weight: 10px;');
                alert('Choix non accepté d\'un fichier, voir son extension !');
                $("input[name='ficjustif']").val("");
            } else {
                document.getElementById("fileHelp").setAttribute("style", "");
                document.getElementById("fileHelp").setAttribute("class", "form-text text-muted");
            }
        });

        $("input[name='image1']").change(function(){
            var name= $("input[name='image1']").val();
            //var name= $("input[name='ficjustif']")[0].files[0];
            if (!recupExtensionImg(name)) {
                document.getElementById("imgHelp").setAttribute("style", 'background-color: red; size-weight: 10px;');
                alert('Choix non accepté d\'un fichier, voir son extension !');
                $("input[name='image1]").val("");
            } else {
                document.getElementById("imgHelp").setAttribute("style", "");
                document.getElementById("imgHelp").setAttribute("class", "form-text text-muted");
            }
        });

        $("input[name='image2']").change(function(){
            var name= $("input[name='image2']").val();
            //var name= $("input[name='ficjustif']")[0].files[0];
            if (!recupExtensionImg(name)) {
                document.getElementById("imgHelp").setAttribute("style", 'background-color: red; size-weight: 10px;');
                alert('Choix non accepté d\'un fichier, voir son extension !');
                $("input[name='image2]").val("");
            } else {
                document.getElementById("imgHelp").setAttribute("style", "");
                document.getElementById("imgHelp").setAttribute("class", "form-text text-muted");
            }
        });

        $("input[name='image3']").change(function(){
            var name= $("input[name='image3']").val();
            //var name= $("input[name='ficjustif']")[0].files[0];
            if (!recupExtensionImg(name)) {
                document.getElementById("imgHelp").setAttribute("style", 'background-color: red; size-weight: 10px;');
                alert('Choix non accepté d\'un fichier, voir son extension !');
                $("input[name='image3]").val("");
            } else {
                document.getElementById("imgHelp").setAttribute("style", "");
                document.getElementById("imgHelp").setAttribute("class", "form-text text-muted");
            }
        });

        $("input[name='image4']").change(function(){
            var name= $("input[name='image4']").val();
            //var name= $("input[name='ficjustif']")[0].files[0];
            if (!recupExtensionImg(name)) {
                document.getElementById("imgHelp").setAttribute("style", 'background-color: red; size-weight: 10px;');
                alert('Choix non accepté d\'un fichier, voir son extension !');
                $("input[name='image4]").val("");
            } else {
                document.getElementById("imgHelp").setAttribute("style", "");
                document.getElementById("imgHelp").setAttribute("class", "form-text text-muted");
            }
        });

        $("input[name='image5']").change(function(){
            var name= $("input[name='image5']").val();
            //var name= $("input[name='ficjustif']")[0].files[0];
            if (!recupExtensionImg(name)) {
                document.getElementById("imgHelp").setAttribute("style", 'background-color: red; size-weight: 10px;');
                alert('Choix non accepté d\'un fichier, voir son extension !');
                $("input[name='image5]").val("");
            } else {
                document.getElementById("imgHelp").setAttribute("style", "");
                document.getElementById("imgHelp").setAttribute("class", "form-text text-muted");
            }
        });









    });

   /* $.ajax({
        //url:'admin/traitement.php',
        url:'presentationtraitementLivreRevue.php',
        type: 'GET',
        success: function(data) {
            $('#display').html(data);
        }
    });*/
