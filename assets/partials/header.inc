
<nav id="headId" class="navbar navbar-expand-sm navbar-dark w-100" style="background-color:cadetblue ">

<div class=" row w-100">
<div class=" col-sm-4 d-flex justify-content-center" style="border: 1px solid #ced4da;">
    <!-- Brand/logo -->
    <a class="navbar-brand" href="#">
      <img id="imgH" src="./assets/images/20050801-alex-art-1.jpg" alt="Boutique Livres & Revues" style="width:40px;">
      <span id="spH" >Boutique Livres & Revues</span>
    </a>
</div>
    <div class="container col-sm-2 d-flex justify-content-center" style="Background-color: #ced4da; border: 1px solid #ced4da;">
    <a class="nav-link" href="./index.php">Accueil</a>
    </div>  

  <div class="container col-sm-3" style="border: 1px solid #ced4da;">
  <!-- Links -->
  <ul class="navbar-nav"   style="position: relative;">
    
    <li class="nav-item dropdown ">
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
      <?php 
        if (isset($_SESSION['Auth'])) {
          if (isset($_SESSION['Auth']['pseudo'])) {
            //echo $_SESSION['Auth']['pseudo'];
            echo "Les listes";
          } else {
            echo "Les listes";
          }
        } else {
          echo "Les listes";
        }
      ?>
        
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="./index.php?action=initial&initial=initialLvR">Les livres/revues</a>
        <?php 
        //session_start();
        
        if (isset($_SESSION['Auth'])) {
          if (isset($_SESSION['Auth']['role']) && ($_SESSION['Auth']['role'] < 5)) { ?>
            <a class="dropdown-item" href="./index.php?action=initial&initial=initialAut">Les auteurs/rédacteurs</a>
         <?php }
        }
        if (isset($_SESSION['Auth'])) {
          if (isset($_SESSION['Auth']['role']) && ($_SESSION['Auth']['role'] < 5)) { ?>
            <a class="dropdown-item" href="./index.php?action=initial&initial=initialEdt">Les éditeurs</a>
         <?php }
        }
      ?>
        
      </div>
    </li>

    <li class="nav-item dropdown ">
      
      <?php 
        if (isset($_SESSION['Auth'])) {
          if (isset($_SESSION['Auth']['role']) && ($_SESSION['Auth']['role'] < 5 )) {
            //echo $_SESSION['Auth']['pseudo']; ?>
            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">Les tableaux</a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="./index.php?action=tableau&tableau=tableauLvR">Les livres/revues</a>
              <a class="dropdown-item" href="./index.php?action=tableau&tableau=tableauAut">Les auteurs/rédacteurs</a>
              <a class="dropdown-item" href="./index.php?action=tableau&tableau=tableauEdt">Les éditeurs</a>
              <?php 
              if (isset($_SESSION['Auth']['role']) && ($_SESSION['Auth']['role'] < 3 )) {
              ?>
                <a class="dropdown-item" href="./index.php?action=tableau&tableau=tableauAbn">Les abonnements</a>
                <a class="dropdown-item" href="./index.php?action=tableau&tableau=tableauCmd">Les commandes</a>
                <a class="dropdown-item" href="./index.php?action=tableau&tableau=tableauPan">Les paniers</a>
                <a class="dropdown-item" href="./index.php?action=tableau&tableau=tableauMon">Les monnaies</a>
                <a class="dropdown-item" href="./index.php?action=tableau&tableau=tableauMcl">Les mots-clés</a>
                <a class="dropdown-item" href="./index.php?action=tableau&tableau=tableauCti">Les clients-invités</a>
                <a class="dropdown-item" href="./index.php?action=tableau&tableau=tableauMbr">Les clients-membres</a>
                <a class="dropdown-item" href="./index.php?action=tableau&tableau=tableauRev">Les revues</a>
                <a class="dropdown-item" href="./index.php?action=tableau&tableau=tableauThm">Les thèmes</a>
              <?php } ?>
              <?php
              if (isset($_SESSION['Auth']['role']) && ($_SESSION['Auth']['role'] < 4 )) {
              ?>
                <a class="dropdown-item" href="./index.php?action=tableau&tableau=tableauArt">Les articles</a>
              <?php } ?>
              <?php 
              if (isset($_SESSION['Auth']['role']) && ($_SESSION['Auth']['role'] == 1 )) {
              ?>
              <a class="dropdown-item" href="./index.php?action=tableau&tableau=tableauUsr">Les utilisateurs</a>
              <?php 
              } ?>
            </div>
            <?php 
          } else {
            ?>
            <a class="nav-link dropdown-toggle" style="Display: none;" href="#" id="navbardrop" data-toggle="dropdown">
            <?php
          }
        }
      ?>
    </li>

    <li class="nav-item ml-auto">
      <?php
    if (!isset($_SESSION['Auth'])) { ?>
            <a class="nav-link" href="./index.php?action=login&login">Administration</a>
      <?php
        }
        ?>
    </li>
    </ul>
    </div>  
    

    <div class="container col-sm-3  w-100" style="border: 1px solid #ced4da;">
     <ul class="navbar-nav">
           <!-- Dropdown -->
    <li class="nav-item dropdown" >
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
      <?php 
        if (isset($_SESSION['Auth'])) {
          if (isset($_SESSION['Auth']['pseudo'])) {
            echo $_SESSION['Auth']['pseudo'];
          } else {
            echo "Connectez-vous";
          }
        } else {
            echo "Connectez-vous";
        }
       
      ?>
        
      </a>
      <div class="dropdown-menu">
        <?php 
        //session_start();
        //echo "<br>SESSION = ".var_dump($_SESSION)."<br>";
        if (!isset($_SESSION['Auth'])) { ?>
            <a class="dropdown-item" href="./index.php?action=login&login">Connexion</a>
         <?php
        }
        if (!isset($_SESSION['Auth'])  || (isset($_SESSION['Auth']) && (int)$_SESSION['Auth']['role'] < 3)) { ?>
          <a class="dropdown-item" href="./index.php?action=register&register=client">Inscription client</a>
        <?php
      }
      if (isset($_SESSION['Auth'])) {
          if (isset($_SESSION['Auth']['role']) && ($_SESSION['Auth']['role'] == 1)) { ?>
            <a class="dropdown-item" href="./index.php?action=register&register=user">Inscription user</a>
         <?php }
        } 
      if (isset($_SESSION['Auth'])) {
          if (isset($_SESSION['Auth']['role']) && ($_SESSION['Auth']['role'] == 1)) { ?>
            <a class="dropdown-item" href="./index.php?action=synthese&synthese=margebrute">Synthèse</a>
         <?php }
        } ?>
        <?php 
        if (isset($_SESSION['Auth']) || (isset($_SESSION['AuthClt']) && (int)$_SESSION['AuthClt']['role'] < 7)) { ?>
          <a class="dropdown-item" href="./index.php?action=voirCommande&voirCommande">Vos commandes</a>
        <?php
        }
        if (isset($_SESSION['Auth'])) { ?>
            <a class="dropdown-item" href="./index.php?action=logout&logout">Déconnexion</a>
         <?php
        }
      ?>
        
      </div>
    </li>
    <li class="nav-item ml-auto ">
      <a class="nav-link" href="./index.php?action=voirpanier&voirpanier" 
      <?php
        if (isset($_SESSION['panier'])) {
          if ($_SESSION['panier'] == 0) {
            echo 'style="pointer-events: none; cursor: default;"';
          } else {
            echo 'style=""';
          }
        } else {
          echo 'style="pointer-events: none; cursor: default;"';
        }
      ?>
      >Panier<?php 
        
        if (!isset($_SESSION['panier'])) {$_SESSION['panier']= 0; echo "(0)";}
        else { echo "(".$_SESSION['panier'].")";}
      ?></a>
    </li>
    </ul>
    </div>  
   
  </div>
</nav>

